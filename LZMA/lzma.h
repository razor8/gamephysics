//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

#ifndef LZMA_H
#define LZMA_H

#include "Bytes.h"

namespace helix
{
	class LZMA
	{
	public:
		static bool Compress(const stdrzr::bytes& _InputData, stdrzr::bytes& _OutputData, int32_t _uCompressionLevel, uint32_t _uDictionarySize);
		static bool Uncompress(const stdrzr::bytes& _InputData, stdrzr::bytes& _OutputData, size_t _uUncompressedSize = 0);
	};
}

#endif