//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

#include "lzma.h"
#include "LzmaEnc.h"
#include "LzmaDec.h"

using namespace helix;

static void * AllocForLzma(void *p, size_t size) { return malloc(size); }
static void FreeForLzma(void *p, void *address) { free(address); }
static ISzAlloc SzAllocForLzma = { &AllocForLzma, &FreeForLzma };

bool LZMA::Compress(const stdrzr::bytes& _InputData, stdrzr::bytes& _OutputData, int32_t _uCompressionLevel, uint32_t _uDictionarySize)
{
	//return if there is nothing to compress
	if (_InputData.size() == 0)
		return false;

	size_t uPropSize = LZMA_PROPS_SIZE;

	//initialize properties
	CLzmaEncProps props;
	LzmaEncProps_Init(&props);

	//default compression level = 4
	props.level = (_uCompressionLevel < 0 || _uCompressionLevel > 9) ? 4 : _uCompressionLevel;
	//default dictionary size = 16mb
	props.dictSize = (_uDictionarySize < 0x1000 || _uDictionarySize > 0x40000000) ? 0x1000000 : _uDictionarySize;
	//dont write the endmark we already know the size of the data
	props.writeEndMark = 1;

	size_t uInputSize = _InputData.size();

	//see link below for output buffer size:
	//http://sourceforge.net/p/sevenzip/discussion/45798/thread/dd3b392c/
	size_t uOutputSize = uInputSize + (uInputSize / 40) + 4096;

	//props are saved infront of the output buffer so they can be read befor uncompressing
	size_t uResultsSize = LZMA_PROPS_SIZE + uOutputSize;

	//resize if the output buffer is too small
	if (_OutputData.size() < uResultsSize)
		_OutputData.resize(uResultsSize);
	
	SRes iRes = LzmaEncode(&_OutputData[LZMA_PROPS_SIZE], &uResultsSize, //define output buffer
							&_InputData[0], uInputSize, //input buffer
							&props, &_OutputData[0], &uPropSize, props.writeEndMark, //props and prop location in output buffer
							NULL, &SzAllocForLzma, &SzAllocForLzma); // define allocators

	if (iRes != SZ_OK)
		return false;

	//resize output buffer to actual result size using infos gained from the encoding process
	_OutputData.resize(uPropSize + uResultsSize);
	
	return true;
}

//---------------------------------------------------------------------------------------------------

bool LZMA::Uncompress(const stdrzr::bytes& _InputData, stdrzr::bytes& _OutputData, size_t _uUncompressedSize)
{
	//no data to compress
	if (_InputData.size() == 0)
		return false;

	if (_uUncompressedSize == 0)
	{
		if (_OutputData.size() > _InputData.size())
		{
			//if theres already allocated mem in the output buffer, use as uncompressed size
			_uUncompressedSize = _OutputData.size();
			//no need to resize, buffer is already allocated
		}
		else
		{
			//in case no uncompressed size was specified we just assume the compression ration was 0.5 (which is very hacky)
			_uUncompressedSize = _InputData.size() * 2;
			_OutputData.resize(_uUncompressedSize);
		}
	}
	else
	{
		//we assume the uncompressed size is correct, no further checks
		_OutputData.resize(_uUncompressedSize);
	}

	ELzmaStatus status;

	size_t uInputSize = _InputData.size() - LZMA_PROPS_SIZE;
	size_t uResultSize = _uUncompressedSize;

	SRes iRes = LzmaDecode( &_OutputData[0], &uResultSize, //define output buffer
							&_InputData[LZMA_PROPS_SIZE], &uInputSize, // input source
							&_InputData[0], LZMA_PROPS_SIZE, //properties 
							LZMA_FINISH_ANY, &status, //mode
							&SzAllocForLzma); // allocator
	if (iRes != SZ_OK)
		return false;

	//resize to actuall uncompressed size (should be equal)
	_OutputData.resize(uResultSize);

	//optional: check status flags

	return true;
}