#ifndef __util_h__
#define __util_h__


#include <string>
#include <cstdint>

std::wstring GetExePath();

void UpdateWindowTitle(const std::wstring& appName, float fPhysicsDeltaTime);


#endif
