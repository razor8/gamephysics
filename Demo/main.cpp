//--------------------------------------------------------------------------------------
// File: main.cpp
//
// The main file containing the entry point main().
//--------------------------------------------------------------------------------------

#include <sstream>
#include <iomanip>
#include <random>
#include <iostream>

//Helix includes
#include "StdAfxH.h"
#include "Logger.h"

#include "Scene.h"
#include "TplGameObjectContainer.h"

#include "XMIntegrators.h"
#include "GameObject.h"
#include "Camera.h"
#include "AnimationController.h"

//Helix Rendering includes
#include "Effect.h"
#include "InstancedMesh.h"

using namespace helix;
using namespace helix::physics;
using namespace helix::datastructures;
//DirectX includes

using namespace DirectX;

// Effect framework includes
#include <d3dx11effect.h>

// DXUT includes
#include <DXUT.h>
#include <DXUTcamera.h>

// DirectXTK includes
#include "Effects.h"
#include "VertexTypes.h"
#include "PrimitiveBatch.h"
#include "GeometricPrimitive.h"
#include "ScreenGrab.h"

// AntTweakBar includes
#include "AntTweakBar.h"

// Internal includes
#include "util/util.h"
#include "util/FFmpeg.h"

// DXUT camera
// NOTE: CModelViewerCamera does not only manage the standard view transformation/camera position 
//       (CModelViewerCamera::GetViewMatrix()), but also allows for model rotation
//       (CModelViewerCamera::GetWorldMatrix()). 
//       Look out for CModelViewerCamera::SetButtonMasks(...).

//#define _MODELVIEW
#ifdef _MODELVIEW
CModelViewerCamera g_camera;
#else
//CFirstPersonCamera g_camera;
scene::Camera g_camera;
scene::Camera g_DebugCamera;
animation::AnimationController g_CameraAnimationContoller;
animation::AnimationController::Vector3TrackHandle g_CameraPositionHandle;
#endif

// --------------------------------------------------------------------- Shortcut defines
const uint32_t iToggleMouseButton = VK_F1;
const uint32_t iToggleSimulationButton = VK_F2;
const uint32_t iToggleCoordinateSystemButton = VK_F3;
const uint32_t iResetCameraButton = VK_F7;


bool bShowMouse = true;

// Effect corresponding to "effect.fx"
ID3DX11Effect* g_pEffect = nullptr;

// Main tweak bar
TwBar* g_pTweakBar;

// DirectXTK effects, input layouts and primitive batches for different vertex types
BasicEffect*                               g_pEffectPositionColor = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionColor = nullptr;
PrimitiveBatch<VertexPositionColor>*       g_pPrimitiveBatchPositionColor = nullptr;

BasicEffect*                               g_pEffectPositionNormal = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionNormal = nullptr;
PrimitiveBatch<VertexPositionNormal>*      g_pPrimitiveBatchPositionNormal = nullptr;

BasicEffect*                               g_pEffectPositionNormalColor = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionNormalColor = nullptr;
PrimitiveBatch<VertexPositionNormalColor>* g_pPrimitiveBatchPositionNormalColor = nullptr;

// DirectXTK simple geometric primitives
std::unique_ptr<GeometricPrimitive> g_pSphere;
std::unique_ptr<GeometricPrimitive> g_pBox;
std::unique_ptr<GeometricPrimitive> g_pTeapot;

// Movable object management
XMINT2   g_viMouseDelta = XMINT2(0, 0);
XMFLOAT3 g_vfMovableObjectPos = XMFLOAT3(0, 0, 0);

// TweakAntBar GUI variables
int   g_iNumSpheres = 0;
float g_fSphereSize = 0.05f;
bool  g_bDrawGameObjects = true;
bool  g_bDrawOctree = false;

bool  g_bDrawTeapot = false;
bool  g_bDrawTriangle = false;
bool  g_bDrawSpheres = false;
bool  g_bDrawSprings = true;
bool  g_bDrawBoundingBox = true;
bool  g_bDrawRBDebugInfo = false;
bool  g_bDrawCoordinateSystem = true;

bool  g_bFreeUpdateInterval = true;
bool  g_bFreeUpdateIntervalPrev = false;
bool  g_bSyncTiming = false;
float g_fDamping = 2.0f;
float g_fStiffness = 100.f;
float g_fGravity = -1.f;
float g_fGravityPrev = 0.f;
double g_fUpdateInterval = 0.02;
double g_fUpdateIntervalPrev = 0.0;

scene::Scene m_Scene;

// Video recorder
FFmpeg* g_pFFmpegVideoRecorder = nullptr;

std::vector<datastructures::GameObject> g_VisibleObjects;
uint64_t g_uVisibleObjectsID = -1;

physics::TRigidBodyForceMap g_RBForces;

TwType g_IntegratorType;
math::XMIntegrator g_Integrator = math::XMIntegrator_RK4;
math::XMIntegrator g_IntegratorPrev = math::XMIntegrator_Euler;

CTwEnumVal g_Iterators[] = { {
		math::XMIntegrator_Euler,
		"Euler" },{
			math::XMIntegrator_RK2,
			"MidPoint" },{
				math::XMIntegrator_Heun,
				"Heun" },{
					math::XMIntegrator_RK3,
					"RK3" },{
						math::XMIntegrator_RK4,
						"RK4" } };

// Camera Settings
const XMFLOAT3 g_CameraPos(0.5f, 3.0f, 0.5f);
const XMFLOAT3 g_CameraLookAt(0.5f, 0.0f, 0.5f);
const XMFLOAT3 g_CameraUp(0.0f, 0.0f, 1.0f);

// RB-Setup
const XMFLOAT3 g_RBAAngularMomentum = XMFLOAT3(0.0f, 0.0f, 0.0f);

// Helix Rendering 
typedef InstancedMesh<Renderer::VertexPosition> InstancedMeshVP;
typedef InstancedMesh<Renderer::VertexPositionNormalTexture> InstancedMeshVPNT;
InstancedMeshVP* g_InstancedBox;

// Create TweakBar and add required buttons and variables
void InitTweakBar(ID3D11Device* pd3dDevice)
{
	TwInit(TW_DIRECT3D11, pd3dDevice);

	g_pTweakBar = TwNewBar("TweakBar");

	// HINT: For buttons you can directly pass the callback function as a lambda expression.
	TwAddVarRW(g_pTweakBar, "Draw GameObjects", TW_TYPE_BOOLCPP, &g_bDrawGameObjects, "");
	TwAddVarRW(g_pTweakBar, "Draw Octree", TW_TYPE_BOOLCPP, &g_bDrawOctree, "");
	//TwAddVarRW(g_pTweakBar, "Draw Teapot",   TW_TYPE_BOOLCPP, &g_bDrawTeapot, "");
	//TwAddVarRW(g_pTweakBar, "Draw Triangle", TW_TYPE_BOOLCPP, &g_bDrawTriangle, "");
	//TwAddVarRW(g_pTweakBar, "Draw Spheres", TW_TYPE_BOOLCPP, &g_bDrawSpheres, "");
	TwAddVarRW(g_pTweakBar, "Draw BBox", TW_TYPE_BOOLCPP, &g_bDrawBoundingBox, "");
	TwAddVarRW(g_pTweakBar, "Draw RB Debug Info", TW_TYPE_BOOLCPP, &g_bDrawRBDebugInfo, "");

	//TwAddVarRW(g_pTweakBar, "Draw Springs", TW_TYPE_BOOLCPP, &g_bDrawSprings, "");
	//TwAddVarRW(g_pTweakBar, "Num Spheres",   TW_TYPE_INT32, &g_iNumSpheres, "min=1");
	TwAddVarRW(g_pTweakBar, "Sphere Size", TW_TYPE_FLOAT, &g_fSphereSize, "min=0.01 step=0.01");

	TwAddVarRW(g_pTweakBar, "FreeInterval", TW_TYPE_BOOLCPP, &g_bFreeUpdateInterval, "");
	TwAddVarRW(g_pTweakBar, "SyncInterval", TW_TYPE_BOOLCPP, &g_bSyncTiming, "");
	TwAddVarRW(g_pTweakBar, "UpdateInterval", TW_TYPE_DOUBLE, &g_fUpdateInterval, "min=0.0001 step=0.001");


	TwAddButton(g_pTweakBar, "(Un)Pause Simulation (F2)", [](void*)
	{
		if (m_Scene.IsPaused())
		{
			m_Scene.Continue();
			g_CameraAnimationContoller.Unpause();
		}
		else
		{
			m_Scene.Pause();
			g_CameraAnimationContoller.Pause();
		}
	}, nullptr, "");
	TwAddVarRW(g_pTweakBar, "Draw Coordinate System (F3)", TW_TYPE_BOOLCPP, &g_bDrawCoordinateSystem, "");
	TwAddButton(g_pTweakBar, "Reset Camera (F7)", [](void *) {g_camera.LookAtFrom(g_CameraPos, g_CameraLookAt, g_CameraUp); }, nullptr, "");
	TwAddButton(g_pTweakBar, "Add GameObject", [](void*)
	{
		TplGameObjectContainer::Instance()->CreateRigidbody(XMFLOAT3_ZERO, XMFLOAT3_ZERO, XMFLOAT3_ZERO, 1.f, 1.f, XMFLOAT3_ZERO, XMFLOAT3_ZERO, XMFLOAT3X3_ZERO);
	},nullptr, "");
}

// Draw the edges of the bounding box [-0.5;0.5]� rotated with the cameras model tranformation.
// (Drawn as line primitives using a DirectXTK primitive batch)
void DrawBoundingBox(ID3D11DeviceContext* pd3dImmediateContext)
{
	// Setup position/color effect
#ifdef _MODELVIEW
	g_pEffectPositionColor->SetWorld(g_camera.GetWorldMatrix()); //DirectX::XMMatrixIdentity
#else
	g_pEffectPositionColor->SetWorld(DirectX::XMMatrixIdentity());
#endif

	g_pEffectPositionColor->Apply(pd3dImmediateContext);
	pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionColor);

	// Draw
	g_pPrimitiveBatchPositionColor->Begin();

	// Lines in x direction (red color)
	for (int i = 0; i<4; i++)
	{
		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor(XMVectorSet(-0.5f, (float)(i % 2) - 0.5f, (float)(i / 2) - 0.5f, 1), Colors::Red),
			VertexPositionColor(XMVectorSet(0.5f, (float)(i % 2) - 0.5f, (float)(i / 2) - 0.5f, 1), Colors::Red)
			);
	}

	// Lines in y direction
	for (int i = 0; i<4; i++)
	{
		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor(XMVectorSet((float)(i % 2) - 0.5f, -0.5f, (float)(i / 2) - 0.5f, 1), Colors::Green),
			VertexPositionColor(XMVectorSet((float)(i % 2) - 0.5f, 0.5f, (float)(i / 2) - 0.5f, 1), Colors::Green)
			);
	}

	// Lines in z direction
	for (int i = 0; i<4; i++)
	{
		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor(XMVectorSet((float)(i % 2) - 0.5f, (float)(i / 2) - 0.5f, -0.5f, 1), Colors::Blue),
			VertexPositionColor(XMVectorSet((float)(i % 2) - 0.5f, (float)(i / 2) - 0.5f, 0.5f, 1), Colors::Blue)
			);
	}

	g_pPrimitiveBatchPositionColor->End();
}

void DebugDraw(ID3D11DeviceContext* pd3dImmediateContext, XMFLOAT3 vPoints[8])
{
	// Setup position/color effect
#ifdef _MODELVIEW
	g_pEffectPositionColor->SetWorld(g_camera.GetWorldMatrix()); //DirectX::XMMatrixIdentity
#else
	g_pEffectPositionColor->SetWorld(DirectX::XMMatrixIdentity());
#endif

	g_pEffectPositionColor->Apply(pd3dImmediateContext);
	pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionColor);

	g_pPrimitiveBatchPositionColor->Begin();
	// Lines in x direction
	for (int32_t i = 0; i < 8; i += 2)
	{
		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor(XMVectorSet(vPoints[i].x, vPoints[i].y, vPoints[i].z, 1.f), Colors::Red),
			VertexPositionColor(XMVectorSet(vPoints[i + 1].x, vPoints[i + 1].y, vPoints[i + 1].z, 1.f), Colors::Red)
			);
	}

	// Lines in y direction
	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor(XMVectorSet(vPoints[0].x, vPoints[0].y, vPoints[0].z, 1.f), Colors::Red),
		VertexPositionColor(XMVectorSet(vPoints[3].x, vPoints[3].y, vPoints[3].z, 1.f), Colors::Red)
		);
	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor(XMVectorSet(vPoints[1].x, vPoints[1].y, vPoints[1].z, 1.f), Colors::Red),
		VertexPositionColor(XMVectorSet(vPoints[2].x, vPoints[2].y, vPoints[2].z, 1.f), Colors::Red)
		);
	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor(XMVectorSet(vPoints[4].x, vPoints[4].y, vPoints[4].z, 1.f), Colors::Red),
		VertexPositionColor(XMVectorSet(vPoints[7].x, vPoints[7].y, vPoints[7].z, 1.f), Colors::Red)
		);
	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor(XMVectorSet(vPoints[5].x, vPoints[5].y, vPoints[5].z, 1.f), Colors::Red),
		VertexPositionColor(XMVectorSet(vPoints[6].x, vPoints[6].y, vPoints[6].z, 1.f), Colors::Red)
		);

	// Lines in z direction
	for (int i = 0; i < 4; i++)
	{
		g_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor(XMVectorSet(vPoints[i].x, vPoints[i].y, vPoints[i].z, 1.f), Colors::Red),
			VertexPositionColor(XMVectorSet(vPoints[i + 4].x, vPoints[i + 4].y, vPoints[i + 4].z, 1.f), Colors::Red)
			);
	}

	g_pPrimitiveBatchPositionColor->End();
}

void DrawCoordinateSystem(ID3D11DeviceContext* pd3dImmediateContext)
{
	// Setup position/color effect
	g_pEffectPositionColor->SetWorld(DirectX::XMMatrixIdentity());

	g_pEffectPositionColor->Apply(pd3dImmediateContext);
	pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionColor);

	g_pPrimitiveBatchPositionColor->Begin();

	// X-Axis
	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor(XMVectorSet(-1000.f, 0.f, 0.f, 1.f), Colors::Red),
		VertexPositionColor(XMVectorSet(1000.f, 0.f, 0.f, 1.f), Colors::Red)
		);

	// Y-Axis
	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor(XMVectorSet(0.f, -1000.f, 0.f, 1.f), Colors::Blue),
		VertexPositionColor(XMVectorSet(0.f, 1000.f, 0.f, 1.f), Colors::Blue)
		);

	// Z-Axis
	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor(XMVectorSet(0.f, 0.f, -1000.f, 1.f), Colors::Green),
		VertexPositionColor(XMVectorSet(0.f, 0.f, 1000.f, 1.f), Colors::Green)
		);

	g_pPrimitiveBatchPositionColor->End();
}

void DrawArrowVector(ID3D11DeviceContext* pd3dImmediateContext, const XMFLOAT3& _Start, const XMFLOAT3& _End, const XMVECTORF32& _Color = Colors::Red)
{
	g_pEffectPositionColor->SetWorld(DirectX::XMMatrixIdentity());

	g_pEffectPositionColor->Apply(pd3dImmediateContext);
	pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionColor);

	g_pPrimitiveBatchPositionColor->Begin();

	g_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor(XMLoadFloat3(&_Start), _Color),
		VertexPositionColor(XMLoadFloat3(&_End), _Color)
		);

	g_pPrimitiveBatchPositionColor->End();

	// Arrow direction
	g_pEffectPositionNormal->SetEmissiveColor(Colors::Black);
	g_pEffectPositionNormal->SetSpecularColor(0.4f * Colors::White);
	g_pEffectPositionNormal->SetSpecularPower(100);
	g_pEffectPositionNormal->SetDiffuseColor(_Color);

	const static XMMATRIX scale = XMMatrixScaling(0.01f, 0.01f, 0.01f);
	XMMATRIX trans = XMMatrixTranslation(_End.x, _End.y, _End.z);
	g_pEffectPositionNormal->SetWorld(scale * trans);

	g_pSphere->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
}

// Draw a large, square plane at y=-1 with a checkerboard pattern
// (Drawn as multiple quads, i.e. triangle strips, using a DirectXTK primitive batch)
void DrawFloor(ID3D11DeviceContext* pd3dImmediateContext)
{
	// Setup position/normal/color effect
	g_pEffectPositionNormalColor->SetWorld(XMMatrixIdentity());
	g_pEffectPositionNormalColor->SetEmissiveColor(Colors::Black);
	g_pEffectPositionNormalColor->SetDiffuseColor(0.8f * Colors::White);
	g_pEffectPositionNormalColor->SetSpecularColor(0.4f * Colors::White);
	g_pEffectPositionNormalColor->SetSpecularPower(1000);

	g_pEffectPositionNormalColor->Apply(pd3dImmediateContext);
	pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionNormalColor);

	// Draw 4*n*n quads spanning x = [-n;n], y = -1, z = [-n;n]
	const float n = 10;
	XMVECTOR normal = XMVectorSet(0, 1, 0, 0);
	XMVECTOR planecenter = XMVectorSet(0, -1, 0, 0);

	g_pPrimitiveBatchPositionNormalColor->Begin();
	for (float z = -n; z < n; z++)
	{
		for (float x = -n; x < n; x++)
		{
			// Quad vertex positions
			XMVECTOR pos[] = { XMVectorSet(x  , -1, z + 1, 0),
				XMVectorSet(x + 1, -1, z + 1, 0),
				XMVectorSet(x + 1, -1, z  , 0),
				XMVectorSet(x  , -1, z  , 0) };

			// Color checkerboard pattern (white & gray)
			XMVECTOR color = ((int(z + x) % 2) == 0) ? XMVectorSet(1, 1, 1, 1) : XMVectorSet(0.6f, 0.6f, 0.6f, 1);

			// Color attenuation based on distance to plane center
			float attenuation[] = {
				1.0f - XMVectorGetX(XMVector3Length(pos[0] - planecenter)) / n,
				1.0f - XMVectorGetX(XMVector3Length(pos[1] - planecenter)) / n,
				1.0f - XMVectorGetX(XMVector3Length(pos[2] - planecenter)) / n,
				1.0f - XMVectorGetX(XMVector3Length(pos[3] - planecenter)) / n };

			g_pPrimitiveBatchPositionNormalColor->DrawQuad(
				VertexPositionNormalColor(pos[0], normal, attenuation[0] * color),
				VertexPositionNormalColor(pos[1], normal, attenuation[1] * color),
				VertexPositionNormalColor(pos[2], normal, attenuation[2] * color),
				VertexPositionNormalColor(pos[3], normal, attenuation[3] * color)
				);
		}
	}
	g_pPrimitiveBatchPositionNormalColor->End();
}

// Draw several objects randomly positioned in [-0.5f;0.5]�  using DirectXTK geometric primitives.
void DrawSomeRandomObjects(ID3D11DeviceContext* pd3dImmediateContext)
{
	// Setup position/normal effect (constant variables)
	g_pEffectPositionNormal->SetEmissiveColor(Colors::Black);
	g_pEffectPositionNormal->SetSpecularColor(0.4f * Colors::White);
	g_pEffectPositionNormal->SetSpecularPower(100);

	std::mt19937 eng;
	std::uniform_real_distribution<float> randCol(0.0f, 1.0f);
	std::uniform_real_distribution<float> randPos(-0.5f, 0.5f);

	for (int i = 0; i<g_iNumSpheres; i++)
	{
		// Setup position/normal effect (per object variables)
		g_pEffectPositionNormal->SetDiffuseColor(0.6f * XMColorHSVToRGB(XMVectorSet(randCol(eng), 1, 1, 0)));
		XMMATRIX scale = XMMatrixScaling(g_fSphereSize, g_fSphereSize, g_fSphereSize);
		XMMATRIX trans = XMMatrixTranslation(randPos(eng), randPos(eng), randPos(eng));
		g_pEffectPositionNormal->SetWorld(scale * trans);//* g_camera.GetWorldMatrix());

														 // Draw
														 // NOTE: The following generates one draw call per object, so performance will be bad for n>>1000 or so
		g_pSphere->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
	}
}

//void DrawMassSpringSystem(ID3D11DeviceContext* pd3dImmediateContext)
//{
//	// Setup position/normal effect (constant variables)
//	g_pEffectPositionNormal->SetEmissiveColor(Colors::Black);
//	g_pEffectPositionNormal->SetSpecularColor(0.4f * Colors::White);
//	g_pEffectPositionNormal->SetSpecularPower(100);
//
//	std::mt19937 eng;
//	std::uniform_real_distribution<float> randCol(0.0f, 1.0f);
//
//	MassSpringSystem::TForceMap Forces;
//	uint32_t length = static_cast<uint32_t>(g_Data.m_Points.size());
//
//	//g_MSS.ClearForces();
//	
//#pragma region HelixRendering
//	std::vector<InstancedMeshVP::InstanceData> PosData;
//	for (uint32_t i = 0; i < length; ++i)
//	{
//		MSPoint& Point = g_Data.m_Points.at(i);
//		if (Point.m_vCurPosition.y < 0.f)
//		{
//			XMVECTOR Force = XMLoadFloat3(&Point.m_vCurForce);
//			Force = XMVector3Reflect(Force, XMVectorSet(0.f, 1.f, 0.f, 0.f));
//			XMStoreFloat3(&Point.m_vCurForce, Force);
//
//			Forces.emplace(i, Point.m_vCurForce);
//		}
//
//		PosData.push_back(InstancedMeshVP::InstanceData(Point.m_vCurPosition));
//	}
//
//	g_InstancedBox->AddInstances(PosData);
//
//	g_InstancedBox->Render(pd3dImmediateContext);
//#pragma endregion
//
//	//for (uint32_t i = 0; i < length; i++)
//	//{
//	//	MSPoint& Point = g_Data.m_Points.at(i);
//	//	if (Point.m_vCurPosition.y < 0.f)
//	//	{
//	//		XMVECTOR Force = XMLoadFloat3(&Point.m_vCurForce);
//	//		Force = XMVector3Reflect(Force, XMVectorSet(0.f, 1.f, 0.f,0.f));
//	//		XMStoreFloat3(&Point.m_vCurForce, Force);
//
//	//		Forces.emplace(i, Point.m_vCurForce);
//	//	}
//
//	//	// Setup position/normal effect (per object variables)
//	//	g_pEffectPositionNormal->SetDiffuseColor(0.6f * XMColorHSVToRGB(XMVectorSet(randCol(eng), 1, 1, 0)));
//	//	XMMATRIX scale = XMMatrixScaling(g_fSphereSize, g_fSphereSize, g_fSphereSize);
//	//	XMMATRIX trans = XMMatrixTranslation(Point.m_vCurPosition.x, Point.m_vCurPosition.y, Point.m_vCurPosition.z);
//
//	//	g_pEffectPositionNormal->SetWorld(scale * trans);
//
//	//	// Draw
//	//	// NOTE: The following generates one draw call per object, so performance will be bad for n>>1000 or so
//	//	g_pSphere->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
//	//}
//
//	if (Forces.empty() == false) g_MSS->AddExternalForces(Forces);
//
//	if (g_bDrawSprings && g_Data.m_Springs.empty() == false)
//	{
//		//Setup position/color effect
//		g_pEffectPositionColor->SetWorld(XMMatrixIdentity());  //g_camera.GetWorldMatrix());
//
//		g_pEffectPositionColor->Apply(pd3dImmediateContext);
//		pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionColor);
//
//		// Draw springs
//		g_pPrimitiveBatchPositionColor->Begin();
//
//		HFOREACH_CONST(it, end, g_Data.m_Springs)
//			g_pPrimitiveBatchPositionColor->DrawLine(
//				VertexPositionColor(XMLoadFloat3(&g_Data.m_Points.at(it->m_pPoint1).m_vCurPosition), Colors::Red),
//				VertexPositionColor(XMLoadFloat3(&g_Data.m_Points.at(it->m_pPoint2).m_vCurPosition), Colors::Red));
//		HFOREACH_CONST_END
//
//		g_pPrimitiveBatchPositionColor->End();
//	}
//}

void DrawOctree(ID3D11DeviceContext* pd3dImmediateContext)
{
	// Setup position/color effect
#ifdef _MODELVIEW
	g_pEffectPositionColor->SetWorld(g_camera.GetWorldMatrix()); //DirectX::XMMatrixIdentity
#else
	g_pEffectPositionColor->SetWorld(DirectX::XMMatrixIdentity());
#endif

	g_pEffectPositionColor->Apply(pd3dImmediateContext);
	pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionColor);

	//if (g_pOctree != nullptr) g_pOctree->DebugDraw(g_pPrimitiveBatchPositionColor);
}

void DrawGameObjects(ID3D11DeviceContext* pd3dImmediateContext)
{
	//m_Scene.GetVisibleObjects(g_VisibleObjects, g_uVisibleObjectsID);

	XMMATRIX scale;
	XMMATRIX trans;
	XMMATRIX rot;

	XMFLOAT3 Corners[8];

	HFOREACH_CONST(it, end, g_VisibleObjects)
		g_pEffectPositionNormal->SetEmissiveColor(Colors::Black);
	g_pEffectPositionNormal->SetSpecularColor(0.4f * Colors::White);
	g_pEffectPositionNormal->SetSpecularPower(100);
	g_pEffectPositionNormal->SetDiffuseColor(Colors::White);

	// Setup position/normal effect (per object variables)			
	scale = XMMatrixScaling(it->m_vScale.x, it->m_vScale.y, it->m_vScale.z);
	trans = XMMatrixTranslation(it->m_vPosition.x, it->m_vPosition.y, it->m_vPosition.z);

	if (it->CheckFlag(GameObjectFlag_Invisible) == false)
	{
		//if (it->m_BV.CheckFlag(collision::CollisionFlag_Naive))
		//{
		//	g_pEffectPositionNormal->SetDiffuseColor(Colors::Blue);
		//}
		//else if (it->m_BV.CheckFlag(collision::CollisionFlag_KDTree))
		//{
		//	g_pEffectPositionNormal->SetDiffuseColor(Colors::Green);
		//}
		//else if (it->m_BV.CheckFlag(collision::CollisionFlag_UniformGrid))
		//{
		//	g_pEffectPositionNormal->SetDiffuseColor(Colors::Red);
		//}

		if (it->CheckFlag(GameObjectFlag_MovingObject))
		{
			g_pEffectPositionNormal->SetWorld(scale * trans);
			g_pSphere->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
		}
		else if (it->CheckFlag(GameObjectFlag_RigidBody))
		{
			//it->m_BV.GetOBB()->GetCorners(Corners);
			//DebugDraw(pd3dImmediateContext, Corners);

			rot = XMLoadFloat3x3(&it->m_mRotation);

			g_pEffectPositionNormal->SetWorld(scale * rot * trans); // rot * scale * trans

			g_pBox->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
		}
		else
		{
			//meh
		}
	}

	//draw bounding volume
	if (it->m_BV.GetOBB() != nullptr)
	{
		it->m_BV.GetOBB()->GetCorners(Corners);
	}
	else if (it->m_BV.GetAABB() != nullptr)
	{
		it->m_BV.GetAABB()->GetCorners(Corners);
	}
	else
	{
		float fRadius = fmaxf(fmaxf(it->m_vScale.x, it->m_vScale.y), it->m_vScale.z);
		collision::BoundingBox aabb(it->m_vPosition, XMFLOAT3(fRadius, fRadius, fRadius));
		aabb.GetCorners(Corners);
	}

	if (g_bDrawBoundingBox)
	{
		DebugDraw(pd3dImmediateContext, Corners);
		if (it->CheckFlag(GameObjectFlag_RigidBody) && g_bDrawRBDebugInfo)
		{
			XMVECTOR tmp = XMLoadFloat3(&g_RBAAngularMomentum);
			tmp = XMVectorScale(tmp, 5.f);
			XMFLOAT3 start;
			XMStoreFloat3(&start, tmp);
			XMFLOAT3 pos = it->GetPosition();
			DrawArrowVector(pd3dImmediateContext, math::Sub(pos, start), pos, Colors::Green);
		}
	}

	HFOREACH_CONST_END
}

// Draw a teapot at the position g_vfMovableObjectPos.
void DrawMovableTeapot(ID3D11DeviceContext* pd3dImmediateContext)
{
	// Setup position/normal effect (constant variables)
	g_pEffectPositionNormal->SetEmissiveColor(Colors::Black);
	g_pEffectPositionNormal->SetDiffuseColor(0.6f * Colors::Cornsilk);
	g_pEffectPositionNormal->SetSpecularColor(0.4f * Colors::White);
	g_pEffectPositionNormal->SetSpecularPower(100);

	XMMATRIX scale = XMMatrixScaling(0.5f, 0.5f, 0.5f);
	XMMATRIX trans = XMMatrixTranslation(g_vfMovableObjectPos.x, g_vfMovableObjectPos.y, g_vfMovableObjectPos.z);
	g_pEffectPositionNormal->SetWorld(scale * trans);

	// Draw
	g_pTeapot->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
}

// Draw a simple triangle using custom shaders (g_pEffect)
void DrawTriangle(ID3D11DeviceContext* pd3dImmediateContext)
{
	//XMMATRIX world = g_camera.world();
	//XMMATRIX view = g_camera.view();
	//XMMATRIX proj = g_camera.proj();
	//XMFLOAT4X4 mViewProj;
	
	g_pEffect->GetVariableByName("g_worldViewProj")->AsMatrix()->SetMatrix((float*)g_camera.GetViewProj().m);
	g_pEffect->GetTechniqueByIndex(0)->GetPassByIndex(0)->Apply(0, pd3dImmediateContext);

	pd3dImmediateContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
	pd3dImmediateContext->IASetIndexBuffer(nullptr, DXGI_FORMAT_R16_UINT, 0);
	pd3dImmediateContext->IASetInputLayout(nullptr);
	pd3dImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	pd3dImmediateContext->Draw(3, 0);
}


// ============================================================
// DXUT Callbacks
// ============================================================


//--------------------------------------------------------------------------------------
// Reject any D3D11 devices that aren't acceptable by returning false
//--------------------------------------------------------------------------------------
bool CALLBACK IsD3D11DeviceAcceptable(const CD3D11EnumAdapterInfo *AdapterInfo, UINT Output, const CD3D11EnumDeviceInfo *DeviceInfo,
	DXGI_FORMAT BackBufferFormat, bool bWindowed, void* pUserContext)
{
	return true;
}


//--------------------------------------------------------------------------------------
// Called right before creating a device, allowing the app to modify the device settings as needed
//--------------------------------------------------------------------------------------
bool CALLBACK ModifyDeviceSettings(DXUTDeviceSettings* pDeviceSettings, void* pUserContext)
{
	return true;
}

//--------------------------------------------------------------------------------------
// Create any D3D11 resources that aren't dependent on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11CreateDevice(ID3D11Device* pd3dDevice, const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext)
{
	HRESULT hr;

	ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();;

	HLOG("Device: %s", DXUTGetDeviceStats());

	//std::wcout << L"Device: " << DXUTGetDeviceStats() << std::endl;

	// Load custom effect from "effect.fxo" (compiled "effect.fx")
	std::wstring effectPath = GetExePath() + L"effect.fxo";

	//helix code
	HR(hr = D3DX11CreateEffectFromFile(effectPath.c_str(), 0, pd3dDevice, &g_pEffect));

	//original code
	//if(FAILED(hr = D3DX11CreateEffectFromFile(effectPath.c_str(), 0, pd3dDevice, &g_pEffect)))
	//{
	//  std::wcout << L"Failed creating effect with error code " << int(hr) << std::endl;
	//	return hr;
	//}


#pragma region HelixRendering
	// Custom Helix Rendering Setup	
	// Sets the default shader folder path e.g. L"C:\\Users\\x\\Desktop\\gamephysics\\Demo\\helix\\"
	Effect::SetShaderFolderPath(GetExePath() + L"..\\..\\Demo\\helix\\");
	InstancedMeshVP::Initialize(pd3dDevice);

	std::vector<Renderer::VertexPosition> VertexBufferData;
	std::vector<uint32_t> IndexBufferData;
	Mesh<Renderer::VertexPosition>::GenerateCube(0.05f, VertexBufferData, IndexBufferData);

	g_InstancedBox = new InstancedMeshVP(pd3dDevice, VertexBufferData, IndexBufferData);
	//g_InstancedBox->AddInstance(XMFLOAT3(0.f, 0.f, 0.f));
	//g_InstancedBox->AddInstance(XMFLOAT3(2.f, 1.f, 1.f));
#pragma endregion

	//g_camera.SetRotateButtons(false, false, true, !bShowMouse);
	//g_camera.SetResetCursorAfterMove(!bShowMouse);
	ShowCursor(true/*bShowMouse*/);

	// Init AntTweakBar GUI
	InitTweakBar(pd3dDevice);

	// Create DirectXTK geometric primitives for later usage
	g_pBox = GeometricPrimitive::CreateCube(pd3dImmediateContext, 1.f, false);
	g_pSphere = GeometricPrimitive::CreateGeoSphere(pd3dImmediateContext, 2.0f, 2, false);
	g_pTeapot = GeometricPrimitive::CreateTeapot(pd3dImmediateContext, 1.5f, 8, false);

	// Create effect, input layout and primitive batch for position/color vertices (DirectXTK)
	{
		// Effect
		g_pEffectPositionColor = new BasicEffect(pd3dDevice);
		g_pEffectPositionColor->SetVertexColorEnabled(true); // triggers usage of position/color vertices

															 // Input layout
		void const* shaderByteCode;
		size_t byteCodeLength;
		g_pEffectPositionColor->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

		pd3dDevice->CreateInputLayout(VertexPositionColor::InputElements,
			VertexPositionColor::InputElementCount,
			shaderByteCode, byteCodeLength,
			&g_pInputLayoutPositionColor);

		// Primitive batch
		g_pPrimitiveBatchPositionColor = new PrimitiveBatch<VertexPositionColor>(pd3dImmediateContext);
	}

	// Create effect, input layout and primitive batch for position/normal vertices (DirectXTK)
	{
		// Effect
		g_pEffectPositionNormal = new BasicEffect(pd3dDevice);
		g_pEffectPositionNormal->EnableDefaultLighting(); // triggers usage of position/normal vertices
		g_pEffectPositionNormal->SetPerPixelLighting(true);

		// Input layout
		void const* shaderByteCode;
		size_t byteCodeLength;
		g_pEffectPositionNormal->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

		pd3dDevice->CreateInputLayout(VertexPositionNormal::InputElements,
			VertexPositionNormal::InputElementCount,
			shaderByteCode, byteCodeLength,
			&g_pInputLayoutPositionNormal);

		// Primitive batch
		g_pPrimitiveBatchPositionNormal = new PrimitiveBatch<VertexPositionNormal>(pd3dImmediateContext);
	}

	// Create effect, input layout and primitive batch for position/normal/color vertices (DirectXTK)
	{
		// Effect
		g_pEffectPositionNormalColor = new BasicEffect(pd3dDevice);
		g_pEffectPositionNormalColor->SetPerPixelLighting(true);
		g_pEffectPositionNormalColor->EnableDefaultLighting();     // triggers usage of position/normal/color vertices
		g_pEffectPositionNormalColor->SetVertexColorEnabled(true); // triggers usage of position/normal/color vertices

																   // Input layout
		void const* shaderByteCode;
		size_t byteCodeLength;
		g_pEffectPositionNormalColor->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

		pd3dDevice->CreateInputLayout(VertexPositionNormalColor::InputElements,
			VertexPositionNormalColor::InputElementCount,
			shaderByteCode, byteCodeLength,
			&g_pInputLayoutPositionNormalColor);

		// Primitive batch
		g_pPrimitiveBatchPositionNormalColor = new PrimitiveBatch<VertexPositionNormalColor>(pd3dImmediateContext);
	}

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11CreateDevice 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11DestroyDevice(void* pUserContext)
{
	SAFE_RELEASE(g_pEffect);

	TwDeleteBar(g_pTweakBar);
	g_pTweakBar = nullptr;
	TwTerminate();

	g_pBox.reset();
	g_pSphere.reset();
	g_pTeapot.reset();

	SAFE_DELETE(g_pPrimitiveBatchPositionColor);
	SAFE_RELEASE(g_pInputLayoutPositionColor);
	SAFE_DELETE(g_pEffectPositionColor);

	SAFE_DELETE(g_pPrimitiveBatchPositionNormal);
	SAFE_RELEASE(g_pInputLayoutPositionNormal);
	SAFE_DELETE(g_pEffectPositionNormal);

	SAFE_DELETE(g_pPrimitiveBatchPositionNormalColor);
	SAFE_RELEASE(g_pInputLayoutPositionNormalColor);
	SAFE_DELETE(g_pEffectPositionNormalColor);

	// Helix Deletes
	delete g_InstancedBox;
	InstancedMeshVP::Destroy();
}

//--------------------------------------------------------------------------------------
// Create any D3D11 resources that depend on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11ResizedSwapChain(ID3D11Device* pd3dDevice, IDXGISwapChain* pSwapChain,
	const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext)
{
	// Update camera parameters
	int width = pBackBufferSurfaceDesc->Width;
	int height = pBackBufferSurfaceDesc->Height;

#ifdef _MODELVIEW
	g_camera.SetWindow(width, height);
#endif

	//g_camera.SetProjParams(XM_PI / 4.0f, float(width) / float(height), 0.1f, 100.0f);
	g_camera.SetLens(XM_PI / 4.0f, float(width) / float(height), 0.1f, 100.0f);
	g_DebugCamera.SetLens(XM_PI / 4.0f, float(width) / float(height), 0.1f, 100.0f);

	// Inform AntTweakBar about back buffer resolution change
	TwWindowSize(pBackBufferSurfaceDesc->Width, pBackBufferSurfaceDesc->Height);

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11ResizedSwapChain 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11ReleasingSwapChain(void* pUserContext)
{
}

//--------------------------------------------------------------------------------------
// Handle key presses
//--------------------------------------------------------------------------------------
void CALLBACK OnKeyboard(UINT nChar, bool bKeyDown, bool bAltDown, void* pUserContext)
{
	HRESULT hr;

	if (bKeyDown)
	{
		switch (nChar)
		{
		case 'I':
		{
			//physics::ForcePoint fp;
			//fp.m_vForce = XMFLOAT3(0, 1.f, 0);

			//for (uint32_t i = 0; i < BoxIndices_NumOfEdges; ++i)
			//{
			//	fp.m_uVertexID = i;
			//	g_RBForces.emplace(1, fp);
			//}

			//PhysicsManager::Instance()->AddRigidBodyForces(g_RBForces);
			g_camera.Walk(0.1f);
			break;
		}

		case 'K':
		{
			//physics::ForcePoint fp;
			//fp.m_vForce = XMFLOAT3(0, -1.f, 0);

			//for (uint32_t i = 0; i < BoxIndices_NumOfEdges; ++i)
			//{
			//	fp.m_uVertexID = i;
			//	g_RBForces.emplace(1, fp);
			//}

			//PhysicsManager::Instance()->AddRigidBodyForces(g_RBForces);
			g_camera.Walk(-0.1f);
			break;
		}

		case 'J':
		{
			//physics::ForcePoint fp;

			//fp.m_vForce = XMFLOAT3(-1.f, 0, 0);
			//for (uint32_t i = 0; i < BoxIndices_NumOfEdges; ++i)
			//{
			//	fp.m_uVertexID = i;
			//	g_RBForces.emplace(1, fp);
			//}

			//PhysicsManager::Instance()->AddRigidBodyForces(g_RBForces);
			g_camera.Strafe(-0.1f);
			break;
		}

		case 'L':
		{
			//physics::ForcePoint fp;
			//fp.m_vForce = XMFLOAT3(1.f, 0, 0);

			//for (uint32_t i = 0; i < BoxIndices_NumOfEdges; ++i)
			//{
			//	fp.m_uVertexID = i;
			//	g_RBForces.emplace(1, fp);
			//}

			//PhysicsManager::Instance()->AddRigidBodyForces(g_RBForces);
			g_camera.Strafe(0.1f);
			break;
		}

		case 'A':
		{
			g_camera.Yaw(-0.1f);
			break;
		}
		
		case 'D':
		{
			g_camera.Yaw(0.1f);
			break;
		}

		case 'W':
		{
			g_camera.Pitch(-0.1f);
			break;
		}
		
		case 'S':
		{
			g_camera.Pitch(0.1f);
			break;
		}

		case 'Q':
		{
			g_camera.Roll(-0.1f);
			break;
		}

		case 'E':
		{
			g_camera.Roll(0.1f);
			break;
		}

		// RETURN: toggle fullscreen
		case VK_RETURN:
		{
			if (bAltDown) DXUTToggleFullScreen();
			break;
		}
		case iToggleMouseButton:	// VK_F1
		{
			bShowMouse = !bShowMouse;
			//g_camera.SetRotateButtons(false, false, bShowMouse, !bShowMouse);
			//g_camera.SetResetCursorAfterMove(!bShowMouse);
			ShowCursor(bShowMouse);

			if (bShowMouse)
				TwDefine(" TweakBar iconified=false ");
			else
				TwDefine(" TweakBar iconified=true ");

			break;
		}
		case iToggleSimulationButton:	// VK_F2
		{
			if (m_Scene.IsPaused())
			{
				m_Scene.Continue();
			}
			else
			{
				m_Scene.Pause();
			}
			break;
		}
		case iToggleCoordinateSystemButton:	// VK_F3
		{
			g_bDrawCoordinateSystem = !g_bDrawCoordinateSystem;
			break;
		}
		case iResetCameraButton:	// VK_F7
		{
			//g_camera.Reset();
			g_camera.LookAtFrom(g_CameraPos, g_CameraLookAt, g_CameraUp);
			g_DebugCamera.LookAtFrom(XMFLOAT3(0.0f,0.0f,0.0f), g_CameraLookAt, g_CameraUp);

			break;
		}
		// F8: Take screenshot
		case VK_F8:
		{
			// Save current render target as png
			static int nr = 0;
			std::wstringstream ss;
			ss << L"Screenshot" << std::setfill(L'0') << std::setw(4) << nr++ << L".png";

			ID3D11Resource* pTex2D = nullptr;
			DXUTGetD3D11RenderTargetView()->GetResource(&pTex2D);
			SaveWICTextureToFile(DXUTGetD3D11DeviceContext(), pTex2D, GUID_ContainerFormatPng, ss.str().c_str());
			SAFE_RELEASE(pTex2D);

			std::wcout << L"Screenshot written to " << ss.str() << std::endl;
			break;
		}
		// F10: Toggle video recording
		case VK_F10:
		{
			if (!g_pFFmpegVideoRecorder) {
				g_pFFmpegVideoRecorder = new FFmpeg(25, 21, FFmpeg::MODE_INTERPOLATE);
				V(g_pFFmpegVideoRecorder->StartRecording(DXUTGetD3D11Device(), DXUTGetD3D11RenderTargetView(), "output.avi"));
			}
			else {
				g_pFFmpegVideoRecorder->StopRecording();
				SAFE_DELETE(g_pFFmpegVideoRecorder);
			}
			break; // added this, maybe it breaks something
		}
		}
	}
}


//--------------------------------------------------------------------------------------
// Handle mouse button presses
//--------------------------------------------------------------------------------------
void CALLBACK OnMouse(bool bLeftButtonDown, bool bRightButtonDown, bool bMiddleButtonDown,
	bool bSideButton1Down, bool bSideButton2Down, int nMouseWheelDelta,
	int xPos, int yPos, void* pUserContext)
{
	// Track mouse movement if left mouse key is pressed
	{
		static int xPosSave = 0, yPosSave = 0;

		if (bLeftButtonDown)
		{
			// Accumulate deltas in g_viMouseDelta
			g_viMouseDelta.x += xPos - xPosSave;
			g_viMouseDelta.y += yPos - yPosSave;
		}

		xPosSave = xPos;
		yPosSave = yPos;
	}
}


//--------------------------------------------------------------------------------------
// Handle messages to the application
//--------------------------------------------------------------------------------------
LRESULT CALLBACK MsgProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam,
	bool* pbNoFurtherProcessing, void* pUserContext)
{
	// Send message to AntTweakbar first
	if (TwEventWin(hWnd, uMsg, wParam, lParam))
	{
		*pbNoFurtherProcessing = true;
		return 0;
	}

	// If message not processed yet, send to camera
	//if (g_camera.HandleMessages(hWnd, uMsg, wParam, lParam))
	//{
	//	*pbNoFurtherProcessing = true;
	//	return 0;
	//}

	return 0;
}


//--------------------------------------------------------------------------------------
// Handle updates to the scene
//--------------------------------------------------------------------------------------
void CALLBACK OnFrameMove(double dTime, float fElapsedTime, void* pUserContext)
{
	UpdateWindowTitle(L"Demo", static_cast<float>(m_Scene.GetDeltaTime()));

	if (g_bFreeUpdateInterval != g_bFreeUpdateIntervalPrev)
	{
		m_Scene.SetFreeUpdateInterval(g_bFreeUpdateInterval);
		g_bFreeUpdateIntervalPrev = g_bFreeUpdateInterval;
	}

	if (g_bFreeUpdateInterval == false)
	{
		if (g_bSyncTiming)
		{
			double fInterval = 1.0 / (DXUTGetFPS() + 10.f);
			m_Scene.SetUpdateInterval(fInterval);
			g_fUpdateIntervalPrev = fInterval;
		}
		else
		{
			if (g_fUpdateInterval != g_fUpdateIntervalPrev)
			{
				m_Scene.SetUpdateInterval(g_fUpdateInterval);
				g_fUpdateIntervalPrev = g_fUpdateInterval;
			}
		}
	}

	//if (g_fGravity != g_fGravityPrev)
	//{
	//	PhysicsManager::Instance()->SetDefaultForce(XMFLOAT3(0, g_fGravity, 0));
	//	g_fGravityPrev = g_fGravity;
	//}	

	//if (g_Integrator != g_IntegratorPrev)
	//{
	//	PhysicsManager::Instance()->SetIntegrator(g_Integrator);
	//	g_IntegratorPrev = g_Integrator;
	//}

	// Move camera
	//g_camera.FrameMove(fElapsedTime);
	//float fRot = 0.5f * XM_PI * fElapsedTime;
	//g_DebugCamera.Yaw(fRot);
	//
	g_CameraAnimationContoller.Update(fElapsedTime);
	g_camera.LookAtFrom(g_CameraAnimationContoller.GetVector3(g_CameraPositionHandle), XMFLOAT3(0.5f,0.0f,0.5f));
	//XMFLOAT3 look = g_CameraAnimationContoller.GetLookAt();
	//g_camera.LookAt(g_CameraAnimationContoller.GetLookAt());

	g_camera.Update();
	g_DebugCamera.Update();

	// Update effects with new view + proj transformations
	XMMATRIX mView = XMLoadFloat4x4(&g_camera.GetView());
	XMMATRIX mProj = XMLoadFloat4x4(&g_camera.GetProj());
	XMMATRIX mViewProj = XMLoadFloat4x4(&g_camera.GetViewProj());

	g_pEffectPositionColor->SetView(mView);
	g_pEffectPositionColor->SetProjection(mProj);

	g_pEffectPositionNormal->SetView(mView);
	g_pEffectPositionNormal->SetProjection(mProj);

	g_pEffectPositionNormalColor->SetView(mView);
	g_pEffectPositionNormalColor->SetProjection(mProj);


#pragma region HelixRendering
	// Helix
	//XMMATRIX view = g_camera.GetViewMatrix();
	//XMMATRIX proj = g_camera.GetProjMatrix();
	//XMMATRIX mViewProj = view * proj;

	// Set ViewProj matrix
	g_InstancedBox->SetVertexShaderConstantBuffer(mViewProj);
	// Set Color
	g_InstancedBox->SetPixelShaderConstantBuffer(XMFLOAT4(1.f, 0.f, 0.f, 1.f));
#pragma endregion


	// Apply accumulated mouse deltas to g_vfMovableObjectPos (move along cameras view plane)
	if (g_viMouseDelta.x != 0 || g_viMouseDelta.y != 0)
	{
		// Calcuate camera directions in world space
		XMMATRIX viewInv = XMMatrixInverse(nullptr, mView);
		XMVECTOR camRightWorld = XMVector3TransformNormal(g_XMIdentityR0, viewInv);
		XMVECTOR camUpWorld = XMVector3TransformNormal(g_XMIdentityR1, viewInv);

		// Add accumulated mouse deltas to movable object pos
		XMVECTOR vMovableObjectPos = XMLoadFloat3(&g_vfMovableObjectPos);

		float speedScale = 0.001f;
		vMovableObjectPos = XMVectorAdd(vMovableObjectPos, speedScale * (float)g_viMouseDelta.x * camRightWorld);
		vMovableObjectPos = XMVectorAdd(vMovableObjectPos, -speedScale * (float)g_viMouseDelta.y * camUpWorld);

		XMStoreFloat3(&g_vfMovableObjectPos, vMovableObjectPos);

		// Reset accumulated mouse deltas
		g_viMouseDelta = XMINT2(0, 0);
	}

	//if(g_MSS != nullptr) g_MSS->GetCurrentData(g_Data);
}

//--------------------------------------------------------------------------------------
// Render the scene using the D3D11 device
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11FrameRender(ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext,
	double fTime, float fElapsedTime, void* pUserContext)
{
	HRESULT hr;

	// Clear render target and depth stencil
	float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

	ID3D11RenderTargetView* pRTV = DXUTGetD3D11RenderTargetView();
	ID3D11DepthStencilView* pDSV = DXUTGetD3D11DepthStencilView();
	pd3dImmediateContext->ClearRenderTargetView(pRTV, ClearColor);
	pd3dImmediateContext->ClearDepthStencilView(pDSV, D3D11_CLEAR_DEPTH, 1.0f, 0);

	// Render Debug camera
	g_DebugCamera.DebugRender(g_pEffectPositionColor, pd3dImmediateContext, g_pPrimitiveBatchPositionColor, g_pInputLayoutPositionColor);
	g_camera.DebugRender(g_pEffectPositionColor, pd3dImmediateContext, g_pPrimitiveBatchPositionColor, g_pInputLayoutPositionColor);

	// Draw floor
	DrawFloor(pd3dImmediateContext);

	// Draw axis box
	//if (g_bDrawBoundingBox) DrawBoundingBox(pd3dImmediateContext);

	// Draw speheres
	if (g_bDrawSpheres) DrawSomeRandomObjects(pd3dImmediateContext);

	// Draw movable teapot
	if (g_bDrawTeapot) DrawMovableTeapot(pd3dImmediateContext);

	// Draw simple triangle
	if (g_bDrawTriangle) DrawTriangle(pd3dImmediateContext);

	//if (g_MSS != nullptr) DrawMassSpringSystem(pd3dImmediateContext);

	//if (g_pOctree != nullptr && g_bDrawOctree) DrawOctree(pd3dImmediateContext);

	if (g_bDrawGameObjects) DrawGameObjects(pd3dImmediateContext);

	if (g_bDrawCoordinateSystem) DrawCoordinateSystem(pd3dImmediateContext);

#pragma region HelixRendering
	//g_InstancedBox->Render(pd3dImmediateContext);
#pragma endregion

	// Draw GUI
	TwDraw();

	if (g_pFFmpegVideoRecorder)
	{
		V(g_pFFmpegVideoRecorder->AddFrame(pd3dImmediateContext, DXUTGetD3D11RenderTargetView()));
	}
}

//--------------------------------------------------------------------------------------
// Initialize everything and go into a render loop
//--------------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	HInitDebug();

	//_CrtSetBreakAlloc(858);

	//Logger::Instance()->WriteToFile();
	Logger::Instance()->WriteToStream(&std::wcout);

#ifdef _DEBUG
	HLOG(">>>DEBUG BUILD");
#endif	

#ifdef _SSE4
	HLOG(">>>SSE4 BUILD");
	if (SSE4::XMVerifySSE4Support() == false)
	{
		HFATAL("Your CPU does not support SSE4");
	}
#else
	HLOG(">>>SSE2 BUILD");
	if (DirectX::XMVerifyCPUSupport() == false)
	{
		HFATAL("Your CPU does not support SSE2");
	}
#endif

	HLOG("Press F1 to enable the mouse cursor");
	HLOG("Press F8 to take a screenshot");
	//HLOG("Press F10 to record the simulation");

	// Set general DXUT callbacks
	DXUTSetCallbackMsgProc(MsgProc);
	DXUTSetCallbackMouse(OnMouse, true);
	DXUTSetCallbackKeyboard(OnKeyboard);

	DXUTSetCallbackFrameMove(OnFrameMove);
	DXUTSetCallbackDeviceChanging(ModifyDeviceSettings);

	// Set the D3D11 DXUT callbacks
	DXUTSetCallbackD3D11DeviceAcceptable(IsD3D11DeviceAcceptable);
	DXUTSetCallbackD3D11DeviceCreated(OnD3D11CreateDevice);
	DXUTSetCallbackD3D11SwapChainResized(OnD3D11ResizedSwapChain);
	DXUTSetCallbackD3D11FrameRender(OnD3D11FrameRender);
	DXUTSetCallbackD3D11SwapChainReleasing(OnD3D11ReleasingSwapChain);
	DXUTSetCallbackD3D11DeviceDestroyed(OnD3D11DestroyDevice);

	// Init camera
	g_camera.LookAtFrom(g_CameraPos, g_CameraLookAt, g_CameraUp);
	g_CameraAnimationContoller.Initialize();
	
	std::vector<XMFLOAT3> vPositions;
	vPositions.push_back(XMFLOAT3(0.0f, 0.0f, 0.0f));
	vPositions.push_back(XMFLOAT3(0.0f, 0.0f, 1.0f));
	vPositions.push_back(XMFLOAT3(0.0f, 0.0f, 2.0f));
	vPositions.push_back(XMFLOAT3(1.0f, 0.0f, 0.0f));
	vPositions.push_back(XMFLOAT3(0.0f, 0.0f, 0.0f));
	g_CameraPositionHandle = g_CameraAnimationContoller.AddVector3Track(vPositions, 0.0f, 10.0f);
	
	//g_CameraAnimationContoller.Pause();


#ifdef _MODELVIEW
	g_camera.SetButtonMasks(MOUSE_MIDDLE_BUTTON, MOUSE_WHEEL, MOUSE_RIGHT_BUTTON);
#endif

	// Init DXUT and create device
	DXUTInit(true, true, NULL); // Parse the command line, show msgboxes on error, no extra command line params
								//DXUTSetIsInGammaCorrectMode( false ); // true by default (SRGB backbuffer), disable to force a RGB backbuffer
	DXUTSetCursorSettings(true, true); // Show the cursor and clip it when in full screen
	DXUTCreateWindow(L"Demo");
	DXUTCreateDevice(D3D_FEATURE_LEVEL_11_0, true, 1600, 900);

	m_Scene.StartInNewThread();

	DXUTMainLoop(); // Enter into the DXUT render loop

	//explixitly call stop to ensure there are no problems with DXUTShutdown
	m_Scene.Stop();
	
	HLOG("Press ENTER to exit");
	std::wcin.get();

	DXUTShutdown(); // Shuts down DXUT (includes calls to OnD3D11ReleasingSwapChain() and OnD3D11DestroyDevice())

	return DXUTGetExitCode();
}
