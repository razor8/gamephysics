//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "CollisionContact.h"

using namespace helix::collision;
using namespace DirectX;

//---------------------------------------------------------------------------------------------------
void CollisionContact::SimpleContact(_In_ const BoundingSphere& sphere1, _In_ const BoundingSphere& sphere2, _Out_ CollisionInfo& _CollsionInfo)
{
	XMVECTOR vCenter1 = XMLoadFloat3(&sphere1.Center);
	XMVECTOR vCenter2 = XMLoadFloat3(&sphere2.Center);
	XMVECTOR vCollisionDir = XMVectorSubtract(vCenter2, vCenter1);
	vCollisionDir = XMVector3Normalize(vCollisionDir);

	XMVECTOR vCollisionPoint = XMVectorScale(vCollisionDir, sphere1.Radius);
	vCollisionPoint = XMVectorAdd(vCenter1, vCollisionPoint);

	CollisionInfo::Contact Contact2;
	//pointing towards Center2
	XMStoreFloat3(&Contact2.m_vNormal, vCollisionDir);

	//pointing towards Center1
	vCollisionDir = XMVectorNegate(vCollisionDir);

	CollisionInfo::Contact Contact1;
	XMStoreFloat3(&Contact1.m_vPoint, vCollisionPoint);
	XMStoreFloat3(&Contact1.m_vNormal, vCollisionDir);

	_CollsionInfo.m_Contacts1.push_back(Contact1);

	vCollisionPoint = XMVectorScale(vCollisionDir, sphere2.Radius);
	vCollisionPoint = XMVectorAdd(vCenter2, vCollisionPoint);

	XMStoreFloat3(&Contact2.m_vPoint, vCollisionPoint);
	_CollsionInfo.m_Contacts2.push_back(Contact2);
}
//---------------------------------------------------------------------------------------------------
void CollisionContact::SimpleContact(_In_ const BoundingBox& box1, _In_ const BoundingBox& box2, _Out_ CollisionInfo& _CollsionInfo, const bool bSwitch)
{
	XMVECTOR vCenter1 = XMLoadFloat3(&box1.Center);
	XMVECTOR vCenter2 = XMLoadFloat3(&box2.Center);
	XMVECTOR vCollisionDir = XMVectorSubtract(vCenter2, vCenter1);
	vCollisionDir = XMVector3Normalize(vCollisionDir);

	static const XMFLOAT3 FaceNormals[6] = { XMFLOAT3(1, 0, 0), XMFLOAT3(0, 1, 0), XMFLOAT3(-1, 0, 0), XMFLOAT3(0, -1, 0), XMFLOAT3(0, 0, 1), XMFLOAT3(0, 0, -1) };

	XMVECTOR vRotatedNormal;
	XMVECTOR vNormal;

	float fBestDotDif = 1.f; //best dif = 0

	//find best normal
	for (uint32_t i = 0; i < 6; ++i)
	{
		vRotatedNormal = XMLoadFloat3(&FaceNormals[i]);

		float fDotDif = 1.f - XMVectorGetX(math::XMVector3Dot(vRotatedNormal, vCollisionDir));

		if (fDotDif <= fBestDotDif)
		{
			fBestDotDif = fDotDif;
			vNormal = vRotatedNormal;
		}
	}

	XMFLOAT3 pCornerPoints[8];
	box2.GetCorners(pCornerPoints);

	//vector pointing from box2 to box1
	vCollisionDir = XMVectorNegate(vCollisionDir);

	uint32_t uContactID = 0;
	fBestDotDif = 1.f;
	//find best point
	for (uint32_t i = 0; i < 8; ++i)
	{
		XMVECTOR vCornerDir = XMVectorSubtract(XMLoadFloat3(&pCornerPoints[i]), vCenter2);
		vCornerDir = XMVector3Normalize(vCornerDir);

		float fDotDif = 1.f - XMVectorGetX(math::XMVector3Dot(vCornerDir, vCollisionDir));

		if (fDotDif < fBestDotDif)
		{
			fBestDotDif = fDotDif;
			uContactID = i;
		}
	}

	CollisionInfo::Contact Contact;
	//contact point on box2
	Contact.m_vPoint = pCornerPoints[uContactID];

	//this is the normal pointing towards box2
	XMStoreFloat3(&Contact.m_vNormal, vNormal);

	//compute the other concat side
	if (bSwitch)
	{
		//normal needed for evaluation of collision on box2
		_CollsionInfo.m_Contacts2.push_back(Contact);
		SimpleContact(box2, box1, _CollsionInfo, false);
	}
	else
	{
		_CollsionInfo.m_Contacts1.push_back(Contact);
	}		
}
//---------------------------------------------------------------------------------------------------
void CollisionContact::SimpleContact(_In_ const BoundingOrientedBox& box1, _In_ const BoundingOrientedBox& box2, _Out_ CollisionInfo& _CollsionInfo, const bool bSwitch)
{
	XMVECTOR vCenter1 = XMLoadFloat3(&box1.Center);
	XMVECTOR vCenter2 = XMLoadFloat3(&box2.Center);
	XMVECTOR vCollisionDir = XMVectorSubtract(vCenter2, vCenter1);
	vCollisionDir = XMVector3Normalize(vCollisionDir);

	XMVECTOR vRot1 = XMLoadFloat4(&box1.Orientation);

	static const XMFLOAT3 FaceNormals[6] = { XMFLOAT3(1, 0, 0), XMFLOAT3(0, 1, 0), XMFLOAT3(-1, 0, 0), XMFLOAT3(0, -1, 0), XMFLOAT3(0, 0, 1), XMFLOAT3(0, 0, -1) };

	XMVECTOR vRotatedNormal;
	XMVECTOR vNormal;

	float fBestDotDif = 1.f;

	//find best normal
	for (uint32_t i = 0; i < 6; ++i)
	{				
		vRotatedNormal = math::XMVector3RotateNormal(XMLoadFloat3(&FaceNormals[i]), vRot1);

		float fDotDif = 1.f - XMVectorGetX(math::XMVector3Dot(vRotatedNormal, vCollisionDir));

		if (fDotDif <= fBestDotDif)
		{
			fBestDotDif = fDotDif;
			vNormal = vRotatedNormal;
		}
	}

	XMFLOAT3 pCornerPoints[8];
	box2.GetCorners(pCornerPoints);

	//vector pointing from box2 to box1
	vCollisionDir = XMVectorNegate(vCollisionDir);

	uint32_t uContactID = 0;
	fBestDotDif = 1.f;
	//find best point
	for (uint32_t i = 0; i < 8; ++i)
	{
		XMVECTOR vCornerDir = XMVectorSubtract(XMLoadFloat3(&pCornerPoints[i]), vCenter2);
		vCornerDir = XMVector3Normalize(vCornerDir);

		float fDotDif = 1.f - XMVectorGetX(math::XMVector3Dot(vCornerDir, vCollisionDir));

		if (fDotDif < fBestDotDif)
		{
			fBestDotDif = fDotDif;
			uContactID = i;
		}
	}

	CollisionInfo::Contact Contact;
	//contact point on box2
	Contact.m_vPoint = pCornerPoints[uContactID];

	//this is the normal pointing towards box2
	XMStoreFloat3(&Contact.m_vNormal, vNormal);

	//compute the other concat side
	if (bSwitch)
	{
		//normal needed for evaluation of collision on box2
		_CollsionInfo.m_Contacts2.push_back(Contact);
		SimpleContact(box2, box1, _CollsionInfo, false);
	}
	else
	{
		_CollsionInfo.m_Contacts1.push_back(Contact);
	}
}
//---------------------------------------------------------------------------------------------------

//TODO: implement this correctly
void CollisionContact::SimpleContact(_In_ const BoundingSphere& sphere, _In_ const BoundingBox& box, _Out_ CollisionInfo& _CollisionInfo)
{
	XMVECTOR vCenter1 = XMLoadFloat3(&sphere.Center);
	XMVECTOR vCenter2 = XMLoadFloat3(&box.Center);
	XMVECTOR vCollisionDir = XMVectorSubtract(vCenter2, vCenter1);

	XMVECTOR vFakeCollisionPoint = XMVectorScale(vCollisionDir, 0.5f);

	vCollisionDir = XMVector3Normalize(vCollisionDir);

	CollisionInfo::Contact Contact;
	//TODO: compute the collision point correctly!
	XMStoreFloat3(&Contact.m_vPoint, vFakeCollisionPoint);
	//normal from sphere to box
	XMStoreFloat3(&Contact.m_vNormal, vCollisionDir);
	_CollisionInfo.m_Contacts2.push_back(Contact);

	//normal pointing from box to sphere
	vCollisionDir = XMVectorNegate(vCollisionDir);

	//static const XMFLOAT3 FaceNormals[6] = { XMFLOAT3(1, 0, 0), XMFLOAT3(0, 1, 0), XMFLOAT3(-1, 0, 0), XMFLOAT3(0, -1, 0), XMFLOAT3(0, 0, 1), XMFLOAT3(0, 0, -1) };

	//XMVECTOR vRotatedNormal;
	//XMVECTOR vNormal;

	//float fBestDotDif = 1.f; //best dif = 0

	////find best normal
	//for (uint32_t i = 0; i < 6; ++i)
	//{
	//	vRotatedNormal = XMLoadFloat3(&FaceNormals[i]);

	//	float fDotDif = 1.f - XMVectorGetX(math::XMVector3Dot(vRotatedNormal, vCollisionDir));

	//	if (fDotDif <= fBestDotDif)
	//	{
	//		fBestDotDif = fDotDif;
	//		vNormal = vRotatedNormal;
	//	}
	//}

	//normal from box to sphere
	//XMStoreFloat3(&Contact.m_vNormal, vNormal);
	XMStoreFloat3(&Contact.m_vNormal, vCollisionDir);
	_CollisionInfo.m_Contacts1.push_back(Contact);	
}
//---------------------------------------------------------------------------------------------------
void CollisionContact::SimpleContact(_In_ const BoundingBox& box, _In_ const BoundingSphere& sphere, _Out_ CollisionInfo& _CollisionInfo)
{
	SimpleContact(sphere, box, _CollisionInfo);
	_CollisionInfo.m_Contacts1.swap(_CollisionInfo.m_Contacts2);
}