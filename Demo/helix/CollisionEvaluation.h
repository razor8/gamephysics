//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef COLLISIONEVALUATION_H
#define COLLISIONEVALUATION_H

#include "MovingObject.h"
#include "RigidBody.h"

#include "CollisionContact.h"

namespace helix
{
	namespace collision
	{
		class CollisionEvaluation
		{
		public:
			static void ComputeCollision(datastructures::GameObject* _pGO1, datastructures::GameObject* _pGO2, const collision::CollisionInfo& _CollisionInfo);

			static void ComputeCollision(datastructures::MovingObject& _MO1, datastructures::MovingObject& _MO2, const collision::CollisionInfo& _CollisionInfo);
			static void ComputeCollision(datastructures::RigidBody& _RB1, datastructures::RigidBody& _RB2, const collision::CollisionInfo& _CollisionInfo);
		};
	} // collision
} // helix

#endif // COLLISIONEVALUATION_H
