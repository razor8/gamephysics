//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef OCTREENODE_H
#define OCTREENODE_H

#include "Debug.h"
#include "CollisionContact.h"
#include "Flag.h"

#include <list>
#include <mutex>

//for debug draw
#include <PrimitiveBatch.h>
#include <VertexTypes.h>

#include "ThreadPool.h"
#include "TplGameObject.h"

namespace helix
{
	namespace datastructures
	{
		enum GatherTask
		{
			GatherTask_GatherCollisions = 0,
			GatherTask_GatherInVolume = 1
		};

		using namespace DirectX;

		// OctreeNode -------------------------------------------------------------------------------------
		class OctreeNode
		{
			typedef std::list<TplGameObject*> TEntityList;
		public:
			HDEBUGNAME("OctreeNode");

			OctreeNode(OctreeNode* _pParentNode, async::ThreadPool* _pThreadPool, const XMFLOAT3& _vCenter, float _fRadius, float _fLooseness, int32_t _iRecursionDepth, bool _bPrealloc);
			~OctreeNode();

			//recursivly draw all aabbs
			void DebugDraw(PrimitiveBatch<VertexPositionColor>* _pPrimitiveBatchPositionColor);

			//Check if the GameObject is contained by the current node, if not it tries to insert it into the parent node
			bool Update(TplGameObject* _pEntity);

			//insert a entity, if _bAllowBottomUp is set, the entity can be passed to the parent node if its not inside the current cell
			bool Insert(TplGameObject* _pEntity, bool _bAllowBottomUp = true);
			
			void Remove(uint32_t _uID);
			static void Remove(TplGameObject* _Entity);
			
			void GatherCollisions(_Out_ std::vector<collision::CollisionInfo>& _Collisions);

			template <typename BoundingVolume>
			void GatherInVolume(const BoundingVolume& _BV, std::vector<TplGameObject*>& _Objects);

			//This function should only be called once after all entities have been updated and before starting any Task!
			//gets number of entities in this node and all child nodes
			//Depth = 0xffffffff => traverse to leaf
			//Depth = 0 => only get num of entities of this node, no recursion
			uint32_t GatherTotalNumOfEntities(uint32_t _uDepth = 0xffffffff);

			//get number of entities stored in this node
			uint32_t GetNumOfEntities() const;

			//get number of entities stored in this node and all subnodes
			uint32_t GetTotalNumOfEntities() const;

			static int32_t CalculateRecursionDepth(float _fMaxRadius, float _fMinRadius);
			static uint32_t CalculateNumOfNodes(int32_t iRecursionDepth);

			XMFLOAT3 GetCenterOfNode(BoxIndices _ChildIndex) const;

		private:
			//Insertion functions:
			bool PlaceInAnyChild(TplGameObject* _pEntity);
			bool PlaceInNearestChild(TplGameObject* _pEntity);

			//Collision gathering functions:
			//==============================

			//Main function called
			void GatherCollisionsTask();

			//This function spawns a new instance of GatherCollisionsTask
			void StartGatherCollisionsTask();

			//Subfunction called in GatherCollisions():
			void GatherCollisions(_In_ TplGameObject* _pEntity, _Out_ std::vector<collision::CollisionInfo>& _Collisions) const;


			//This functions copys the collision info to _Collisions
			void GetCollisions(_Out_ std::vector<collision::CollisionInfo>& _Collisions);

			//Gather in volumes functions:
			//============================

			template <typename BoundingVolume>
			void GatherInVolumeTask(const BoundingVolume& _BV);

			//This function spawns a new instance of GatherCollisionsTask
			template <typename BoundingVolume>
			void StartGatherInVolumeTask(const BoundingVolume& _BV);

			//This functions copys the objects in from previous GatherInVolumeTask to _Objects
			void GetObjectsInVolume(std::vector<TplGameObject*>& _Objects);

		private:
			async::ThreadPool* const m_pThreadPool;

			OctreeNode* const m_pParentNode;

			OctreeNode* m_pChildren[BoxIndices_NumOfEdges];

			BoundingBox m_AABB;

			TEntityList m_Entities;

			const float m_fRadius;
			const int32_t m_iDepth;
			const float m_fLooseness;
			const bool m_bPrealloc;

			//number of entities stored in this node and all subnodes
			uint32_t m_uCurTotalEntityCount;
			//number of entities stored in this node
			uint32_t m_uCurEntityCount;

			std::mutex m_Mutex;

			//current batch of collisions gathered
			std::vector<collision::CollisionInfo> m_Collisions;
			std::vector<TplGameObject*> m_ObjectsInVolume;
		};

		inline uint32_t OctreeNode::GetNumOfEntities() const { /*std::lock_guard<std::recursive_mutex> Lock(m_Mutex);*/ return m_uCurEntityCount; }
		inline uint32_t OctreeNode::GetTotalNumOfEntities() const { /*std::lock_guard<std::recursive_mutex> Lock(m_Mutex);*/ return m_uCurTotalEntityCount; }
	};
};

//include template functions here:
#include "OctreeNodeTemplate.h"

#endif //EOCTREENODE_H