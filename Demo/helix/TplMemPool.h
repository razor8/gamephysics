//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel, Moritz Becher

#ifndef TPLMEMPOOL_H
#define TPLMEMPOOL_H

#include <tuple>
#include <vector>
#include <set>

#include "Logger.h"

namespace helix
{
	namespace datastructures
	{
		template <typename Parent, typename ... Ts>
		class TplMemPool
		{
			//MemBlock - Storage container
			//---------------------------------------------------------------------------------------------------

			//Forward declaration:
			template<typename... Types>
			struct MemBlock;

			//Specialization for two or more types
			template <typename First, typename ...Rest>
			struct MemBlock<First, Rest...>
			{
				MemBlock() : m_Parent(m_Data)
				{
				}

				First m_Parent;
				std::tuple<Rest...> m_Data;
			};

			//Spepialization for one type
			template <typename First>
			struct MemBlock<First>
			{
				First m_Parent;
			};

			//MemUsage - used for sorting MemBlocks by usage
			//---------------------------------------------------------------------------------------------------
			struct MemUsage
			{
				//compare function implements lhs<rhs
				bool operator< (const MemUsage& _RUsg) const
				{
					if (m_bUsed == false && _RUsg.m_bUsed)
						return true;
					else if (m_bUsed && _RUsg.m_bUsed == false)
						return false;
					return m_pParent < _RUsg.m_pParent;
				}

				Parent* m_pParent = nullptr;
				bool m_bUsed = false;
			};

			typedef typename std::set<MemUsage>::iterator UsageIterator;

		public:

#pragma region Iterators
			// iterator class
			//---------------------------------------------------------------------------------------------------
			class iterator : public std::iterator<std::random_access_iterator_tag, Parent>
			{
			public:
				iterator() {}
				iterator(UsageIterator p) : p_(p) {}
				iterator(const iterator& other) : p_(other.p_) {}
				const iterator& operator=(const iterator& other) { p_ = other.p_; return other; }

				iterator& operator++() { p_++; return *this; } // prefix++
				iterator  operator++(int) { iterator tmp(*this); ++(*this); return tmp; } // postfix++
				iterator& operator--() { p_--; return *this; } // prefix--
				iterator  operator--(int) { iterator tmp(*this); --(*this); return tmp; } // postfix--

				void     operator+=(const std::size_t& n) { p_ += n; }
				void     operator+=(const iterator& other) { p_ += other.p_; }
				iterator operator+ (const std::size_t& n) { iterator tmp(*this); tmp += n; return tmp; }
				iterator operator+ (const iterator& other) { iterator tmp(*this); tmp += other; return tmp; }

				void        operator-=(const std::size_t& n) { p_ -= n; }
				void        operator-=(const iterator& other) { p_ -= other.p_; }
				iterator    operator- (const std::size_t& n) { iterator tmp(*this); tmp -= n; return tmp; }
				std::size_t operator- (const iterator& other) { return p_ - other.p_; }

				bool operator< (const iterator& other) { return (p_ - other.p_)< 0; }
				bool operator<=(const iterator& other) { return (p_ - other.p_) <= 0; }
				bool operator> (const iterator& other) { return (p_ - other.p_)> 0; }
				bool operator>=(const iterator& other) { return (p_ - other.p_) >= 0; }
				bool operator==(const iterator& other) { return  p_ == other.p_; }
				bool operator!=(const iterator& other) { return  p_ != other.p_; }

				Parent& operator[](const int& n) { return *((p_ + n)->m_pParent); }
				Parent& operator*() { return *(p_->m_pParent); }
				Parent* operator->() { return  p_->m_pParent; }

				const UsageIterator& GetUsageIterator() const { return p_; }

			private:
				UsageIterator p_;
			};

			// cosnt iterator class
			//---------------------------------------------------------------------------------------------------
			class const_iterator : public std::iterator<std::random_access_iterator_tag, Parent>
			{
			public:
				const_iterator() {}
				const_iterator(const UsageIterator& p) : p_(p) {}
				const_iterator(const const_iterator& other) : p_(other.p_) {}
				const const_iterator& operator=(const const_iterator& other) { p_ = other.p_; return other; }

				const_iterator& operator++() { p_++; return *this; } // prefix++
				const_iterator  operator++(int) { const_iterator tmp(*this); ++(*this); return tmp; } // postfix++
				const_iterator& operator--() { p_--; return *this; } // prefix--
				const_iterator  operator--(int) { const_iterator tmp(*this); --(*this); return tmp; } // postfix--

				void     operator+=(const std::size_t& n) { p_ += n; }
				void     operator+=(const const_iterator& other) { p_ += other.p_; }
				const_iterator operator+ (const std::size_t& n) const { const_iterator tmp(*this); tmp += n; return tmp; }
				const_iterator operator+ (const const_iterator& other) const { const_iterator tmp(*this); tmp += other; return tmp; }

				void        operator-=(const std::size_t& n) { p_ -= n; }
				void        operator-=(const const_iterator& other) { p_ -= other.p_; }
				const_iterator operator- (const std::size_t& n) const { const_iterator tmp(*this); tmp -= n; return tmp; }
				std::size_t operator- (const const_iterator& other) const { return p_ - other.p_; }

				bool operator< (const const_iterator& other) const { return (p_ - other.p_)< 0; }
				bool operator<=(const const_iterator& other) const { return (p_ - other.p_) <= 0; }
				bool operator> (const const_iterator& other) const { return (p_ - other.p_)> 0; }
				bool operator>=(const const_iterator& other) const { return (p_ - other.p_) >= 0; }
				bool operator==(const const_iterator& other) const { return  p_ == other.p_; }
				bool operator!=(const const_iterator& other) const { return  p_ != other.p_; }

				const Parent& operator[](const int& n) const { return *((p_ + n)->m_pParent); }
				const Parent& operator*() const { return *(p_->m_pParent); }
				const Parent* operator->() const { return  p_->m_pParent; }

				const UsageIterator& GetUsageIterator() const { return p_; }

			private:
				const UsageIterator p_;
			};

			iterator begin() { return iterator(m_FirstUsedItr); }
			iterator end() { return iterator(m_UsageSet.end()); }

			const_iterator cbegin() const { return const_iterator(m_FirstUsedItr); }
			const_iterator cend() const { return const_iterator(m_UsageSet.cend()); }

#pragma endregion Iterators			

			//TplMemPool - Tuple memory pool implementation using sorted set 
			//---------------------------------------------------------------------------------------------------

			TplMemPool(uint32_t _uElements = 1000)
			{
				HASSERTD(_uElements > 0, "Insufficient number of elements specified!");

				Resize(_uElements);
			}

			//ATTENTION!
			//This function will break all previous references to objects held by this container if _uElements > m_Blocks.capacity()!
			//Only call this function if you are totally aware of the consequences!
			//---------------------------------------------------------------------------------------------------
			void Resize(uint32_t _uElements)
			{
				m_UsageSet.clear();

				m_FirstUsedItr = m_UsageSet.end();
				m_Blocks.resize(_uElements);

				MemUsage Usage;

				//Initialization of references
				for (uint32_t i = 0; i < _uElements; ++i)
				{
					Usage.m_pParent = &m_Blocks[i].m_Parent;
					m_UsageSet.insert(Usage);
				}
			}

			//---------------------------------------------------------------------------------------------------
			Parent* Alloc()
			{
				//Free MemBlocks are sorted to the front of the vector
				UsageIterator Itr = m_UsageSet.begin();

				if (Itr->m_bUsed == false)
				{
					//Flag the block as used
					MemUsage Usg;
					Usg.m_pParent = Itr->m_pParent;
					Usg.m_bUsed = true;

					//remove the old entry and insert the flaged one to be sorted to the end of the set
					m_UsageSet.erase(Itr);
					std::pair<UsageIterator, bool> Ret = m_UsageSet.insert(Usg);

					if (m_FirstUsedItr == m_UsageSet.end())
					{
						m_FirstUsedItr = Ret.first;
					}
					else if (Ret.first->m_pParent < m_FirstUsedItr->m_pParent)
					{
						m_FirstUsedItr = Ret.first;
					}

					return Usg.m_pParent;
				}

				//if the first block in the set is used then there is no free block left
				return nullptr;
			}

			//---------------------------------------------------------------------------------------------------
			iterator Free(Parent* pMem)
			{
				//Flag the pointer as used. This makes it impossible to free pointers that are currently not in use
				//The set also prevents freeing memory that is not in this pool
				MemUsage Usg;
				Usg.m_pParent = pMem;
				Usg.m_bUsed = true;

				iterator Itr = iterator(m_UsageSet.find(Usg));

				return Free(Itr);
			}

			//---------------------------------------------------------------------------------------------------
			iterator Free(const iterator& _Itr)
			{
				const UsageIterator& Itr = _Itr.GetUsageIterator();

				if (Itr != m_UsageSet.end())
				{
					MemUsage Usg;
					Usg.m_pParent = Itr->m_pParent;
					//Reinsert the block flaged as unused
					Usg.m_bUsed = false;

					if (Itr == m_FirstUsedItr)
						m_FirstUsedItr++;

					iterator NextUsed = iterator(m_UsageSet.erase(Itr));
					m_UsageSet.insert(Usg);

					return NextUsed;
				}

				//The used block wasn't found in the set
				return end();
			}

		private:

			UsageIterator m_FirstUsedItr;
			std::set<MemUsage> m_UsageSet;
			std::vector< MemBlock<Parent, Ts...> > m_Blocks;
		};
	};
};

#endif
