//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

#ifndef SINGLETON_H
#define SINGLETON_H

#include <mutex>
#include <atomic>
#include <memory>

namespace helix{

//Add Instance(Type) to your class definition to use the Singleton pattern
//#define INSTANCE(TYPE,...) 	static TYPE* Instance(){\
//						static std::mutex m_SingletonMutex; \
//						static std::unique_lock<std::mutex> lock(m_SingletonMutex);\
//						static TYPE inst = TYPE(__VA_ARGS__); return &inst; }



	//Usage:
	//class A : public TSingleton<A> {
	//	A() {
	//		std::cout << "A constructed!\n";
	//	}
	//
	//	~A() {
	//		std::cout << "A destructed!\n";
	//	}
	//	int k;
	//};


	template<typename T>
	class TSingleton {
		static std::shared_ptr<T> m_pSingletonInstance;

	public:
		static T* Instance() {
			auto instance_ptr = std::atomic_load_explicit(&m_pSingletonInstance, std::memory_order_acquire);

			if (instance_ptr == nullptr) {
				auto new_instance = std::make_shared<T>();
				std::atomic_compare_exchange_strong_explicit(&m_pSingletonInstance, &instance_ptr, new_instance, std::memory_order_release, std::memory_order_acquire);
				return std::atomic_load_explicit(&m_pSingletonInstance, std::memory_order_acquire).get();
			}

			return instance_ptr.get();
		}
	};

	template<typename T>
	std::shared_ptr<T> TSingleton<T>::m_pSingletonInstance = nullptr;
}

#endif // SINGLETON_H