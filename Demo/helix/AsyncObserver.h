//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef ASYNCOBSERVER_H
#define ASYNCOBSERVER_H

#include <condition_variable>
#include <atomic>
#include "Logger.h"

namespace helix
{
	namespace async
	{
		class IAsyncObserver
		{
		private:
			std::condition_variable m_CV;
			std::mutex m_WaitMutex;
			bool m_bNotified;
			bool m_bUseBarrier;

			uint32_t m_uNumOfNotifications;
			uint32_t m_uNotificationCount;

		public:
			HDEBUGNAME("IAsyncObserver");

			IAsyncObserver()
			{
				m_bUseBarrier = false;
				m_bNotified = false;
				m_uNumOfNotifications = 0;
				m_uNotificationCount = 0;
			}

			~IAsyncObserver()
			{
				std::lock_guard<std::mutex> Lock(m_WaitMutex);
				m_bNotified = true;
				m_CV.notify_one();

				if (m_bUseBarrier && m_uNotificationCount < m_uNumOfNotifications)
				{
					HWARNINGD("Barrier still active! Observer has not been signaled before destruction.");
				}
			}

			void SetBarrier(uint32_t _uNumOfNotifications = 1)
			{
				std::lock_guard<std::mutex> Lock(m_WaitMutex);

				m_uNumOfNotifications = _uNumOfNotifications;
				m_uNotificationCount = 0;
				m_bNotified = false;
				m_bUseBarrier = true;
			}

			void NotifyBarrier()
			{

				if (m_bUseBarrier)
				{
					if (++m_uNotificationCount == m_uNumOfNotifications)
					{
						{
							std::lock_guard<std::mutex> Lock(m_WaitMutex);
							m_bNotified = true;
						}
						m_CV.notify_one();
					}
				}
				else
				{
					//This can be desired behaviour in a case where no barrier has been set (first execution).
					HWARNINGD("Using thread barrier without calling setBarrier()!");
				}				
			}

			void WaitForNotifications()
			{
				if (m_bUseBarrier)
				{
					std::unique_lock<std::mutex> Lock(m_WaitMutex);
					while (m_bNotified == false)
					{
						m_CV.wait(Lock);
					}

					m_bNotified = false;
					m_bUseBarrier = false;

					m_uNotificationCount = 0;
				}
				else
				{
					HWARNINGD("Using thread barrier without calling setBarrier()!");
				}
			}

			void NotifyOne()
			{
				std::lock_guard<std::mutex> Lock(m_WaitMutex);

				HASSERTD (m_bUseBarrier == false,"Calling NotifyOne() is not allowed when using the thread barrier!");				

				m_bNotified = true;
				m_CV.notify_one();
			}

			void NotifyAll()
			{
				std::lock_guard<std::mutex> Lock(m_WaitMutex);

				HASSERTD(m_bUseBarrier == false, "Calling NotifyAll() is not allowed when using the thread barrier!");

				m_bNotified = true;
				m_CV.notify_all();
			}

		}; // IAsyncObserver

		template <typename T>
		class IAsyncTemplateObserver : public IAsyncObserver
		{
		public:
			virtual void Signal(T* _Obj) = 0;
		};
	}; // Namespace Async

}; // Namespace Helix

#endif