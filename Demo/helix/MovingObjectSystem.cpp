//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "MovingObjectSystem.h"
#include "StdAfxH.h"

using namespace helix::math;
using namespace helix::async;
using namespace helix::physics;

//---------------------------------------------------------------------------------------------------
MovingObjectSystem::MovingObjectSystem() : PhysicsSystem<TMovingObjects, TMovingObjectForceMap>()
{

}
//---------------------------------------------------------------------------------------------------
MovingObjectSystem::MovingObjectSystem(IAsyncObserver* _Observer) : PhysicsSystem<TMovingObjects, TMovingObjectForceMap>(_Observer)
{

}
//---------------------------------------------------------------------------------------------------
MovingObjectSystem::~MovingObjectSystem()
{

}
//---------------------------------------------------------------------------------------------------
//run simulation step here
void MovingObjectSystem::Simulate(float _fDeltaTime)
{
	TMovingObjectForceMap::iterator fit, fend = m_ExternalData.end();
	XMVECTOR f, df, x0, v0, a0;
	float fMass, fDamping;

	//load default force
	df = XMLoadFloat3(&m_DefaultForce);

	HFOREACH_PARALLEL(m_Data, [&], datastructures::MovingObject*, pMO)
		pMO->Lock();

		//load current force
		f = XMLoadFloat3(&pMO->m_vCurForce);

		//add default force
		f = XMVectorAdd(df, f);

		fit = m_ExternalData.find(pMO->m_uID);

		//if there is an additional external force, add it here
		if (fit != fend)
			f = XMVectorAdd(f, XMLoadFloat3(&fit->second));

		x0 = XMLoadFloat3(&pMO->m_vPosition);
		v0 = XMLoadFloat3(&pMO->m_vLinearVelocity);

		//f = m*a => a = f / m
		a0 = XMVectorScale(f, pMO->m_fInvMass);

		fDamping = pMO->m_fDamping;

		//integrate
		switch (m_Integrator)
		{
		case XMIntegrator_Euler:
			XMVectorEuler(x0, v0, a0, fDamping, _fDeltaTime);
			break;
		case XMIntegrator_Heun:
			XMVectorHeun(x0, v0, a0, fDamping, _fDeltaTime);
			break;
		case XMIntegrator_RK2:
			XMVectorRungeKutta2(x0, v0, a0, fDamping, _fDeltaTime);
			break;
		case XMIntegrator_RK3:
			XMVectorRungeKutta3(x0, v0, a0, fDamping, _fDeltaTime);
			break;
		case XMIntegrator_RK4:
		default:
			XMVectorRungeKutta4(x0, v0, a0, fDamping, _fDeltaTime);
		}

		//store new velocity and position
		XMStoreFloat3(&pMO->m_vLinearVelocity, v0);
		XMStoreFloat3(&pMO->m_vPosition, x0);

		//reset current force
		pMO->m_vCurForce = XMFLOAT3_ZERO;

		pMO->UpdateBoundingVolumes();

		pMO->Unlock();
	HFOREACH_PARALLEL_END

	//clear forces
	m_ExternalData.clear();
}
//---------------------------------------------------------------------------------------------------
void MovingObjectSystem::ClearData()
{
	std::lock_guard<std::mutex> lock(m_LockMutex);
	m_Data.resize(0);
}
//---------------------------------------------------------------------------------------------------
void MovingObjectSystem::ClearExternalData()
{
	std::lock_guard<std::mutex> lock(m_LockMutex);
	m_ExternalData.clear();
}
//---------------------------------------------------------------------------------------------------
void MovingObjectSystem::SetDefaultForce(const XMFLOAT3& _DefaultForce)
{
	std::lock_guard<std::mutex> lock(m_LockMutex);
	m_DefaultForce = _DefaultForce;
}
//---------------------------------------------------------------------------------------------------
void MovingObjectSystem::SetIntegrator(XMIntegrator _Integrator)
{
	std::lock_guard<std::mutex> lock(m_LockMutex);
	m_Integrator = _Integrator;
}
//---------------------------------------------------------------------------------------------------