//Copyright(c) 2014
//Authors: Fabian Wahlster

#ifndef ASYNCOBJECT_H
#define ASYNCOBJECT_H

#include <memory>

#include "MutableThread.h"
#include "Logger.h"
#include <future>

namespace helix
{
	namespace async
	{
		class IAsyncObject
		{
		public:
			HDEBUGNAME("IAsyncObject");

			IAsyncObject(IAsyncObserver * _pObserver = nullptr, bool _bLoopedExecution = false, bool _bPauseAfterExecution = false);

			~IAsyncObject();

			//Start synchron without thread
			void Start();

			//Starts bound function in the std::thread
			bool StartInNewThread();

			//Starts bound function in MutableThread
			bool MoveToThread(MutableThread* thread);

			//Signals the loop function to stop execution
			void Stop();

			//Signals the loop function to continue execution
			void Continue();

			//Signals the loop function to pause execution
			void Pause();

			//This will join the std::thread (not the MutableThread!)
			void StopThread();		

			//Set new observer
			void SetObserver(IAsyncObserver * _Observer);

			//Get bound function
			const std::function<void()>& GetFunction() const;

			std::future<void> GetFuture(std::launch _kPolicy = std::launch::async) const;

			//Returns true if the execution is currently paused
			bool IsPaused() const;
		private:
			//Repeatedly calls the Execute() function
			void ExecuteLooped();

		protected:
			//main work loop; has to be implemented by derived class
			virtual void Execute() = 0;

			// This function is called at the end of the Stop() function, the child class should implement this function
			// and stop all AsyncObjects that are members of the parent class.
			// This function can be empty if the parent object does not have any child AsyncObjects.
			virtual void StopSubsystems() = 0;

			// This function is called before executing the bound function, it should set all child AsyncObjects observers and start them (in a thread or synchronous)
			// if they are not started in the Execute function itself.
			// This function can be empty if the parent object does not have any child AsyncObjects.
			virtual void InitializeSubsystems() = 0;

			//Calls NotifyBarrier of Observer object
			void SignalObserverBarrier();

		private:
			//don't provide access to derived class
			std::unique_ptr<std::thread> m_StdThread;
			std::function<void()> m_Function;

			IAsyncObserver* m_Observer = nullptr;

			bool m_bPauseAfterExecution;

			std::mutex m_ObserverLockMutex;
			std::mutex m_ThreadLockMutex;

			bool m_bRun;
			bool m_bPause;

			std::mutex m_WaitMutex;
			std::condition_variable m_CV;
		};

		inline const std::function<void()>& IAsyncObject::GetFunction() const { return m_Function; }

		inline std::future<void> IAsyncObject::GetFuture(std::launch _kPolicy) const
		{
			HASSERTD(m_StdThread != nullptr, "The function of this AsyncObject is already bound to a thread!");
			return std::async(_kPolicy, m_Function);
		}

		//Start synchron without thread
		inline void IAsyncObject::Start() { InitializeSubsystems(); m_Function();}

		inline void IAsyncObject::SetObserver(IAsyncObserver * _Observer) { std::lock_guard<std::mutex> lock(m_ObserverLockMutex); m_Observer = _Observer; }

		//TODO: protect with mutex
		inline bool IAsyncObject::IsPaused() const { return m_bPause; }

	}; // Namespace aync
}; // Namespace helix

#endif