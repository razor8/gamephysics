//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!Never include DirectXMath.h, always include this header instead!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


//http://msdn.microsoft.com/en-us/library/windows/desktop/ee418728%28v=vs.85%29.aspx

//The FXMVECTOR, GXMVECTOR, HXMVECTOR, and CXMVECTOR aliases support these conventions :
//
//Use the FXMVECTOR alias to pass up to the first three instances of XMVECTOR used as arguments to a function.
//Use the GXMVECTOR alias to pass the 4th instance of an XMVECTOR used as an argument to a function.
//Use the HXMVECTOR alias to pass the 5th and 6th instances of an XMVECTOR used as an argument to a function.For info about additional considerations, see the __vectorcall documentation.
//Use the CXMVECTOR alias to pass any further instances of XMVECTOR used as arguments.
//
//Note  For output parameters, always use XMVECTOR* or XMVECTOR& and ignore them with respect to the preceding rules for input parameters.
//
//Because of limitations with __vectorcall, we recommend that you not use GXMVECTOR or HXMVECTOR for C++ constructors.Just use FXMVECTOR for the first three XMVECTOR values, then use CXMVECTOR for the rest.
//
//The FXMMATRIX and CXMMATRIX aliases help support taking advantage of the HVA argument passing with __vectorcall.
//
//Use the FXMMATRIX alias to pass the first XMMATRIX as an argument to the function.This assumes you don't have more than two FXMVECTOR arguments or more than two float, double, or FXMVECTOR arguments to the �right� of the matrix. For info about additional considerations, see the __vectorcall documentation.
//Use the CXMMATRIX alias otherwise.
//
//Because of limitations with __vectorcall, we recommend that you never use FXMMATRIX for C++ constructors.Just use CXMMATRIX.
//
//In addition to the type aliases, you must also use the XM_CALLCONV annotation to make sure the function uses the appropriate calling convention(__fastcall versus __vectorcall) based on your compiler and architecture.Because of limitations with __vectorcall, we recommend that you not use XM_CALLCONV for C++ constructors.

#ifndef XMMATH_H
#define XMMATH_H

//SSE 4.1 adds native DotProduct cpu functions
//http://blogs.msdn.com/b/chuckw/archive/2012/09/11/directxmath-sse4-1-and-sse-4-2.aspx

#ifndef _SSE4
#pragma warning (disable: 4838)
#include <DirectXMath.h>
#pragma warning (default: 4838)

#else

#pragma warning (disable: 4838)
#include <DirectXMathSSE4.h>
#pragma warning (default: 4838)

//remap to new SSE4 functions
namespace DirectX
{
	//-------------------------------------------------------------------------------------
	// Vector
	//-------------------------------------------------------------------------------------

#define XMVectorGetYPtr SSE4::XMVectorGetYPtr
#define XMVectorGetZPtr SSE4::XMVectorGetZPtr
#define XMVectorGetWPtr SSE4::XMVectorGetWPtr
#define XMVectorGetIntY SSE4::XMVectorGetIntY
#define XMVectorGetIntZ SSE4::XMVectorGetIntZ
#define XMVectorGetIntW SSE4::XMVectorGetIntW
#define XMVectorGetIntYPtr SSE4::XMVectorGetIntYPtr
#define XMVectorGetIntZPtr SSE4::XMVectorGetIntZPtr
#define XMVectorGetIntWPtr SSE4::XMVectorGetIntWPtr
#define XMVectorSetY SSE4::XMVectorSetY
#define XMVectorSetZ SSE4::XMVectorSetZ
#define XMVectorSetW SSE4::XMVectorSetW
#define XMVectorSetIntY SSE4::XMVectorSetIntY
#define XMVectorSetIntZ SSE4::XMVectorSetIntZ
#define XMVectorSetIntW SSE4::XMVectorSetIntW
#define XMVectorRound SSE4::XMVectorRound
#define XMVectorTruncate SSE4::XMVectorTruncate
#define XMVectorFloor SSE4::XMVectorFloor
#define XMVectorCeiling SSE4::XMVectorCeiling

	//-------------------------------------------------------------------------------------
	// Vector2
	//-------------------------------------------------------------------------------------

#define XMVector2Dot SSE4::XMVector2Dot
#define XMVector2LengthSq SSE4::XMVector2LengthSq
#define XMVector2ReciprocalLengthEst SSE4::XMVector2ReciprocalLengthEst
#define XMVector2ReciprocalLength SSE4::XMVector2ReciprocalLength
#define XMVector2LengthEst SSE4::XMVector2LengthEst
#define XMVector2Length SSE4::XMVector2Length
#define XMVector2NormalizeEst SSE4::XMVector2NormalizeEst
#define XMVector2Normalize SSE4::XMVector2Normalize


	//-------------------------------------------------------------------------------------
	// Vector3
	//-------------------------------------------------------------------------------------

#define XMVector3Dot SSE4::XMVector3Dot
#define XMVector3LengthSq SSE4::XMVector3LengthSq
#define XMVector3ReciprocalLengthEst SSE4::XMVector3ReciprocalLengthEst
#define XMVector3ReciprocalLength SSE4::XMVector3ReciprocalLength
#define XMVector3LengthEst SSE4::XMVector3LengthEst
#define XMVector3Length SSE4::XMVector3Length
#define XMVector3NormalizeEst SSE4::XMVector3NormalizeEst
#define XMVector3Normalize SSE4::XMVector3Normalize

	//-------------------------------------------------------------------------------------
	// Vector4
	//-------------------------------------------------------------------------------------

#define XMVector4Dot SSE4::XMVector4Dot
#define XMVector4LengthSq SSE4::XMVector4LengthSq
#define XMVector4ReciprocalLengthEst SSE4::XMVector4ReciprocalLengthEst
#define XMVector4ReciprocalLength SSE4::XMVector4ReciprocalLength
#define XMVector4LengthEst SSE4::XMVector4LengthEst
#define XMVector4Length SSE4::XMVector4Length
#define XMVector4NormalizeEst SSE4::XMVector4NormalizeEst
#define XMVector4Normalize SSE4::XMVector4Normalize

	//-------------------------------------------------------------------------------------
	// Plane
	//-------------------------------------------------------------------------------------

#define XMPlaneNormalizeEst SSE4::XMPlaneNormalizeEst
#define XMPlaneNormalize SSE4::XMPlaneNormalize

}; // namespace DirectX;

#endif // _SSE4

namespace helix
{
	namespace math
	{
		using namespace DirectX;

		//-------------------------------------------------------------------------------------
		// Defines
		//-------------------------------------------------------------------------------------

#define XMFLOAT2_ZERO XMFLOAT2(0,0)
#define XMFLOAT3_ZERO XMFLOAT3(0,0,0)
#define XMFLOAT4_ZERO XMFLOAT4(0,0,0,0)
#define XMFLOAT2A_ZERO XMFLOAT2A(0,0)
#define XMFLOAT3A_ZERO XMFLOAT3A(0,0,0)
#define XMFLOAT4A_ZERO XMFLOAT4A(0,0,0,0)
#define XMFLOAT3X3_ZERO XMFLOAT3X3(0,0,0,0,0,0,0,0,0)
#define XMFLOAT4X4_ZERO XMFLOAT4X4(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
#define XMMATRIX_ZERO XMMATRIX(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
#define XMVECTOR_ZERO XMVectorZero()

		//-------------------------------------------------------------------------------------
		// Functions
		//-------------------------------------------------------------------------------------
		// Vector 2
		inline XMFLOAT2 Scale(const float _fScale, const XMFLOAT2& _v0)
		{
			return XMFLOAT2(_fScale * _v0.x, _fScale * _v0.y);
		}

		inline XMFLOAT2 Add(const XMFLOAT2& _lhs, const XMFLOAT2& _rhs)
		{
			return XMFLOAT2(_lhs.x + _rhs.x, _lhs.y + _rhs.y);
		}

		inline XMFLOAT2 Sub(const XMFLOAT2& _lhs, const XMFLOAT2& _rhs)
		{
			return XMFLOAT2(_lhs.x - _rhs.x, _lhs.y - _rhs.y);
		}

		inline XMFLOAT2 Mul(const XMFLOAT2& _lhs, const XMFLOAT2& _rhs)
		{
			return XMFLOAT2(_lhs.x * _rhs.x, _lhs.y * _rhs.y);
		}

		inline XMFLOAT2 Div(const XMFLOAT2& _lhs, const XMFLOAT2& _rhs)
		{
			return XMFLOAT2(_lhs.x / _rhs.x, _lhs.y / _rhs.y);
		}

		// Vector 3
		inline XMFLOAT3 Scale(const float _fScale, const XMFLOAT3& _v0)
		{
			return XMFLOAT3(_fScale * _v0.x, _fScale * _v0.y, _fScale * _v0.z);
		}

		inline XMFLOAT3 Add(const XMFLOAT3& _lhs, const XMFLOAT3& _rhs)
		{
			return XMFLOAT3(_lhs.x + _rhs.x, _lhs.y + _rhs.y, _lhs.z + _rhs.z);
		}

		inline XMFLOAT3 Sub(const XMFLOAT3& _lhs, const XMFLOAT3& _rhs)
		{
			return XMFLOAT3(_lhs.x - _rhs.x, _lhs.y - _rhs.y, _lhs.z - _rhs.z);
		}

		inline XMFLOAT3 Mul(const XMFLOAT3& _lhs, const XMFLOAT3& _rhs)
		{
			return XMFLOAT3(_lhs.x * _rhs.x, _lhs.y * _rhs.y, _lhs.z * _rhs.z);
		}

		inline XMFLOAT3 Div(const XMFLOAT3& _lhs, const XMFLOAT3& _rhs)
		{
			return XMFLOAT3(_lhs.x / _rhs.x, _lhs.y / _rhs.y, _lhs.z / _rhs.z);
		}

		inline XMFLOAT3 XMFloat3Get(_In_ FXMVECTOR _vVector)
		{
			XMFLOAT3 vRet;
			XMStoreFloat3(&vRet, _vVector);
			return vRet;
		}

		inline XMFLOAT4 XMFloat4Get(_In_ FXMVECTOR _vVector)
		{
			XMFLOAT4 vRet;
			XMStoreFloat4(&vRet, _vVector);
			return vRet;
		}

		inline XMFLOAT3X3 XMFloat3x3Get(_In_ FXMMATRIX _mMatrix)
		{
			XMFLOAT3X3 mRet;
			XMStoreFloat3x3(&mRet, _mMatrix);
			return mRet;
		}

		inline XMFLOAT4X4 XMFloat4x4Get(_In_ FXMMATRIX _mMatrix)
		{
			XMFLOAT4X4 mRet;
			XMStoreFloat4x4(&mRet, _mMatrix);
			return mRet;
		}


		inline XMFLOAT3A XMFloat3AGet(_In_ FXMVECTOR _vVector)
		{
			XMFLOAT3A vRet;
			XMStoreFloat3A(&vRet, _vVector);
			return vRet;
		}

		inline XMFLOAT4A XMFloat4AGet(_In_ FXMVECTOR _vVector)
		{
			XMFLOAT4A vRet;
			XMStoreFloat4A(&vRet, _vVector);
			return vRet;
		}

		//compares all components of a XMVECTOR
		inline bool XM_CALLCONV XMVectorCompare(FXMVECTOR V1, FXMVECTOR V2)
		{
			uint32_t uRes;
			XMVectorEqualR(&uRes, V1, V2);
			return XMComparisonAllTrue(uRes);
		}

		//Computes Quaternion Q2*Q1
		inline XMFLOAT4 MulQuat(const XMFLOAT4& _Q1, const XMFLOAT4& _Q2)
		{
			return XMFloat4Get(XMQuaternionMultiply(XMLoadFloat4(&_Q1), XMLoadFloat4(&_Q2)));
		}

		inline XMVECTOR XM_CALLCONV XMVector3Rotate(FXMVECTOR _vPoint, FXMVECTOR _vQuaternion)
		{
			XMMATRIX R = XMMatrixRotationQuaternion(_vQuaternion);
			return XMVector3Transform(_vPoint, R);
		}

		inline XMVECTOR XM_CALLCONV XMVector3RotateNormal(FXMVECTOR _vNormal, FXMVECTOR _vQuaternion)
		{
			XMMATRIX R = XMMatrixRotationQuaternion(_vQuaternion);
			return XMVector3TransformNormal(_vNormal, R);
		}

		inline XMVECTOR XM_CALLCONV XMVector3Distance(FXMVECTOR V1, FXMVECTOR V2)
		{
			XMVECTOR ret = XMVectorSubtract(V1, V2);
			ret = XMVector3Length(ret);
			return ret;
		}

		inline XMVECTOR XM_CALLCONV XMVector3Distance(const XMFLOAT3& V1, const XMFLOAT3& V2)
		{
			return XMVector3Distance(XMLoadFloat3(&V1), XMLoadFloat3(&V2));
		}

		inline float XM_CALLCONV XMVector3DistanceF(FXMVECTOR V1, FXMVECTOR V2)
		{
			XMVECTOR ret = XMVector3Distance(V1, V2);
			return XMVectorGetX(ret);
		}

		inline float XMVector3DistanceF(const XMFLOAT3& V1, const XMFLOAT3& V2)
		{
			return XMVector3DistanceF(XMLoadFloat3(&V1), XMLoadFloat3(&V2));
		}

		inline float XM_CALLCONV XMVector3LengthF(const XMVECTOR& V1)
		{
			return XMVectorGetX(XMVector3Length(V1));
		}
		
		inline float XM_CALLCONV XMVector4LengthF(const XMVECTOR& V1)
		{
			return XMVectorGetX(XMVector4Length(V1));
		}

		inline float XMVector3LengthF(const XMFLOAT3& V1)
		{
			return XMVectorGetX(XMVector3Length(XMLoadFloat3(&V1)));
		}

		inline float XMVector4LengthF(const XMFLOAT4& V1)
		{
			return XMVectorGetX(XMVector4Length(XMLoadFloat4(&V1)));
		}

		inline float XM_CALLCONV XMVector3LengthEstF(const XMVECTOR& V1)
		{
			return XMVectorGetX(XMVector3LengthEst(V1));
		}

		inline float XM_CALLCONV XMVector4LengthEstF(const XMVECTOR& V1)
		{
			return XMVectorGetX(XMVector4LengthEst(V1));
		}

		inline float XMVector3LengthEstF(const XMFLOAT3& V1)
		{
			return XMVectorGetX(XMVector3LengthEst(XMLoadFloat3(&V1)));
		}

		inline float XMVector4LengthEstF(const XMFLOAT4& V1)
		{
			return XMVectorGetX(XMVector4LengthEst(XMLoadFloat4(&V1)));
		}

		inline const XMMATRIX XM_CALLCONV XMMatrixTensor(const XMFLOAT3& vec)
		{
			// vec * vec^T
			XMVECTOR Diagonal = XMLoadFloat3(&vec);
			XMVECTOR Triangle = XMVectorSwizzle(Diagonal, 1, 2, 0, 3);

			Triangle = XMVectorMultiply(Diagonal, Triangle);
			Diagonal = XMVectorMultiply(Diagonal, Diagonal);

			return XMMatrixSet(XMVectorGetX(Diagonal), XMVectorGetX(Triangle), XMVectorGetZ(Triangle), 0.f,
				XMVectorGetX(Triangle), XMVectorGetY(Diagonal), XMVectorGetY(Triangle), 0.f,
				XMVectorGetZ(Triangle), XMVectorGetY(Triangle), XMVectorGetZ(Diagonal), 0.f,
				0.f, 0.f, 0.f, 0.f);
		}
	}; // namespace math
}; // namespace helix

#endif