//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef K3TREE_H
#define K3TREE_H

#include "K3TreeNode.h"

namespace helix
{
	namespace datastructures
	{
		class K3Tree
		{
		public:
			K3Tree();
			~K3Tree();

			void Insert(TEntityVector::iterator _Begin, TEntityVector::iterator _End);

			void GatherCollisions(_In_ const TEntityVector& _Entities,_Out_ std::vector<collision::CollisionInfo>& _Collisions);
		private:
			K3TreeNode* m_pRootNode = nullptr;

		};
	}
}

#endif