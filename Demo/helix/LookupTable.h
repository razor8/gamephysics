//Copyright(c) 2015
//Authors: Fabian Wahlster, Steffen Wiewel, Moritz Becher
#ifndef LOOKUPTABLE_H
#define LOOKUPTABLE_H

#include <vector>
#include <map>
#include "Logger.h"

namespace helix
{
	namespace animation
	{
		// A uniform lookup contains uniform distributed key value pairs
		template<typename Key, typename Value>
		class UniformLookup
		{
			HDEBUGNAME("UniformLookup");
		public:
			UniformLookup(Value (*func) (Key), Key _From, Key _To, size_t _NumSamples)
				: m_From(_From), m_To(To), m_EntrySize(static_cast<double>(_NumSamples - 1) / (_To - _From))
			{
				for (size_t i = 0; i < _NumSamples; ++i)
				{
					Key num = (_To - _From) * i / (static_cast<double>(_NumSamples - 1));
					Value sample = func(num);
					m_Samples.push_back(sample);
				}
			}

			Value operator()(Key _Key) const
			{
				return At(_Key);
			}

			inline Value At(Key _Key) const
			{
				HASSERT(_Key <= m_To && _Key >= m_From, "Table subscript out of range");
				size_t Index = Round((_Key - m_From) * m_EntrySize);
				return m_Samples.at(Index);
			}

		private:

			static Key Round(Key _Key) { return floor(_Key + 0.5); }

			std::vector<Value> m_Samples;
			Key m_From;
			Key m_To;
			Key m_EntrySize;
		};

		// A clumped lookup contains NON uniform distributed key value pairs.
		template<typename Key, typename Value>
		class ClumpedLookup
		{
		public:
			HDEBUGNAME("ClumpedLookup");
			ClumpedLookup()
			{
			}

			void Insert(const Key _Key, const Value _Value)
			{
				//if (_Key < m_From)
				//{
				//	m_From = _Key;
				//}
				//if (_Key > m_To)
				//{
				//	m_To = _Key;
				//}

				m_Table.emplace(_Key, _Value);
			}

			///<returns>A interpolated value between the two nearest entries in the table</returns>
			Value At(const Key _Key) const
			{
				//HASSERT(_Key <= m_To && _Key >= m_From, "Table subscript out of range");
				HASSERT(!m_Table.empty(), "Table is empty");

				auto Upper = m_Table.lower_bound(_Key);
				auto Lower = Upper;
				if (Upper->first <= 0.0f)
				{
					return 0.0f;
				}
				--Lower;
				
				const Key T = (_Key - Lower->first) / (Upper->first - Lower->first);
				
				return Lower->second + T * (Upper->second - Lower->second);
			}

		private:
			static Key Round(Key _Key) { return floor(_Key + 0.5); }

			std::map<Key, Value> m_Table;
			//Key m_From;
			//Key m_To;
		};
	}
}

#endif