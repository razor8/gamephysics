//Copyright(c) 2014
//Authors: Fabian Wahlster

#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <vector>
#include <mutex>
#include <future>

namespace helix
{
	namespace async
	{
		class ThreadPool
		{
		public:

			ThreadPool(uint32_t _uMaxNumOfTasks = 8)
			{
				m_uMaxNumOfTasks = _uMaxNumOfTasks;
			}

			~ThreadPool()
			{
				WaitForAllTasks();
			}

			inline void SetMaxNumOfTasks(uint32_t _uMaxNumOfTasks)
			{
				std::lock_guard<std::mutex> lock(m_LockMutex);
				m_uMaxNumOfTasks = _uMaxNumOfTasks;
			}

			inline bool AddTask(std::future<void>&& _Task)
			{
				std::lock_guard<std::mutex> lock(m_LockMutex);
				if (m_Tasks.size() < m_uMaxNumOfTasks)
				{
					m_Tasks.push_back(std::move(_Task));
					return true;
				}
				else
				{
					return false;
				}
			}			

			inline void WaitForAllTasks()
			{
				std::lock_guard<std::mutex> lock(m_LockMutex);

				for (std::future<void>& Task : m_Tasks)
				{
					//Join task, this is a blocking function
					Task.get();
				}

				// clear tasks
				m_Tasks.resize(0);
			}

		private:
			std::vector<std::future<void> > m_Tasks;

			std::mutex m_LockMutex;

			uint32_t m_uMaxNumOfTasks;
		};
	}; // Namespace Async
	
}; // Namespace helix

#endif