//Copyright(c) 2015
//Authors: Fabian Wahlster, Steffen Wiewel, Moritz Becher

/////////////////////////////////////////////////////////////////////////////////////////////////
// AnimationController
//-----------------------------------------------------------------------------------------------
// Summary:
// Controls animation tracks for one single actor.
//-----------------------------------------------------------------------------------------------
// Future Work:
//  - Remove return of Track Pointers from track management methods
//  - Load a file with animation track data
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef ANIMATIONCONTROLLER_H
#define ANIMATIONCONTROLLER_H

#include "AnimationTrack.h"
#include "XMMath.h"
#include "UniqueID.h"
#include <unordered_map>

namespace helix
{
	namespace animation
	{
		class AnimationController
		{
			HDEBUGNAME("AnimationController");
		public:
			typedef uint64_t Vector3TrackHandle;
			typedef uint64_t ScalarTrackHandle;
			typedef uint64_t QuaternionTrackHandle;

			AnimationController();
			~AnimationController();
			
			void Initialize();

		// UPDATE
			void Update(float _fDeltaTime);
			void Pause();
			void Unpause();

		// TRACK MANAGEMENT
			Vector3TrackHandle AddVector3Track(std::vector<XMFLOAT3>& _vPostitions, float _fStartTime, float _fEndTime);
			//ScalarTrackHandle AddScalarTrack(std::vector<float>& _vValues, float _fStartTime, float _fEndTime);
			//QuaternionTrackHandle AddQuaternionTrack(std::vector<XMFLOAT4>& _vOrientations, float _fStartTime, float _fEndTime);
		
		// GETTERS
			XMFLOAT3 GetVector3(Vector3TrackHandle _Handle);
		protected:
			std::unordered_map<Vector3TrackHandle, AnimationTrack<XMFLOAT3>> m_Vector3AnimationTracks;
			//std::unordered_map<ScalarTrackHandle, AnimationTrack<float>> m_ScalarAnimationTracks;
			//std::unordered_map<QuaternionTrackHandle, AnimationTrack<XMFLOAT4>> m_QuaternionAnimationTracks;

			UniqueID<uint64_t> m_ID;

			bool m_bIsPaused;
			float m_fTime;
			//XMFLOAT3 m_vPosition;
			//XMFLOAT3 m_vLookAt;
		};

		inline void AnimationController::Pause() { m_bIsPaused = true; }
		inline void AnimationController::Unpause() { m_bIsPaused = false; }
		
		//inline const XMFLOAT3& AnimationController::GetPostion() const { return m_vPosition; }
		//inline const XMFLOAT3& AnimationController::GetLookAt() const { return m_vLookAt; }
	}
}

#endif