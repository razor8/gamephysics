//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "TplGameObject.h"
#include "XMCollision.h"
#include "OctreeNode.h"

using namespace helix;
using namespace helix::datastructures;

//---------------------------------------------------------------------------------------------------
//default GameObject constructor
TplGameObject::TplGameObject() : m_kType(ObjectType_GameObject)
{
}
//---------------------------------------------------------------------------------------------------
TplGameObject::TplGameObject(std::tuple<RigidBodyData>&  _TplReferences) :
	m_kType(ObjectType_RigidBody),
	m_pRigidBody(&std::get<RigidBodyData>(_TplReferences))
{
}
//---------------------------------------------------------------------------------------------------
// Destructor
TplGameObject::~TplGameObject()
{
}
//---------------------------------------------------------------------------------------------------
void TplGameObject::Initialize(const XMFLOAT3 & _vPosition, const XMFLOAT3 & _vScale, const XMFLOAT3 & _vEulerAngles, TplGameObjectFlag _kFlags)
{
	m_uID = m_UniqueID.NextID();

	SetFlag(_kFlags);

	m_vPosition = _vPosition;
	m_vScale = _vScale;

	XMFLOAT3 RotRadians = XMFLOAT3(XMConvertToRadians(_vEulerAngles.x), XMConvertToRadians(_vEulerAngles.y), XMConvertToRadians(_vEulerAngles.z));

	m_vOrientation = math::XMFloat4Get(XMQuaternionRotationRollPitchYaw(RotRadians.x, RotRadians.y, RotRadians.z));

	m_mRotation = math::XMFloat3x3Get(XMMatrixRotationRollPitchYaw(RotRadians.x, RotRadians.y, RotRadians.z));
}
//---------------------------------------------------------------------------------------------------
// InitializeRigidBody()  requires Initialize() being called first.
// Damping should be in [0,1]
void TplGameObject::InitializeRigidBody(float _fMass, float _fDamping, const XMFLOAT3 & _vLinearVelocity, const XMFLOAT3 & _vAngularVelocity, const XMFLOAT3X3 & _mInvInertiaTensor)
{
	HASSERTD(m_kType == ObjectType_RigidBody && nullptr != m_pRigidBody, "Game Object is not a Rigidbody");

	m_pRigidBody->m_fInvMass = 1.0f / _fMass;
	m_pRigidBody->m_fDamping = _fDamping;
	m_pRigidBody->m_vLinearVelocity = _vLinearVelocity;
	m_pRigidBody->m_vAngularVelocity = _vAngularVelocity;
	m_pRigidBody->m_mInvInertiaTensor = _mInvInertiaTensor;

	// Load current rotation and initial inertia tensor
	XMMATRIX Rot = XMLoadFloat3x3(&m_mRotation);
	XMMATRIX I = XMLoadFloat3x3(&m_pRigidBody->m_mInvInertiaTensor);

	// Rotate Inertia Tensor by initial rotation
	// I_0^-1 = R^T * I * R
	I = XMMatrixMultiply(XMMatrixTranspose(Rot), I);
	I = XMMatrixMultiply(I, Rot);

	// Compute angular momentum
	XMVECTOR w = XMLoadFloat3(&m_pRigidBody->m_vAngularVelocity);
	// L = I * w
	XMVECTOR L = XMVector3Transform(w, I);
	XMStoreFloat3(&m_pRigidBody->m_vAngularMomentum, L);
}
//---------------------------------------------------------------------------------------------------
void TplGameObject::InitializeAABB(const XMFLOAT3 & _vPositionOffset, const XMFLOAT3 & _vExtents)
{
	m_AABB = BoundingBox(math::Add(m_vPosition, _vPositionOffset), math::Mul(_vExtents, m_vScale));
}
//---------------------------------------------------------------------------------------------------
void TplGameObject::InitializeOBB(const XMFLOAT3 & _vPositionOffset, const XMFLOAT3 & _vExtents, const XMFLOAT4 & _vOrientation)
{
	m_InitialOBB = BoundingOrientedBox(_vPositionOffset, _vExtents, _vOrientation);
	m_OBB = BoundingOrientedBox(math::Add(m_vPosition, _vPositionOffset), math::Mul(_vExtents, m_vScale), math::MulQuat(_vOrientation, m_vOrientation));
}
//---------------------------------------------------------------------------------------------------
void TplGameObject::UpdateBoundingVolumes()
{
	//Update OBB
	m_OBB.Center = math::Add(m_vPosition, m_InitialOBB.Center);
	m_OBB.Extents = math::Mul(m_InitialOBB.Extents, m_vScale);

	//Rotate bounding box with current gameobject rotation
	m_OBB.Orientation = math::MulQuat(m_InitialOBB.Orientation, m_vOrientation);

	//update AABB based on OBBs current state
	collision::ConvertOBBToAABB(m_OBB, m_AABB);
}
//---------------------------------------------------------------------------------------------------
bool TplGameObject::Intersects(const TplGameObject& _GO) const
{
	return m_AABB.Intersects(_GO.GetAABB()) && collision::Intersects(m_OBB, _GO.GetOBB());
}
//---------------------------------------------------------------------------------------------------
bool TplGameObject::Intersects(_In_ const TplGameObject& _GO, _Out_ collision::CollisionInfo& _Info) const
{
	if (m_AABB.Intersects(_GO.GetAABB()))
	{
		XMFLOAT3 vMTV;
		float fPenetrationDepth;
		if (collision::Intersects(m_OBB, _GO.GetOBB(), vMTV, fPenetrationDepth))
		{
			_Info.m_bIsValid = true;
			//_Info.m_pGO1 = this;
			//_Info.m_pGO2 = &_GO;
			//TODO: compute contact points
			return true;
		}
	}

	return false;
}
//---------------------------------------------------------------------------------------------------
bool TplGameObject::Intersects(const BoundingSphere & _BoundingSphere) const
{
	return m_AABB.Intersects(_BoundingSphere) && m_OBB.Intersects(_BoundingSphere);
}
//---------------------------------------------------------------------------------------------------
bool TplGameObject::Intersects(const BoundingFrustum & _BoundingFrustum) const
{
	return _BoundingFrustum.Intersects(m_AABB) && _BoundingFrustum.Intersects(m_OBB);
}
//---------------------------------------------------------------------------------------------------
bool TplGameObject::Intersects(const BoundingBox & _BoundingBox) const
{
	return  m_AABB.Intersects(_BoundingBox) && collision::Intersects(m_OBB, _BoundingBox);
}
//---------------------------------------------------------------------------------------------------
bool TplGameObject::Contains(const TplGameObject & _GO) const
{
	return m_AABB.Contains(_GO.GetAABB()) == CONTAINS && collision::Contains(m_OBB, _GO.GetOBB()) == CONTAINS;
}
//---------------------------------------------------------------------------------------------------
bool TplGameObject::ContainedBy(const BoundingBox & _BoundingBox) const
{
	//if the AABB contains the GOs AABB it also contains the GOs OBB
	//but if it doesn't, it's still possible that the AABB contains the GOs OBB
	return _BoundingBox.Contains(m_AABB) || collision::Contains(m_OBB, _BoundingBox);
}
//---------------------------------------------------------------------------------------------------