//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

#include "Effect.h"
#include "Logger.h"

using namespace helix;

//---------------------------------------------------------------------------------------------------
std::wstring Effect::ShaderFolderPath = L"";
//---------------------------------------------------------------------------------------------------
Effect::Effect()
{
}
//---------------------------------------------------------------------------------------------------
Effect::~Effect()
{
}
//---------------------------------------------------------------------------------------------------
void Effect::SetShaderFolderPath(std::wstring _sShaderFolderPath)
{
	Effect::ShaderFolderPath = _sShaderFolderPath;
}
//---------------------------------------------------------------------------------------------------
HRESULT Effect::CompileShaderFromFile(std::wstring _sFileName, LPCSTR _sShaderTarget, ID3DBlob** _pShaderBlob)
{
	HRESULT hr;

	std::wstring sPathToFile = Effect::ShaderFolderPath + _sFileName;

	UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
	flags |= D3DCOMPILE_DEBUG;
#endif

	*_pShaderBlob = nullptr;
	ID3DBlob* shaderBlob = nullptr;
	ID3DBlob* errorBlob = nullptr;
	//D3D_COMPILE_STANDARD_FILE_INCLUDE
	HR(hr = D3DCompileFromFile(sPathToFile.c_str(), NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "main", _sShaderTarget, flags, 0, &shaderBlob, &errorBlob));

	if (FAILED(hr))
	{
		if (errorBlob)
		{
			OutputDebugStringA((char*)errorBlob->GetBufferPointer());
			errorBlob->Release();
		}
		if (shaderBlob)
		{
			shaderBlob->Release();
		}

		return hr;
	}

	*_pShaderBlob = shaderBlob;
	return hr;
}
//---------------------------------------------------------------------------------------------------
HRESULT CompileShaderFromMemory( LPCVOID _pSrcData, SIZE_T _uSrcDataSize, LPCSTR _sShaderTarget, ID3DBlob** _pShaderBlob)
{
	HRESULT hr;

	UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
	flags |= D3DCOMPILE_DEBUG;
#endif

	*_pShaderBlob = nullptr;
	ID3DBlob* shaderBlob = nullptr;
	ID3DBlob* errorBlob = nullptr;
	HR(hr = D3DCompile2(_pSrcData, _uSrcDataSize, NULL, NULL, NULL, "main", _sShaderTarget, flags, 0, 0, NULL, 0, &shaderBlob, &errorBlob));

	if (FAILED(hr))
	{
		if (errorBlob)
		{
			OutputDebugStringA((char*)errorBlob->GetBufferPointer());
			errorBlob->Release();
		}
		if (shaderBlob)
		{
			shaderBlob->Release();
		}

		return hr;
	}

	*_pShaderBlob = shaderBlob;
	return hr;
}