//Copyright(c) 2014
//Authors: Fabian Wahlster

#ifndef SCENE_H
#define SCENE_H

#include "AsyncObject.h"
#include "AsyncObserver.h"
#include "Debug.h"
#include "Camera.h"
#include "Timer.h"

#include "ThreadPool.h"
#include "TplGameObjectContainer.h"
#include "OctreeNode.h"
#include "RigidBodySystem.h"

//Currently this is only a concept of how a "Scene" could be defined.
//Each level should have its own scene (generated on load)

namespace helix
{
	namespace scene
	{
		//A scene also needs a definition of a "View" on the Scene, there for i'll start with the concept of a View:
		class View
		{			
			class ViewPort;
			class Technique; // shader / pass wrapper etc
		public:
			//Several Views should be able to share the same camera (and therefore same visible objects)
			View(Camera& const _Camera) : m_Camera(_Camera) {}
			~View() {}

			void Render()
			{
				//m_pViewPort->DoStuff();
				//std::vector<datastructures::TplGameObject*> VisibleObjecs = Camera.GetObjects();
				//for (auto pGO : VisibleObjecs)
				//{
				//	//render the object
				//	m_pTechnique->Render(*pGO);
				//}
			}
		private:

			Camera& const m_Camera; //const reference to non-const camera object
			//The camera should hold a vector of (pointers to) gameobjects currently (in this simulation step) visible in the Camera's frustum

			//Members needed for rendering:
			//this dont't actually have to be pointers, i just want this concept to compile =)
			ViewPort* m_pViewPort;
			Technique* m_pTechnique;
			//RenderTarget and more stuff...
		};


		//---------------------------------------------------------------------------------------------------
		//There should always only ONE scene in engine, a Scene encapsulates the current "level" with all its information

		class Scene : public async::IAsyncObserver, public async::IAsyncObject		
		{
		public:
			HDEBUGNAME("Scene");

			Scene();
			~Scene();

			Camera& AddNewCamera()
			{
				//Init with parameters or pass by reference and then copy into vector using push_back
				//Camera cam;
				//m_Cameras.push_back(cam);
				return *m_Cameras.rbegin();
			}

			//Should be called like this:
			//Camera& cam = AddNewCamera(...args)
			//AddNewView(cam, ...more_args);

			void AddNewView(Camera& _Camera)
			{
				m_Views.push_back(View(_Camera));
			}

			//set desired update interval (deltatime), this is a recommdation for the computation time spent on simulations
			void SetUpdateInterval(double _fUpdateInterval);
			//returns the actual computation time for the last simulation interval
			double GetDeltaTime();
			//if this is set to true the simulations will use the exact UpdateInterval set with the SetUpdateInterval, independant from the computation time.
			void SetFixedUpdateInterval(bool _bFixedUpdateInterval);
			//unlocks free computation interval, simulations will run as fast as possible
			void SetFreeUpdateInterval(bool _bFreeUpdateInterval);

		private:
			virtual void Execute();

			virtual void StopSubsystems();

			virtual void InitializeSubsystems();
		private:
			//datastructures
			datastructures::TplGameObjectContainer* const m_pGameObjects;
	
			//RootNode
			datastructures::OctreeNode* m_pOctree = nullptr;
			std::vector<collision::CollisionInfo> m_Collisions;

			async::ThreadPool* m_pThreadPool = nullptr;

			physics::RigidBodySystem m_RigidBodySystem;

			std::vector<Camera> m_Cameras;
			std::vector<View> m_Views;

			//locking and thread controll
			std::condition_variable m_CV;
			
			physics::TRigidBodies m_RigidBodyIterators;

			std::mutex m_WaitMutex;
			//std::mutex m_LockMutex;

			//simulation time
			Timer m_Timer;
			double m_fUpdateInterval = 0.02;
			double m_fDeltaTime;
			bool m_bFreeUpdateInterval = false;
			bool m_bFixedUpdateInterval = false;
			uint64_t m_uNumOfSimulations = 0;
			uint64_t m_uNumOfResumes = 0;
		};
	};
}; // helix

#endif // SCENE_H