#ifndef CURVE_H
#define CURVE_H

namespace helix
{
	namespace animation
	{
		template<class T>
		class Curve
		{
		public:
			virtual void VAdd(const T& _Point, const T& _ControllIn, const T& _ControllOut);
			virtual void VPointFromArcLenght(_Out_ T& _Point, _In_ float _fArcLength);
		};
	}
}


#endif
