//Copyright(c) 2014
//Authors: Fabian Wahlster

#include "Scene.h"

using namespace helix::datastructures;
using namespace helix::scene;

Scene::Scene() :
	//no observer, looped execution, no suspend after execution
	async::IAsyncObject(nullptr, true, false),
	m_pGameObjects(TplGameObjectContainer::Instance())
{
	//load scene info somehow
	uint32_t uGOCount = 100;
	uint32_t uRBCount = 1000;

	//The TplGameObjectContainer is a Singleton, this means all Scenes share the same gameobjects, BUT there should be only active scene in the engine.
	m_pGameObjects->Resize(uGOCount, uRBCount);

	//TODO: unquote this as soon as the ThreadPool is working again!
	//m_pThreadPool = new async::ThreadPool(8);

	//Init / build octree
	XMFLOAT3 vOctreePosition = XMFLOAT3_ZERO;
	float fOctreeMaxRadius = 100.f; //half edge length of root cube 
	int32_t iOctreeDepth = 3;
	float fOctreeLooseness = 1.1f;
	bool bOctreePreallocate = true;

	m_pOctree = new OctreeNode(nullptr, m_pThreadPool, vOctreePosition, fOctreeMaxRadius,fOctreeLooseness,  iOctreeDepth, bOctreePreallocate);
}
//---------------------------------------------------------------------------------------------------
Scene::~Scene()
{
	HSAFE_DELETE(m_pOctree);
}
//---------------------------------------------------------------------------------------------------
void Scene::Execute()
{
	m_Timer.Reset();

	m_Collisions.resize(0);

	//initialize data here:
	double fDeltaTime = m_fUpdateInterval;
	double fTotalDeltaTime = 0.0;

	int64_t iResumeThreadThreshold = 0;

	//Lock gameobjectcontainer to ensure iterators
	m_pGameObjects->Lock();

	//Set iterators for this simulation step
	m_RigidBodyIterators.begin = m_pGameObjects->RigidBodiesBegin();
	m_RigidBodyIterators.end = m_pGameObjects->RigidBodiesEnd();

	//>>>>>>>>>>>>>>>>>>>ASYNCHRON<<<<<<<<<<<<<<<<<<<<<<<

	//update systems here:
	{
		//Theres only one system atm
		SetBarrier(1);

		//ATTENTION this system works directly on the m_GameObjects!
		//BVs are updated inside this system
		{	//start RigidBodySystems
			m_RigidBodySystem.SetDeltaTime(static_cast<float>(fDeltaTime));
			m_RigidBodySystem.SetData(m_RigidBodyIterators);
			m_RigidBodySystem.Continue();
		}

		//Thread barrier: wait for all systems to finish -> till they call SignalObserver
		WaitForNotifications();
	}

	//>>>>>>>>>>>>>>>>>>>SYNCHRONIZED<<<<<<<<<<<<<<<<<<<<<<<

	//update octree

	//update RigidBodies:
	for (RigidBodyIterator itr = m_RigidBodyIterators.begin; itr != m_RigidBodyIterators.end; ++itr)
	{
		m_pOctree->Update(&(*itr));
	}

	//update Static Gameobjects:
	//TODO: implement if needed

	//this function needs to be called once all gameobjects have been updated and before gathering collisions
	m_pOctree->GatherTotalNumOfEntities();
		
	//gather collisions
	m_pOctree->GatherCollisions(m_Collisions);

	std::vector<datastructures::TplGameObject*> ObjectsInVolume; // This should be a member of the Camera!
	BoundingFrustum frustum;
	m_pOctree->GatherInVolume<BoundingFrustum>(frustum, ObjectsInVolume);

	//>>>>>>>>>>>>>>>>>>>ASYNCHRON<<<<<<<<<<<<<<<<<<<<<<<

	//Evaluate collisions

	//HFOREACH_PARALLEL(m_Collisions, [&], const collision::CollisionInfo&, Info)

	//	collision::CollisionEvaluation::ComputeCollision(Info.m_pGO1, Info.m_pGO2, Info);

	//HFOREACH_PARALLEL_END

	//>>>>>>>>>>>>>>>>>>>SYNCHRONIZED<<<<<<<<<<<<<<<<<<<<<<<

	//this is new:

	for(Camera& cam : m_Cameras) //use parallel for each if possible
	{
		//cam.update(fDeltatime);
		//cam.SetVisibleObjects(m_Octree.GatherInVolumes(cam.GetFrustum()));
	}

	//all tasks are done, measure time it took
	fDeltaTime = m_Timer.TotalTimeD();

	//calculate remaining time for UpdateInterval to be complete
	int64_t iTimeToSleepInMicroSec = static_cast<int64_t>((m_fUpdateInterval - fDeltaTime) * 1000000.0);

	//sleep for remaining time
	if (m_bFreeUpdateInterval == false && iTimeToSleepInMicroSec > iResumeThreadThreshold)
	{
		std::unique_lock<std::mutex> wait(m_WaitMutex);
		m_CV.wait_for(wait, std::chrono::microseconds(iTimeToSleepInMicroSec));

		//update the delta time
		fDeltaTime = m_Timer.TotalTimeD();

		++m_uNumOfResumes;
	}

	fTotalDeltaTime += fDeltaTime;
	m_fDeltaTime = fDeltaTime;
	++m_uNumOfSimulations;

	//rendering;
	for (View& view : m_Views)
	{
		view.Render();
	}

	//unlock gameobjectcontainer and remove destroyed gameobjects:
	m_pGameObjects->Unlock();

	//CallBack:
	//OnCreateGameObjects...
}
//---------------------------------------------------------------------------------------------------
void Scene::StopSubsystems()
{
	// stop all subsystems
	m_RigidBodySystem.Stop();
}
//---------------------------------------------------------------------------------------------------
void Scene::InitializeSubsystems()
{
	m_RigidBodySystem.SetObserver(this);
	//m_RigidBodySystem.SetDefaultForce(XMFLOAT3(0, -9.81f, 0));
	m_RigidBodySystem.StartInNewThread();
}
//---------------------------------------------------------------------------------------------------
void Scene::SetUpdateInterval(double _fUpdateInterval)
{
	//std::lock_guard<std::mutex> lock(m_LockMutex);
	m_fUpdateInterval = _fUpdateInterval;
}
//---------------------------------------------------------------------------------------------------
double Scene::GetDeltaTime()
{
	if (IsPaused())
	{
		return 0.0;
	}
	else
	{
		//std::lock_guard<std::mutex> lock(m_LockMutex);
		return m_fDeltaTime;
	}
}
//---------------------------------------------------------------------------------------------------
void Scene::SetFixedUpdateInterval(bool _bFixedUpdateInterval)
{
	//std::lock_guard<std::mutex> lock(m_LockMutex);
	m_bFixedUpdateInterval = _bFixedUpdateInterval;
}
//---------------------------------------------------------------------------------------------------
void Scene::SetFreeUpdateInterval(bool _bFreeUpdateInterval)
{
	//std::lock_guard<std::mutex> lock(m_LockMutex);
	m_bFreeUpdateInterval = _bFreeUpdateInterval;
}