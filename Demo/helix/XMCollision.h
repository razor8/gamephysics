//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel, Moritz Becher

#ifndef XMCOLLISION_H
#define XMCOLLISION_H

//This Header needs to be included BEFORE any DirectX header to overwrite math functions!
#include "XMMath.h"

#pragma warning (disable: 4838 4458)
#include <DirectXCollision.h>
#pragma warning (default: 4838 4458)

#include "Logger.h"

namespace helix
{
	namespace collision
	{
		using namespace DirectX;

		//-----------------------------------------------------------------------------
		// Fast oriented box / oriented box intersection test using the separating axis 
		// theorem.
		//-----------------------------------------------------------------------------
		inline bool Intersects(_In_ const BoundingOrientedBox& box1, _In_ const BoundingOrientedBox& box2)
		{
			// Build the 3x3 rotation matrix that defines the orientation of B relative to A.
			XMVECTOR A_quat = XMLoadFloat4(&box1.Orientation);
			XMVECTOR B_quat = XMLoadFloat4(&box2.Orientation);

			HASSERTD(DirectX::Internal::XMQuaternionIsUnit(A_quat), "Quaternion does not have unit length!");
			HASSERTD(DirectX::Internal::XMQuaternionIsUnit(B_quat), "Quaternion does not have unit length!");

			XMVECTOR Q = XMQuaternionMultiply(A_quat, XMQuaternionConjugate(B_quat));
			XMMATRIX R = XMMatrixRotationQuaternion(Q);

			// Compute the translation of B relative to A.
			XMVECTOR A_cent = XMLoadFloat3(&box1.Center);
			XMVECTOR B_cent = XMLoadFloat3(&box2.Center);
			XMVECTOR t = XMVector3InverseRotate(B_cent - A_cent, A_quat);

			//
			// h(A) = extents of A.
			// h(B) = extents of B.
			//
			// a(u) = axes of A = (1,0,0), (0,1,0), (0,0,1)
			// b(u) = axes of B relative to A = (r00,r10,r20), (r01,r11,r21), (r02,r12,r22)
			//  
			// For each possible separating axis l:
			//   d(A) = sum (for i = u,v,w) h(A)(i) * abs( a(i) dot l )
			//   d(B) = sum (for i = u,v,w) h(B)(i) * abs( b(i) dot l )
			//   if abs( t dot l ) > d(A) + d(B) then disjoint
			//

			// Load extents of A and B.
			XMVECTOR h_A = XMLoadFloat3(&box1.Extents);
			XMVECTOR h_B = XMLoadFloat3(&box2.Extents);

			// Rows. Note R[0,1,2]X.w = 0.
			// normalize test axis, to get correct MTVs
			XMVECTOR R0X = XMVector3NormalizeEst(R.r[0]);
			XMVECTOR R1X = XMVector3NormalizeEst(R.r[1]);
			XMVECTOR R2X = XMVector3NormalizeEst(R.r[2]);

			R = XMMatrixTranspose(R);

			// Columns. Note RX[0,1,2].w = 0.
			XMVECTOR RX0 = XMVector3NormalizeEst(R.r[0]);
			XMVECTOR RX1 = XMVector3NormalizeEst(R.r[1]);
			XMVECTOR RX2 = XMVector3NormalizeEst(R.r[2]);

			// Absolute value of rows.
			XMVECTOR AR0X = XMVectorAbs(R0X);
			XMVECTOR AR1X = XMVectorAbs(R1X);
			XMVECTOR AR2X = XMVectorAbs(R2X);

			// Absolute value of columns.
			XMVECTOR ARX0 = XMVectorAbs(RX0);
			XMVECTOR ARX1 = XMVectorAbs(RX1);
			XMVECTOR ARX2 = XMVectorAbs(RX2);

			XMVECTOR ALLTRUE = XMVectorTrueInt();

			// Test each of the 15 possible seperating axii.
			XMVECTOR d, d_A, d_B;

			// l = a(u) = (1, 0, 0)
			// t dot l = t.x
			// d(A) = h(A).x
			// d(B) = h(B) dot abs(r00, r01, r02)
			d = XMVectorSplatX(t);
			d_A = XMVectorSplatX(h_A);
			d_B = XMVector3Dot(h_B, AR0X);

			XMVECTOR vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			float fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			// l = a(v) = (0, 1, 0)
			// t dot l = t.y
			// d(A) = h(A).y
			// d(B) = h(B) dot abs(r10, r11, r12)
			d = XMVectorSplatY(t);
			d_A = XMVectorSplatY(h_A);
			d_B = XMVector3Dot(h_B, AR1X);
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			// l = a(w) = (0, 0, 1)
			// t dot l = t.z
			// d(A) = h(A).z
			// d(B) = h(B) dot abs(r20, r21, r22)
			d = XMVectorSplatZ(t);
			d_A = XMVectorSplatZ(h_A);
			d_B = XMVector3Dot(h_B, AR2X);
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			// l = b(u) = (r00, r10, r20)
			// d(A) = h(A) dot abs(r00, r10, r20)
			// d(B) = h(B).x
			d = XMVector3Dot(t, RX0);
			d_A = XMVector3Dot(h_A, ARX0);
			d_B = XMVectorSplatX(h_B);
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			// l = b(v) = (r01, r11, r21)
			// d(A) = h(A) dot abs(r01, r11, r21)
			// d(B) = h(B).y
			d = XMVector3Dot(t, RX1);
			d_A = XMVector3Dot(h_A, ARX1);
			d_B = XMVectorSplatY(h_B);
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			// l = b(w) = (r02, r12, r22)
			// d(A) = h(A) dot abs(r02, r12, r22)
			// d(B) = h(B).z
			d = XMVector3Dot(t, RX2);
			d_A = XMVector3Dot(h_A, ARX2);
			d_B = XMVectorSplatZ(h_B);
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			// l = a(u) x b(u) = (0, -r20, r10)
			// d(A) = h(A) dot abs(0, r20, r10)
			// d(B) = h(B) dot abs(0, r02, r01)
			// normalize test axis, to get correct MTVs, d should only depend on t and projection (not length of test axis)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_0W, XM_PERMUTE_1Z, XM_PERMUTE_0Y, XM_PERMUTE_0X>(RX0, -RX0)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(ARX0));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(AR0X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			// l = a(u) x b(v) = (0, -r21, r11)
			// d(A) = h(A) dot abs(0, r21, r11)
			// d(B) = h(B) dot abs(r02, 0, r00)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_0W, XM_PERMUTE_1Z, XM_PERMUTE_0Y, XM_PERMUTE_0X>(RX1, -RX1)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(ARX1));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(AR0X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			// l = a(u) x b(w) = (0, -r22, r12)
			// d(A) = h(A) dot abs(0, r22, r12)
			// d(B) = h(B) dot abs(r01, r00, 0)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_0W, XM_PERMUTE_1Z, XM_PERMUTE_0Y, XM_PERMUTE_0X>(RX2, -RX2)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(ARX2));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(AR0X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			// l = a(v) x b(u) = (r20, 0, -r00)
			// d(A) = h(A) dot abs(r20, 0, r00)
			// d(B) = h(B) dot abs(0, r12, r11)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_0Z, XM_PERMUTE_0W, XM_PERMUTE_1X, XM_PERMUTE_0Y>(RX0, -RX0)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(ARX0));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(AR1X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			// l = a(v) x b(v) = (r21, 0, -r01)
			// d(A) = h(A) dot abs(r21, 0, r01)
			// d(B) = h(B) dot abs(r12, 0, r10)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_0Z, XM_PERMUTE_0W, XM_PERMUTE_1X, XM_PERMUTE_0Y>(RX1, -RX1)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(ARX1));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(AR1X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			// l = a(v) x b(w) = (r22, 0, -r02)
			// d(A) = h(A) dot abs(r22, 0, r02)
			// d(B) = h(B) dot abs(r11, r10, 0)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_0Z, XM_PERMUTE_0W, XM_PERMUTE_1X, XM_PERMUTE_0Y>(RX2, -RX2)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(ARX2));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(AR1X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			// l = a(w) x b(u) = (-r10, r00, 0)
			// d(A) = h(A) dot abs(r10, r00, 0)
			// d(B) = h(B) dot abs(0, r22, r21)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_1Y, XM_PERMUTE_0X, XM_PERMUTE_0W, XM_PERMUTE_0Z>(RX0, -RX0)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(ARX0));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(AR2X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			// l = a(w) x b(v) = (-r11, r01, 0)
			// d(A) = h(A) dot abs(r11, r01, 0)
			// d(B) = h(B) dot abs(r22, 0, r20)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_1Y, XM_PERMUTE_0X, XM_PERMUTE_0W, XM_PERMUTE_0Z>(RX1, -RX1)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(ARX1));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(AR2X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			// l = a(w) x b(w) = (-r12, r02, 0)
			// d(A) = h(A) dot abs(r12, r02, 0)
			// d(B) = h(B) dot abs(r21, r20, 0)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_1Y, XM_PERMUTE_0X, XM_PERMUTE_0W, XM_PERMUTE_0Z>(RX2, -RX2)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(ARX2));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(AR2X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			return true;
		}

		//-----------------------------------------------------------------------------
		// Fast oriented box / oriented box intersection test using the separating axis 
		// theorem. Returns the minimal penetration depth along a face normal if
		// MINIMAL_TRANSLATION_VECTOR is NOT defined.
		//-----------------------------------------------------------------------------
		inline bool Intersects(_In_ const BoundingOrientedBox& box1, _In_ const BoundingOrientedBox& box2, _Out_ XMFLOAT3& _vMTV, _Out_ float& _fPenetrationDepth)
		{
			// Build the 3x3 rotation matrix that defines the orientation of B relative to A.
			XMVECTOR A_quat = XMLoadFloat4(&box1.Orientation);
			XMVECTOR B_quat = XMLoadFloat4(&box2.Orientation);

			HASSERTD(DirectX::Internal::XMQuaternionIsUnit(A_quat), "Quaternion does not have unit length!");
			HASSERTD(DirectX::Internal::XMQuaternionIsUnit(B_quat), "Quaternion does not have unit length!");

			XMVECTOR Q = XMQuaternionMultiply(A_quat, XMQuaternionConjugate(B_quat));
			XMMATRIX R = XMMatrixRotationQuaternion(Q);

			// Compute the translation of B relative to A.
			XMVECTOR A_cent = XMLoadFloat3(&box1.Center);
			XMVECTOR B_cent = XMLoadFloat3(&box2.Center);
			XMVECTOR t = XMVector3InverseRotate(B_cent - A_cent, A_quat);

			//
			// h(A) = extents of A.
			// h(B) = extents of B.
			//
			// a(u) = axes of A = (1,0,0), (0,1,0), (0,0,1)
			// b(u) = axes of B relative to A = (r00,r10,r20), (r01,r11,r21), (r02,r12,r22)
			//  
			// For each possible separating axis l:
			//   d(A) = sum (for i = u,v,w) h(A)(i) * abs( a(i) dot l )
			//   d(B) = sum (for i = u,v,w) h(B)(i) * abs( b(i) dot l )
			//   if abs( t dot l ) > d(A) + d(B) then disjoint
			//

			// Load extents of A and B.
			XMVECTOR h_A = XMLoadFloat3(&box1.Extents);
			XMVECTOR h_B = XMLoadFloat3(&box2.Extents);

			// Rows. Note R[0,1,2]X.w = 0.
			// normalize test axis, to get correct MTVs
			XMVECTOR R0X = XMVector3NormalizeEst(R.r[0]);
			XMVECTOR R1X = XMVector3NormalizeEst(R.r[1]);
			XMVECTOR R2X = XMVector3NormalizeEst(R.r[2]);

			R = XMMatrixTranspose(R);

			// Columns. Note RX[0,1,2].w = 0.
			XMVECTOR RX0 = XMVector3NormalizeEst(R.r[0]);
			XMVECTOR RX1 = XMVector3NormalizeEst(R.r[1]);
			XMVECTOR RX2 = XMVector3NormalizeEst(R.r[2]);

			// Absolute value of rows.
			XMVECTOR AR0X = XMVectorAbs(R0X);
			XMVECTOR AR1X = XMVectorAbs(R1X);
			XMVECTOR AR2X = XMVectorAbs(R2X);

			// Absolute value of columns.
			XMVECTOR ARX0 = XMVectorAbs(RX0);
			XMVECTOR ARX1 = XMVectorAbs(RX1);
			XMVECTOR ARX2 = XMVectorAbs(RX2);

			XMVECTOR ALLTRUE = XMVectorTrueInt();

			// Test each of the 15 possible seperating axii.
			XMVECTOR d, d_A, d_B;

			// l = a(u) = (1, 0, 0)
			// t dot l = t.x
			// d(A) = h(A).x
			// d(B) = h(B) dot abs(r00, r01, r02)
			d = XMVectorSplatX(t);
			d_A = XMVectorSplatX(h_A);
			d_B = XMVector3Dot(h_B, AR0X);

			XMVECTOR vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			float fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}

			// MTV variables
			float fMinimalOverlap = fOverlap;
			XMVECTOR vMinimumTranslationVector = vOverlapAxis;

			// l = a(v) = (0, 1, 0)
			// t dot l = t.y
			// d(A) = h(A).y
			// d(B) = h(B) dot abs(r10, r11, r12)
			d = XMVectorSplatY(t);
			d_A = XMVectorSplatY(h_A);
			d_B = XMVector3Dot(h_B, AR1X);
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimumTranslationVector = vOverlapAxis;
			}

			// l = a(w) = (0, 0, 1)
			// t dot l = t.z
			// d(A) = h(A).z
			// d(B) = h(B) dot abs(r20, r21, r22)
			d = XMVectorSplatZ(t);
			d_A = XMVectorSplatZ(h_A);
			d_B = XMVector3Dot(h_B, AR2X);
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimumTranslationVector = vOverlapAxis;
			}

			// l = b(u) = (r00, r10, r20)
			// d(A) = h(A) dot abs(r00, r10, r20)
			// d(B) = h(B).x
			d = XMVector3Dot(t, RX0);
			d_A = XMVector3Dot(h_A, ARX0);
			d_B = XMVectorSplatX(h_B);
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimumTranslationVector = vOverlapAxis;
			}

			// l = b(v) = (r01, r11, r21)
			// d(A) = h(A) dot abs(r01, r11, r21)
			// d(B) = h(B).y
			d = XMVector3Dot(t, RX1);
			d_A = XMVector3Dot(h_A, ARX1);
			d_B = XMVectorSplatY(h_B);
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimumTranslationVector = vOverlapAxis;
			}

			// l = b(w) = (r02, r12, r22)
			// d(A) = h(A) dot abs(r02, r12, r22)
			// d(B) = h(B).z
			d = XMVector3Dot(t, RX2);
			d_A = XMVector3Dot(h_A, ARX2);
			d_B = XMVectorSplatZ(h_B);
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimumTranslationVector = vOverlapAxis;
			}

			// l = a(u) x b(u) = (0, -r20, r10)
			// d(A) = h(A) dot abs(0, r20, r10)
			// d(B) = h(B) dot abs(0, r02, r01)
			// normalize test axis, to get correct MTVs, d should only depend on t and projection (not length of test axis)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_0W, XM_PERMUTE_1Z, XM_PERMUTE_0Y, XM_PERMUTE_0X>(RX0, -RX0)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(ARX0));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(AR0X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}
#ifdef MINIMAL_TRANSLATION_VECTOR
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimumTranslationVector = vOverlapAxis;
			}
#endif

			// l = a(u) x b(v) = (0, -r21, r11)
			// d(A) = h(A) dot abs(0, r21, r11)
			// d(B) = h(B) dot abs(r02, 0, r00)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_0W, XM_PERMUTE_1Z, XM_PERMUTE_0Y, XM_PERMUTE_0X>(RX1, -RX1)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(ARX1));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(AR0X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}
#ifdef MINIMAL_TRANSLATION_VECTOR
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimumTranslationVector = vOverlapAxis;
			}
#endif

			// l = a(u) x b(w) = (0, -r22, r12)
			// d(A) = h(A) dot abs(0, r22, r12)
			// d(B) = h(B) dot abs(r01, r00, 0)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_0W, XM_PERMUTE_1Z, XM_PERMUTE_0Y, XM_PERMUTE_0X>(RX2, -RX2)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(ARX2));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(AR0X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}
#ifdef MINIMAL_TRANSLATION_VECTOR
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimumTranslationVector = vOverlapAxis;
			}
#endif

			// l = a(v) x b(u) = (r20, 0, -r00)
			// d(A) = h(A) dot abs(r20, 0, r00)
			// d(B) = h(B) dot abs(0, r12, r11)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_0Z, XM_PERMUTE_0W, XM_PERMUTE_1X, XM_PERMUTE_0Y>(RX0, -RX0)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(ARX0));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(AR1X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}
#ifdef MINIMAL_TRANSLATION_VECTOR
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimumTranslationVector = vOverlapAxis;
			}
#endif

			// l = a(v) x b(v) = (r21, 0, -r01)
			// d(A) = h(A) dot abs(r21, 0, r01)
			// d(B) = h(B) dot abs(r12, 0, r10)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_0Z, XM_PERMUTE_0W, XM_PERMUTE_1X, XM_PERMUTE_0Y>(RX1, -RX1)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(ARX1));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(AR1X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}
#ifdef MINIMAL_TRANSLATION_VECTOR
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimumTranslationVector = vOverlapAxis;
			}
#endif

			// l = a(v) x b(w) = (r22, 0, -r02)
			// d(A) = h(A) dot abs(r22, 0, r02)
			// d(B) = h(B) dot abs(r11, r10, 0)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_0Z, XM_PERMUTE_0W, XM_PERMUTE_1X, XM_PERMUTE_0Y>(RX2, -RX2)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(ARX2));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(AR1X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}
#ifdef MINIMAL_TRANSLATION_VECTOR
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimumTranslationVector = vOverlapAxis;
			}
#endif

			// l = a(w) x b(u) = (-r10, r00, 0)
			// d(A) = h(A) dot abs(r10, r00, 0)
			// d(B) = h(B) dot abs(0, r22, r21)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_1Y, XM_PERMUTE_0X, XM_PERMUTE_0W, XM_PERMUTE_0Z>(RX0, -RX0)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(ARX0));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_W, XM_SWIZZLE_Z, XM_SWIZZLE_Y, XM_SWIZZLE_X>(AR2X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}
#ifdef MINIMAL_TRANSLATION_VECTOR
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimumTranslationVector = vOverlapAxis;
			}
#endif

			// l = a(w) x b(v) = (-r11, r01, 0)
			// d(A) = h(A) dot abs(r11, r01, 0)
			// d(B) = h(B) dot abs(r22, 0, r20)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_1Y, XM_PERMUTE_0X, XM_PERMUTE_0W, XM_PERMUTE_0Z>(RX1, -RX1)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(ARX1));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Z, XM_SWIZZLE_W, XM_SWIZZLE_X, XM_SWIZZLE_Y>(AR2X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}
#ifdef MINIMAL_TRANSLATION_VECTOR
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimumTranslationVector = vOverlapAxis;
			}
#endif

			// l = a(w) x b(w) = (-r12, r02, 0)
			// d(A) = h(A) dot abs(r12, r02, 0)
			// d(B) = h(B) dot abs(r21, r20, 0)
			d = XMVector3Dot(t, XMVector3NormalizeEst(XMVectorPermute<XM_PERMUTE_1Y, XM_PERMUTE_0X, XM_PERMUTE_0W, XM_PERMUTE_0Z>(RX2, -RX2)));
			d_A = XMVector3Dot(h_A, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(ARX2));
			d_B = XMVector3Dot(h_B, XMVectorSwizzle<XM_SWIZZLE_Y, XM_SWIZZLE_X, XM_SWIZZLE_W, XM_SWIZZLE_Z>(AR2X));
			vOverlapAxis = XMVectorSubtract(XMVectorAdd(d_A, d_B), XMVectorAbs(d));
			fOverlap = math::XMVector3LengthF(vOverlapAxis);
			if (fOverlap < 0.f)
			{
				return false;
			}
#ifdef MINIMAL_TRANSLATION_VECTOR
			else if (fOverlap < fMinimalOverlap)
			{
				fMinimalOverlap = fOverlap;
				vMinimumTranslationVector = vOverlapAxis;
			}
#endif

			// No seperating axis found, boxes must intersect.
			XMStoreFloat3(&_vMTV, vMinimumTranslationVector);
			_fPenetrationDepth = fMinimalOverlap;

			return true;
		}

		//-----------------------------------------------------------------------------
		// BoundingVolume intersection pass-through functions:
		//-----------------------------------------------------------------------------


		// OBB & AABB
		_Use_decl_annotations_
		inline bool Intersects(_In_ const BoundingOrientedBox& box1, _In_ const BoundingBox& box2)
		{
			// Make the axis aligned box oriented and do an OBB vs OBB test.
			BoundingOrientedBox obox(box2.Center, box2.Extents, XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			return Intersects(box1, obox);
		}

		// AABB & OBB 
		_Use_decl_annotations_
			inline bool Intersects(_In_ const BoundingBox& box1, _In_ const BoundingOrientedBox& box2)
		{
			// Make the axis aligned box oriented and do an OBB vs OBB test.
			BoundingOrientedBox obox(box1.Center, box1.Extents, XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			return Intersects(obox, box2);
		}

		// AABB & Sphere
		_Use_decl_annotations_
			inline bool Intersects(_In_ const BoundingBox& box, _In_ const BoundingSphere& sphere)
		{
			return box.Intersects(sphere);
		}

		// Sphere & AABB
		_Use_decl_annotations_
			inline bool Intersects(_In_ const BoundingSphere& sphere, _In_ const BoundingBox& box)
		{
			return box.Intersects(sphere);
		}


		// AABB & Frustum 
		// DXMath does not have a function for AABB & frustum, so just convert AABB to OBB, TODO: check if there is a better way.
		_Use_decl_annotations_
			inline bool Intersects(_In_ const BoundingBox& box, _In_ const BoundingFrustum& frustum)
		{
			// Make the axis aligned box oriented and do an OBB vs frustum test.
			BoundingOrientedBox obox(box.Center, box.Extents, XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			return frustum.Intersects(obox);
		}

		// Frustum & AABB
		_Use_decl_annotations_
			inline bool Intersects(_In_ const BoundingFrustum& frustum, _In_ const BoundingBox& box)
		{
			// Make the axis aligned box oriented and do an OBB vs frustum test.
			BoundingOrientedBox obox(box.Center, box.Extents, XMFLOAT4(0.f, 0.f, 0.f, 1.f));
			return frustum.Intersects(obox);
		}

		// OBB & Frustum
		_Use_decl_annotations_
			inline bool Intersects(_In_ const BoundingOrientedBox& box, _In_ const BoundingFrustum& frustum)
		{
			return frustum.Intersects(box);
		}

		//Frustum & OBB
		_Use_decl_annotations_
			inline bool Intersects(_In_ const BoundingFrustum& frustum, _In_ const BoundingOrientedBox& box)
		{
			return frustum.Intersects(box);
		}

		//-----------------------------------------------------------------------------
		// Oriented box in axis-aligned box test
		//-----------------------------------------------------------------------------
		_Use_decl_annotations_
		inline ContainmentType Contains(const BoundingOrientedBox& box1, const BoundingBox& box2)
		{
			if (Intersects(box1, box2) == false)
				return DISJOINT;

			XMVECTOR vCenter = XMLoadFloat3(&box2.Center);
			XMVECTOR vExtents = XMLoadFloat3(&box2.Extents);

			// Subtract off the AABB center to remove a subtract below
			XMVECTOR oCenter = XMLoadFloat3(&box1.Center) - vCenter;

			XMVECTOR oExtents = XMLoadFloat3(&box1.Extents);
			XMVECTOR oOrientation = XMLoadFloat4(&box1.Orientation);

			HASSERTD(DirectX::Internal::XMQuaternionIsUnit(oOrientation), "Quaternion does not have unit length!");

			XMVECTOR Inside = XMVectorTrueInt();

			for (size_t i = 0; i < BoundingOrientedBox::CORNER_COUNT; ++i)
			{
				XMVECTOR C = XMVector3Rotate(oExtents * g_BoxOffset[i], oOrientation) + oCenter;
				XMVECTOR d = XMVector3LengthSq(C);
				Inside = XMVectorAndInt(Inside, XMVectorLessOrEqual(d, vExtents));
			}

			return (XMVector3EqualInt(Inside, XMVectorTrueInt())) ? CONTAINS : INTERSECTS;
		}

		//-----------------------------------------------------------------------------
		// Oriented bounding box in oriented bounding box
		//-----------------------------------------------------------------------------
		_Use_decl_annotations_
		inline ContainmentType Contains(const BoundingOrientedBox& box1, const BoundingOrientedBox& box2)
		{
			if (Intersects(box1, box2) == false)
				return DISJOINT;

			// Load the boxes
			XMVECTOR aCenter = XMLoadFloat3(&box1.Center);
			XMVECTOR aExtents = XMLoadFloat3(&box1.Extents);
			XMVECTOR aOrientation = XMLoadFloat4(&box1.Orientation);

			HASSERTD(DirectX::Internal::XMQuaternionIsUnit(aOrientation), "Quaternion does not have unit length!");

			XMVECTOR bCenter = XMLoadFloat3(&box2.Center);
			XMVECTOR bExtents = XMLoadFloat3(&box2.Extents);
			XMVECTOR bOrientation = XMLoadFloat4(&box2.Orientation);

			HASSERTD(DirectX::Internal::XMQuaternionIsUnit(bOrientation), "Quaternion does not have unit length!");

			XMVECTOR offset = bCenter - aCenter;

			for (size_t i = 0; i < 8; ++i)
			{
				// Cb = rotate( bExtents * corneroffset[i], bOrientation ) + bcenter
				// Ca = invrotate( Cb - aCenter, aOrientation )

				XMVECTOR C = XMVector3Rotate(bExtents * g_BoxOffset[i], bOrientation) + offset;
				C = XMVector3InverseRotate(C, aOrientation);

				if (XMVector3InBounds(C, aExtents) == false)
					return INTERSECTS;
			}

			return CONTAINS;
		}

		inline void ConvertOBBToAABB(_In_ const BoundingOrientedBox& _OBB, _Out_ BoundingBox& _OutAABB)
		{
			XMFLOAT3 pPoints[8];
			_OBB.GetCorners(pPoints);

			// Find the minimum and maximum x, y, and z
			XMVECTOR vMin, vMax, vPoint;

			vMin = vMax = XMLoadFloat3(pPoints);

			for (uint32_t i = 1; i < 8; ++i)
			{
				vPoint = XMLoadFloat3(&pPoints[i]);

				vMin = XMVectorMin(vMin, vPoint);
				vMax = XMVectorMax(vMax, vPoint);
			}

			// Store center and extents.
			//XMStoreFloat3(&_Out.Center, (vMin + vMax) * 0.5f);
			_OutAABB.Center = _OBB.Center;
			XMStoreFloat3(&_OutAABB.Extents, (vMax - vMin) * 0.5f);
		}

	}; // collision
}; // helix


#endif // XMCOLLISION_H