//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "MassSpringSystem.h"
#include "Logger.h"
#include "StdAfxH.h"
#include <algorithm>
#include "XMIntegrators.h"

using namespace helix::math;
using namespace helix::async;
using namespace helix::physics;

//---------------------------------------------------------------------------------------------------
//default constructor
MassSpringSystem::MassSpringSystem()
{
	m_RunSimulation.store(true);
	//Start in a suspended state
	m_PauseSimulation.store(true);
}
MassSpringSystem::MassSpringSystem(IAsyncObserver* _Observer) : IAsyncPhysicsObject(_Observer)
{
	m_RunSimulation.store(true);
	//Start in a suspended state
	m_PauseSimulation.store(true);
}
//---------------------------------------------------------------------------------------------------
//deconstructor
MassSpringSystem::~MassSpringSystem()
{
	//stop execution
	m_RunSimulation.store(false);
	m_PauseSimulation.store(false);
	m_CV.notify_all();

	//when the current simulation step is done, the observer will be notified, so no need to call SignalObserver here
	//just wait for the thread to finish

	//std::unique_lock<std::mutex> lock(m_LockMutex);
	//std::unique_lock<std::mutex> wait(m_WaitMutex);
}
//---------------------------------------------------------------------------------------------------
//main work loop; has to be implemented by implemented (derived from IAsyncObject)
void MassSpringSystem::Execute()
{
	//compute initial length of springs
	if (m_Data.m_Springs.empty() == false)
	{
		std::lock_guard<std::mutex> lock(m_LockMutex);
		InitSprings(m_Data.m_Points,m_Data.m_Springs.begin(), m_Data.m_Springs.end());
	}

	do{

		//suspend simulation
		while (m_PauseSimulation.load())
		{
			std::unique_lock<std::mutex> wait(m_WaitMutex);
			m_CV.wait(wait);
			if (m_RunSimulation.load() == false)
			{
				SignalObserver();
				return;
			}
		}

		//lock the data while simulating
		{
			std::lock_guard<std::mutex> lock(m_LockMutex);
			//update simulation	
			Simulate(static_cast<float>(m_fDeltaTime));

			//add new input data for the next simulation
			if (m_NewInputData != nullptr)
			{
				std::vector<MSSpring>::iterator it;

				uint32_t uPointOffset = static_cast<uint32_t>(m_Data.m_Points.size());

				m_Data.m_Points.insert(m_Data.m_Points.end(), m_NewInputData->m_Points.begin(), m_NewInputData->m_Points.end());
				it = m_Data.m_Springs.insert(m_Data.m_Springs.end(), m_NewInputData->m_Springs.begin(), m_NewInputData->m_Springs.end());

				InitSprings(m_Data.m_Points, it, m_Data.m_Springs.end(), uPointOffset);

				m_NewInputData = nullptr;
			}

			//update external forces
			if (m_NewInputForces.empty() == false)
			{
				m_Forces = std::move(m_NewInputForces);
			}

			m_PauseSimulation.store(true);

			//notify the physics manager that work is done here
			SignalObserver();
		}		

	} while (m_RunSimulation.load());

}
//---------------------------------------------------------------------------------------------------
//run simulation step here
void MassSpringSystem::Simulate(float _fDeltaTime)
{
	//add external forces here
	{
		if (m_Forces.empty())
		{
			HFOREACH_CONST(it, end, m_Data.m_Points)
				if (it->m_bStatic) continue;

				//f = m*a
				XMVECTOR vForce = XMLoadFloat3(&m_DefaultForce);
				vForce = XMVectorScale(vForce, it->m_fMass);
				XMStoreFloat3(&it->m_vCurForce, vForce);
			HFOREACH_CONST_END
		}
		else
		{
			uint32_t length = static_cast<uint32_t>(m_Data.m_Points.size());
			for (uint32_t i = 0; i < length; i++)
			{
				MSPoint& Point = m_Data.m_Points.at(i);

				if (Point.m_bStatic) continue;

				XMVECTOR vForce = XMLoadFloat3(&m_DefaultForce); // f = m*a
				vForce = XMVectorScale(vForce, Point.m_fMass);				

				//lookup external force
				std::unordered_map<uint32_t, XMFLOAT3>::iterator it = m_Forces.find(i);
				if (it != m_Forces.end())
				{
					vForce = XMVectorAdd(vForce, XMLoadFloat3(&it->second));
					Point.m_vVelocity = XMFLOAT3(0, 0, 0);
					//Point.m_bIdle = false;
				}

				XMStoreFloat3(&Point.m_vCurForce, vForce);
			}
			//remove applied forces
			m_Forces.clear();
		}
	}

	//// Compute internal force

	HFOREACH_CONST(it,end,m_Data.m_Springs)
		MSPoint* pPt1 = &m_Data.m_Points[it->m_pPoint1];
		MSPoint* pPt2 = &m_Data.m_Points[it->m_pPoint2];

		//if both points are static, the force can't change
		if (pPt1->m_bStatic && pPt2->m_bStatic) continue;

		XMVECTOR p1 = XMLoadFloat3(&pPt1->m_vCurPosition);
		XMVECTOR p2 = XMLoadFloat3(&pPt2->m_vCurPosition);
		//p1 = x0 - x1
		p1 = XMVectorSubtract(p1, p2);
		XMVECTOR l_i = XMVector3Length(p1);

		//consider springs tearing apart
		float f_li = XMVectorGetX(l_i);

		if (f_li < it->m_fInitialLength * m_fMinLengthConstraintFactor)
		{
			f_li = it->m_fInitialLength * m_fMinLengthConstraintFactor;
		}

		if (f_li > it->m_fInitialLength * m_fMaxLengthConstraintFactor)
		{
			f_li = it->m_fInitialLength * m_fMaxLengthConstraintFactor;
			if (it->m_bCanTear) it->m_bRemove = true;
		}

		l_i = XMVectorReplicate(f_li);

		XMVECTOR k_i = XMVectorReplicate(it->m_fStiffness);

		XMVECTOR L_i = XMVectorReplicate(it->m_fInitialLength);

		//l_i - L_i
		L_i = XMVectorSubtract(l_i, L_i);

		//k_i * (l_i - L_i)
		L_i = XMVectorMultiply(L_i, k_i);

		//(x0 - x1) / l_i
		p1 = XMVectorDivide(p1, l_i);

		//(k_i * (l_i - L_i)) * ((x0 - x1) / l_i)
		L_i = XMVectorMultiply(L_i, p1);

		// ADD to end points
		// F_i += L_i  (=Force)
		p2 = XMLoadFloat3(&pPt1->m_vCurForce);
		p2 = XMVectorSubtract(p2, L_i);
		XMStoreFloat3(&pPt1->m_vCurForce, p2);

		// F_j -= L_i
		p2 = XMLoadFloat3(&pPt2->m_vCurForce);
		p2 = XMVectorAdd(p2, L_i);
		XMStoreFloat3(&pPt2->m_vCurForce, p2);
	HFOREACH_CONST_END

	//just to test that this is actually working with parallel stl
	auto remove = std::experimental::parallel::remove_if(std::experimental::parallel::par,
		std::begin(m_Data.m_Springs),
		std::end(m_Data.m_Springs),
		[](const MSSpring& spring){ return spring.m_bRemove; });

	//remove broken springs
	m_Data.m_Springs.erase(remove,std::end(m_Data.m_Springs));

	HFOREACH_CONST(it, end, m_Data.m_Points)
		if (it->m_bStatic) continue;

		//integrate velocity
		XMVECTOR v0 = XMLoadFloat3(&it->m_vVelocity);
		XMVECTOR x0 = XMLoadFloat3(&it->m_vCurPosition);
		XMVECTOR a0 = XMLoadFloat3(&it->m_vCurForce) / it->m_fMass;

		switch (m_Integrator)
		{
		case math::XMIntegrator_Euler:
			math::XMVectorEuler(x0, v0, a0, it->m_fMass, it->m_fDamping, _fDeltaTime);
			break;
		case math::XMIntegrator_Heun:
			math::XMVectorHeun(x0, v0, a0, it->m_fMass, it->m_fDamping, _fDeltaTime);
			break;
		case math::XMIntegrator_RK2:
			math::XMVectorRungeKutta2(x0, v0, a0, it->m_fMass, it->m_fDamping, _fDeltaTime);
			break;
		case math::XMIntegrator_RK3:
			math::XMVectorRungeKutta3(x0, v0, a0, it->m_fMass, it->m_fDamping, _fDeltaTime);
			break;
		case math::XMIntegrator_RK4:
		default:
			math::XMVectorRungeKutta4(x0, v0, a0, it->m_fMass, it->m_fDamping, _fDeltaTime);
		}

		XMStoreFloat3(&it->m_vVelocity, v0);
		XMStoreFloat3(&it->m_vCurPosition, x0);

		//if (it->m_vCurPosition.y < 0.f) it->m_vCurPosition.y = 0.0f;

		//float fDist = math::XMVector3DistanceF(it->m_vPrevPosition, it->m_vCurPosition);
		//it->m_bIdle = fDist < m_fIdleThreshold;
	HFOREACH_CONST_END
}
//---------------------------------------------------------------------------------------------------
void MassSpringSystem::Stop()
{
	m_RunSimulation.store(false);
	m_PauseSimulation.store(false);
	m_CV.notify_one();
}
//---------------------------------------------------------------------------------------------------
void MassSpringSystem::Pause()
{
	m_PauseSimulation.store(true);
	m_CV.notify_one();
}
//---------------------------------------------------------------------------------------------------
void MassSpringSystem::Continue()
{
	m_PauseSimulation.store(false);
	m_CV.notify_one();
}
//---------------------------------------------------------------------------------------------------
void MassSpringSystem::InitSprings(const std::vector<MassSpringSystem::MSPoint>& _Points, std::vector<MassSpringSystem::MSSpring>::iterator it, std::vector<MassSpringSystem::MSSpring>::iterator end, uint32_t _uPointOffset)
{
	for (; it != end; ++it)
	{
		it->m_pPoint1 += _uPointOffset;
		it->m_pPoint2 += _uPointOffset;

		it->m_fInitialLength = math::XMVector3DistanceF(_Points.at(it->m_pPoint1).m_vCurPosition, _Points.at(it->m_pPoint2).m_vCurPosition);
	}
}
//---------------------------------------------------------------------------------------------------
void MassSpringSystem::GetCurrentData(MSData& _Data)
{
	if (m_PauseSimulation.load() == false)
	{
		std::lock_guard<std::mutex> lock(m_LockMutex);
		_Data = m_Data;
	}
}
//---------------------------------------------------------------------------------------------------
void MassSpringSystem::ClearData()
{
	std::lock_guard<std::mutex> lock(m_LockMutex);
	m_Data.m_Points.clear();
	m_Data.m_Springs.clear();
}
//---------------------------------------------------------------------------------------------------
void MassSpringSystem::AddData(const MSData* _Data)
{
	std::lock_guard<std::mutex> lock(m_LockMutex);
	if (m_NewInputData != _Data) m_NewInputData = _Data;
}
//---------------------------------------------------------------------------------------------------
void MassSpringSystem::AddExternalForces(TForceMap _Forces)
{
	std::lock_guard<std::mutex> lock(m_LockMutex);
	m_NewInputForces = std::move(_Forces);
}
//---------------------------------------------------------------------------------------------------
void MassSpringSystem::ClearForces()
{
	std::lock_guard<std::mutex> lock(m_LockMutex);
	m_Forces.clear();
}
//---------------------------------------------------------------------------------------------------
void MassSpringSystem::SetDefaultForce(const XMFLOAT3& _DefaultForce)
{
	std::lock_guard<std::mutex> lock(m_LockMutex);
	m_DefaultForce = _DefaultForce;
}
//---------------------------------------------------------------------------------------------------
void MassSpringSystem::SetIntegrator(XMIntegrator _Integrator)
{
	std::lock_guard<std::mutex> lock(m_LockMutex);
	m_Integrator = _Integrator;
}
//---------------------------------------------------------------------------------------------------
XMIntegrator MassSpringSystem::GetIntegrator(){
	std::lock_guard<std::mutex> lock(m_LockMutex);
	return m_Integrator;
}