//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef MASSSPRINGSYSTEM_H
#define MASSSPRINGSYSTEM_H

#include "AsyncObject.h"
#include "XMMath.h"
#include "XMIntegrators.h"
#include "Timer.h"
#include <vector>
#include <atomic>
#include <unordered_map>

using namespace DirectX;

namespace helix
{
	namespace physics
	{
		//All time values are in Seconds
		class MassSpringSystem : public async::IAsyncObject
		{
		public:

			typedef std::unordered_map<uint32_t, XMFLOAT3> TForceMap;

			struct MSPoint
			{
				XMFLOAT3 m_vCurPosition;
				//XMFLOAT3 m_vPrevPosition;
				XMFLOAT3 m_vVelocity;
				XMFLOAT3 m_vCurForce;
				//XMFLOAT3 m_vPrevForce;

				float m_fMass;
				float m_fDamping;
				bool m_bStatic;
				//bool m_bIdle;

				MSPoint()
				{
					//m_vPrevForce = XMFLOAT3(0, 0, 0);
					//m_vPrevPosition = XMFLOAT3(0, 0, 0);
					m_fMass = 1.f;
					m_vVelocity = XMFLOAT3(0.f, 0.f, 0.f);
					m_fDamping = -0.5f;
					m_bStatic = false;
					//m_bIdle = false;
				}

				MSPoint(XMFLOAT3 _vPosition, float _fMass = 1.f, bool _bStatic = false, float _fDamping = 0.5f)
				{
					//m_vPrevForce = XMFLOAT3(0, 0, 0);
					//m_vPrevPosition = XMFLOAT3(0, 0, 0);
					m_vCurPosition = _vPosition;
					m_fMass = _fMass;
					m_vVelocity = XMFLOAT3(0.f, 0.f, 0.f);
					m_fDamping = -_fDamping;
					m_bStatic = _bStatic;
					//m_bIdle = false;
				}
			};

			struct MSSpring
			{
				uint32_t m_pPoint1;
				uint32_t m_pPoint2;
				float m_fStiffness;
				float m_fInitialLength;
				bool m_bCanTear;
				bool m_bRemove;

				MSSpring(uint32_t _pPoint1, uint32_t _pPoint2, float _fStiffness = 1.f, bool _bCanTear = false)
				{
					m_pPoint1 = _pPoint1;
					m_pPoint2 = _pPoint2;
					m_fStiffness = _fStiffness;
					m_bCanTear = _bCanTear;
					m_bRemove = false;
				}
			};

			struct MSData
			{
				std::vector<MSPoint> m_Points;
				std::vector<MSSpring> m_Springs;
			};

		public:
			HDEBUGNAME("MassSpringSystem");

			//don't allow copying
			MassSpringSystem(const MassSpringSystem& mss) = delete;

			MassSpringSystem();
			MassSpringSystem(async::IAsyncObserver* _Observer);

			~MassSpringSystem();

			//from IAsyncPhysicsObject
			virtual void Stop();
			virtual void Pause();
			virtual void Continue();

			static void InitSprings(const std::vector<MSPoint>& _Points, std::vector<MSSpring>::iterator begin, std::vector<MSSpring>::iterator end, uint32_t _uPointOffset = 0);

			//writes updated position vectors to _vPositions
			void GetCurrentData(MSData& _Data);
			void ClearData();
			void AddData(const MSData* _Data);

			//Addes forces to the system
			void AddExternalForces(TForceMap _Forces);
			void ClearForces();

			//this is actually the default acceleration => f = m*a will be done internal
			void SetDefaultForce(const XMFLOAT3& _DefaultForce);

			void SetIntegrator(math::XMIntegrator _Integrator);
			math::XMIntegrator GetIntegrator();

		private:
			//from IAsyncObject
			virtual void Execute();
			void Simulate(float _fDeltaTime);
		private:

			math::XMIntegrator m_Integrator = math::XMIntegrator_RK4;
			MSData m_Data;
			TForceMap m_Forces;

			const MSData* m_NewInputData = nullptr;
			//point index , force vector
			TForceMap m_NewInputForces;

			float m_fMinLengthConstraintFactor = 0.f;
			float m_fMaxLengthConstraintFactor = FLT_MAX;

			std::atomic<bool> m_RunSimulation;
			std::atomic<bool> m_PauseSimulation;

			XMFLOAT3 m_DefaultForce;

			std::mutex m_WaitMutex;
			std::condition_variable m_CV;
		};
	}; // namespace physics
	
}

#endif