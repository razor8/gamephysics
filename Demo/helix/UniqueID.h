//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

#ifndef UNIQUEID_H
#define UNIQUEID_H

//usage:
//global application context:
//uint32_t gid = UniqueID<uint32_t>::NextGlobalID();

//local class context:
//UniqueID<uint64_t> uID;
//uint64_t id = uID.NextID();

#include <mutex>
namespace helix{

	template <class T>
	class UniqueID
	{
	private:
		T ID = T(0);
		static T GlobalID;
	public:
		T NextID()
		{
			return ID++;
		}
		static T NextGlobalID()
		{
			//T ret;
			//{
			//	std::lock_guard<std::mutex> Lock(Mutex);
			//	ret = ID++;
			//}
			//return ret;
			static std::mutex Mutex;
			std::unique_lock<std::mutex> Lock(Mutex);
			return GlobalID++;
		}
	};

template<class T> T UniqueID<T>::GlobalID = T(0);

}

#endif // UNIQUEID_H