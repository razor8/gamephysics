//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef RIGIDBODYSYSTEM_H
#define RIGIDBODYSYSTEM_H

#include "PhysicsSystem.h"
#include "TplGameObject.h"
#include "XMMath.h"
#include "XMIntegrators.h"

#include <vector>
#include <atomic>
#include <unordered_map>

using namespace DirectX;

namespace helix
{
	namespace physics
	{

		struct ForcePoint
		{
			uint32_t m_uVertexID;
			XMFLOAT3 m_vForce;
		};

		struct TRigidBodies
		{
			datastructures::RigidBodyIterator begin;
			datastructures::RigidBodyIterator end;
		};

		typedef std::unordered_multimap<uint32_t, ForcePoint> TRigidBodyForceMap;

		//All time values are in Seconds
		class RigidBodySystem : public PhysicsSystem<TRigidBodies, TRigidBodyForceMap>
		{
		public:
			HDEBUGNAME("RigidBodySystem");

			//don't allow copying
			RigidBodySystem(const RigidBodySystem& rbs) = delete;

			RigidBodySystem();
			RigidBodySystem(async::IAsyncObserver* _Observer);

			~RigidBodySystem();

			//from PhysicsSystem
			virtual void ClearData();
			virtual void ClearExternalData();

			//this is actually the default acceleration => f = m*a will be done internal
			void SetDefaultForce(const XMFLOAT3& _DefaultForce);

			void SetIntegrator(math::XMIntegrator _Integrator);

		private:
			//from PhysicsSystem
			virtual void Execute();

			virtual void StopSubsystems();

			virtual void InitializeSubsystems();
		private:
			math::XMIntegrator m_Integrator;
			XMFLOAT3 m_DefaultForce;

		};
	}; // namespace physics

}

#endif //RIGIDBODYSYSTEM_H