//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef K3TREENODE_H
#define K3TREENODE_H

#include "Debug.h"
#include "GameObject.h"
#include "CollisionContact.h"
//#include "AsyncObject.h"
#include "StdAfxH.h"
#include <mutex>
#include <vector>

#include <algorithm>

namespace helix
{
	namespace datastructures
	{
		using namespace DirectX;

		enum SeparatingAxis
		{
			SeparatingAxis_X = 0,
			SeparatingAxis_Y = 1,
			SeparatingAxis_Z = 2
		};

		typedef std::vector<GameObject*> TEntityVector;

		class K3TreeNode /*: public IAsyncObject*/
		{
		public:
			HDEBUGNAME("K3Node");

			K3TreeNode(TEntityVector::iterator _Begin, TEntityVector::iterator _End, uint32_t _uDepth);
			~K3TreeNode();

			void GatherCollisions(_In_ GameObject* _pEntity, _Out_ std::vector<collision::CollisionInfo>& _Collisions);

		private:
			SeparatingAxis m_Axis;

			GameObject* m_pEntity = nullptr;

			K3TreeNode* m_pLeft = nullptr;
			K3TreeNode* m_pRight = nullptr;
		};
	}
}


#endif // K3TREENODE_H