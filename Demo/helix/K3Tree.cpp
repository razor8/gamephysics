//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "K3Tree.h"
#include "StdAfxH.h"

using namespace helix::datastructures;
using namespace helix::collision;

K3Tree::K3Tree()
{

}

//---------------------------------------------------------------------------------------------------

K3Tree::~K3Tree()
{
	HSAFE_DELETE(m_pRootNode);
}

//---------------------------------------------------------------------------------------------------

void K3Tree::Insert(TEntityVector::iterator _Begin, TEntityVector::iterator _End)
{
	HSAFE_DELETE(m_pRootNode);

	m_pRootNode = new K3TreeNode(_Begin, _End, 0);
}

//---------------------------------------------------------------------------------------------------

void K3Tree::GatherCollisions(_In_ const TEntityVector& _Entities, _Out_ std::vector<CollisionInfo>& _Collisions)
{
	if (m_pRootNode == nullptr)
		return;

	HFOREACH_CONST(it, end, _Entities)	
		m_pRootNode->GatherCollisions(*it, _Collisions);
	HFOREACH_CONST_END
}