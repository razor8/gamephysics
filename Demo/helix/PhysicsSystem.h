//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef PHYSICSSYSTEM_H
#define PHYSICSSYSTEM_H

#include "AsyncObject.h"

namespace helix
{
	namespace physics
	{

		//All time values are in Seconds
		template <class MAINDATA, class EXTERNALDATA>
		class PhysicsSystem : public async::IAsyncObject
		{
		public:
		
			//don't allow copying
			PhysicsSystem(const PhysicsSystem& rbs) = delete;

			PhysicsSystem(async::IAsyncObserver* _Observer = nullptr)
				: async::IAsyncObject(_Observer, true, true)
			{
			}

			~PhysicsSystem()
			{
			}

			//extract results of the last simulation step
			inline void GetCurrentData(_Out_ MAINDATA& _OutputData)
			{
				std::lock_guard<std::mutex> lock(m_LockMutex);
				_OutputData = m_Data;
			}

			//set input data for the next simulation step
			inline void SetData(_In_ const MAINDATA& _InputData)
			{
				std::lock_guard<std::mutex> lock(m_LockMutex);
				m_Data = _InputData;
			}

			//add external data to the system for the next simulation step
			inline void AddExternalData(_In_ const EXTERNALDATA& _ExternalData)
			{
				std::lock_guard<std::mutex> lock(m_LockMutex);
				m_ExternalData = _ExternalData;
			}

			virtual void ClearExternalData() = 0;
			virtual void ClearData() = 0;

			inline void SetDeltaTime(float _fDeltaTime) { std::lock_guard<std::mutex> lock(m_LockMutex); m_fDeltaTime = _fDeltaTime; }
			inline float GetDeltaTime() const { return m_fDeltaTime; }

		protected:
			MAINDATA m_Data;
			EXTERNALDATA m_ExternalData;

			std::mutex m_LockMutex;

			//delta time (in seconds) for the current simulation step
			float m_fDeltaTime = 0.02f;
		};	

	}; // namespace physics

}

#endif //PHYSICSSYSTEM_H