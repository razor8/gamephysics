#include "MassSpringGenerator.h"

using namespace helix::physics;

#define POS(x,y,z,w) ((x) + (w) * ((y) + (w)*(z)))
#define VALID(x,y,z,w) ((x) < (w) && (x) > -1 && (y) < (w) && (y) > -1 && (z) < (w) && (z) > -1)
//---------------------------------------------------------------------------------------------------
//default constructor
MassSpringGenerator::MassSpringGenerator()
{
}
//---------------------------------------------------------------------------------------------------
//deconstructor
MassSpringGenerator::~MassSpringGenerator()
{
}
//---------------------------------------------------------------------------------------------------
//construct a MS cube
MassSpringSystem::MSData MassSpringGenerator::Cube(XMFLOAT3 _vPosition, float _fEdgelength, int32_t _uPointsPerEdge, MassSpringSystem::MSPoint _SamplePoint, MassSpringSystem::MSSpring _SampleSpring)
{
	MassSpringSystem::MSData Data;

	int32_t w = _uPointsPerEdge;

	float fSubEdgeLength = _fEdgelength / static_cast<float>(_uPointsPerEdge);

	Data.m_Points.resize(_uPointsPerEdge*_uPointsPerEdge*_uPointsPerEdge);

	for (int x = 0; x < _uPointsPerEdge; ++x)
	{
		for (int y = 0; y < _uPointsPerEdge; ++y)
		{
			for (int z = 0; z < _uPointsPerEdge; ++z)
			{
				XMFLOAT3 vPos = _vPosition;
				vPos.x += x*fSubEdgeLength;
				vPos.y += y*fSubEdgeLength;
				vPos.z += z*fSubEdgeLength;

				_SamplePoint.m_vCurPosition = vPos;
				int32_t uIDX = POS(x, y, z, _uPointsPerEdge);
				Data.m_Points[uIDX] = _SamplePoint;

				_SampleSpring.m_pPoint1 = uIDX;

				for (int32_t ox = -1; ox < 2; ++ox)
				{
					for (int32_t oy = -1; oy < 2; ++oy)
					{
						for (int32_t oz = -1; oz < 2; ++oz)
						{
							if (ox == 0 && oy == 0 && oz == 0) continue;
							if (VALID(x + ox, y + oy, z + oz, w))
							{
								_SampleSpring.m_pPoint2 = POS(x + ox, y + oy, z + oz, w);
								Data.m_Springs.push_back(_SampleSpring);
							}
						}
					}
				}

			}
		}
	}

	return Data;
}