//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

#ifndef FLAG_H
#define FLAG_H

#include <cstdint>
#include <mutex>

namespace helix
{
	//use this macro to define your flags, it forces the compiler to use a 32bit int for enum storage:
#define Flag32E(Name) enum Name : uint32_t
#define Flag64E(Name) enum Name : uint64_t

	template <typename T>
	class Flag
	{
	public:
		Flag(std::recursive_mutex *_pMutex = nullptr)
		{
			m_Flag = 0;
			m_pMutex = _pMutex;
		}

		Flag(const Flag& _Flag, std::recursive_mutex* _pMutex = nullptr)
		{
			m_Flag = _Flag.m_Flag;
			m_pMutex = _pMutex;
		}

		Flag(T _Flag, std::recursive_mutex* _pMutex = nullptr)
		{
			m_Flag = _Flag;
			m_pMutex = _pMutex;
		}

		inline void SetMutex(std::recursive_mutex* _pMutex)
		{
			//try to remove all locks
			if (m_pMutex != nullptr)
			{
				m_pMutex->lock();
				m_pMutex->unlock();
			}

			m_pMutex = _pMutex;
		}

		inline T GetFlag() const
		{
			if (m_pMutex != nullptr)
			{
				std::lock_guard<std::recursive_mutex> Lock(*m_pMutex);
				return m_Flag;
			}
			else
			{
				return m_Flag;
			}
		}

		inline bool CheckFlag(T _Flag) const
		{
			if (m_pMutex != nullptr)
			{
				std::lock_guard<std::recursive_mutex> Lock(*m_pMutex);
				return (m_Flag & _Flag) == _Flag;
			}
			else
			{
				return (m_Flag & _Flag) == _Flag;
			}
		}

		inline void SetFlag(T _Flag)
		{
			if (m_pMutex != nullptr)
			{
				std::lock_guard<std::recursive_mutex> Lock(*m_pMutex);
				m_Flag |= _Flag;
			}
			else
			{
				m_Flag |= _Flag;
			}
		}

		inline void ResetFlag()
		{
			if (m_pMutex != nullptr)
			{
				std::lock_guard<std::recursive_mutex> Lock(*m_pMutex);
				m_Flag = 0;
			}
			else
			{
				m_Flag = 0;
			}
		}

		inline void ClearFlag(T _Flag)
		{
			//0001001 mFlag
			//1001000 _Flag

			//1000001 XOR
			//0001001 mFlag
			//0000001 AND

			if (m_pMutex != nullptr)
			{
				std::lock_guard<std::recursive_mutex> Lock(*m_pMutex);
				m_Flag &= (m_Flag ^ _Flag);
			}
			else
			{
				m_Flag &= (m_Flag ^ _Flag);
			}
		}

	private:
		T m_Flag;

		mutable std::recursive_mutex *m_pMutex = nullptr;
	};
};

#endif // FLAG_H