//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef MOVINGOBJECT_H
#define MOVINGOBJECT_H

#include "GameObject.h"
#include "CollisionContact.h"
#include "Logger.h"

namespace helix
{
	namespace datastructures
	{
		using namespace DirectX;

		class MovingObject : public GameObject
		{		

		public:
			HDEBUGNAME("MovingObject");

			MovingObject();
			MovingObject(const XMFLOAT3& _vPosition, const XMFLOAT3& _vScale, const XMFLOAT3& _vEulerAngles, float _fMass, float _fDamping = 0.99f, GameObjectFlags _kFlag = GameObjectFlag_MovingObject, const XMFLOAT3& _vLinearVelocity = XMFLOAT3_ZERO, const XMFLOAT3& _vForce = XMFLOAT3_ZERO);
			MovingObject(const MovingObject& _Other);
			MovingObject(MovingObject&& _Other);
			MovingObject& operator =(MovingObject& _Other);
			~MovingObject();

			static void ComputeCollision(MovingObject& _MO1, MovingObject& _MO2, const collision::CollisionInfo& _CollisionInfo);

			// GETTER / SETTER

			const float& GetMass() const;
			void SetMass(const float& _fMass);

			const float& GetDamping() const;
			void SetDamping(const float& _fDamping);

			const XMFLOAT3& GetLinearVelocity() const;
			void SetLinearVelocity(const XMFLOAT3& _vLinearVelocity);

			void AddForce(const XMFLOAT3& _vForce);

		private:			

		public:
			float m_fDamping;
			float m_fInvMass; //M

			XMFLOAT3 m_vLinearVelocity; //v
			XMFLOAT3 m_vCurForce; //force for the next simulation step
		};

		inline const float& MovingObject::GetMass() const	{ std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return 1.f / m_fInvMass; }
		inline void MovingObject::SetMass(const float& _fMass){ std::lock_guard<std::recursive_mutex> Lock(m_Mutex); m_fInvMass = 1.f / _fMass; }

		inline const float& MovingObject::GetDamping() const { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return -m_fDamping; }
		inline void MovingObject::SetDamping(const float& _fDamping) { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); m_fDamping = -_fDamping; }

		inline const XMFLOAT3& MovingObject::GetLinearVelocity() const { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return m_vLinearVelocity; }
		inline void MovingObject::SetLinearVelocity(const XMFLOAT3& _vLinearVelocity) { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); m_vLinearVelocity = _vLinearVelocity; }

		inline void MovingObject::AddForce(const XMFLOAT3& _vForce) { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); m_vCurForce = math::Add(m_vCurForce, _vForce); }

	}; // namespace datastructures
}; // namespace helix

#endif // MOVINGOBJECT_H