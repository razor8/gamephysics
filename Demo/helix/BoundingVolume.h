//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef BOUNDINGVOLUME_H
#define BOUNDINGVOLUME_H

#include "XMMath.h"
#include "CollisionContact.h"
#include "StdAfxH.h"
#include "Flag.h"
#include "Debug.h"
#include "Logger.h"
#include <memory>

namespace helix
{
	namespace collision
	{
		enum VolumeType
		{
			VolumeType_Sphere = 0,
			VolumeType_AABB = 1,
			VolumeType_OBB = 2,
			VolumeType_Tree = 3
		};

		Flag32E(CollisionFlags)
		{
			CollisionFlag_Continuous = 1 << 0,
			CollisionFlag_NoCollision = 1 << 1,
			//CollisionFlag_Naive = 1 << 2,
			//CollisionFlag_UniformGrid = 1 << 3,
			//CollisionFlag_KDTree = 1 << 4,
			//CollisionFlag_Octree = 1 << 5
		};

		using namespace DirectX;

#pragma warning(disable:4172)

		class BoundingVolume : public Flag<uint32_t>
		{
		public:
			HDEBUGNAME("BoundingVolume");

			BoundingVolume();
			BoundingVolume(const BoundingVolume& _BV);
			~BoundingVolume();

			//BoundingSphere constructor
			BoundingVolume(_In_ const XMFLOAT3& _vPos, _In_ float _fRadius, _In_ const XMFLOAT3& _vOffset = XMFLOAT3_ZERO);

			//AABB constructor
			BoundingVolume(_In_ const XMFLOAT3& _vPos, _In_ const XMFLOAT3& _vExtents, _In_ const XMFLOAT3& _vOffset = XMFLOAT3_ZERO);

			//OBB constructor
			BoundingVolume(_In_ const XMFLOAT3& _vPos, _In_ const XMFLOAT3& _vExtents, _In_ const XMFLOAT4& _vOrientation, _In_ const XMFLOAT3& _vOffset = XMFLOAT3_ZERO);		

			//Frustum constructor
			BoundingVolume(_In_ const XMFLOAT3& _vOrigin, _In_ const XMFLOAT4& _vOrientation, _In_ float _fRightSlope, _In_ float _fLeftSlope, _In_ float _fTopSlope, _In_ float _fBottomSlope, _In_ float _fNear, _In_ float _fFar, _In_ const XMFLOAT3& _vOffset = XMFLOAT3_ZERO);

			//Frustum constructor 2
			BoundingVolume(_In_ CXMMATRIX _mProjectition);

			void Update(const XMFLOAT3& _vPos);
			void Update(const XMFLOAT4& _vRot);
			void Update(const XMFLOAT3& _vPos, const XMFLOAT4& _vRot);

			//Compares bounding volumes of the same type and decends to the best fitting volume if available
			bool IntersectsPairwise(_In_ const BoundingVolume& sh, _Out_ VolumeType& _IntersectingVolume) const;

			bool Intersects(_In_ const BoundingVolume& _BV, _Out_ VolumeType& _Volume1, _Out_ VolumeType& _Volume2) const;

			bool Intersects(_In_ const BoundingSphere& _BSphere) const;
			bool Intersects(_In_ const BoundingBox& _AABB) const;
			bool Intersects(_In_ const BoundingOrientedBox& _OBB) const;

			//returns true if the BVs intersect and writes the info to _CollisionInfo
			bool GetPairwiseCollisionInfo(_In_ const BoundingVolume& _BV, _Out_ CollisionInfo& _CollisionInfo);
			bool GetCollisionInfo(_In_ const BoundingVolume& _BV, _Out_ CollisionInfo& _CollisionInfo);

			//checks if any of the volumes is inside the AABB and decends to the best fitting volume if available
			ContainmentType ContainedBy(_In_ const BoundingBox& box) const;

			inline std::shared_ptr<BoundingSphere> GetBSphere() const { return m_pBSphere; }
			inline std::shared_ptr<BoundingBox> GetAABB() const { return m_pAABB; }
			inline std::shared_ptr<BoundingOrientedBox> GetOBB() const { return m_pOBB; }
			inline std::shared_ptr<BoundingFrustum> GetBFrustum() const { return m_pBFrustum; }

			void SetNewBSphere(_In_ const XMFLOAT3& _vPos, _In_ float _fRadius, _In_ const XMFLOAT3& _vOffset = XMFLOAT3_ZERO);
			void SetNewAABB(_In_ const XMFLOAT3& _vPos, _In_ const XMFLOAT3& _vExtents, _In_ const XMFLOAT3& _vOffset = XMFLOAT3_ZERO);
			void SetNewOBB(_In_ const XMFLOAT3& _vPos, _In_ const XMFLOAT3& _vExtents, _In_ const XMFLOAT4& _vOrientation, _In_ const XMFLOAT3& _vOffset = XMFLOAT3_ZERO);
			void SetNewBFrustum(_In_ const XMFLOAT3& _vOrigin, _In_ const XMFLOAT4& _vOrientation, _In_ float _fRightSlope, _In_ float _fLeftSlope, _In_ float _fTopSlope, _In_ float _fBottomSlope, _In_ float _fNear, _In_ float _fFar, _In_ const XMFLOAT3& _vOffset = XMFLOAT3_ZERO);
			void SetNewBFrustum(_In_ CXMMATRIX _mProjectition);

			const XMFLOAT3& GetBSpherePosition();
			const XMFLOAT3& GetAABBPosition();
			const XMFLOAT3& GetOBBPosition();
			const XMFLOAT3& GetBFrustumPosition();

			const XMFLOAT3& GetPosition();
			bool GetOrientation(_Out_ XMFLOAT4& _vOrientation) const;
			bool GetRadius(_Out_ float& _fRadius) const;

		private:
			std::shared_ptr<BoundingSphere> m_pBSphere = nullptr;
			std::shared_ptr<BoundingBox> m_pAABB = nullptr;
			std::shared_ptr<BoundingOrientedBox> m_pOBB = nullptr;
			std::shared_ptr<BoundingFrustum> m_pBFrustum = nullptr;

			//Maybe add a BSP tree

			XMFLOAT3 m_vBSphereOffset = XMFLOAT3_ZERO;
			XMFLOAT3 m_vAABBOffset = XMFLOAT3_ZERO;
			XMFLOAT3 m_vOBBOffset = XMFLOAT3_ZERO;
			XMFLOAT3 m_vBFrustumOffset = XMFLOAT3_ZERO;
		};
	}
}

#endif