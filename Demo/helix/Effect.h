//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

#ifndef EFFECT_H
#define EFFECT_H

#include <d3d11_2.h>
#include <d3dcompiler.h>
#include <string>

#include "Debug.h"
#include "XMMath.h"

using namespace DirectX;

namespace helix {

	class Effect
	{
	public:
		HDEBUGNAME("Effect");

		~Effect();

		static void SetShaderFolderPath(std::wstring _sShaderFolderPath);
		static HRESULT CompileShaderFromFile( std::wstring _sFileName, LPCSTR _sShaderTarget, ID3DBlob** _pShaderBlob);
		// TODO: test CompileShaderFromMemory function
		static HRESULT CompileShaderFromMemory( LPCVOID _pSrcData, SIZE_T _uSrcDataSize, LPCSTR _sShaderTarget, ID3DBlob** _pShaderBlob);

	private:
		Effect();

	private:
		Effect(const Effect& _Effect);
		Effect& operator=(const Effect& _Effect);
	private:
		static std::wstring ShaderFolderPath;
	};


}; // namespace helix

#endif //EFFECT_H