//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef UNIFORMGRID_H
#define UNIFORMGRID_H

#include "Debug.h"
#include "GameObject.h"
#include <vector>
#include <list>

namespace helix
{
	namespace datastructures
	{
		using namespace DirectX;

		typedef std::list<GameObject*> TCell;
		typedef std::vector<TCell> TCells;

		class UniformGrid
		{
		public:
			UniformGrid::UniformGrid(uint32_t _uCellCount, float _fGridCellSize);

			void Update(GameObject* _pEntity);

			void GatherCollisions(_Out_ std::vector<collision::CollisionInfo>& _Collisions);

			uint32_t ComputeHash(const XMFLOAT3& _vPosition);
			uint32_t ComputeHash(int32_t _iX, int32_t _iY, int32_t _iZ);

		private:
			TCells m_Cells;

			uint32_t m_uCellCount;
			float m_fGridCellSize;
		};
	}
}

#endif // UNIFORMGRID_H