//Copyright(c) 2015
//Authors: Fabian Wahlster, Steffen Wiewel, Moritz Becher

/////////////////////////////////////////////////////////////////////////////////////////////////
// AnimationTrack
//-----------------------------------------------------------------------------------------------
// Summary:
// This class contains a single animation track. The template parameter T can be a FLOAT2, 
// FLOAT3, FLOAT4 as well as any scalar value. 
// Bezier interpolation currently only works on FLOAT3 Values.
//-----------------------------------------------------------------------------------------------
// Future Work:
//	- Parabolic Ease-In/Ease-Out (REF#1 P89)
//	! Interpolation of velocities between keyframes 
//	- Extended Quaternion Interpolation
//	- Let each keyframe contain all curves, simple switch
//	- Rename to AnimationTrack?
//	- Lookup Table for simple beziers
//	- Lookup Table for time distance functions
//	- Path Following
//	- Frenet Frame Orientation
//	- Keyframes may be better represented as segments
//-----------------------------------------------------------------------------------------------
// References:
// 1) Computer Animation: Algorithms and Techniques, by Rick Parent, ISBN:978-0124158429
/////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef ANIMATION_H
#define ANIMATION_H

#include <stdint.h>
#include <vector>
#include "XMMath.h"
#include "Logger.h"
#include "Flag.h"
#include "StdAfxH.h"
#include "CurveMath.h"

namespace helix
{
	namespace animation
	{
		using namespace DirectX;
		
		Flag32E(AnimationFlag)
		{
			AnimationFlag_None = 0, // 0
			AnimationFlag_LinearInterpolation = 1 << 0, // 1
			AnimationFlag_BezierInterpolation = 1 << 1, // 2
			AnimationFlag_Quaternion = 1 << 2,	// don't set this flag if the used data type isn't a XMFLOAT4

			AnimationFlag_ConstantVelocity = 1 << 3,
			AnimationFlag_Easing = 1 << 4,
		};

		template <class T>
		class AnimationTrack
		{
			HDEBUGNAME("Animation");

		public:
			// A KeyFrame defines the keyframe and the segment before. 
			template<class T>
			struct KeyFrame : public Flag<uint32_t>
			{
				KeyFrame() : fTime(0.0f), fEaseIn(0.0f), fEaseOut(0.0f), Data(), OutControl(), InControl(), Bezier(), ArcLengthFunction()
				{
				}

				KeyFrame(float _fTime, T _Data, T _OutControl, T _InControl, float _fEaseIn = 0.0f, float _fEaseOut = 0.0f, BezierCurve _Bezier = BezierCurve(), ArcLength _ArcLength = ArcLength())
					: fTime(_fTime),  Data(_Data), OutControl(_OutControl), InControl(_InControl), fEaseIn(_fEaseIn), fEaseOut(_fEaseOut), Bezier(_Bezier), ArcLengthFunction(_ArcLength)
				{
				}
				float fTime;
				float fEaseIn;						// slow down to keyframe
				float fEaseOut;						// slowly accelerate from keyframe
													// float fTangentInLength;			// porition of keyframe's tangent in interpolation of the previous segment
													// float fTangentOutLength;			// porition of keyframe's tangent in interpolation of next segment
				T Data;								// value at keyframe time
				T OutControl;						// the direction of data after the keyframe
				T InControl;
				BezierCurve Bezier;					// The function used to interpolate data between keyframes. e.G. Bezier Curves
				ArcLength ArcLengthFunction;
			};



		// GETTERS
			// Get animated data if _fTime is in the animation interval. Otherwise the data of the first or last frame is set.
			void GetAnimationData(_Out_ T& _Output, _In_ const float _fTime);
			// Get position in future of track. This can be used for orienting in animation direction.
			void GetAnimationOrientation(_Out_ T& _Output, _In_ const float _fTime);

		// TRACK & KEYFRAMES
			// Builds track from data with constant velocity
			void BuildTrack(const std::vector<T>& _Data, float _fStartTime, float _fEndTime, const AnimationFlag _Flags);
			// Add a keyframe to the end of the animation track
			//void AppendKeyFrame(const float _fTime,
			//						const T& _Data,
			//						const AnimationFlag _Flags = AnimationFlag(AnimationFlag_LinearInterpolation),
			//						const float _fEaseIn = 0.0f,
			//						const float _fEaseOut = 0.0f,
			//						const T& _Tangent = T(),
			//						const float _fTangentInLength = 1.0f,
			//						const float _fTangentOutLength = 1.0f);

		protected:

			void ComputeCombinedControl(_Out_ KeyFrame<XMFLOAT3>& _KeyFrame, float _fSmoothness = 1.0f);

		// INTERPOLATION
			// Select interpolation to use in the current segment
			void Interpolate(T& _Output, const float _fTime, const KeyFrame<T>& _StartFrame, KeyFrame<T>& _EndFrame);
			// Computer Animation: Algorithms and Techniques, S78
			// The time distance function is used for speed control. It's input is a time (t) and it's output the arc length refered to as distance (s).
			float TimeToDistance(const float _fTime, const KeyFrame<T>& _StartFrame, const KeyFrame<T>& _EndFrame);
			// Interpolate between two values, portion of start and end value can be controlled by the interpolant.
			void LinearInterpolation(_Out_ T& _Output, const _In_ T& _vStart, const _In_ T& _vEnd, _In_ const float _fInterpolant);
			// Interpolate between two quaternions, portion of start and end value can be controlled by the interpolant.
			void LinearInterpolationQuaternion(_Out_ T& _Output, const _In_ T& _vStart, const _In_ T& _vEnd, _In_ const float _fInterpolant);
			
		// VARIABLES
			std::vector<KeyFrame<T>> m_KeyFrameList;
		};

	}
}

#include "AnimationTrack.inl"

#endif
