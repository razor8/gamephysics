//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

#ifndef LOGGER_H
#define LOGGER_H

#include <string>
#include <fstream>

#include "Debug.h"
#include "Singleton.h"
#include <mutex>

namespace helix
{

enum HERROR_E
{
	HERROR_NONE = 0,
	HERROR_WARNING = 1,
	HERROR_ERROR = 2,
	HERROR_FATAL = 3
};

class Logger : public TSingleton<Logger>
{
public:
	HDEBUGNAME("Logger");

	Logger(HERROR_E kType = HERROR_NONE);
	~Logger ();

	void WriteToStream(std::wostream* _Stream);
	void WriteToFile(const TCHAR* pFilePath);

	void Log(HERROR_E kType, const TCHAR* pDbgName, const TCHAR* pFuncName, const TCHAR* pSourceFile, uint32_t uLineNum, const TCHAR * pFormat, ...);

	inline void SetLogLevel(HERROR_E level){ m_LogLevel = level;}
	inline void SetLogToFile(bool _bLogToFile){ m_bLogToFile = _bLogToFile; }
	inline void SetLogToStream(bool _bLogToStream){ m_bLogToStream = _bLogToStream; }
	inline void SetLogToOutputConsole(bool _bLogToOutputConsole){ m_bLogToOutputConsole = _bLogToOutputConsole; }
	inline void SetShowMessageBoxOnFatal(bool _bShowMessageBoxOnFatal){ m_bShowMessageBoxOnFatal = _bShowMessageBoxOnFatal; }

protected:

	bool m_bFirstLog;

	bool m_bLogToFile;
	bool m_bLogToStream;
	bool m_bLogToOutputConsole;
	bool m_bShowMessageBoxOnFatal;

	std::basic_fstream<TCHAR> m_FileStream;
	HERROR_E m_LogLevel;	
	std::mutex m_Mutex;
	std::wostream* m_Stream;
};

#define HLOG(format,...) Logger::Instance()->Log(HERROR_NONE,nullptr,nullptr, nullptr, NULL,TEXT(format),__VA_ARGS__)
#define HWARNING(format,...) Logger::Instance()->Log(HERROR_WARNING,nullptr,TEXT(__FUNCTION__), TEXT(__FILE__), __LINE__,TEXT(format),__VA_ARGS__)
#define HERROR(format,...) Logger::Instance()->Log(HERROR_ERROR,nullptr,TEXT(__FUNCTION__), TEXT(__FILE__), __LINE__,TEXT(format),__VA_ARGS__)
#define HFATAL(format,...) Logger::Instance()->Log(HERROR_FATAL,nullptr,TEXT(__FUNCTION__), TEXT(__FILE__), __LINE__,TEXT(format),__VA_ARGS__)
#define HASSERT(predicate, format, ...) {if ((predicate) == false) HFATAL(format, __VA_ARGS__);}

#ifdef _DEBUG
//Only log when in debug build
#define HLOGD(format,...) Logger::Instance()->Log(HERROR_NONE,getDebugName(),nullptr, nullptr, NULL,TEXT(format),__VA_ARGS__)
#define HWARNINGD(format,...) Logger::Instance()->Log(HERROR_WARNING,getDebugName(),TEXT(__FUNCTION__), TEXT(__FILE__), __LINE__,TEXT(format),__VA_ARGS__)
#define HERRORD(format,...) Logger::Instance()->Log(HERROR_ERROR,getDebugName(),TEXT(__FUNCTION__), TEXT(__FILE__), __LINE__,TEXT(format),__VA_ARGS__)
#define HFATALD(format,...) Logger::Instance()->Log(HERROR_FATAL,getDebugName(),TEXT(__FUNCTION__), TEXT(__FILE__), __LINE__,TEXT(format),__VA_ARGS__)

//this version does not use the getDebugName() function
#define HLOGD2(format,...) Logger::Instance()->Log(HERROR_NONE,nullptr, nullptr , nullptr, NULL,TEXT(format),__VA_ARGS__)
#define HWARNINGD2(format,...) Logger::Instance()->Log(HERROR_WARNING,nullptr,TEXT(__FUNCTION__), TEXT(__FILE__), __LINE__,TEXT(format),__VA_ARGS__)
#define HERRORD2(format,...) Logger::Instance()->Log(HERROR_ERROR,nullptr,TEXT(__FUNCTION__), TEXT(__FILE__), __LINE__,TEXT(format),__VA_ARGS__)
#define HFATALD2(format,...) Logger::Instance()->Log(HERROR_FATAL,nullptr,TEXT(__FUNCTION__), TEXT(__FILE__), __LINE__,TEXT(format),__VA_ARGS__)

#define HASSERTD(predicate, format, ...) {if ((predicate) == false) HFATALD2(format, __VA_ARGS__);}

#else //Release

#define HLOGD(format,...)
#define HWARNINGD(format,...)
#define HERRORD(format,...)
#define HFATALD(format,...)

#define HLOGD2(format,...)
#define HWARNINGD2(format,...)
#define HERRORD2(format,...)
#define HFATALD2(format,...)

#define HASSERTD(predicate, format, ...)

#endif

//override definition from Debug.h
#undef HR
#define HR(x) \
	{\
		if(FAILED(x)){\
			LPTSTR msg; \
			if(FormatMessage((FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS), NULL, (HRESULT)x, 0, (LPTSTR)&msg, 0, NULL) != 0){ \
				HFATAL("%s in %s", msg, TEXT(#x));\
				LocalFree(msg);\
			}\
		}\
	}

}; // namespace helix

#endif LOGGER_H