//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "GameObjectContainer.h"
#include "Logger.h"

#include <experimental\algorithm>

using namespace helix::datastructures;
using namespace helix::collision;

//---------------------------------------------------------------------------------------------------
GameObjectContainer::GameObjectContainer()
{

}
//---------------------------------------------------------------------------------------------------
GameObjectContainer::~GameObjectContainer()
{

}
//---------------------------------------------------------------------------------------------------
GameObject* GameObjectContainer::Insert(GameObject& _GO)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);

	if (_GO.m_uID == HUNDEFINED)
	{
		_GO.m_uID = m_UniqueID.NextID();

		if (_GO.CheckFlag(GameObjectFlag_RigidBody))
		{
			return InsertRigidBody(_GO);
		}
		//else if (_GO.CheckFlag(GameObjectFlag_MovingObject))
		//{
		//	return InsertMovingObject(_GO);
		//}
		else
		{
			return InsertGameObject(_GO);
		}		
	}
	else
	{
		HWARNINGD("GameObject %d already defined!", _GO.m_uID);
	}

	return nullptr;
}
//---------------------------------------------------------------------------------------------------
GameObject* GameObjectContainer::Get(uint32_t _ID)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);

	TPhysicsEntities::iterator it = m_AllEntities.find(_ID);

	if (it != m_AllEntities.end())
		return it->second;
	else
		return nullptr;	
}
//---------------------------------------------------------------------------------------------------
bool GameObjectContainer::Remove(uint32_t _ID)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);

	TPhysicsEntities::iterator it = m_AllEntities.find(_ID);

	if (it != m_AllEntities.end())
	{
		if (it->second->CheckFlag(GameObjectFlag_RigidBody))
		{
			m_RigidBodies.erase(_ID);
		}
		//else if (it->second->CheckFlag(GameObjectFlag_MovingObject))
		//{
		//	m_MovingObjects.erase(_ID);
		//}
		else
		{
			m_GameObjects.erase(_ID);
		}

		m_AllEntities.erase(_ID);

		return true;
	}		
	else
	{
		return false;
	}
}
//---------------------------------------------------------------------------------------------------
GameObject* GameObjectContainer::InsertRigidBody(GameObject& _RB)
{
	uint32_t uID = _RB.m_uID;

	RigidBody* pRB = dynamic_cast<RigidBody*>(&_RB);
	if (pRB != nullptr)
	{
		auto it = m_RigidBodies.insert(std::make_pair(uID, std::move(*pRB)));
		if (it.second == false)
		{
			HWARNINGD("GameObject %d already in RigidBody map", uID);
		}
		else
		{
			pRB = &it.first->second;
			m_AllEntities[uID] = pRB;
			return pRB;
		}
	}
	else
	{
		HFATALD("Flag RigidBody was set, but type conversion failed");
	}

	return nullptr;
}
//---------------------------------------------------------------------------------------------------
//GameObject* GameObjectContainer::InsertMovingObject(GameObject& _MO)
//{
//	uint32_t uID = _MO.m_uID;
//
//	MovingObject* pMO = dynamic_cast<MovingObject*>(&_MO);
//	if (pMO != nullptr)
//	{
//		auto it = m_MovingObjects.insert(std::make_pair(uID, std::move(*pMO)));
//		if (it.second == false)
//		{
//			HWARNINGD("GameObject %d already in MovingObject map", uID);
//		}
//		else
//		{
//			pMO = &it.first->second;
//			m_AllEntities[uID] = pMO;
//			return pMO;
//		}
//	}
//	else
//	{
//		HFATALD("Flag MovingObject was set, but type conversion failed");
//	}
//
//	return nullptr;
//}
//---------------------------------------------------------------------------------------------------
GameObject* GameObjectContainer::InsertGameObject(GameObject& _GO)
{
	uint32_t uID = _GO.m_uID;

	auto it = m_GameObjects.insert(std::make_pair(uID, std::move(_GO)));
	if (it.second == false)
	{
		HWARNINGD("GameObject %d already in m_GameObjects map", uID);
	}
	else
	{
		GameObject* pGO = &it.first->second;
		m_AllEntities[uID] = pGO;
		return pGO;
	}

	return nullptr;
}
//---------------------------------------------------------------------------------------------------
void GameObjectContainer::remove_if(_In_ const GameObjectFlags _Flag, _In_ const bool _bIsSet)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);

	//TODO: try to use parallel stl

	for (TPhysicsEntities::iterator it = m_AllEntities.begin(); it != m_AllEntities.end();)
	{
		if (it->second->CheckFlag(_Flag) == _bIsSet)
		{
			uint32_t uID = it->first;

			if (it->second->CheckFlag(GameObjectFlag_RigidBody))
			{
				m_RigidBodies.erase(uID);
			}
			//else if (it->second->CheckFlag(GameObjectFlag_MovingObject))
			//{
			//	m_MovingObjects.erase(uID);
			//}
			else
			{
				m_GameObjects.erase(uID);
			}

			it = m_AllEntities.erase(it);			
		}
		else
		{
			++it;
		}
	}
}
//---------------------------------------------------------------------------------------------------
void GameObjectContainer::GetRigidBodiesWithFlag(_Out_ std::vector<RigidBody*>& _RigidBodies, _In_ const GameObjectFlags _Flag, _In_ const bool _bIsSet)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	HFOREACH_CONST(it, end, m_RigidBodies)
		if (it->second.CheckFlag(_Flag) == _bIsSet)
			_RigidBodies.push_back(&it->second);
	HFOREACH_CONST_END
}
//---------------------------------------------------------------------------------------------------
void GameObjectContainer::GetRigidBodiesWithCollisionFlag(_Out_ std::vector<RigidBody*>& _RigidBodies, _In_ const CollisionFlags _Flag, _In_ const bool _bIsSet)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	HFOREACH_CONST(it, end, m_RigidBodies)
		if (it->second.m_BV.CheckFlag(_Flag) == _bIsSet)
			_RigidBodies.push_back(&it->second);
	HFOREACH_CONST_END
}
//---------------------------------------------------------------------------------------------------
//void GameObjectContainer::GetMovingObjectsWithFlag(_Out_ std::vector<MovingObject*>& _MovingObjects, _In_ const GameObjectFlags _Flag, _In_ const bool _bIsSet)
//{
//	std::lock_guard<std::mutex> Lock(m_Mutex);
//	HFOREACH_CONST(it, end, m_MovingObjects)
//		if (it->second.CheckFlag(_Flag) == _bIsSet)
//			_MovingObjects.push_back(&it->second);
//	HFOREACH_CONST_END
//}
////---------------------------------------------------------------------------------------------------
//void GameObjectContainer::GetMovingObjectsWithCollisionFlag(_Out_ std::vector<MovingObject*>& _MovingObjects, _In_ const CollisionFlags _Flag, _In_ const bool _bIsSet)
//{
//	std::lock_guard<std::mutex> Lock(m_Mutex);
//	HFOREACH_CONST(it, end, m_MovingObjects)
//		if (it->second.m_BV.CheckFlag(_Flag) == _bIsSet)
//			_MovingObjects.push_back(&it->second);
//	HFOREACH_CONST_END
//}
//---------------------------------------------------------------------------------------------------
void GameObjectContainer::GetGameObjectsWithFlag(_Out_ std::vector<GameObject*>& _GameObjects, const GameObjectFlags _Flag, const bool _bIsSet)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	
	HFOREACH_CONST(it, end, m_GameObjects)
		if (it->second.CheckFlag(_Flag) == _bIsSet)
			_GameObjects.push_back(&it->second);
	HFOREACH_CONST_END
	

	/*TPhysicsEntities::iterator it = m_AllEntities.begin(), end = m_AllEntities.end();

	while (true)
	{
		it = std::experimental::parallel::find_if(std::experimental::parallel::par,
		it,
		end,
		[&](TPhysicsEntities::value_type& go){ return go.second->CheckFlag(_uFlag) == _bIsSet; });

		if (it == end)
			break;

		_GameObjects.push_back(it->second);
	}*/
}
//---------------------------------------------------------------------------------------------------
void GameObjectContainer::GetGameObjectsWithCollisionFlag(_Out_ std::vector<GameObject*>& _GameObjects, _In_ const CollisionFlags _Flag, _In_ const bool _bIsSet)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	
	HFOREACH_CONST(it, end, m_GameObjects)
		if (it->second.m_BV.CheckFlag(_Flag) == _bIsSet)
			_GameObjects.push_back(&it->second);
	HFOREACH_CONST_END
}
//---------------------------------------------------------------------------------------------------
void GameObjectContainer::GetPhysicsEntitiesWithFlag(_Out_ std::vector<GameObject*>& _GameObjects, const GameObjectFlags _Flag, const bool _bIsSet)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	HFOREACH_CONST(it, end, m_AllEntities)
		if (it->second->CheckFlag(_Flag) == _bIsSet)
			_GameObjects.push_back(it->second);
	HFOREACH_CONST_END
}
//---------------------------------------------------------------------------------------------------
void GameObjectContainer::GetPhysicsEntitiesWithCollisionFlag(_Out_ std::vector<GameObject*>& _GameObjects, _In_ const CollisionFlags _Flag, _In_ const bool _bIsSet)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	HFOREACH_CONST(it, end, m_AllEntities)
		if (it->second->m_BV.CheckFlag(_Flag) == _bIsSet)
			_GameObjects.push_back(it->second);
	HFOREACH_CONST_END
}