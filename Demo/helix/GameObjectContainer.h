//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef GAMEOBJECTCONTAINER_H
#define GAMEOBJECTCONTAINER_H

#include "GameObject.h"
#include "RigidBody.h"
#include "MovingObject.h"
#include <unordered_map>
#include "UniqueID.h"
#include "Debug.h"
#include <mutex>

namespace helix
{
	namespace datastructures
	{

		typedef std::unordered_map<uint32_t, GameObject*> TPhysicsEntities;
		typedef std::unordered_map<uint32_t, GameObject> TGameObjects;
		//typedef std::unordered_map<uint32_t, MovingObject> TMovingObjects;
		typedef std::unordered_map<uint32_t, RigidBody> TRigidBodies;

		class GameObjectContainer
		{
		public:
			HDEBUGNAME("GameObjectContainer");

			GameObjectContainer();
			~GameObjectContainer();

		public:
			GameObject* Insert(GameObject& _GO);
			GameObject* Get(uint32_t _ID);

			bool Remove(uint32_t _ID);

			inline TPhysicsEntities::iterator begin() { return m_AllEntities.begin(); }
			inline TPhysicsEntities::iterator end() { return m_AllEntities.end(); }

			inline TPhysicsEntities::const_iterator cbegin(){ return m_AllEntities.cbegin(); }
			inline TPhysicsEntities::const_iterator cend(){ return m_AllEntities.cend(); }

			void remove_if(_In_ const GameObjectFlags _Flag, _In_ const bool _bIsSet = true);

			//rigidbodies only
			void GetRigidBodiesWithFlag(_Out_ std::vector<RigidBody*>& _RigidBodies, _In_ const GameObjectFlags _Flag, _In_ const bool _bIsSet = true);
			void GetRigidBodiesWithCollisionFlag(_Out_ std::vector<RigidBody*>& _RigidBodies, _In_ const collision::CollisionFlags _Flag, _In_ const bool _bIsSet = true);

			//movingobjects only
			//void GetMovingObjectsWithFlag(_Out_ std::vector<MovingObject*>& _MovingObjects, _In_ const GameObjectFlags _Flag, _In_ const bool _bIsSet = true);
			//void GetMovingObjectsWithCollisionFlag(_Out_ std::vector<MovingObject*>& _MovingObjects, _In_ const collision::CollisionFlags _Flag, _In_ const bool _bIsSet = true);

			//gameobjects only
			void GetGameObjectsWithFlag(_Out_ std::vector<GameObject*>& _GameObjects, _In_ const GameObjectFlags _Flag, _In_ const bool _bIsSet = true);
			void GetGameObjectsWithCollisionFlag(_Out_ std::vector<GameObject*>& _GameObjects, _In_ const collision::CollisionFlags _Flag, _In_ const bool _bIsSet = true);

			//all objects
			void GetPhysicsEntitiesWithFlag(_Out_ std::vector<GameObject*>& _GameObjects, _In_ const GameObjectFlags _Flag, _In_ const bool _bIsSet = true);
			void GetPhysicsEntitiesWithCollisionFlag(_Out_ std::vector<GameObject*>& _GameObjects, _In_ const collision::CollisionFlags _Flag, _In_ const bool _bIsSet = true);

		private:
			GameObject* InsertRigidBody(GameObject& _RB);
			//GameObject* InsertMovingObject(GameObject& _MO);
			GameObject* InsertGameObject(GameObject& _GO);

		private:
			UniqueID<uint32_t> m_UniqueID;

			TPhysicsEntities m_AllEntities;
			TGameObjects m_GameObjects;
			//TMovingObjects m_MovingObjects;
			TRigidBodies m_RigidBodies;

			std::mutex m_Mutex;
		};
	}; // namespace datastructures
}; // namespace helix

#endif // GAMEOBJECTCONTAINER_H