//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "RigidBodySystem.h"
#include "Logger.h"
#include "StdAfxH.h"

using namespace helix::math;
using namespace helix::async;
using namespace helix::physics;

//---------------------------------------------------------------------------------------------------
RigidBodySystem::RigidBodySystem() : PhysicsSystem<TRigidBodies, TRigidBodyForceMap>()
{
	m_Integrator = XMIntegrator_Heun;
	m_DefaultForce = XMFLOAT3_ZERO;
}
//---------------------------------------------------------------------------------------------------
RigidBodySystem::RigidBodySystem(IAsyncObserver* _Observer) : PhysicsSystem<TRigidBodies, TRigidBodyForceMap>(_Observer)
{
	m_Integrator = XMIntegrator_Heun;
	m_DefaultForce = XMFLOAT3_ZERO;
}
//---------------------------------------------------------------------------------------------------
RigidBodySystem::~RigidBodySystem()
{

}
//---------------------------------------------------------------------------------------------------
void RigidBodySystem::StopSubsystems()
{
	// this function is empty since we dont have any subsystems
}
//---------------------------------------------------------------------------------------------------
void RigidBodySystem::InitializeSubsystems()
{
	// no subsystems to initialize
}
//---------------------------------------------------------------------------------------------------
//run simulation step here
void RigidBodySystem::Execute()
{
	//HLOGD(TEXT("Simulate %f"), m_fDeltaTime);
	//add external forces here

	XMFLOAT3 XI[8];
	XMVECTOR F = XMLoadFloat3(&m_DefaultForce);

	//TODO: evenly distribute the force over all vertices by dividing it by the total number of vertices in the physics BV
	XMVECTOR fd = XMVectorScale(F, 1.f / 8.f);

	XMVECTOR xi;
	XMVECTOR fi;
	XMVECTOR q; // torque

	for (datastructures::RigidBodyIterator itr = m_Data.begin; itr != m_Data.end; ++itr)
	{
		//TODO: fix iterator < operator, or dont use random_access_iterator_tag
		//HFOREACH_PARALLEL_RANGE(m_Data.begin, m_Data.end, [&], datastructures::TplGameObject, GO)
			//pRB->Lock();
		datastructures::TplGameObject& GO = *itr;
		datastructures::RigidBodyData* const pRB = GO.GetRigidBodyData();
		HASSERTD(pRB != nullptr, "GameObject is not of type RigidBody!");

		XMVECTOR XWorld = XMLoadFloat3(&GO.m_vPosition);

		q = XMVectorZero();
		F = XMLoadFloat3(&m_DefaultForce);

		GO.GetOBB().GetCorners(XI);
		for (uint32_t i = 0; i < BoxIndices_NumOfEdges; ++i)
		{
			xi = XMLoadFloat3(&XI[i]);
			xi = XMVectorSubtract(xi, XWorld);

			//q += xi x fd
			q = XMVectorAdd(q, XMVector3Cross(xi, fd));
		}

		std::pair<TRigidBodyForceMap::iterator, TRigidBodyForceMap::iterator> p = m_ExternalData.equal_range(GO.GetID());
		//iterate over all forces for a specific rigidbody
		for (; p.first != p.second; ++p.first)
		{
			const ForcePoint& fp = p.first->second;
			fi = XMLoadFloat3(&fp.m_vForce);
			xi = XMLoadFloat3(&XI[fp.m_uVertexID]);
			xi = XMVectorSubtract(xi, XWorld);

			//q += xi x fi
			q = XMVectorAdd(q, XMVector3Cross(xi, fi));

			//accumulate total force for rigidbody
			F = XMVectorAdd(F, fi);
		}

		XMVECTOR x = XMLoadFloat3(&GO.m_vPosition);
		XMVECTOR v = XMLoadFloat3(&pRB->m_vLinearVelocity);
		XMVECTOR a = XMVectorScale(F, pRB->m_fInvMass);

		XMVECTOR r = XMLoadFloat4(&GO.m_vOrientation);
		XMVECTOR L = XMLoadFloat3(&pRB->m_vAngularMomentum);
		XMVECTOR w = XMLoadFloat3(&pRB->m_vAngularVelocity);
		XMVectorSetW(w, 0.f); // (w,0)

		float fDamping = pRB->m_fDamping;

		switch (m_Integrator)
		{
		case XMIntegrator_Euler:
			//v += h*(F/M)
			//x += h*v
			XMVectorEuler(x, v, a, fDamping, m_fDeltaTime);
			break;
		case XMIntegrator_Heun:
			XMVectorHeun(x, v, a, fDamping, m_fDeltaTime);
			break;
		case XMIntegrator_RK2:
			XMVectorRungeKutta2(x, v, a, fDamping, m_fDeltaTime);
			break;
		case XMIntegrator_RK3:
			XMVectorRungeKutta3(x, v, a, fDamping, m_fDeltaTime);
			break;
		case XMIntegrator_RK4:
		default:
			XMVectorRungeKutta4(x, v, a, fDamping, m_fDeltaTime);
		}

		XMStoreFloat3(&GO.m_vPosition, x);
		XMStoreFloat3(&pRB->m_vLinearVelocity, v);

		//r' = (w,0) * r
		w = XMQuaternionMultiply(w, r);

		//r += h/2 * r'
		r = math::XMVectorEuler(r, w, m_fDeltaTime * 0.5f);
		r = XMQuaternionNormalize(r);
		XMStoreFloat4(&GO.m_vOrientation, r);

		// L += h*q
		L = math::XMVectorEuler(L, q, m_fDeltaTime * fDamping);
		XMStoreFloat3(&pRB->m_vAngularMomentum, L);

		XMMATRIX I = XMLoadFloat3x3(&pRB->m_mInvInertiaTensor);
		XMMATRIX R = XMMatrixRotationQuaternion(r);
		XMStoreFloat3x3(&GO.m_mRotation, R);

		XMMATRIX Rt = XMMatrixTranspose(R);
		// R^T * I * R
		I = XMMatrixMultiply(Rt, I);
		I = XMMatrixMultiply(I, R);

		w = XMVector3Transform(L, I);
		XMStoreFloat3(&pRB->m_vAngularVelocity, w);

		GO.UpdateBoundingVolumes();

		//pRB->Unlock();
	//HFOREACH_PARALLEL_RANGE_END
	}

	m_ExternalData.clear();
}
//---------------------------------------------------------------------------------------------------
void RigidBodySystem::ClearData()
{
	/*std::lock_guard<std::mutex> lock(m_LockMutex);
	m_Data.resize(0);*/
}
//---------------------------------------------------------------------------------------------------
void RigidBodySystem::ClearExternalData()
{
	//std::lock_guard<std::mutex> lock(m_LockMutex);
	m_ExternalData.clear();
}
//---------------------------------------------------------------------------------------------------
void RigidBodySystem::SetDefaultForce(const XMFLOAT3& _DefaultForce)
{
	//std::lock_guard<std::mutex> lock(m_LockMutex);
	m_DefaultForce = _DefaultForce;
}
//---------------------------------------------------------------------------------------------------
void RigidBodySystem::SetIntegrator(XMIntegrator _Integrator)
{
	//std::lock_guard<std::mutex> lock(m_LockMutex);
	m_Integrator = _Integrator;
}
//---------------------------------------------------------------------------------------------------
