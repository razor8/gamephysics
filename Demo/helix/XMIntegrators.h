//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef XMINTEGRATORS_H
#define XMINTEGRATORS_H

#include "XMMath.h"

namespace helix
{
	namespace math
	{
		using namespace DirectX;

		enum XMIntegrator
		{
			XMIntegrator_Euler,
			XMIntegrator_LeapFrog,
			XMIntegrator_Heun,
			XMIntegrator_RK2, //MidPoint
			XMIntegrator_RK3,
			XMIntegrator_RK4
		};

		//f(x0,v,t) = x0 + v*t
		inline XMVECTOR XM_CALLCONV XMVectorEuler(_In_ FXMVECTOR V0,_In_ FXMVECTOR V1, float _step)
		{
			return  XMVectorAdd(V0, XMVectorScale(V1, _step));;
		}

		//results will be returned in x0 and v0
		inline void XM_CALLCONV XMVectorEuler(_Out_ XMVECTOR& x0, _Out_ XMVECTOR& v0, _In_ FXMVECTOR a0, _In_ float _damping, _In_ float _dt)
		{
			x0 += _dt * v0;
			v0 += _dt * a0 * _damping;			
		}

		//results will be returned in x0 and v0
		inline void XM_CALLCONV XMVectoLeapFrog(_Out_ XMVECTOR& x0, _Out_ XMVECTOR& v0, _In_ FXMVECTOR a0, _In_ float _damping, _In_ float _dt)
		{
			v0 += _dt * a0 * _damping;
			x0 += _dt * v0;			
		}

		//results will be returned in x0 and v0
		inline void XM_CALLCONV XMVectorHeun(_Out_ XMVECTOR& x0, _Out_ XMVECTOR& v0, _In_ FXMVECTOR a0, _In_ float _damping, _In_ float _dt)
		{
			//compute x_t/2 = x0 + v0 * t/2
			x0 += _dt * 0.5f * v0;
			//compute v_t = v0 + a0 * t
			v0 += _dt * a0 * _damping;
			//compute x_t = x_t/2 + v_t * t/2
			x0 += _dt * 0.5f * v0;
		}

		//RK2 actually is the same as the MidPoint  method
		//results will be returned in x0 and v0
		inline void XM_CALLCONV XMVectorRungeKutta2(_Out_ XMVECTOR& x0, _Out_ XMVECTOR& v0, _In_ FXMVECTOR a0, _In_ float _damping, _In_ float _dt)
		{

			XMVECTOR a = a0 + _damping * v0;
			XMVECTOR k1 = _dt * a;

			a = a0 + _damping * (v0 + k1);
			XMVECTOR k2 = _dt * a;

			v0 += (k1 + k2) / 2.f;
			x0 += v0 * _dt;
		}

		//results will be returned in x0 and v0
		inline void XM_CALLCONV XMVectorRungeKutta3(_Out_ XMVECTOR& x0, _Out_ XMVECTOR& v0, _In_ FXMVECTOR a0, _In_ float _damping, _In_ float _dt)
		{

			XMVECTOR a = a0 + _damping * v0;
			XMVECTOR k1 = _dt * a;

			a = a0 + _damping * (v0 + k1 / 2.f);
			XMVECTOR k2 = _dt * a;

			a = a0 + _damping * (v0 - k1 + k2 * 2.f);
			XMVECTOR k3 = _dt * a;

			v0 += (k1 + 4.f*k2 + k3) / 6.f;
			x0 += v0 * _dt;
		}
		
		// _f is a function calculating the time derivative of _y
		// _f calculates the slope of the function at the current timestep with respect to the corresponding _y value
		inline void XM_CALLCONV XMVectorRungeKutta4(_Out_ XMVECTOR& _y, _In_ const FXMVECTOR _dy, _In_ const float _h, _In_ const XMVECTOR (*_f)(float, FXMVECTOR), _In_ const float _damping = 1.f)
		{
			XMVECTOR k1 = _f(0.f, _y); // = f(0,y)
			XMVECTOR k2 = _f(0.5f*_h, _y + 0.5f*_h*k1);	// = f(0.5*h, y+0.5*h*k1)
			XMVECTOR k3 = _f(0.5f*_h, _y + 0.5f*_h*k2); // = f(0.5*h, y+0.5*h*k2)
			XMVECTOR k4 = _f(_h, _y + _h*k3); // = f(h, y+h*k3)
			_y += _damping * _h *(k1 + 2.f*k2 + 2.f*k3 + k4) / 6.f;
		}

		//results will be returned in x0 and v0
		inline void XM_CALLCONV XMVectorRungeKutta4(_Out_ XMVECTOR& x0, _Out_ XMVECTOR& v0, _In_ FXMVECTOR a0, _In_ float _damping, _In_ float _dt)
		{
			////our acceleration function is reduced to damping
			////y'(t,y) = (F - d*V(t)) / m

			////F = F - d*V
			////a = F / M
			////a = a - (d*v)/m => we divide d by m to save this step in the following steps => d/m = d_m
			//_damping /= _mass;
			////a = a + (d_m*v) // since d is negative:


			//TODO fix all the stuff (except Heun)
			XMVECTOR k1 = a0;

			XMVECTOR k2 = _damping * k1 * 0.5f;

			XMVECTOR k3 = _damping * k2 * 0.5f;

			XMVECTOR k4 = _damping * k3;

			v0 += (k1 + 2.f*k2 + 2.f*k3 + k4) * _dt / 6.f;
			x0 += v0 * _dt;

			////I know i said operators are bad, but for the sake of readability I decided to use them.
			////We can use the functions when we are done testing on monday
		}

	}; // namespace math

}; // namespace helix

#endif // XMINTEGRATORS_H