//Copyright(c) 2014
//Authors: Fabian Wahlster

#ifndef MUTABLETHREAD_H
#define MUTABLETHREAD_H

#include <thread>
#include <functional>
#include <mutex>
#include <condition_variable>
#include <system_error>

#include "AsyncObserver.h"
#include "Logger.h"

//isable unreferenced variable warning
#pragma warning( disable : 4101 )

namespace helix
{
	namespace async
	{
		class MutableThread
		{
		private:
			std::thread m_Thread;
			std::function<void()> m_Function;
			bool m_bNotified;
			bool m_bRun;
			std::mutex m_LockMutex;
			std::mutex m_WaitMutex;
			std::condition_variable m_CV;
			IAsyncTemplateObserver<MutableThread>* m_Observer = nullptr;

		private:
			void Execute()
			{
				while (m_bRun)
				{
					while (m_bNotified == false)
					{
						std::unique_lock<std::mutex> wait(m_WaitMutex);
						m_CV.wait(wait);
					}

					//reset notification state
					m_bNotified = false;

					bool bFinished = false;

					// Execute the function
					{
						std::lock_guard<std::mutex> lock(m_LockMutex);

						if (m_bRun && m_Function)
						{
							m_Function();
							m_Function = std::function<void()>();

							bFinished = true;
						}
					}
					
					//signal the observer / threadpool
					if (bFinished && m_Observer != nullptr)
						m_Observer->Signal(this);
				}
			}

		public:
			HDEBUGNAME("MutableThread");

			MutableThread(const MutableThread& thread) = delete;

			MutableThread(IAsyncTemplateObserver<MutableThread>* _Observer)
			{
				m_Observer = _Observer;
				m_bNotified = false;
				m_bRun = true;
				m_Thread = std::thread(&MutableThread::Execute, this);
			}

			MutableThread()
			{
				m_Observer = nullptr;
				m_bNotified = false;
				m_bRun = true;
				m_Thread = std::thread(&MutableThread::Execute, this);
			}

			~MutableThread()
			{
				m_bRun = false;

				m_bNotified = true;
				m_CV.notify_one();

				try
				{
					if (m_Thread.joinable())
						m_Thread.join();
				}
				catch (std::system_error& ex)
				{
					HWARNINGD("%s", ex.what());
				}
			}

			inline bool Start(const std::function<void()>& f)
			{
				std::lock_guard<std::mutex> lock(m_LockMutex);

				if (m_Function != nullptr)
					return false;

				m_Function = f;

				m_bNotified = true;
				m_CV.notify_one();

				return true;
			}

			inline bool isFree()
			{
				std::lock_guard<std::mutex> lock(m_LockMutex);
				return !m_Function;
			}

			inline std::thread::id getId()
			{
				//std::lock_guard<std::mutex> lock(m_LockMutex);
				return m_Thread.get_id();
			}

			inline bool Join()
			{
				std::lock_guard<std::mutex> lock(m_LockMutex);

				m_bRun = false;

				m_bNotified = true;
				m_CV.notify_one();

				try
				{
					if (m_Thread.joinable())
						m_Thread.join();
				}
				catch (std::system_error& ex)
				{
					HWARNINGD("%s", ex.what());
				}
			}
		};
	}; // Namespace Async
}; // Namepspace Helix

#endif