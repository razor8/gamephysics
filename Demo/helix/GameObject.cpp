//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "GameObject.h"

using namespace helix::datastructures;
using namespace helix::math;

//---------------------------------------------------------------------------------------------------
//default constructor
GameObject::GameObject() : Flag<uint32_t>(GameObjectFlag_GameObject, &m_Mutex)
{
	m_uID = HUNDEFINED;
}
//---------------------------------------------------------------------------------------------------
//init constructor
GameObject::GameObject(const XMFLOAT3& _vPosition, const XMFLOAT3& _vScale, const XMFLOAT3& _vEulerAngles, GameObjectFlags _kFlags) :
Flag<uint32_t>(_kFlags | GameObjectFlag_GameObject, &m_Mutex)
{
	m_vPosition = _vPosition;
	m_vScale = _vScale;
	
	m_vOrientation = XMFloat4Get(XMQuaternionRotationRollPitchYaw(XMConvertToRadians(_vEulerAngles.x), XMConvertToRadians(_vEulerAngles.y), XMConvertToRadians(_vEulerAngles.z)));

	m_mRotation = XMFloat3x3Get(XMMatrixRotationRollPitchYaw(XMConvertToRadians(_vEulerAngles.x), XMConvertToRadians(_vEulerAngles.y), XMConvertToRadians(_vEulerAngles.z)));

	//get most significant dimension, divide Scale by 2 since BoundingVolume works with extends in each dimension form the midpoint
	const float fRadius = fmaxf(fmaxf(_vScale.x, _vScale.y), _vScale.z) /** 0.5f*/;

	m_BV = collision::BoundingVolume(m_vPosition, fRadius);
}
//---------------------------------------------------------------------------------------------------
//copy constructor
GameObject::GameObject(const GameObject& _GameObject) : Flag<uint32_t>(_GameObject.GetFlag() | GameObjectFlag_GameObject, &m_Mutex)
{
	//ClearFlag(GameObjectFlag_RigidBody | GameObjectFlag_MovingObject);

	//lock the other gameobject to prevent data being changed while copying
	std::lock_guard<std::recursive_mutex> Lock(_GameObject.m_Mutex);

	//IDs are unique, and only the GameObjectContainer should assign them
	m_uID = HUNDEFINED;

	m_uMeshID = _GameObject.m_uMeshID;
	m_pNode = _GameObject.m_pNode;

	m_BV = collision::BoundingVolume(_GameObject.m_BV);
	m_BV.SetMutex(&m_Mutex);

	m_vPosition = _GameObject.m_vPosition;
	m_vScale = _GameObject.m_vScale;

	m_vOrientation = _GameObject.m_vOrientation;
	m_mRotation = _GameObject.m_mRotation;
}
//---------------------------------------------------------------------------------------------------
//move constructor
GameObject::GameObject(GameObject&& _GameObject) : Flag<uint32_t>(_GameObject.GetFlag() | GameObjectFlag_GameObject, &m_Mutex)
{
	//ClearFlag(GameObjectFlag_RigidBody | GameObjectFlag_MovingObject);

	std::lock_guard<std::recursive_mutex> Lock(_GameObject.m_Mutex);
	
	//IDs are unique, and only the GameObjectContainer should assign them
	m_uID = _GameObject.m_uID;
	//invalidate old gameobject
	_GameObject.m_uID = HUNDEFINED;

	m_uMeshID = _GameObject.m_uMeshID;
	m_pNode = _GameObject.m_pNode;

	m_BV = std::move(_GameObject.m_BV);
	m_BV.SetMutex(&m_Mutex);

	m_vPosition = std::move(_GameObject.m_vPosition);
	m_vScale = std::move(_GameObject.m_vScale);

	m_vOrientation = std::move(_GameObject.m_vOrientation);
	m_mRotation = std::move(_GameObject.m_mRotation);
}
//---------------------------------------------------------------------------------------------------
GameObject& GameObject::operator=(GameObject& _Other)
{
	if (this != &_Other)
	{
		std::lock_guard<std::recursive_mutex> MyLock(m_Mutex);

		//GetFlag locks the other mutex so this has to be done before locking it manually below
		SetFlag(_Other.GetFlag() | GameObjectFlag_GameObject);
		ClearFlag(GameObjectFlag_RigidBody | GameObjectFlag_MovingObject);

		std::lock_guard<std::recursive_mutex> OtherLock(_Other.m_Mutex);

		m_uID = HUNDEFINED;

		m_uMeshID = _Other.m_uMeshID;
		m_pNode = _Other.m_pNode;

		m_BV = _Other.m_BV;
		m_BV.SetMutex(&m_Mutex);

		m_vPosition = _Other.m_vPosition;
		m_vScale = _Other.m_vScale;

		m_vOrientation = _Other.m_vOrientation;
		m_mRotation = _Other.m_mRotation;
	}

	return *this;
}
//---------------------------------------------------------------------------------------------------
GameObject::~GameObject()
{
	//HSAFE_DELETE(m_pBV);
}
//---------------------------------------------------------------------------------------------------
void GameObject::UpdateBoundingVolumes()
{
	std::lock_guard<std::recursive_mutex> Lock(m_Mutex);
	m_BV.Update(m_vPosition, m_vOrientation);
}
//---------------------------------------------------------------------------------------------------
GameObjectFlags GameObject::GetType()
{
	if (CheckFlag(GameObjectFlag_MovingObject))
		return GameObjectFlag_MovingObject;
	if (CheckFlag(GameObjectFlag_RigidBody))
		return GameObjectFlag_RigidBody;

	return GameObjectFlag_GameObject;
}
//---------------------------------------------------------------------------------------------------