//Copyright(c) 2015
//Authors: Fabian Wahlster, Steffen Wiewel, Moritz Becher

#ifndef CAMERA_H
#define CAMERA_H

#include "XMMath.h"
#include "XMCollision.h"

#include "Effects.h"
#include "VertexTypes.h"
#include "PrimitiveBatch.h"
#include <DXUT.h>

namespace helix
{
	namespace scene
	{
		using namespace DirectX;

		class Camera
		{
		public:
			HDEBUGNAME("Camera");

			Camera();

			// Getters & Setters

			// Position
			const XMFLOAT3& GetPosition() const;
			void SetPosition(const XMFLOAT3& _vPos);

			// Camera Basis Vector in world space
			const XMFLOAT3& GetUp() const;
			const XMFLOAT3& GetRight() const;
			const XMFLOAT3& GetLook() const;

			// View & Proj Matrices
			const XMFLOAT4X4& GetView() const;
			const XMFLOAT4X4& GetProj() const;
			const XMFLOAT4X4& GetViewProj() const;

			// Frustum used for culling intersection tests
			const BoundingFrustum& GetViewFrustum() const;

			// Get frustum properties.
			float GetNearDistance() const;
			float GetFarDistance() const;
			float GetAspect() const;
			float GetFovY() const;
			float GetFovX() const;

			// Near and far plane dimensions in view space coordinates.
			float GetNearWindowWidth() const;
			float GetNearWindowHeight() const;
			float GetFarWindowWidth() const;
			float GetFarWindowHeight() const;

			// Calculate a new projection matrix
			void SetLens(const float _fFieldOfViewY, const float _fAspectRatio, const float _fNearDistance, const float _fFarDistance);
		
			// Transform
			void LookAt(const XMFLOAT3& _vTarget);
			void LookAtFrom(const XMFLOAT3& _vPos, const XMFLOAT3& _vTarget, const XMFLOAT3& _vUp = XMFLOAT3(0.0f, 1.0f, 0.0f));
			
			void Strafe(float _fDistance);
			void Elevate(float _fDistance);
			void Walk(float _fDistance);
			
			// camera coordinate rotation
			void RollPitchYaw(float _fRollAngle, float _fPitchAngle, float _fYawAngle);
			void Roll(float _fRollAngle);
			void Pitch(float _PitchAngle);
			void Yaw(float _fYawAngle);

			// world coordinate rotation
			void Rotate(float _fAngleX, float _fAngleY, float _fAngleZ);
			void RotateX(float _fAngleX);
			void RotateY(float _fAngleY);
			void RotateZ(float _fAngleZ);
	
			// Update view and view-projection matrix after transforming camera once per frame
			void Update();

			void DebugRender(BasicEffect* _pEffect, ID3D11DeviceContext* _pContext, PrimitiveBatch<VertexPositionColor>* _pBatch, ID3D11InputLayout* _pInputLayout);

		private:

			// View matrix must be recalculated after transforming the camera 
			void UpdateViewMatrix();
			void UpdateViewProjectionMatrix();
			void UpdateFrustum();

			// Camera coordinate system
			XMFLOAT3 m_vPosition;	// origin
			XMFLOAT3 m_vRight;		// x-axis
			XMFLOAT3 m_vUp;			// y-axis
			XMFLOAT3 m_vLook;		// z-axis

			// Frustum
			// 8 corners position of bounding frustum.
			//     Near    Far
			//    0----1  4----5
			//    |    |  |    |
			//    |    |  |    |
			//    3----2  7----6
			BoundingFrustum m_Frustum;
			BoundingFrustum m_TransformFrustum;

			// Frustum properties
			float m_fFovY;			// Y field of view
			float m_fAspectRatio;	// Width / Height
			float m_fNearDistance;
			float m_fFarDistance;
			float m_fNearWindowHeight;
			float m_fFarWindowHeight;


			// View & Projection matrices
			XMFLOAT4X4 m_mView;
			XMFLOAT4X4 m_mProj;
			XMFLOAT4X4 m_mViewProj;
		};

		// Position
		inline const XMFLOAT3& Camera::GetPosition() const { return m_vPosition; }
		inline void Camera::SetPosition(const XMFLOAT3& _vPos) { m_vPosition = _vPos; }

		// Camera Basis Vectors
		inline const XMFLOAT3& Camera::GetUp() const { return m_vUp; };
		inline const XMFLOAT3& Camera::GetRight() const { return m_vRight; };
		inline const XMFLOAT3& Camera::GetLook() const { return m_vLook; };

		// View & Projection matrices
		inline const XMFLOAT4X4& Camera::GetView() const { return m_mView; }
		inline const XMFLOAT4X4& Camera::GetProj() const { return m_mProj; }
		inline const XMFLOAT4X4& Camera::GetViewProj() const { return m_mViewProj; }

		inline const BoundingFrustum& Camera::GetViewFrustum() const { return m_Frustum; }

		// Get frustum properties.
		inline float Camera::GetNearDistance() const { return m_fNearDistance; }
		inline float Camera::GetFarDistance() const { return m_fFarDistance; }
		inline float Camera::GetAspect()const { return m_fAspectRatio; }
		inline float Camera::GetFovY()const { return m_fFovY; }

		// Near and far plane dimensions in view space coordinates.
		inline float Camera::GetNearWindowHeight() const { return m_fNearWindowHeight; }
		inline float Camera::GetFarWindowHeight() const { return m_fFarWindowHeight; }
		inline float Camera::GetNearWindowWidth() const { return m_fAspectRatio * m_fNearWindowHeight; }
		inline float Camera::GetFarWindowWidth() const { return m_fAspectRatio * m_fFarWindowHeight; }

	}; // scene
}; // helix

#endif // CAMERA_H