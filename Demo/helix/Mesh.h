//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

#ifndef MESH_H
#define MESH_H

#include <d3d11_2.h>
#include <vector>
#include <algorithm>
#include <map>

#include "Debug.h"
#include "XMMath.h"
#include "Effect.h"
#include "RenderDefines.h"

using namespace DirectX;

namespace helix {

	template <class T>
	class Mesh
	{
	public:
		HDEBUGNAME("Mesh");

		static void GenerateCube(float _fSize, _Out_ std::vector<T>& _VertexBufferData, _Out_ std::vector<uint32_t>& _IndexBufferData)
		{
			const int iFaceCount = 6;

			static const XMVECTOR FaceNormals[iFaceCount] =
			{
				{ 0, 0, 1 },
				{ 0, 0, -1 },
				{ 1, 0, 0 },
				{ -1, 0, 0 },
				{ 0, 1, 0 },
				{ 0, -1, 0 },
			};
			static const XMFLOAT2 TextureCoordinates[4] =
			{
				{ 1, 0 },
				{ 1, 1 },
				{ 0, 1 },
				{ 0, 0 },
			};

			_VertexBufferData.clear();
			_IndexBufferData.clear();

			_fSize *= 0.5f;

			T VertexData;

			// Create each face in turn.
			for (int i = 0; i < iFaceCount; i++)
			{
				XMVECTOR Normal = FaceNormals[i];

				// Get two vectors perpendicular both to the face normal and to each other.
				XMVECTOR Basis = (i >= 4) ? g_XMIdentityR2 : g_XMIdentityR1;

				XMVECTOR Side1 = XMVector3Cross(Normal, Basis);
				XMVECTOR Side2 = XMVector3Cross(Normal, Side1);

				// Six indices (two triangles) per face.
				uint32_t vbase = static_cast<uint32_t>(_VertexBufferData.size());
				_IndexBufferData.push_back(vbase + 0);
				_IndexBufferData.push_back(vbase + 1);
				_IndexBufferData.push_back(vbase + 2);

				_IndexBufferData.push_back(vbase + 0);
				_IndexBufferData.push_back(vbase + 2);
				_IndexBufferData.push_back(vbase + 3);

				// Four vertices per face
				VertexData.Clear();
				XMVECTOR PositionData = XMVectorScale(XMVectorSubtract(XMVectorSubtract(Normal, Side1), Side2), _fSize);
				FillVertexData(VertexData, PositionData, Normal, TextureCoordinates[0]);
				_VertexBufferData.push_back(VertexData);
				VertexData.Clear();
				PositionData = XMVectorScale(XMVectorAdd(XMVectorSubtract(Normal, Side1), Side2), _fSize);
				FillVertexData(VertexData, PositionData, Normal, TextureCoordinates[1]);
				_VertexBufferData.push_back(VertexData);
				VertexData.Clear();
				PositionData = XMVectorScale(XMVectorAdd(XMVectorAdd(Normal, Side1), Side2), _fSize);
				FillVertexData(VertexData, PositionData, Normal, TextureCoordinates[2]);
				_VertexBufferData.push_back(VertexData);
				VertexData.Clear();
				PositionData = XMVectorScale(XMVectorSubtract(XMVectorAdd(Normal, Side1), Side2), _fSize);
				FillVertexData(VertexData, PositionData, Normal, TextureCoordinates[3]);
				_VertexBufferData.push_back(VertexData);


				//_VertexBufferData.push_back(VertexPosition((Normal - Side1 - Side2) * _fSize));
				//_VertexBufferData.push_back(VertexPosition((Normal - Side1 + Side2) * _fSize));
				//_VertexBufferData.push_back(VertexPosition((Normal + Side1 + Side2) * _fSize));
				//_VertexBufferData.push_back(VertexPosition((Normal + Side1 - Side2) * _fSize));

				//_VertexBufferData.push_back(VertexPositionNormalTexture((Normal - Side1 - Side2) * _fSize, Normal, TextureCoordinates[0]));
				//_VertexBufferData.push_back(VertexPositionNormalTexture((Normal - Side1 + Side2) * _fSize, Normal, TextureCoordinates[1]));
				//_VertexBufferData.push_back(VertexPositionNormalTexture((Normal + Side1 + Side2) * _fSize, Normal, TextureCoordinates[2]));
				//_VertexBufferData.push_back(VertexPositionNormalTexture((Normal + Side1 - Side2) * _fSize, Normal, TextureCoordinates[3]));
			}
		}
		static void GenerateTriangle(float _fSize, _Out_ std::vector<T>& _VertexBufferData, _Out_ std::vector<uint32_t>& _IndexBufferData)
		{
			_VertexBufferData.clear();
			_IndexBufferData.clear();

			_fSize *= 0.5f;

			T VertexData = T();
			FillVertexData(VertexData, XMFLOAT3(-_fSize, -_fSize, 0.0f));
			//VertexData.m_vVertexPosition = XMFLOAT3(-_fSize, -_fSize, 0.0f);
			_VertexBufferData.push_back(VertexData);

			VertexData.Clear();
			FillVertexData(VertexData, XMFLOAT3(0.f, _fSize, 0.0f));
			//VertexData.m_vVertexPosition = XMFLOAT3(0.f, _fSize, 0.0f);
			_VertexBufferData.push_back(VertexData);

			VertexData.Clear();
			FillVertexData(VertexData, XMFLOAT3(_fSize, -_fSize, 0.0f));
			//VertexData.m_vVertexPosition = XMFLOAT3(_fSize, -_fSize, 0.0f);
			_VertexBufferData.push_back(VertexData);

			//_VertexBufferData.push_back(VertexPosition(XMFLOAT3(-_fSize, -_fSize, 0.0f)));
			//_VertexBufferData.push_back(VertexPosition(XMFLOAT3(0.f, _fSize, 0.0f)));
			//_VertexBufferData.push_back(VertexPosition(XMFLOAT3(_fSize, -_fSize, 0.0f)));

			_IndexBufferData.push_back(0);
			_IndexBufferData.push_back(1);
			_IndexBufferData.push_back(2);
		}

		//TODO: implement GenerateSphere()
		//void GenerateSphere();
	
	protected:
		Mesh() :
			m_VertexBuffer(nullptr),
			m_IndexBuffer(nullptr)
		{}
		Mesh(std::vector<T>& _VertexBufferData, std::vector<float>& _IndexBufferData) :
			m_VertexBufferData(_VertexBufferData),
			m_IndexBufferData(_IndexBufferData)
		{}
		~Mesh()
		{
			if (m_VertexBuffer)
			{
				m_VertexBuffer->Release();
				m_VertexBuffer = nullptr;
			}
			if (m_IndexBuffer)
			{
				m_IndexBuffer->Release();
				m_IndexBuffer = nullptr;
			}
		}
	public:
		virtual void Render(ID3D11DeviceContext* _pDeviceContext) = 0;

		static void Initialize(ID3D11Device* _pDevice);
		static void Destroy();
	private:
		static void FillVertexData(Renderer::VertexPosition& _VertexData, XMVECTOR _Position, XMVECTOR _Normal = XMVectorZero(), XMFLOAT2 _TextureCoordinates = XMFLOAT2_ZERO)
		{
			XMStoreFloat3(&_VertexData.m_vVertexPosition, _Position);
		}
		static void FillVertexData(Renderer::VertexPosition& _VertexData, XMFLOAT3 _Position, XMVECTOR _Normal = XMVectorZero(), XMFLOAT2 _TextureCoordinates = XMFLOAT2_ZERO)
		{
			_VertexData.m_vVertexPosition = _Position;
		}
		static void FillVertexData(Renderer::VertexPositionNormalTexture& _VertexData, XMVECTOR _Position, XMVECTOR _Normal, XMFLOAT2 _TextureCoordinates)
		{			
			XMStoreFloat3(&_VertexData.m_vVertexPosition, _Position);
			XMStoreFloat3(&_VertexData.m_vVertexNormal, _Normal);
			_VertexData.m_vTextureCoordinate = _TextureCoordinates;
		}
		static void FillVertexData(Renderer::VertexPositionNormalTexture& _VertexData, XMFLOAT3 _Position, XMFLOAT3 _Normal, XMFLOAT2 _TextureCoordinates)
		{
			_VertexData.m_vVertexPosition = _Position;
			_VertexData.m_vVertexNormal = _Normal;
			_VertexData.m_vTextureCoordinate = _TextureCoordinates;
		}
		//static void FillVertexData(T& _VertexData, XMVECTOR _Position, XMVECTOR _Normal = XMVectorZero(), XMFLOAT2 _TextureCoordinates = XMFLOAT2(0))
		//{
		//	switch (_VertexData.m_uID)
		//	{
		//	case Renderer::VertexType_VertexPositionNormalTexture:
		//		XMStoreFloat3(&_VertexData.m_vVertexPosition, _Position);
		//		XMStoreFloat3(&_VertexData.m_vVertexNormal, _Normal);
		//		_VertexData.m_vTextureCoordinate = _TextureCoordinates;
		//		break;
		//	case Renderer::VertexType_VertexPosition:
		//		XMStoreFloat3(&_VertexData.m_vVertexPosition, _Position);
		//		break;
		//	default:							
		//		HFATAL(TEXT("Cant find fitting VertexType!"));
		//		break;
		//	}
		//}

		//static void FillVertexData(T& _VertexData, XMFLOAT3 _Position, XMFLOAT3 _Normal = XMFLOAT3(0), XMFLOAT2 _TextureCoordinates = XMFLOAT2(0))
		//{
		//	switch (_VertexData.m_uID)
		//	{
		//	case Renderer::VertexType_VertexPositionNormalTexture:
		//		_VertexData.m_vVertexPosition = _Position;
		//		_VertexData.m_vVertexNormal = _Normal;
		//		_VertexData.m_vTextureCoordinate = _TextureCoordinates;
		//		break;
		//	case Renderer::VertexType_VertexPosition:
		//		_VertexData.m_vVertexPosition = _Position;
		//		break;
		//	default:				
		//		HFATAL(TEXT("Cant find fitting VertexType!"));
		//		break;
		//	}
		//}
	protected:
		std::vector<T> m_VertexBufferData;
		std::vector<uint32_t> m_IndexBufferData;

		ID3D11Buffer* m_VertexBuffer;
		ID3D11Buffer* m_IndexBuffer;
	};

}; // namespace helix

#endif