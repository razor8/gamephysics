//Copyright(c) 2015
//Authors: Fabian Wahlster, Steffen Wiewel, Moritz Becher

#include "AnimationController.h"
#include "StdAfxH.h"

using namespace helix::animation;

AnimationController::AnimationController()
{
	m_fTime = 0;
	m_bIsPaused = false;
	
}


AnimationController::~AnimationController()
{
	m_Vector3AnimationTracks.clear();
	//m_ScalarAnimationTracks.clear();
	//m_QuaternionAnimationTracks.clear();
}

void AnimationController::Initialize()
{

}

void AnimationController::Update(float _fDeltaTime)
{
	if (m_bIsPaused)
	{
		return;
	}

	m_fTime += _fDeltaTime;
	if (m_fTime > 10.0f)
	{
		m_fTime = 0;
	}
}

AnimationController::Vector3TrackHandle AnimationController::AddVector3Track(std::vector<XMFLOAT3>& _vPostitions, float _fStartTime, float _fEndTime)
{
	AnimationFlag DefaultFlag = static_cast<AnimationFlag> (AnimationFlag_BezierInterpolation);
	Vector3TrackHandle iHandle = m_ID.NextID();
	AnimationTrack<XMFLOAT3> Animation;
	Animation.BuildTrack(_vPostitions, _fStartTime, _fEndTime, DefaultFlag);
	m_Vector3AnimationTracks.emplace(iHandle, Animation);
	return iHandle;
}

XMFLOAT3 AnimationController::GetVector3(Vector3TrackHandle _Handle)
{
	XMFLOAT3 result(0.0f,0.0f, 0.0f);
	m_Vector3AnimationTracks.at(_Handle).GetAnimationData(result, m_fTime);
	return result;
}


//AnimationController::ScalarTrackHandle AnimationController::AddScalarTrack(std::vector<float>& _vValues, float _fStartTime, float _fEndTime)
//{
//	ScalarTrackHandle iHandle = m_ID.NextID();
//	AnimationTrack<float> Animation;
//	m_ScalarAnimationTracks.emplace(iHandle, Animation);
//	return iHandle;
//}
//
//AnimationController::QuaternionTrackHandle AnimationController::AddQuaternionTrack(std::vector<XMFLOAT4>& _vOrientations, float _fStartTime, float _fEndTime)
//{
//	QuaternionTrackHandle iHandle = m_ID.NextID();
//	AnimationTrack<XMFLOAT4> Animation;
//	m_QuaternionAnimationTracks.emplace(iHandle, Animation);
//	return iHandle;
//}