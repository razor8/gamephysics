//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef MOVINGOBJECTSYSTEM_H
#define MOVINGOBJECTSYSTEM_H

#include "PhysicsSystem.h"
#include "MovingObject.h"
#include "XMMath.h"
#include "XMIntegrators.h"

#include <vector>
#include <atomic>
#include <unordered_map>

using namespace DirectX;

namespace helix
{
	namespace physics
	{

		typedef std::vector<datastructures::MovingObject*> TMovingObjects;
		typedef std::unordered_multimap<uint32_t, XMFLOAT3> TMovingObjectForceMap;

		//All time values are in Seconds
		class MovingObjectSystem : public PhysicsSystem<TMovingObjects, TMovingObjectForceMap>
		{
		public:
			HDEBUGNAME("MovingObjectSystem");

			//don't allow copying
			MovingObjectSystem(const MovingObjectSystem& rbs) = delete;

			MovingObjectSystem();
			MovingObjectSystem(async::IAsyncObserver* _Observer);

			~MovingObjectSystem();

			//from PhysicsSystem
			virtual void ClearData();
			virtual void ClearExternalData();

			void SetDefaultForce(const XMFLOAT3& _DefaultForce);

			void SetIntegrator(math::XMIntegrator _Integrator);

		private:
			//from PhysicsSystem
			virtual void Simulate(float _fDeltaTime);

		private:
			math::XMIntegrator m_Integrator = math::XMIntegrator_Heun;
			XMFLOAT3 m_DefaultForce = XMFLOAT3_ZERO;

		};

	}; // namespace physics

}

#endif // MOVINGOBJECTSYSTEM_H