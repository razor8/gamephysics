//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

#ifndef TIMER_H
#define TIMER_H

#include <stdint.h>
#include <string>
#include "Debug.h"

namespace helix{
	class Timer{
	public:
		HDEBUGNAME("Timer");
		static int64_t GetCurrentCount();
		static size_t GetLocalTimeString(TCHAR* _pBuffer, size_t _BufferSize);

		Timer();		

		void Pause();
		void Unpause();
		void Reset();
		void FrameTick();
		float ElapsedTimeF();
		double ElapsedTimeD();

		double TotalTimeD();
	private:
		double m_secPerCount;
		double m_elapsedTime;

		//per frame
		int64_t m_prevTime;
		int64_t m_curTime;

		int64_t m_startTime;//time the timer was started

		int64_t m_pausedTime;//time when timer was last paused
		int64_t m_unpausedTime;//time when tiemr was last unpaused
		int64_t m_totalPausedTime; //time interval the timer was paused

		bool m_paused;
	};
}

#endif