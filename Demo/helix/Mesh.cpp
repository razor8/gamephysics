//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

//#include "Mesh.h"
//#include "Logger.h"
//
//using namespace helix;

//---------------------------------------------------------------------------------------------------
// Explicit class template specialization 
//template class Mesh < Renderer::VertexPosition >;
//template class Mesh < Renderer::VertexPositionNormalTexture > ;
//---------------------------------------------------------------------------------------------------
//template <class T>
//Mesh<T>::Mesh() :
//	m_VertexBuffer(nullptr),
//	m_IndexBuffer(nullptr),
//	m_VertexBufferData(NULL),
//	m_IndexBufferData(NULL)
//{
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//Mesh<T>::~Mesh()
//{
//	if (m_VertexBuffer)
//	{
//		m_VertexBuffer->Release();
//		m_VertexBuffer = nullptr;
//	}
//	if (m_IndexBuffer)
//	{
//		m_IndexBuffer->Release();
//		m_IndexBuffer = nullptr;
//	}
//}
//---------------------------------------------------------------------------------------------------
// Creates a triangle primitive
//template <class T>
//void Mesh<T>::GenerateTriangle(float _fSize, std::vector</*VertexPosition*/T>& _VertexBufferData, std::vector<uint32_t>& _IndexBufferData)
//{
//	_VertexBufferData.clear();
//	_IndexBufferData.clear();
//
//	_fSize *= 0.5f;
//
//	T VertexData = T();
//	FillVertexData(VertexData, XMFLOAT3(-_fSize, -_fSize, 0.0f));
//	//VertexData.m_vVertexPosition = XMFLOAT3(-_fSize, -_fSize, 0.0f);
//	_VertexBufferData.push_back(VertexData);
//
//	VertexData.Clear();
//	FillVertexData(VertexData, XMFLOAT3(0.f, _fSize, 0.0f));
//	//VertexData.m_vVertexPosition = XMFLOAT3(0.f, _fSize, 0.0f);
//	_VertexBufferData.push_back(VertexData);
//
//	VertexData.Clear();
//	FillVertexData(VertexData, XMFLOAT3(_fSize, -_fSize, 0.0f));
//	//VertexData.m_vVertexPosition = XMFLOAT3(_fSize, -_fSize, 0.0f);
//	_VertexBufferData.push_back(VertexData);
//	
//	//_VertexBufferData.push_back(VertexPosition(XMFLOAT3(-_fSize, -_fSize, 0.0f)));
//	//_VertexBufferData.push_back(VertexPosition(XMFLOAT3(0.f, _fSize, 0.0f)));
//	//_VertexBufferData.push_back(VertexPosition(XMFLOAT3(_fSize, -_fSize, 0.0f)));
//		
//	_IndexBufferData.push_back(0);
//	_IndexBufferData.push_back(1);
//	_IndexBufferData.push_back(2);
//}
//---------------------------------------------------------------------------------------------------
// Creates a cube primitive
//template <class T>
//void Mesh<T>::GenerateCube(float _fSize, std::vector</*VertexPosition*/T>& _VertexBufferData, std::vector<uint32_t>& _IndexBufferData)
//{
//	const int iFaceCount = 6;
//
//	static const XMVECTOR FaceNormals[iFaceCount] =
//	{
//		{ 0, 0, 1 },
//		{ 0, 0, -1 },
//		{ 1, 0, 0 },
//		{ -1, 0, 0 },
//		{ 0, 1, 0 },
//		{ 0, -1, 0 },
//	};
//
//	static const XMFLOAT2 TextureCoordinates[4] =
//	{
//		{ 1, 0 },
//		{ 1, 1 },
//		{ 0, 1 },
//		{ 0, 0 },
//	};
//
//	_VertexBufferData.clear();
//	_IndexBufferData.clear();
//
//	_fSize *= 0.5f;
//
//	T VertexData;
//
//	// Create each face in turn.
//	for (int i = 0; i < iFaceCount; i++)
//	{
//		XMVECTOR Normal = FaceNormals[i];
//
//		// Get two vectors perpendicular both to the face normal and to each other.
//		XMVECTOR Basis = (i >= 4) ? g_XMIdentityR2 : g_XMIdentityR1;
//
//		XMVECTOR Side1 = XMVector3Cross(Normal, Basis);
//		XMVECTOR Side2 = XMVector3Cross(Normal, Side1);
//
//		// Six indices (two triangles) per face.
//		uint32_t vbase = static_cast<uint32_t>(_VertexBufferData.size());
//		_IndexBufferData.push_back(vbase + 0);
//		_IndexBufferData.push_back(vbase + 1);
//		_IndexBufferData.push_back(vbase + 2);
//
//		_IndexBufferData.push_back(vbase + 0);
//		_IndexBufferData.push_back(vbase + 2);
//		_IndexBufferData.push_back(vbase + 3);
//
//		// Four vertices per face
//		Renderer::VertexPosition a = Renderer::VertexPosition();
//		VertexData.Clear();
//		XMVECTOR PositionData = XMVectorScale(XMVectorSubtract(XMVectorSubtract(Normal, Side1), Side2), _fSize);
//		FillVertexData(VertexData, PositionData, Normal, TextureCoordinates[0]);
//		_VertexBufferData.push_back(VertexData);
//		VertexData.Clear();
//		PositionData = XMVectorScale(XMVectorAdd(XMVectorSubtract(Normal, Side1), Side2), _fSize);
//		FillVertexData(VertexData, PositionData, Normal, TextureCoordinates[1]);
//		_VertexBufferData.push_back(VertexData);
//		VertexData.Clear();
//		PositionData = XMVectorScale(XMVectorAdd(XMVectorAdd(Normal, Side1), Side2), _fSize);
//		FillVertexData(VertexData, PositionData, Normal, TextureCoordinates[2]);
//		_VertexBufferData.push_back(VertexData);
//		VertexData.Clear();
//		PositionData = XMVectorScale(XMVectorSubtract(XMVectorAdd(Normal, Side1), Side2), _fSize);
//		FillVertexData(VertexData, PositionData, Normal, TextureCoordinates[3]);
//		_VertexBufferData.push_back(VertexData);
//
//		
//		//_VertexBufferData.push_back(VertexPosition((Normal - Side1 - Side2) * _fSize));
//		//_VertexBufferData.push_back(VertexPosition((Normal - Side1 + Side2) * _fSize));
//		//_VertexBufferData.push_back(VertexPosition((Normal + Side1 + Side2) * _fSize));
//		//_VertexBufferData.push_back(VertexPosition((Normal + Side1 - Side2) * _fSize));
//
//		//_VertexBufferData.push_back(VertexPositionNormalTexture((Normal - Side1 - Side2) * _fSize, Normal, TextureCoordinates[0]));
//		//_VertexBufferData.push_back(VertexPositionNormalTexture((Normal - Side1 + Side2) * _fSize, Normal, TextureCoordinates[1]));
//		//_VertexBufferData.push_back(VertexPositionNormalTexture((Normal + Side1 + Side2) * _fSize, Normal, TextureCoordinates[2]));
//		//_VertexBufferData.push_back(VertexPositionNormalTexture((Normal + Side1 - Side2) * _fSize, Normal, TextureCoordinates[3]));
//	}
//}
//---------------------------------------------------------------------------------------------------

//TODO: implement this... dont know how
//template <class T>
//void Mesh<T>::FillVertexData(T& _VertexData, XMVECTOR _Position, XMVECTOR _Normal, XMFLOAT2 _TextureCoordinates)
//{
//	switch (_VertexData.m_uID)
//	{
//	case Renderer::VertexType_VertexPositionNormalTexture:
//		XMStoreFloat3(&_VertexData.m_vVertexPosition, _Position);
//		//XMStoreFloat3(&_VertexData.m_vVertexNormal, _Normal);
//		//_VertexData.m_vTextureCoordinate = _TextureCoordinates;
//		break;
//	case Renderer::VertexType_VertexPosition:
//		XMStoreFloat3(&_VertexData.m_vVertexPosition, _Position);
//		break;
//	default:
//		break;
//		//HFATALD(TEXT("Cant find fitting VertexType!"));
//	}
//}
////---------------------------------------------------------------------------------------------------
//template <class T>
//void Mesh<T>::FillVertexData(T& _VertexData, XMFLOAT3 _Position, XMFLOAT3 _Normal, XMFLOAT2 _TextureCoordinates)
//{
//	switch (_VertexData.m_uID)
//	{
//	case Renderer::VertexType_VertexPositionNormalTexture:
//		_VertexData.m_vVertexPosition = _Position;
//		//_VertexData.m_vVertexNormal = _Normal;
//		//_VertexData.m_vTextureCoordinate = _TextureCoordinates;
//		break;
//	case Renderer::VertexType_VertexPosition:		
//		_VertexData.m_vVertexPosition = _Position;
//		break;
//	default:
//		break;
//		//HFATALD(TEXT("Cant find fitting VertexType!"));
//	}
//}