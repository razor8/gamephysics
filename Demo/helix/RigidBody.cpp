//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "RigidBody.h"
#include "XMMath.h"

using namespace helix::collision;
using namespace helix::datastructures;
using namespace helix::math;
//---------------------------------------------------------------------------------------------------
//default constructor
RigidBody::RigidBody()
{
	m_uID = HUNDEFINED;
	SetFlag(GameObjectFlag_RigidBody);
}
//---------------------------------------------------------------------------------------------------
//_fDamping is a value between [0..1]
RigidBody::RigidBody(const XMFLOAT3& _vPosition, const XMFLOAT3& _vScale, const XMFLOAT3& _vEulerAngles, float _fMass, float _fDamping, GameObjectFlags _kFlag, const XMFLOAT3& _vLinearVelocity, const XMFLOAT3& _vAngularVelocity) :
GameObject(_vPosition, _vScale, _vEulerAngles, static_cast<GameObjectFlags>(_kFlag | GameObjectFlag_RigidBody))
{
	ClearFlag(GameObjectFlag_MovingObject | GameObjectFlag_GameObject);

	bool bStatic = CheckFlag(GameObjectFlag_Static);

	m_fDamping = _fDamping;
	//set InvMass to 0 if object is static
	m_fInvMass = bStatic ? 0.f : 1.f / _fMass;
	m_vLinearVelocity = _vLinearVelocity; //v
	m_vAngularVelocity = _vAngularVelocity; //w

	//divide Scale by 2 since BoundingVolume works with extends in each dimension from the midpoint
	m_BV.SetNewOBB(m_vPosition, Mul(m_vScale, XMFLOAT3(0.5f, 0.5f, 0.5f)), m_vOrientation);
	//m_BV.SetNewAABB(m_vPosition, m_vScale);

	//construct rotation matrix from quaternion
	XMVECTOR r = XMLoadFloat4(&m_vOrientation);
	XMMATRIX R = XMMatrixRotationQuaternion(r);
	XMStoreFloat3x3(&m_mRotation, R);

	if (bStatic == false)
	{
		//compute cube inverted inertia tensor
		_fMass /= 12.f;

		std::shared_ptr<BoundingOrientedBox> OBB = m_BV.GetOBB();

		XMFLOAT3 dim = OBB->Extents;
		dim.x *= dim.x;
		dim.y *= dim.y;
		dim.z *= dim.z;

		//compute cube tensor
		m_mInvInertiaTensor = XMFLOAT3X3(_fMass * (dim.y + dim.z), 0, 0,
										0, _fMass * (dim.x + dim.z), 0,
										0, 0, _fMass * (dim.x + dim.y));
		//compute AngularMomentum
		XMMATRIX I = XMLoadFloat3x3(&m_mInvInertiaTensor);
		XMVECTOR w = XMLoadFloat3(&m_vAngularVelocity);
		w = XMVector3Transform(w, I); //L = Iw
		XMStoreFloat3(&m_vAngularMomentum, w); // store L
		
		//invert inertia tensor
		I = XMMatrixInverse(NULL, I);
		XMStoreFloat3x3(&m_mInvInertiaTensor, I);

		//rotate tensor into inital rotation state
		XMMATRIX Rt = XMMatrixTranspose(R);
		// R^T * I * R
		I = XMMatrixMultiply(Rt, I);
		I = XMMatrixMultiply(I, R);

		//compute angular velocity
		/*XMVECTOR L = XMLoadFloat3(&m_vAngularMomentum);
		XMVECTOR w = XMVector3Transform(L, I);
		XMStoreFloat3(&m_vAngularVelocity, w);*/
	}
	else
	{
		// object is static => inv inertia tensor = 0 => L = 0 => w = 0
		m_mInvInertiaTensor = XMFLOAT3X3_ZERO;
		m_vAngularMomentum = XMFLOAT3_ZERO;
		m_vAngularVelocity = XMFLOAT3_ZERO;
	}
}

//copy constructor
RigidBody::RigidBody(const RigidBody& _Other) : GameObject(_Other)
{
	//lock the other gameobject to prevent data being changed while copying
	std::lock_guard<std::recursive_mutex> Lock(_Other.m_Mutex);

	SetFlag(GameObjectFlag_RigidBody);
	ClearFlag(GameObjectFlag_MovingObject | GameObjectFlag_GameObject);

	//RigidBody properties
	m_fDamping = _Other.m_fDamping;
	m_fInvMass = _Other.m_fInvMass;
	m_vLinearVelocity = _Other.m_vLinearVelocity;
	m_vAngularVelocity = _Other.m_vAngularVelocity;
	m_vAngularMomentum = _Other.m_vAngularMomentum;
	m_mInvInertiaTensor = _Other.m_mInvInertiaTensor;
}
//---------------------------------------------------------------------------------------------------
//move constructor
RigidBody::RigidBody(RigidBody&& _Other) : GameObject(std::move(_Other))
{
	std::lock_guard<std::recursive_mutex> Lock(_Other.m_Mutex);

	SetFlag(GameObjectFlag_RigidBody);
	ClearFlag(GameObjectFlag_MovingObject | GameObjectFlag_GameObject);

	//RigidBody properties
	m_fDamping = std::move(_Other.m_fDamping);
	m_fInvMass = std::move(_Other.m_fInvMass);
	m_vLinearVelocity = std::move(_Other.m_vLinearVelocity);
	m_vAngularVelocity = std::move(_Other.m_vAngularVelocity);
	m_vAngularMomentum = std::move(_Other.m_vAngularMomentum);
	m_mInvInertiaTensor = std::move(_Other.m_mInvInertiaTensor);
}
//---------------------------------------------------------------------------------------------------
//assignment operator
RigidBody& RigidBody::operator=(RigidBody& _Other)
{
	if (this != &_Other)
	{
		GameObject::operator=(_Other);

		std::lock_guard<std::recursive_mutex> OtherLock(_Other.m_Mutex);
		std::lock_guard<std::recursive_mutex> MyLock(m_Mutex);

		//RigidBody properties
		m_fDamping = _Other.m_fDamping;
		m_fInvMass = _Other.m_fInvMass;
		m_vLinearVelocity = _Other.m_vLinearVelocity;
		m_vAngularVelocity = _Other.m_vAngularVelocity;
		m_vAngularMomentum = _Other.m_vAngularMomentum;
		m_mInvInertiaTensor = _Other.m_mInvInertiaTensor;
	}

	return *this;
}
//---------------------------------------------------------------------------------------------------
//destructor
RigidBody::~RigidBody()
{

}
//---------------------------------------------------------------------------------------------------
float RigidBody::ComputeImpulse(RigidBody& _RBa, RigidBody& _RBb, const CollisionInfo::Contact& Contact)
{
	std::lock_guard<std::recursive_mutex> MyLock(_RBa.m_Mutex);
	std::lock_guard<std::recursive_mutex> OtherLock(_RBb.m_Mutex);

	// P = collision point
	const XMVECTOR P = XMLoadFloat3(&Contact.m_vPoint);
	// n = collision normal
	const XMVECTOR N = XMLoadFloat3(&Contact.m_vNormal);

	// Xa = center of mass position of RB A
	const XMVECTOR X_a = XMLoadFloat3(&_RBa.m_vPosition);
	// R_ap = distance vector from center of mass of RB A to the collision point P
	const XMVECTOR R_ap = XMVectorSubtract(P, X_a);

	// Xb = center of mass position of RB B
	const XMVECTOR X_b = XMLoadFloat3(&_RBb.m_vPosition);
	// R_bp = distance vector from center of mass of RB B to the collision point P
	const XMVECTOR R_bp = XMVectorSubtract(P, X_b);

	// V_a = linear velocity of RB A
	const XMVECTOR V_a = XMLoadFloat3(&_RBa.m_vLinearVelocity);
	// W_a = angular velocity of RB A
	const XMVECTOR W_a = XMLoadFloat3(&_RBa.m_vAngularVelocity);
	// V_ap = velocity of rotating and translating RB A at collision point P
	// V_ap = V_a + (W_a x R_ap)
	const XMVECTOR V_ap = XMVectorAdd(V_a, XMVector3Cross(W_a, R_ap));

	// V_b = linear velocity of RB B
	const XMVECTOR V_b = XMLoadFloat3(&_RBb.m_vLinearVelocity);
	// W_b = angular velocity of RB B
	const XMVECTOR W_b = XMLoadFloat3(&_RBb.m_vAngularVelocity);
	// V_bp = velocity of rotating and translating RB B at collision point P
	// V_bp = V_b + (W_b x R_bp)
	const XMVECTOR V_bp = XMVectorAdd(V_b, XMVector3Cross(W_b, R_bp));

	// V_rel = relative collision speed of RB A and RB B
	// V_rel = V_ap - V_bp
	const XMVECTOR V_rel = XMVectorSubtract(V_ap, V_bp);


	// ###############################################################################################################################################
	// ##																																			##
	// ##	Impulse calculation																														##
	// ##																																			##
	// ##	j = ( -(1+c) * (V_rel dot N) )  /  ( M_a_inv + M_b_inv + [((I_a_inv * (R_ap x N)) x R_ap) + ((I_b_inv * (R_bp x N)) x R_bp)] dot N )	##
	// ##																																			##
	// ###############################################################################################################################################

	const XMMATRIX I_a_inv = XMLoadFloat3x3(&_RBa.m_mInvInertiaTensor);
	const XMMATRIX I_b_inv = XMLoadFloat3x3(&_RBb.m_mInvInertiaTensor);
	// c = factor of restitution
	float c = 0.f; // 0 = inelastic; 1 = perfectly elastic

	// ##########################################
	// ## Dividend: ( -(1+c) * (V_rel dot N) ) ##
	// ##########################################

	// vDividend = V_rel dot N
	XMVECTOR vDividend = XMVector3Dot(V_rel, N);
	// vDividend = ( -(1+c) * vDividend ) = ( -(1+c) * (V_rel dot N) )
	vDividend = XMVectorScale(vDividend, -(1.f + c));
	float fDividend = XMVectorGetX(vDividend);

	// ###################################################################################################################
	// ## Divisor: ( M_a_inv + M_b_inv + [((I_a_inv * (R_ap x N)) x R_ap) + ((I_b_inv * (R_bp x N)) x R_bp)] dot N )	##
	// ###################################################################################################################

	// fDivisor = M_a_inv + M_b_inv
	float fDivisor = _RBa.m_fInvMass + _RBb.m_fInvMass;

	// tmp = (R_ap x N)
	XMVECTOR tmp = XMVector3Cross(R_ap, N);
	// tmp = (I_a_inv * (R_ap x N)) 
	tmp = XMVector3Transform(tmp, I_a_inv);
	// tmp = tmp x R_ap
	tmp = XMVector3Cross(tmp, R_ap);

	// vDivisor = ((I_a_inv * (R_ap x N)) x R_ap) 
	XMVECTOR vDivisor = tmp;

	// Reset tmp to Zero
	tmp = XMVectorZero();

	// tmp = (R_bp x N)
	tmp = XMVector3Cross(R_bp, N);
	// tmp = (I_b_inv * (R_bp x N)) 
	tmp = XMVector3Transform(tmp, I_b_inv);
	// tmp = tmp x R_bp
	tmp = XMVector3Cross(tmp, R_bp);

	// vDivisor = [((I_a_inv * (R_ap x N)) x R_ap) + ((I_b_inv * (R_bp x N)) x R_bp)]
	vDivisor = XMVectorAdd(vDivisor, tmp);
	// vDivisor = vDivisor dot N
	vDivisor = XMVector3Dot(vDivisor, N);

	// fDivisor += [((I_a_inv * (R_ap x N)) x R_ap) + ((I_b_inv * (R_bp x N)) x R_bp)] dot N
	fDivisor += XMVectorGetX(vDivisor);

	// ############################
	// ## J: Dividend/Divisor	 ##
	// ############################

	return fDividend / fDivisor;
}

//---------------------------------------------------------------------------------------------------
void RigidBody::ApplyImpulse(float _fMomentum, const CollisionInfo::Contact _Contact)
{
	// P = collision point
	const XMVECTOR P = XMLoadFloat3(&_Contact.m_vPoint);
	// n = collision normal
	const XMVECTOR N = XMLoadFloat3(&_Contact.m_vNormal);

	// X = center of mass position of RB
	const XMVECTOR X = XMLoadFloat3(&m_vPosition);
	// R_xp = distance vector from center of mass of RB to the collision point P
	const XMVECTOR R_xp = XMVectorSubtract(P, X);

	// Jn = j * N
	XMVECTOR Jn = XMVectorScale(N, _fMomentum);

	// L = angular momentum of RB
	XMVECTOR L = XMLoadFloat3(&m_vAngularMomentum);

	// L = L + (R_xp x Jn)
	L = XMVectorAdd(L, XMVector3Cross(R_xp, Jn));
	XMStoreFloat3(&m_vAngularMomentum, L);

	// v = linear velocity of RB
	XMVECTOR v = XMLoadFloat3(&m_vLinearVelocity);

	// Jn = Jn * M_inv
	Jn = XMVectorScale(Jn, m_fInvMass);
	//v = v + Jn/M
	v = XMVectorAdd(v, Jn);
	XMStoreFloat3(&m_vLinearVelocity, v);


	// ###########################################################################
	// ##																		##
	// ##	Position Correction: Transformation									##
	// ##	(r = orientation quaternion; P = collision point; X = COM of RB)	##
	// ##																		##
	// ##	X_new = X + Normalize(-R_xp) * fPositionOffsetScale					##
	// ##																		##
	// ###########################################################################

	// Penetration-Hack: Shift Objects away from each other, by offsetting their position in collision direction
	// get normalized vector pointing from collision point to own center of mass...
	// R_px = Normalize(-R_xp)
	XMVECTOR R_px = XMVector3NormalizeEst(XMVectorNegate(R_xp));

	// and scale the resulting vector to receive an infinitesimal small direction vector
	// R_px = Normalize(-R_xp) * fPositionOffsetScale	
	static const float fPositionOffsetScale = 0.002f;
	R_px = XMVectorScale(R_px, fPositionOffsetScale);

	// calculate the new position by offsetting it with the calculated direction vector
	// X_new = X + Normalize(-R_xp) * fPositionOffsetScale	
	const XMVECTOR X_new = XMVectorAdd(X, R_px);
	XMStoreFloat3(&m_vPosition, X_new);

	// Lecture Alternative:

	//// Lecture-Position-Hack:
	//// X_new = X + Jn/M
	//const XMVECTOR X_new = XMVectorAdd(X, Jn);
	//XMStoreFloat3(&m_vPosition, X_new);


	//// #########################################################################
	//// ##																		##
	//// ##	Position Correction: Orientation									##
	//// ##	(r = orientation quaternion; P = collision point; X = COM of RB)	##
	//// ##																		##
	//// ##	r_new = r + [I_inv * (P x Jn)] / [(I_inv * (X x Jn) x X) Dot N]		##
	//// ##																		##
	//// #########################################################################

	//const XMVECTOR r = XMLoadFloat4(&m_vOrientation);
	//const XMMATRIX I_inv = XMLoadFloat3x3(&m_mInvInertiaTensor);

	//// ###################################
	//// ##	Dividend: Rotation Change	##
	//// ##	dr = I_inv * (P x Jn)		##
	//// ###################################
	//
	//// vDividend = (P x Jn)
	//XMVECTOR vDividend = XMVector3Cross(P, Jn);
	//// vDividend = I_inv * (P x Jn)
	//vDividend = XMVector3Transform(vDividend, I_inv);

	//if (XMVectorGetX(XMVector3LengthEst(vDividend)) <= FLT_EPSILON)
	//	return;

	//// ###############################################
	//// ##	Divisor: angular component of inertia	##
	//// ##	k = (I_inv * (X x Jn) x X) Dot N		##
	//// ###############################################

	//// vDivisor = (X x Jn)
	//XMVECTOR vDivisor = XMVector3Cross(X, Jn);
	//// vDivisor = vDivisor x X
	//vDivisor = XMVector3Cross(vDivisor, X);
	//// vDivisor = I_inv * (X x Jn)
	//vDivisor = XMVector3Transform(vDivisor, I_inv);
	//// vDivisor = vDivisor Dot N
	//vDivisor = XMVector3Dot(vDivisor, N);

	//float fDivisor = XMVectorGetX(vDivisor);

	//if (fDivisor <= FLT_EPSILON)
	//	return;

	//// ###################################################
	//// ##	Orientation Update:							##
	//// ##	r_new = r + vDividend ScaleWith 1/fDivisor	##
	//// ###################################################

	//XMVECTOR vTmp = XMVectorScale(vDividend, 1.f/fDivisor);
	//XMVECTOR r_new = XMVectorAdd(r, vTmp);

	//// normalize rotation quaternion
	//r_new = XMVector4Normalize(r_new);
	//// store rotation quaternion
	//XMStoreFloat4(&m_vOrientation, r_new);
	//// convert quaternion r to rotation matrix
	//XMMATRIX R = XMMatrixRotationQuaternion(r_new);
	//XMStoreFloat3x3(&m_mRotation, R);
}