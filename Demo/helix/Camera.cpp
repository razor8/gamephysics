#include "Camera.h"

using namespace helix::scene;

//---------------------------------------------------------------------------------
Camera::Camera()
{
	LookAtFrom(XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT3(0.0f, 1.0f, 0.0f));
	SetLens(4.0f * XM_PI, 1.0f, 1.0f, 100'000.0f);
}

//=================================================================================
// Getters & Setters
//=================================================================================

//---------------------------------------------------------------------------------
// Calculate a new projection matrix
void Camera::SetLens(const float _fFieldOfViewY, const float _fAspectRatio, const float _fNearDistance, const float _fFarDistance)
{
	m_fFovY = _fFieldOfViewY;
	m_fAspectRatio = _fAspectRatio;
	m_fNearDistance = _fNearDistance;
	m_fFarDistance = _fFarDistance;

	m_fNearWindowHeight = 2.0f * m_fNearDistance * tanf(0.5f * m_fFovY);
	m_fFarWindowHeight = 2.0f * m_fFarDistance * tanf(0.5f * m_fFovY);

	XMMATRIX mProjectionMatrix = XMMatrixPerspectiveFovLH(m_fFovY, m_fAspectRatio, m_fNearDistance, m_fFarDistance);
	XMStoreFloat4x4(&m_mProj, mProjectionMatrix);

	UpdateViewMatrix();
	UpdateViewProjectionMatrix();

	//XMMATRIX mViewProj = XMLoadFloat4x4(&m_mViewProj);
	BoundingFrustum::CreateFromMatrix(m_Frustum, mProjectionMatrix);
	BoundingFrustum::CreateFromMatrix(m_TransformFrustum, mProjectionMatrix);
	
	UpdateFrustum();
}

//---------------------------------------------------------------------------------
// Field of view in x axis must be calculated
float Camera::GetFovX() const 
{
	float halfWidth = 0.5f * GetNearWindowWidth();
	return 2.0f * atan(halfWidth / m_fNearDistance);
}

//=================================================================================
// Transform camera
//=================================================================================

//---------------------------------------------------------------------------------
// Look at point in world space
void Camera::LookAt(const XMFLOAT3& _vTarget)
{
	XMVECTOR vPos = XMLoadFloat3(&m_vPosition);
	XMVECTOR vUp = XMLoadFloat3(&m_vUp);
	XMVECTOR vLook = XMVector3Normalize(XMVectorSubtract(XMLoadFloat3(&_vTarget), vPos));
	
	// this allows flipped cameras
	if (XMVectorGetY(vUp) >= 0.0f)
	{
		vUp = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	}
	else
	{
		vUp = XMVectorSet(0.0f, -1.0f, 0.0f, 0.0f);
	}

	// Compute new right vector from the world space up vector and the look vector
	//	^ Up' := (0, Up.y / |Up.y| , 0)
	//	|   
	//	|  ^ Look 
	//	| /
	//	|/
	//	'------------> Right = Up' x Look
	XMVECTOR vRight = XMVector3Cross(vUp, vLook);

	// Compute new up vector from the right and the look vector
	//	^ Up = Look x Right
	//	|   
	//	|  ^ Look 
	//	| /
	//	|/
	//	'------------> Right		
	vUp = XMVector3Cross(vLook, vRight);
	
	XMStoreFloat3(&m_vRight, vRight);
	XMStoreFloat3(&m_vUp, vUp);
	XMStoreFloat3(&m_vLook, vLook);
}

//---------------------------------------------------------------------------------
// Look at a point in world space, from a new position
void Camera::LookAtFrom(const XMFLOAT3& _vPos, const XMFLOAT3& _vTarget, const XMFLOAT3& _vUp)
{
	XMVECTOR vPos = XMLoadFloat3(&_vPos);
	XMVECTOR vUp = XMLoadFloat3(&_vUp);
	// Look = Target - Position
	XMVECTOR vLook = XMVector3Normalize(XMVectorSubtract(XMLoadFloat3(&_vTarget), vPos));

	if (XMComparisonAllTrue(XMVector3EqualR(vLook, XMVECTOR_ZERO)))
	{
		//Use previous look dir if new one is invalid (zero)
		vLook = XMLoadFloat3(&m_vLook);
	}
	else
	{
		//store new look dir
		XMStoreFloat3(&m_vLook, vLook);
	}

	XMStoreFloat3(&m_vPosition, vPos);
	// Right = Up x Look
	XMStoreFloat3(&m_vRight, XMVector3Cross(vUp, vLook));
	XMStoreFloat3(&m_vUp, vUp);	
}

//---------------------------------------------------------------------------------
// Movement

// move along camera's right vector
void Camera::Strafe(float _fDistance)
{
	// m_vPosition += _fDistance * m_vRight
	XMVECTOR vDistance = XMVectorReplicate(_fDistance);
	XMVECTOR vRight = XMLoadFloat3(&m_vRight);
	XMVECTOR vPosition = XMLoadFloat3(&m_vPosition);
	XMStoreFloat3(&m_vPosition, XMVectorMultiplyAdd(vDistance, vRight, vPosition));
}
//---------------------------------------------------------------------------------
// move along camera's up vector
void Camera::Elevate(float _fDistance)
{
	// m_vPosition += _fDistance * m_vUp
	XMVECTOR vDistance = XMVectorReplicate(_fDistance);
	XMVECTOR vUp = XMLoadFloat3(&m_vUp);
	XMVECTOR vPosition = XMLoadFloat3(&m_vPosition);
	XMStoreFloat3(&m_vPosition, XMVectorMultiplyAdd(vDistance, vUp, vPosition));
}
//---------------------------------------------------------------------------------
// move along camera's look vector
void Camera::Walk(float _fDistance)
{
	// m_vPosition += _fDistance * m_vLook
	XMVECTOR vDistance = XMVectorReplicate(_fDistance);
	XMVECTOR vLook = XMLoadFloat3(&m_vLook);
	XMVECTOR vPosition = XMLoadFloat3(&m_vPosition);
	XMStoreFloat3(&m_vPosition, XMVectorMultiplyAdd(vDistance, vLook, vPosition));
}

//---------------------------------------------------------------------------------
// Orientation

// camera coordinate rotation
void Camera::RollPitchYaw(float _fRollAngle, float _fPitchAngle, float _fYawAngle)
{
	this->Roll(_fRollAngle);
	this->Pitch(_fPitchAngle);
	this->Yaw(_fYawAngle);
}
//---------------------------------------------------------------------------------
void Camera::Roll(float _fRollAngle)
{
	XMMATRIX R = XMMatrixRotationAxis(XMLoadFloat3(&m_vLook), _fRollAngle);
	XMStoreFloat3(&m_vRight, XMVector3TransformNormal(XMLoadFloat3(&m_vRight), R));
	XMStoreFloat3(&m_vUp, XMVector3TransformNormal(XMLoadFloat3(&m_vUp), R));
}
//---------------------------------------------------------------------------------
void Camera::Pitch(float _PitchAngle)
{
	XMMATRIX R = XMMatrixRotationAxis(XMLoadFloat3(&m_vRight), _PitchAngle);
	XMStoreFloat3(&m_vUp, XMVector3TransformNormal(XMLoadFloat3(&m_vUp), R));
	XMStoreFloat3(&m_vLook, XMVector3TransformNormal(XMLoadFloat3(&m_vLook), R));
}
//---------------------------------------------------------------------------------
void Camera::Yaw(float _fYawAngle)
{
	XMMATRIX R = XMMatrixRotationAxis(XMLoadFloat3(&m_vUp), _fYawAngle);
	XMStoreFloat3(&m_vRight, XMVector3TransformNormal(XMLoadFloat3(&m_vRight), R));
	XMStoreFloat3(&m_vLook, XMVector3TransformNormal(XMLoadFloat3(&m_vLook), R));
}
//---------------------------------------------------------------------------------
// world coordinate rotation
void Camera::Rotate(float _fAngleX, float _fAngleY, float _fAngleZ)
{
	XMMATRIX R = XMMatrixRotationRollPitchYaw(_fAngleX, _fAngleY, _fAngleZ);
	XMStoreFloat3(&m_vRight, XMVector3TransformNormal(XMLoadFloat3(&m_vRight), R));
	XMStoreFloat3(&m_vUp, XMVector3TransformNormal(XMLoadFloat3(&m_vUp), R));
	XMStoreFloat3(&m_vLook, XMVector3TransformNormal(XMLoadFloat3(&m_vLook), R));
}
//---------------------------------------------------------------------------------
void Camera::RotateX(float _fAngleX)
{
	XMMATRIX R = XMMatrixRotationX(_fAngleX);
	XMStoreFloat3(&m_vRight, XMVector3TransformNormal(XMLoadFloat3(&m_vRight), R));
	XMStoreFloat3(&m_vUp, XMVector3TransformNormal(XMLoadFloat3(&m_vUp), R));
	XMStoreFloat3(&m_vLook, XMVector3TransformNormal(XMLoadFloat3(&m_vLook), R));
}
//---------------------------------------------------------------------------------
void Camera::RotateY(float _fAngleY)
{
	XMMATRIX R = XMMatrixRotationY(_fAngleY);
	XMStoreFloat3(&m_vRight, XMVector3TransformNormal(XMLoadFloat3(&m_vRight), R));
	XMStoreFloat3(&m_vUp, XMVector3TransformNormal(XMLoadFloat3(&m_vUp), R));
	XMStoreFloat3(&m_vLook, XMVector3TransformNormal(XMLoadFloat3(&m_vLook), R));
}
//---------------------------------------------------------------------------------
void Camera::RotateZ(float _fAngleZ)
{
	XMMATRIX R = XMMatrixRotationZ(_fAngleZ);
	XMStoreFloat3(&m_vRight, XMVector3TransformNormal(XMLoadFloat3(&m_vRight), R));
	XMStoreFloat3(&m_vUp, XMVector3TransformNormal(XMLoadFloat3(&m_vUp), R));
	XMStoreFloat3(&m_vLook, XMVector3TransformNormal(XMLoadFloat3(&m_vLook), R));
}

//=================================================================================
// Update
//=================================================================================

//---------------------------------------------------------------------------------
// Update Camera, called at the beginning of each frame
void Camera::Update()
{
	UpdateViewMatrix();
	UpdateViewProjectionMatrix();
	UpdateFrustum();
}
//---------------------------------------------------------------------------------
void Camera::DebugRender(BasicEffect* _pEffect, ID3D11DeviceContext* _pContext, PrimitiveBatch<VertexPositionColor>* _pBatch, ID3D11InputLayout* _pInputLayout)
{
	// Setup position/color effect
	_pEffect->SetWorld(DirectX::XMMatrixIdentity());

	_pEffect->Apply(_pContext);
	_pContext->IASetInputLayout(_pInputLayout);

	XMFLOAT3 vCorners[8];
	
	m_Frustum.GetCorners(vCorners);
	XMVECTOR vCornersXM[8];
	
	for (int i = 0; i < 8; ++i)
	{
		vCornersXM[i] = XMLoadFloat3(&vCorners[i]);
	}

	_pBatch->Begin();

	// Near Plane
	// Top
	_pBatch->DrawLine(
		VertexPositionColor(vCornersXM[0], Colors::Red),
		VertexPositionColor(vCornersXM[1], Colors::Red)
		);
	// Right
	_pBatch->DrawLine(
		VertexPositionColor(vCornersXM[1], Colors::Red),
		VertexPositionColor(vCornersXM[2], Colors::Red)
		);
	// Bottom
	_pBatch->DrawLine(
		VertexPositionColor(vCornersXM[2], Colors::Red),
		VertexPositionColor(vCornersXM[3], Colors::Red)
		);
	// Left
	_pBatch->DrawLine(
		VertexPositionColor(vCornersXM[3], Colors::Red),
		VertexPositionColor(vCornersXM[0], Colors::Red)
		);


	// Far Plane
	// Top
	_pBatch->DrawLine(
		VertexPositionColor(vCornersXM[4], Colors::Red),
		VertexPositionColor(vCornersXM[5], Colors::Red)
		);
	// Right
	_pBatch->DrawLine(
		VertexPositionColor(vCornersXM[5], Colors::Red),
		VertexPositionColor(vCornersXM[6], Colors::Red)
		);
	// Bottom
	_pBatch->DrawLine(
		VertexPositionColor(vCornersXM[6], Colors::Red),
		VertexPositionColor(vCornersXM[7], Colors::Red)
		);
	// Left
	_pBatch->DrawLine(
		VertexPositionColor(vCornersXM[7], Colors::Red),
		VertexPositionColor(vCornersXM[4], Colors::Red)
		);


	// Connections
	// Top Left
	_pBatch->DrawLine(
		VertexPositionColor(vCornersXM[0], Colors::Red),
		VertexPositionColor(vCornersXM[4], Colors::Red)
		);
	// Top Right
	_pBatch->DrawLine(
		VertexPositionColor(vCornersXM[1], Colors::Red),
		VertexPositionColor(vCornersXM[5], Colors::Red)
		);
	// Bottom Right
	_pBatch->DrawLine(
		VertexPositionColor(vCornersXM[2], Colors::Red),
		VertexPositionColor(vCornersXM[6], Colors::Red)
		);
	// Bottom Left
	_pBatch->DrawLine(
		VertexPositionColor(vCornersXM[3], Colors::Red),
		VertexPositionColor(vCornersXM[7], Colors::Red)
		);

	_pBatch->End();
}
 
//---------------------------------------------------------------------------------
// Update view matrix after transforming the camera
void Camera::UpdateViewMatrix()
{
	XMVECTOR vRight = XMLoadFloat3(&m_vRight);
	XMVECTOR vUp = XMLoadFloat3(&m_vUp);
	XMVECTOR vLook = XMLoadFloat3(&m_vLook);
	XMVECTOR vPosition = XMLoadFloat3(&m_vPosition);

	//XMMATRIX mView = XMMatrixLookAtLH(vPosition, vPosition + vLook, vUp);
	//XMStoreFloat4x4(&m_mView, mView);

	// Orthonormalize the right, up and look vectors
	vLook = XMVector3Normalize(vLook);
	vUp = XMVector3Normalize(XMVector3Cross(vLook, vRight));
	vRight = XMVector3Cross(vUp, vLook);

	XMStoreFloat3(&m_vRight, vRight);
	XMStoreFloat3(&m_vUp, vUp);
	XMStoreFloat3(&m_vLook, vLook);

	// Fill in the view matrix
	float fX = -XMVectorGetX(XMVector3Dot(vPosition, vRight));
	float fY = -XMVectorGetX(XMVector3Dot(vPosition, vUp));
	float fZ = -XMVectorGetX(XMVector3Dot(vPosition, vLook));

	m_mView(0, 0) = m_vRight.x;
	m_mView(1, 0) = m_vRight.y;
	m_mView(2, 0) = m_vRight.z;
	m_mView(3, 0) = fX;

	m_mView(0, 1) = m_vUp.x;
	m_mView(1, 1) = m_vUp.y;
	m_mView(2, 1) = m_vUp.z;
	m_mView(3, 1) = fY;

	m_mView(0, 2) = m_vLook.x;
	m_mView(1, 2) = m_vLook.y;
	m_mView(2, 2) = m_vLook.z;
	m_mView(3, 2) = fZ;

	m_mView(0, 3) = 0.0f;
	m_mView(1, 3) = 0.0f;
	m_mView(2, 3) = 0.0f;
	m_mView(3, 3) = 1.0f;
}

//---------------------------------------------------------------------------------
// Update view-projection matrix after updating the view matrix
void Camera::UpdateViewProjectionMatrix()
{
	XMMATRIX mView = XMLoadFloat4x4(&m_mView);
	XMMATRIX mProj = XMLoadFloat4x4(&m_mProj);
	// ViewProj = View * Proj
	XMStoreFloat4x4(&m_mViewProj, XMMatrixMultiply(mView, mProj));
}

//---------------------------------------------------------------------------------
// Frustum
void Camera::UpdateFrustum()
{
	XMMATRIX mView = XMLoadFloat4x4(&m_mView);
	//Output Determinant is optional
	m_TransformFrustum.Transform(m_Frustum, XMMatrixInverse(nullptr, mView));
}