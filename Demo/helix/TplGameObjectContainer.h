//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef TPLGAMEOBJECTCONTAINER_H
#define TPLGAMEOBJECTCONTAINER_H

#include "TplMemPool.h"
#include "TplGameObject.h"
#include "Singleton.h"
#include <mutex>

namespace helix
{
	namespace datastructures
	{
		class TplGameObjectContainer : public TSingleton<TplGameObjectContainer>
		{
		public:
			HDEBUGNAME("TplGameObjectContainer");

			TplGameObjectContainer() {}
			TplGameObjectContainer(uint32_t _uGameObjectCount, uint32_t _uRigidBodiesCount);
			~TplGameObjectContainer();

			//Attention: This will break all previous references to objects held by this container
			void Resize(uint32_t _uGameObjectCount, uint32_t _uRigidBodiesCount);

			TplGameObject* Create(const XMFLOAT3& _vPosition, const XMFLOAT3& _vScale, const XMFLOAT3& _vEulerAngles, TplGameObjectFlag _kFlags = TplGameObjectFlag_None);
			TplGameObject* CreateRigidbody(const XMFLOAT3& _vPosition, const XMFLOAT3& _vScale, const XMFLOAT3& _vEulerAngles,
				float _fMass, float _fDamping, const XMFLOAT3 & _vLinearVelocity, const XMFLOAT3 & _vAngularVelocity, const XMFLOAT3X3 & _mInvInertiaTensor,
				TplGameObjectFlag _kFlags = TplGameObjectFlag_None);

			//Call this function at the beginning of each simulation step
			void Lock();
			//Call this function at the end of each simulation step
			void Unlock();

			GameObjectIterator GameObjectsBegin();
			RigidBodyIterator RigidBodiesBegin();

			GameObjectIterator GameObjectsEnd();
			RigidBodyIterator RigidBodiesEnd();

			GameObjectConstIterator GameObjectsConstBegin() const;
			RigidBodyConstIterator RigidBodiesConstBegin() const;

			GameObjectConstIterator GameObjectsConstEnd() const;
			RigidBodyConstIterator RigidBodiesConstEnd() const;

		private:
			void Cleanup();

		private:
			std::mutex m_Mutex;

			TplMemPool<TplGameObject> m_GameObjectPool;
			TplMemPool<TplGameObject, RigidBodyData> m_RigidBodyPool;
		};

		//Iterators
		inline GameObjectIterator TplGameObjectContainer::GameObjectsBegin() { return  m_GameObjectPool.begin(); }
		inline RigidBodyIterator TplGameObjectContainer::RigidBodiesBegin() { return  m_RigidBodyPool.begin(); }

		inline GameObjectIterator TplGameObjectContainer::GameObjectsEnd() { return  m_GameObjectPool.end(); }
		inline RigidBodyIterator TplGameObjectContainer::RigidBodiesEnd() { return  m_RigidBodyPool.end(); }

		//Const iterators:
		inline GameObjectConstIterator TplGameObjectContainer::GameObjectsConstBegin() const { return  m_GameObjectPool.cbegin(); }
		inline RigidBodyConstIterator TplGameObjectContainer::RigidBodiesConstBegin() const { return  m_RigidBodyPool.cbegin(); }

		inline GameObjectConstIterator TplGameObjectContainer::GameObjectsConstEnd() const { return  m_GameObjectPool.cend(); }
		inline RigidBodyConstIterator TplGameObjectContainer::RigidBodiesConstEnd() const { return  m_RigidBodyPool.cend(); }

	}; // datastructures
}; // helix

#endif