//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel, Moritz Becher

#include "OctreeNode.h"

namespace helix
{
	namespace datastructures
	{
		template <typename BoundingVolume>
		void OctreeNode::StartGatherInVolumeTask(const BoundingVolume& _BV)
		{
			//static_cast<void(OctreeNode::*)(const BoundingVolume&)>  is just a fix for the compiler bug: https://connect.microsoft.com/VisualStudio/feedback/details/1090183/passing-member-template-function-to-std-async-produces-compilation-error
			if ((m_pThreadPool && m_pThreadPool->AddTask(std::async(std::launch::async, static_cast<void(OctreeNode::*)(const BoundingVolume&)> (&OctreeNode::GatherInVolumeTask<BoundingVolume>), this, _BV))) == false)
			{
				//no ThreadPool or no free task slot available in ThreadPool, start sync
				GatherInVolumeTask<BoundingVolume>(_BV);
			}
		}

		//---------------------------------------------------------------------------------------------------
		template <typename BoundingVolume>
		inline void OctreeNode::GatherInVolume(const BoundingVolume & _BV, std::vector<TplGameObject*>& _Objects)
		{
			HASSERTD(m_pParentNode == nullptr, "this function should only be called on the RootNode of the Octree!");

			//check if the BV is actually in the octree
			if (collision::Intersects(m_AABB, _BV))
			{
				// starts gather task sync or async if a ThreadPool is set
				StartGatherInVolumeTask<BoundingVolume>(_BV);

				if (m_pThreadPool)
				{
					m_pThreadPool->WaitForAllTasks();
				}

				//Collect objects found in volume
				GetObjectsInVolume(_Objects);
			}
		}

		//---------------------------------------------------------------------------------------------------
		template <typename BoundingVolume>
		void OctreeNode::GatherInVolumeTask(const BoundingVolume& _BV)
		{
			//always use our Intersects functions!

			m_ObjectsInVolume.resize(0);

			HFOREACH_CONST(it, end, m_Entities)
				if ((*it)->Intersects(_BV))
				{
					m_ObjectsInVolume.push_back(*it);
				}
			HFOREACH_CONST_END

			//add subnodes to the task list
			for (uint32_t i = 0; i < 8; ++i)
			{
				if (m_pChildren[i] != nullptr &&  m_pChildren[i]->GetTotalNumOfEntities() > 0)
				{
					//check if the OctreeNode's AABB intersects with the BV
					if (collision::Intersects(m_pChildren[i]->m_AABB, _BV))
					{
						m_pChildren[i]->StartGatherInVolumeTask<BoundingVolume>(_BV);
					}
				}
			}

		}

	}; // datastructures
}; // helix

