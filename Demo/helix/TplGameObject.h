//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef TPLGAMEOBJECT_H
#define TPLGAMEOBJECT_H

#include "CollisionContact.h"
#include "TplMemPool.h"
#include "Debug.h"
#include "Logger.h"
#include "Flag.h"
#include "StdAfxH.h"
#include "UniqueID.h"

namespace helix
{
	namespace datastructures
	{
		using namespace DirectX;

		//class OctreeNode;

		Flag32E(TplGameObjectFlag)
		{
			TplGameObjectFlag_None = 0, // 0
			TplGameObjectFlag_Destroyed = 1 << 0, // 1
			TplGameObjectFlag_Static = 1 << 1, // 2
			TplGameObjectFlag_Invisible = 1 << 2,  // 4
			TplGameObjectFlag_NoCollision = 1 << 3 // 8 
		};

		//GameObject types should not be part of the GameObjecFlags since they must not be changed
		//this also means every constructor needs to set the correct type dependig on the input reference tuple
		enum ObjectType
		{
			ObjectType_GameObject = 0,
			ObjectType_RigidBody = 1
		};

		//additional data
		struct RigidBodyData
		{
			float m_fDamping;
			float m_fInvMass; //m^-1

			XMFLOAT3 m_vLinearVelocity; //v
			XMFLOAT3 m_vAngularVelocity; //w
			XMFLOAT3 m_vAngularMomentum; //L		
			XMFLOAT3X3 m_mInvInertiaTensor; // I_0^-1
		};

		class TplGameObject : public Flag<uint32_t>
		{
			friend class TplGameObjectContainer;
			friend class OctreeNode; // to be able to access GetNode()

		public:
			HDEBUGNAME("TplGameObjecet");

			TplGameObject();
			TplGameObject(std::tuple<RigidBodyData>& _TplReferences);
			~TplGameObject();

			void InitializeAABB(const XMFLOAT3& _vPositionOffset, const XMFLOAT3& _vExtents);
			void InitializeOBB(const XMFLOAT3& _vPositionOffset, const XMFLOAT3& _vExtents, const XMFLOAT4& _vOrientation);
			// Call this function once per physics update
			void UpdateBoundingVolumes();

			bool Intersects(const TplGameObject& _GO) const;
			bool Intersects(_In_ const TplGameObject& _GO, _Out_ collision::CollisionInfo& _Info) const;
			bool Intersects(const BoundingSphere& _BoundingSphere) const;
			bool Intersects(const BoundingFrustum& _BoundingFrustum) const;
			bool Intersects(const BoundingBox& _BoundingBox) const;

			bool Contains(const TplGameObject& _GO) const;
			bool ContainedBy(const BoundingBox& _BoundingBox) const;

			//Getter / Setter
			const BoundingBox& GetAABB() const;
			const BoundingOrientedBox& GetOBB() const;
			RigidBodyData* const GetRigidBodyData() const; // constant pointer to non const data
			const ObjectType GetType() const;
			const uint32_t GetID() const;
			OctreeNode* const GetNodeConst() const;

		private:
			void Initialize(const XMFLOAT3& _vPosition, const XMFLOAT3& _vScale, const XMFLOAT3& _vEulerAngles, TplGameObjectFlag _kFlags);
			void InitializeRigidBody(float _fMass, float _fDamping, const XMFLOAT3& _vLinearVelocity, const XMFLOAT3& _vAngularVelocity, const XMFLOAT3X3& _mInvInertiaTensor);
			OctreeNode*& GetNode();

		private:
			//common variables
			const ObjectType m_kType;

			uint32_t m_uID = HUNDEFINED;
			uint32_t m_uMeshID = HUNDEFINED;
			UniqueID<uint32_t> m_UniqueID;

			OctreeNode* m_pNode = nullptr;

			//BoundingVolumes
			BoundingOrientedBox m_OBB;
			BoundingBox m_AABB;

			//Do not change this data after calling InitializeAABB or InitializeOBB!
			BoundingOrientedBox m_InitialOBB;

			//type dependent variables
			RigidBodyData* const m_pRigidBody = nullptr;

		public:
			//when accessing any of these, call UpdateBoundingVolumes() afterwards!
			XMFLOAT3 m_vPosition;
			XMFLOAT3 m_vScale;

			XMFLOAT4 m_vOrientation;
			XMFLOAT3X3 m_mRotation;
		};

		//typedefs:
		typedef TplMemPool<TplGameObject>::iterator GameObjectIterator;
		typedef TplMemPool<TplGameObject, RigidBodyData>::iterator RigidBodyIterator;

		typedef TplMemPool<TplGameObject>::const_iterator GameObjectConstIterator;
		typedef TplMemPool<TplGameObject, RigidBodyData>::const_iterator RigidBodyConstIterator;

		//getters / setters
		inline const BoundingBox& TplGameObject::GetAABB() const { return m_AABB; }
		inline const BoundingOrientedBox& TplGameObject::GetOBB() const { return m_OBB; }
		inline RigidBodyData* const TplGameObject::GetRigidBodyData() const { return m_pRigidBody; }
		inline const ObjectType TplGameObject::GetType() const { return m_kType; }
		inline const uint32_t TplGameObject::GetID() const { return m_uID; }
		inline OctreeNode*& TplGameObject::GetNode() { return m_pNode; }
		inline OctreeNode* const TplGameObject::GetNodeConst() const { return m_pNode; }

	}; // datastructures
}; // helix

#endif