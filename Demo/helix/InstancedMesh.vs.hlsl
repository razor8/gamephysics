//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

#include "InstancedMeshDefines.hlsli"

cbuffer cbPerFrame
{
	float4	g_fScaleFactor;
	//float3	g_EyePosWorld;
	float4x4 g_ViewProj;
};

VS_OUT main(VS_IN _Input)
{
	VS_OUT Output;

	// TODO: check calculation, maybe wrong
	float4x4 mScale = TransformationMatrix;
	mScale._11_22_33 = g_fScaleFactor.xyz;

	float4x4 mRot = TransformationMatrix;

	float4x4 mTrans = TransformationMatrix;
	mTrans._41_42_43 = _Input.vInstancePositionWorld; // transposed

	float4x4 mModel = mul(mul(mScale, mRot), mTrans); // SRT or TRS

	//worldMat = float4x4(
	//	float4(1.f, 0.f, 0.f, 0.f),
	//	float4(0.f, 1.f, 0.f, 0.f),
	//	float4(0.f, 0.f, 1.f, 0.f),
	//	float4(_input.vInstancePositionWorld.xyz, 1.f)
	//	);

	float4x4 WVP = mul(mModel, g_ViewProj);
	Output.vPositionWorld = mul(_Input.vPositionLocal, WVP);

	uint uSwitchTemp = _Input.uVertexId % 3;

	//Colored output, remove after light implementation
	switch (uSwitchTemp)
	{
	case 0:
		Output.vTempColor = float4(1.f, 0.f, 0.f, 1.f);
		return Output;
	case 1:
		Output.vTempColor = float4(0.f, 1.f, 0.f, 1.f);
		return Output;
	case 2:
		Output.vTempColor = float4(0.f, 0.f, 1.f, 1.f);
		return Output;
	default:
		Output.vTempColor = float4(0.f, 0.f, 0.f, 1.f);
		return Output;
	}

	return Output;
}