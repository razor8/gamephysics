//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

#ifndef RENDERDEFINES_H
#define RENDERDEFINES_H

#include "XMMath.h"
#include "StdAfxH.h"

using namespace DirectX;

namespace helix {
	namespace Renderer
	{
		enum VertexType
		{
			VertexType_VertexPosition = 0,
			VertexType_VertexPositionNormalTexture = 1,
			VertexType_NumOf
		};

		// Shader Input Structs
		struct VertexPosition
		{
			VertexPosition()
			{
			}

			VertexPosition(XMFLOAT3 const& _vPosition) :
				m_vVertexPosition(_vPosition)
			{
			}

			VertexPosition(FXMVECTOR _vPosition)
			{
				XMStoreFloat3(&this->m_vVertexPosition, _vPosition);
			}

			void Clear()
			{
				m_vVertexPosition = XMFLOAT3(0,0,0);
			}

			VertexPosition& operator=(const VertexPosition& _Arg)
			{
				this->m_vVertexPosition = _Arg.m_vVertexPosition;
				return *this;
			}

			XMFLOAT3 m_vVertexPosition; // Objectspace
			
			const uint8_t m_uID = VertexType::VertexType_VertexPosition;
		};
		struct VertexPositionNormalTexture
		{
			VertexPositionNormalTexture()
			{
			}

			VertexPositionNormalTexture(XMFLOAT3 const& _vPosition, XMFLOAT3 const& _vNormal, XMFLOAT2 const& _vTextureCoordinate) :
				m_vVertexPosition(_vPosition),
				m_vVertexNormal(_vNormal),
				m_vTextureCoordinate(_vTextureCoordinate)
			{
			}

			VertexPositionNormalTexture(FXMVECTOR _vPosition, FXMVECTOR _vNormal, FXMVECTOR _vTextureCoordinate)
			{
				XMStoreFloat3(&this->m_vVertexPosition, _vPosition);
				XMStoreFloat3(&this->m_vVertexNormal, _vNormal);
				XMStoreFloat2(&this->m_vTextureCoordinate, _vTextureCoordinate);
			}

			void Clear()
			{
				m_vVertexPosition = XMFLOAT3(0, 0, 0);
				m_vVertexNormal = XMFLOAT3(0, 0, 0);
				m_vTextureCoordinate = XMFLOAT2(0, 0);
			}

			VertexPositionNormalTexture& operator=(const VertexPositionNormalTexture& _Arg)
			{
				this->m_vVertexPosition = _Arg.m_vVertexPosition;
				this->m_vVertexNormal = _Arg.m_vVertexNormal;
				this->m_vTextureCoordinate = _Arg.m_vTextureCoordinate;
				return *this;
			}

			XMFLOAT3 m_vVertexPosition;
			XMFLOAT3 m_vVertexNormal;
			XMFLOAT2 m_vTextureCoordinate;

			const uint8_t m_uID = VertexType::VertexType_VertexPositionNormalTexture;
		};
	};
}; // namespace helix

#endif //RENDERDEFINES_H