//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

struct VS_IN
{
	float4	vPositionLocal			: POSITION;
	float3	vInstancePositionWorld	: INSTANCEPOS;
	uint	uInstanceId				: SV_InstanceID;
	uint	uVertexId				: SV_VertexID;
};
struct VS_OUT
{
	float4	vPositionWorld	: SV_POSITION;
	float4	vTempColor		: COLOR;
};

static const float4x4 TransformationMatrix = float4x4(
	float4(1.f, 0.f, 0.f, 0.f),
	float4(0.f, 1.f, 0.f, 0.f),
	float4(0.f, 0.f, 1.f, 0.f),
	float4(0.f, 0.f, 0.f, 1.f)
);