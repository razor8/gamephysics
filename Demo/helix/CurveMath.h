#ifndef CURVEMATH_H
#define CURVEMATH_H

#include "StdAfxH.h"
#include "Logger.h"
#include "XMMath.h"
#include "LookupTable.h"

namespace helix
{
	namespace animation
	{
		using namespace DirectX;

		struct Curve
		{
			XMFLOAT3 vP0;
			XMFLOAT3 vP1;
			XMFLOAT3 vP2;
			XMFLOAT3 vP3;
		};

		struct XMVectorCurve
		{
			XMVECTOR vP0;
			XMVECTOR vP1;
			XMVECTOR vP2;
			XMVECTOR vP3;
		};

		//==============================================================================================================
		// Arc Length
		//==============================================================================================================
		class ArcLength
		{
		private:
			struct Interval
			{
				float fLength;
				float fLower;
				float fUpper;
			};

		public:
			ArcLength();
			
			// Generate lookup table for arc length
			void Initialize(const Curve& _Curve, const float _fLower = 0.0f, const float _fUpper = 1.0f);
			
			// Get a value from the lookup table
			float At(const float _fT) const;

			// The total length of the arc
			float GetTotalLength() const;

		private:
			//void SplitBezier(XMVectorCurve& _FullBezier, XMVectorCurve& _LeftBezier, XMVectorCurve& _RightBezier);

			// Integration
			float GaussianQuadrature(const Interval& _FullInterval);
			// 4th order polynome
			float EvaluatePolynome(const float _fT);
			// recursive subdivision algorithm
			//float Subdivide(const Interval& _FullInterval, float _fTotalLength, const float _fTolerance = 0.00001f);
			void Subdivide(const XMVectorCurve& _FullCurve, float& _fTotalLength, const float _fTolerance = 0.00001f, float _fChunkSize = 1.0f, float _fT = 0.0f);

			float m_fCoeff[5];
			ClumpedLookup<float, float> m_ParameterToArcLength;
			float m_fTotalLength;
		};

		inline float ArcLength::GetTotalLength() const { return m_fTotalLength; }

		//==============================================================================================================
		// Bezier
		//==============================================================================================================
		class BezierCurve
		{
			HDEBUGNAME("Bezier Curve");
			
		public:
			BezierCurve();
			BezierCurve(const XMFLOAT3& _P0, const XMFLOAT3& _P1, const XMFLOAT3& _P2, const XMFLOAT3& _P3);

			void Calculate(_Out_ XMFLOAT3& _Point, _In_ float _fArcT);

			Curve& GetCurve() { return m_CurvePoints; }
			static void Split(const XMVectorCurve& _Full, XMVectorCurve& _Left, XMVectorCurve& _Right);

		protected:
			Curve m_CurvePoints;
		};

		inline void BezierCurve::Split(const XMVectorCurve& _Full, XMVectorCurve& _Left, XMVectorCurve& _Right)
		{
			_Left.vP0 = _Full.vP0;
			_Right.vP3 = _Full.vP3;
			_Left.vP1 = XMVectorLerp(_Full.vP0, _Full.vP1, 0.5f);
			XMVECTOR vP5 = XMVectorLerp(_Full.vP1, _Full.vP2, 0.5f);
			_Right.vP2 = XMVectorLerp(_Full.vP2, _Full.vP3, 0.5f);
			_Left.vP2 = XMVectorLerp(_Left.vP1, vP5, 0.5f);
			_Right.vP1 = XMVectorLerp(vP5, _Right.vP2, 0.5f);
			_Right.vP0 = _Left.vP3 = XMVectorLerp(_Left.vP2, _Right.vP1, 0.5f);
		}
	}
}

#endif
