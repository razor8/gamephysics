//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef COLLISIONCONTACT_H
#define COLLISIONCONTACT_H

#include "XMMath.h"

#pragma warning (disable: 4838 4458)
#include <DirectXCollision.h>
#pragma warning (default: 4838 4458)

#include <vector>

namespace helix
{
	namespace datastructures
	{
		class TplGameObject;
	};

	namespace collision
	{
		struct CollisionInfo
		{
			struct Contact
			{
				DirectX::XMFLOAT3 m_vPoint;
				DirectX::XMFLOAT3 m_vNormal;
			};

			bool m_bIsValid = false;

			datastructures::TplGameObject* m_pGO1; // Object 1
			datastructures::TplGameObject* m_pGO2; // Object 2

			std::vector<Contact> m_Contacts1;
			std::vector<Contact> m_Contacts2;
		};

		class CollisionContact
		{
		public:
			static void SimpleContact(_In_ const DirectX::BoundingSphere& sphere1, _In_ const DirectX::BoundingSphere& sphere2, _Out_ CollisionInfo& _CollsionInfo);
			static void SimpleContact(_In_ const DirectX::BoundingBox& box1, _In_ const DirectX::BoundingBox& box2, _Out_ CollisionInfo& _CollsionInfo, const bool bSwitch = true);
			static void SimpleContact(_In_ const DirectX::BoundingOrientedBox& box1, _In_ const DirectX::BoundingOrientedBox& box2, _Out_ CollisionInfo& _CollsionInfo, const bool bSwitch = true);

			static void SimpleContact(_In_ const DirectX::BoundingSphere& sphere, _In_ const DirectX::BoundingBox& box, _Out_ CollisionInfo& _CollisionInfo);
			static void SimpleContact(_In_ const DirectX::BoundingBox& box, _In_ const DirectX::BoundingSphere& sphere, _Out_ CollisionInfo& _CollisionInfo);
		};

	};
};


#endif // COLLISIONCONTACT_H