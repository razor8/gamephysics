//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

#include "Logger.h"
#include "Timer.h"
#include <tchar.h>
#include <strsafe.h>

using namespace helix;

#define HNULLSTR(ptr) (((ptr) != nullptr) ? (ptr) : TEXT("") )

//---------------------------------------------------------------------------------------------------

Logger::Logger(HERROR_E kType)
{
	std::unique_lock<std::mutex> Lock(m_Mutex);

	//log all messages above or equal to HERROR_NON level
	m_LogLevel = kType;

	m_bLogToOutputConsole = true;
	m_bLogToFile = false;	
	m_bLogToStream = false;

	m_bFirstLog = true;

	m_Stream = nullptr;
}

//---------------------------------------------------------------------------------------------------

Logger::~Logger()
{
	Log(HERROR_NONE, nullptr, nullptr, nullptr, NULL, TEXT("========== LOGGER STOPPED =========="));

	std::unique_lock<std::mutex> Lock(m_Mutex);
	if (m_FileStream.is_open())
		m_FileStream.close();
}

//---------------------------------------------------------------------------------------------------
//TODO: TimeStamp
void Logger::Log(HERROR_E kType, const TCHAR* pDbgName, const TCHAR* pFuncName, const TCHAR* pSourceFile, uint32_t uLineNum, const TCHAR * pFormat, ...)
{
	//if this is the first time Log(...) was called print logger started msg
	if(m_bFirstLog)
	{
		m_bFirstLog = false;
		Log(HERROR_NONE,nullptr,nullptr, nullptr, 0, TEXT("========== LOGGER STARTED =========="));
	}

	//only log message if type level is equal or above m_logLevel (HERROR_NONE is default log level)
	if (kType < m_LogLevel)
		return;

	//Enter critical section - threadsafety of buffer and stream
	std::lock_guard<std::mutex> Lock(m_Mutex);	

	TCHAR pMsgBuffer[2048];

	int chars = -1;

	//format message like with printf using variadic args
	va_list args;
	va_start(args, pFormat);
	StringCbVPrintf(pMsgBuffer, _countof(pMsgBuffer), pFormat, args);
	va_end (args);

	//Error Type to print
	LPCTSTR errType = TEXT('\0');

	switch (kType){
		//case HERROR_NONE: nothing to print
	case HERROR_WARNING:
		errType = TEXT("[WARNING] ");
		break;		
	case HERROR_ERROR:
		errType = TEXT("[ERROR] ");
		break;	
	case HERROR_FATAL:
		errType = TEXT("[FATAL] ");
		break;
	}

	//construct the error message and add location
	int mbox = 0;

	//Only display MessageBox on fatal errors - close game
	if (m_bLogToOutputConsole && kType == HERROR_FATAL)
		mbox = MessageBox(NULL, pMsgBuffer, errType, MB_ABORTRETRYIGNORE | MB_ICONERROR | MB_DEFBUTTON3);

	TCHAR pOutBuffer[4096];
	LPCTSTR pszFormat;
	
	TCHAR pTime[9];
	Timer::GetLocalTimeString(pTime,9);

	if (kType > HERROR_NONE)
	{
		//time errType MSG dbgName funcName sourceFile lineNum
		pszFormat = TEXT("%s %s %s in %s::%s %s:%d\n");
		StringCbPrintf(pOutBuffer, _countof(pOutBuffer), pszFormat, pTime, errType, pMsgBuffer, HNULLSTR(pDbgName), HNULLSTR(pFuncName), HNULLSTR(pSourceFile), uLineNum);
	}
	else
	{
		//(time) MSG
		pszFormat = TEXT("%s %s\n");
		StringCbPrintf(pOutBuffer, _countof(pOutBuffer), pszFormat, pTime, pMsgBuffer);
	}

	//Print on debug console if debugger is present
	if (m_bLogToOutputConsole)
		OutputDebugString(pOutBuffer);
	
	//Log to file
	if (m_bLogToFile && m_FileStream.is_open() && m_FileStream.good())
		m_FileStream << pOutBuffer;

	//Log to output string
	if (m_bLogToStream && m_Stream != nullptr)
		*m_Stream << pOutBuffer;

	if (kType == HERROR_FATAL)
	{
		switch (mbox)
		{
		case IDABORT: DebugBreak(); break;  // break into the debugger
		case IDIGNORE:  break;		
		case IDRETRY:	break;
		default:		break;
		}
	}
}

//---------------------------------------------------------------------------------------------------

void Logger::WriteToFile(const TCHAR* pFilePath)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	m_bLogToFile = true;
	if (m_FileStream.is_open() == false)
		m_FileStream.open(pFilePath, std::ios_base::out);
}

//---------------------------------------------------------------------------------------------------

void Logger::WriteToStream(std::wostream* _Stream)
{
	std::lock_guard<std::mutex> Lock(m_Mutex);
	m_bLogToStream = true;
	m_Stream = _Stream;
}