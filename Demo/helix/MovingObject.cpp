//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "MovingObject.h"

using namespace helix::collision;
using namespace helix::datastructures;
using namespace helix::math;

//---------------------------------------------------------------------------------------------------
//default constructor
MovingObject::MovingObject()
{
	m_uID = HUNDEFINED;
	SetFlag(GameObjectFlag_MovingObject);
}
//---------------------------------------------------------------------------------------------------
MovingObject::MovingObject(const XMFLOAT3& _vPosition, const XMFLOAT3& _vScale, const XMFLOAT3& _vEulerAngles, float _fMass, float _fDamping, GameObjectFlags _kFlag, const XMFLOAT3& _vLinearVelocity, const XMFLOAT3& _vForce) :
GameObject(_vPosition, _vScale, _vEulerAngles, static_cast<GameObjectFlags>(_kFlag | GameObjectFlag_MovingObject))
{
	ClearFlag(GameObjectFlag_RigidBody | GameObjectFlag_GameObject);

	m_fDamping = _fDamping;
	m_fInvMass = 1.f / _fMass;
	m_vLinearVelocity = _vLinearVelocity; //v
	m_vCurForce = _vForce;

	m_BV.SetNewAABB(_vPosition, _vScale);
}
//---------------------------------------------------------------------------------------------------
//copy constructor
MovingObject::MovingObject(const MovingObject& _Other) : GameObject(_Other)
{
	//lock the other gameobject to prevent data being changed while copying
	std::lock_guard<std::recursive_mutex> Lock(_Other.m_Mutex);

	SetFlag(GameObjectFlag_MovingObject);
	ClearFlag(GameObjectFlag_RigidBody | GameObjectFlag_GameObject);

	//RigidBody properties
	m_fDamping = _Other.m_fDamping;
	m_fInvMass = _Other.m_fInvMass;
	m_vLinearVelocity = _Other.m_vLinearVelocity;
	m_vCurForce = _Other.m_vCurForce;
}
//---------------------------------------------------------------------------------------------------
//move constructor
MovingObject::MovingObject(MovingObject&& _Other) : GameObject(std::move(_Other))
{
	std::lock_guard<std::recursive_mutex> Lock(_Other.m_Mutex);

	SetFlag(GameObjectFlag_MovingObject);
	ClearFlag(GameObjectFlag_RigidBody | GameObjectFlag_GameObject);

	//RigidBody properties
	m_fDamping = std::move(_Other.m_fDamping);
	m_fInvMass = std::move(_Other.m_fInvMass);
	m_vLinearVelocity = std::move(_Other.m_vLinearVelocity);
	m_vCurForce = std::move(_Other.m_vCurForce);
}
//---------------------------------------------------------------------------------------------------
//assignment operator
MovingObject& MovingObject::operator=(MovingObject& _Other)
{
	if (this != &_Other)
	{
		GameObject::operator=(_Other);

		std::lock_guard<std::recursive_mutex> OtherLock(_Other.m_Mutex);
		std::lock_guard<std::recursive_mutex> MyLock(m_Mutex);

		//RigidBody properties
		m_fDamping = _Other.m_fDamping;
		m_fInvMass = _Other.m_fInvMass;
		m_vLinearVelocity = _Other.m_vLinearVelocity;
		m_vCurForce = _Other.m_vCurForce;
	}

	return *this;
}
//---------------------------------------------------------------------------------------------------
//destructor
MovingObject::~MovingObject()
{

}
