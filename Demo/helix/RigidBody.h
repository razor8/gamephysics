//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef RIGIDBODY_H
#define RIGIDBODY_H

#include "GameObject.h"
#include "CollisionContact.h"
#include "Logger.h"

namespace helix
{
	namespace datastructures
	{
		using namespace DirectX;

		class RigidBody : public GameObject
		{

		public:
			HDEBUGNAME("RigidBody");

			RigidBody();
			RigidBody(const XMFLOAT3& _vPosition, const XMFLOAT3& _vScale, const XMFLOAT3& _vEulerAngles, float _fMass, float _fDamping = 0.99f, GameObjectFlags _kFlag = GameObjectFlag_RigidBody, const XMFLOAT3& _vLinearVelocity = XMFLOAT3_ZERO, const XMFLOAT3& _vAngularVelocity = XMFLOAT3_ZERO);
			RigidBody(const RigidBody& _Other);
			RigidBody(RigidBody&& _Other);
			RigidBody& operator =(RigidBody& _Other);
			~RigidBody();

			static float ComputeImpulse(RigidBody& _RBa, RigidBody& _RBb, const collision::CollisionInfo::Contact& Contact);

			// GETTER / SETTER

			const float& GetMass() const;
			void SetMass(const float& _fMass);

			const float& GetDamping() const;
			void SetDamping(const float& _fDamping);

			const XMFLOAT3& GetLinearVelocity() const;
			void SetLinearVelocity(const XMFLOAT3& _vLinearVelocity);

			const XMFLOAT3& GetAngularVelocity() const;
			void SetAngularVelocity(const XMFLOAT3& _vAngularVelocity);

			const XMFLOAT3& GetAngularMomentum() const;
			void SetAngularMomentum(const XMFLOAT3& _vAngularMomentum);

			const XMFLOAT3X3& GetInvInertiaTensor() const;
			void SetInvInertiaTensor(const XMFLOAT3X3& _mInvInertiaTensor);

			//this function assumes the mutex is already locked!
			void ApplyImpulse(float _fMomentum, const collision::CollisionInfo::Contact _Contact);

		public:
			float m_fDamping;
			float m_fInvMass; //M

			XMFLOAT3 m_vLinearVelocity; //v

			XMFLOAT3 m_vAngularVelocity; //w

			XMFLOAT3 m_vAngularMomentum; //L		

			XMFLOAT3X3 m_mInvInertiaTensor; // I_0^-1

		};

		inline const float& RigidBody::GetMass() const	{ std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return 1.f / m_fInvMass; }
		inline void RigidBody::SetMass(const float& _fMass){ std::lock_guard<std::recursive_mutex> Lock(m_Mutex); m_fInvMass = 1.f / _fMass; }

		inline const float& RigidBody::GetDamping() const { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return -m_fDamping; }
		inline void RigidBody::SetDamping(const float& _fDamping) {std::lock_guard<std::recursive_mutex> Lock(m_Mutex); m_fDamping = -_fDamping; }
		
		inline const XMFLOAT3& RigidBody::GetLinearVelocity() const { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return m_vLinearVelocity; }
		inline void RigidBody::SetLinearVelocity(const XMFLOAT3& _vLinearVelocity) { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); m_vLinearVelocity = _vLinearVelocity; }

		inline const XMFLOAT3& RigidBody::GetAngularVelocity() const { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return m_vAngularVelocity; }
		inline void RigidBody::SetAngularVelocity(const XMFLOAT3& _vAngularVelocity) { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); m_vAngularVelocity = _vAngularVelocity; }

		inline const XMFLOAT3& RigidBody::GetAngularMomentum() const { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return m_vAngularMomentum; }
		inline void RigidBody::SetAngularMomentum(const XMFLOAT3& _vAngularMomentum) { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); m_vAngularMomentum = _vAngularMomentum; }

		inline const XMFLOAT3X3& RigidBody::GetInvInertiaTensor() const { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return m_mInvInertiaTensor; }
		inline void RigidBody::SetInvInertiaTensor(const XMFLOAT3X3& _mInvInertiaTensor) { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); m_mInvInertiaTensor = _mInvInertiaTensor; }

	}; // namespace datastructures
}; // namespace helix

#endif // RIGIDBODY_H