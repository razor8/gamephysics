#ifndef MASSSPRINGGENERATOR_H
#define MASSSPRINGGENERATOR_H

#include "MassSpringSystem.h"

namespace helix
{
	namespace physics
	{
		class MassSpringGenerator
		{
		public:
			MassSpringGenerator();
			~MassSpringGenerator();

			static MassSpringSystem::MSData Cube(XMFLOAT3 _vPosition, float _fEdgelength, int32_t _uPointsPerEdge, MassSpringSystem::MSPoint _SamplePoint, MassSpringSystem::MSSpring _SampleSpring);
		};
	}; //namespace physics

}; //namespace helix

#endif


