//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

#include "InstancedMeshDefines.hlsli"

cbuffer cbPerFrame
{
	float4	g_Color;
};

float4 main(VS_OUT _Input) : SV_TARGET
{
	//return g_Color;
	return _Input.vTempColor;
}