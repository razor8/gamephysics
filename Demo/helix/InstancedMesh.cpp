//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster
//
//#include "InstancedMesh.h"
//#include "Logger.h"
//
//using namespace helix;

//---------------------------------------------------------------------------------------------------
// Explicit class template specialization 
//template class InstancedMesh < Renderer::VertexPosition > ;
//template class InstancedMesh < Renderer::VertexPositionNormalTexture >;
//---------------------------------------------------------------------------------------------------
// Static members
//
//template <class T> ID3D11InputLayout* InstancedMesh<T>::InstancedMeshLayout = nullptr;
//template <class T> ID3DBlob* InstancedMesh<T>::pVertexShaderBlob = nullptr;
//template <class T> ID3DBlob* InstancedMesh<T>::pPixelShaderBlob = nullptr;
//template <class T> ID3D11VertexShader* InstancedMesh<T>::pVertexShader = nullptr;
//template <class T> ID3D11PixelShader* InstancedMesh<T>::pPixelShader = nullptr;
//template <class T> ID3D11RasterizerState* InstancedMesh<T>::pDefaultRasterizerState = nullptr;
//// Input Layout Description Defines:
//template <class T> const D3D11_INPUT_ELEMENT_DESC InstancedMesh<T>::InstancedMeshDesc[2] =
//{
//	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//	// TODO: InputSlot = 1 could be wrong
//	{ "INSTANCEPOS", 0, DXGI_FORMAT_R32G32B32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1 }
//};

//---------------------------------------------------------------------------------------------------
//InstancedMesh::InstancedMesh(ID3D11Device* _pDevice) :
//	m_InstanceBuffer(nullptr),
//	m_InstanceBufferData(NULL)
//{	
//	InstancedMesh::Initialize(_pDevice);
//	CreateConstantBuffers(_pDevice);
//	CreateRenderBuffers(_pDevice);
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//InstancedMesh<T>::InstancedMesh(ID3D11Device* _pDevice, std::vector<T>& _VertexBufferData, std::vector<uint32_t>& _IndexBufferData) :
//	m_InstanceBuffer(nullptr),
//	m_InstanceBufferData(NULL),
//	m_uMaxInstancesAmount(100000)
//{
//	// TODO: reserve initial memory for m_InstanceBufferData
//
//	m_VertexBufferData = _VertexBufferData;
//	m_IndexBufferData = _IndexBufferData;
//	InstancedMesh::Initialize(_pDevice);
//	CreateConstantBuffers(_pDevice);
//	CreateRenderBuffers(_pDevice);
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//InstancedMesh<T>::~InstancedMesh()
//{
//	if (m_InstanceBuffer)
//	{
//		m_InstanceBuffer->Release();
//		m_InstanceBuffer = nullptr;
//	}
//	if (m_VertexShaderConstantBuffer)
//	{
//		m_VertexShaderConstantBuffer->Release();
//		m_VertexShaderConstantBuffer = nullptr;
//	}
//	if (m_PixelShaderConstantBuffer)
//	{
//		m_PixelShaderConstantBuffer->Release();
//		m_PixelShaderConstantBuffer = nullptr;
//	}
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//bool InstancedMesh<T>::AddInstance(InstanceData const& _InstData)
//{
//	if (m_InstanceBufferData.size() >= m_uMaxInstancesAmount)
//		return false;	
//
//	m_InstanceBufferData.push_back(_InstData);
//	return true;
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//bool InstancedMesh<T>::AddInstance(XMFLOAT3 const& _vPosition)
//{
//	if (m_InstanceBufferData.size() >= m_uMaxInstancesAmount)
//		return false;
//
//	m_InstanceBufferData.push_back(InstanceData(_vPosition));
//	return true;
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//bool InstancedMesh<T>::AddInstances(std::vector<InstanceData> const& _InstData)
//{
//	if (_InstData.size() >= m_uMaxInstancesAmount)
//		return false;
//
//	m_InstanceBufferData = _InstData;
//	return true;
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//bool InstancedMesh<T>::EditInstance(uint32_t _uId, InstanceData const& _InstData)
//{
//	if (m_InstanceBufferData.size() < _uId)
//		return false;
//
//	m_InstanceBufferData.at(_uId) = _InstData;
//	return true;
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//bool InstancedMesh<T>::EditInstance(uint32_t _uId, XMFLOAT3 const& _vPosition)
//{
//	if (m_InstanceBufferData.size() < _uId)
//		return false;
//
//	m_InstanceBufferData.at(_uId).m_vInstancePosition = XMFLOAT4(_vPosition.x, _vPosition.y, _vPosition.z, 1.f);
//	return true;
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//void InstancedMesh<T>::ClearInstances()
//{
//	m_InstanceBufferData.clear();
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//void InstancedMesh<T>::SetVertexShaderConstantBuffer(XMMATRIX const& _ViewProj, XMFLOAT4 const& _vScaleFactor)
//{
//	XMFLOAT4X4 TransposedViewProj;
//	XMStoreFloat4x4(&TransposedViewProj, XMMatrixTranspose(_ViewProj));
//
//	m_VertexShaderData.g_ViewProj = TransposedViewProj;
//	m_VertexShaderData.g_vScaleFactor = _vScaleFactor;
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//void InstancedMesh<T>::SetVertexShaderConstantBuffer(XMFLOAT4X4 const& _ViewProj, XMFLOAT4 const& _vScaleFactor)
//{
//	XMFLOAT4X4 TransposedViewProj;
//	XMStoreFloat4x4(&TransposedViewProj, XMMatrixTranspose(XMLoadFloat4x4(&_ViewProj)));
//
//	m_VertexShaderData.g_ViewProj = TransposedViewProj;
//	m_VertexShaderData.g_vScaleFactor = _vScaleFactor;
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//void InstancedMesh<T>::SetVertexShaderConstantBuffer(VertexShaderConstantBuffer& _VertexShaderData)
//{
//	m_VertexShaderData = _VertexShaderData;
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//void InstancedMesh<T>::SetPixelShaderConstantBuffer(XMFLOAT4 const& _Color)
//{
//	m_PixelShaderData.g_Color = _Color;
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//void InstancedMesh<T>::SetPixelShaderConstantBuffer(PixelShaderConstantBuffer& _PixelShaderData)
//{
//	m_PixelShaderData = _PixelShaderData;
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//void InstancedMesh<T>::CreateConstantBuffers(ID3D11Device* _pDevice)
//{
//	D3D11_BUFFER_DESC vertexShaderConstantBufferDesc;
//	ZeroMemory(&vertexShaderConstantBufferDesc, sizeof(vertexShaderConstantBufferDesc));
//	vertexShaderConstantBufferDesc.ByteWidth = sizeof(VertexShaderConstantBuffer);
//	vertexShaderConstantBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
//	vertexShaderConstantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
//	vertexShaderConstantBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//	_pDevice->CreateBuffer(&vertexShaderConstantBufferDesc, nullptr, &m_VertexShaderConstantBuffer);
//
//	D3D11_BUFFER_DESC pixelShaderConstantBufferDesc;
//	ZeroMemory(&pixelShaderConstantBufferDesc, sizeof(pixelShaderConstantBufferDesc));
//	pixelShaderConstantBufferDesc.ByteWidth = sizeof(PixelShaderConstantBuffer);
//	pixelShaderConstantBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
//	pixelShaderConstantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
//	pixelShaderConstantBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//	D3D11_SUBRESOURCE_DATA pixelShaderConstantBufferInitData;
//	pixelShaderConstantBufferInitData.pSysMem = &m_PixelShaderData.g_Color;
//	pixelShaderConstantBufferInitData.SysMemPitch = 0;
//	pixelShaderConstantBufferInitData.SysMemSlicePitch = 0;
//	_pDevice->CreateBuffer(&pixelShaderConstantBufferDesc, &pixelShaderConstantBufferInitData, &m_PixelShaderConstantBuffer);
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//void InstancedMesh<T>::CreateRenderBuffers(ID3D11Device* _pDevice)
//{
//	D3D11_BUFFER_DESC vertexBufferDesc;
//	ZeroMemory(&vertexBufferDesc, sizeof(vertexBufferDesc));
//	vertexBufferDesc.ByteWidth = sizeof(VertexShaderConstantBuffer)*m_VertexBufferData.size();
//	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
//	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//	if (m_VertexBufferData.size() > 0)
//	{
//		D3D11_SUBRESOURCE_DATA vertexBufferInitialData;
//		vertexBufferInitialData.pSysMem = &m_VertexBufferData[0];
//		vertexBufferInitialData.SysMemPitch = 0;
//		vertexBufferInitialData.SysMemSlicePitch = 0;
//		_pDevice->CreateBuffer(&vertexBufferDesc, &vertexBufferInitialData, &m_VertexBuffer);
//	}
//	else
//	{
//		_pDevice->CreateBuffer(&vertexBufferDesc, NULL, &m_VertexBuffer);
//	}
//
//	D3D11_BUFFER_DESC indexBufferDesc;
//	ZeroMemory(&indexBufferDesc, sizeof(indexBufferDesc));
//	indexBufferDesc.ByteWidth = sizeof(uint32_t)* m_IndexBufferData.size();
//	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
//	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
//	if (m_IndexBufferData.size() > 0)
//	{
//		D3D11_SUBRESOURCE_DATA indexBufferInitialData;
//		indexBufferInitialData.pSysMem = &m_IndexBufferData[0];
//		indexBufferInitialData.SysMemPitch = 0;
//		indexBufferInitialData.SysMemSlicePitch = 0;
//		_pDevice->CreateBuffer(&indexBufferDesc, &indexBufferInitialData, &m_IndexBuffer);
//	}
//	else
//	{
//		_pDevice->CreateBuffer(&indexBufferDesc, NULL, &m_IndexBuffer);
//	}
//
//	D3D11_BUFFER_DESC instanceBufferDesc;
//	ZeroMemory(&instanceBufferDesc, sizeof(instanceBufferDesc));
//	instanceBufferDesc.ByteWidth = sizeof(InstanceData)*m_uMaxInstancesAmount;//m_InstanceBufferData.size();
//	instanceBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
//	instanceBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//	instanceBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//	if (m_InstanceBufferData.size() > 0)
//	{
//		D3D11_SUBRESOURCE_DATA instanceBufferInitialData;
//		instanceBufferInitialData.pSysMem = &m_InstanceBufferData[0];
//		instanceBufferInitialData.SysMemPitch = 0;
//		instanceBufferInitialData.SysMemSlicePitch = 0;
//		_pDevice->CreateBuffer(&instanceBufferDesc, &instanceBufferInitialData, &m_InstanceBuffer);
//	}
//	else
//	{
//		HRESULT hr = _pDevice->CreateBuffer(&instanceBufferDesc, NULL, &m_InstanceBuffer);
//		int i = 1;
//	}
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//void InstancedMesh<T>::Render(ID3D11DeviceContext* _pDeviceContext)
//{
//	uint32_t stride[2] = { sizeof(T), sizeof(InstanceData) };
//	uint32_t offset[2] = { 0, 0 };
//	ID3D11Buffer* vbs[2] = { m_VertexBuffer, m_InstanceBuffer };
//
//	D3D11_MAPPED_SUBRESOURCE mappedResource;
//	// copy VertexShader constant buffer to GPU
//	ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
//	_pDeviceContext->Map(m_VertexShaderConstantBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
//	memcpy(mappedResource.pData, &m_VertexShaderData, sizeof(VertexShaderConstantBuffer));
//	_pDeviceContext->Unmap(m_VertexShaderConstantBuffer, 0);
//	// copy PixelShader constant buffer to GPU
//	ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
//	_pDeviceContext->Map(m_PixelShaderConstantBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
//	memcpy(mappedResource.pData, &m_PixelShaderData, sizeof(PixelShaderConstantBuffer));
//	_pDeviceContext->Unmap(m_PixelShaderConstantBuffer, 0);
//	// copy instance buffer to GPU
//	ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
//	if (m_InstanceBufferData.size() > 0)
//	{
//		_pDeviceContext->Map(m_InstanceBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
//		memcpy(mappedResource.pData, &m_InstanceBufferData[0], sizeof(InstanceData)*m_InstanceBufferData.size());
//		mappedResource.pData = &m_InstanceBufferData;
//		_pDeviceContext->Unmap(m_InstanceBuffer, 0);
//	}
//
//	_pDeviceContext->IASetVertexBuffers(0, 2, vbs, stride, offset);
//	_pDeviceContext->IASetInputLayout(InstancedMesh::InstancedMeshLayout);
//	_pDeviceContext->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R32_UINT, 0);
//	_pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
//	_pDeviceContext->VSSetShader(InstancedMesh::pVertexShader, nullptr, 0);
//	_pDeviceContext->VSSetConstantBuffers(0, 1, &m_VertexShaderConstantBuffer);
//	_pDeviceContext->PSSetShader(InstancedMesh::pPixelShader, nullptr, 0);
//	_pDeviceContext->PSSetConstantBuffers(0, 1, &m_PixelShaderConstantBuffer);
//
//	//ID3D11RasterizerState* OldRasterizerState;
//	//_pDeviceContext->RSGetState(&OldRasterizerState);
//	_pDeviceContext->RSSetState(InstancedMesh::pDefaultRasterizerState);
//
//	_pDeviceContext->DrawIndexedInstanced(m_IndexBufferData.size(), m_InstanceBufferData.size(), 0, 0, 0);
//
//	//_pDeviceContext->RSSetState(OldRasterizerState);
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//void InstancedMesh<T>::Initialize(ID3D11Device* _pDevice)
//{
//	if (InstancedMesh::pVertexShaderBlob == nullptr)
//	{
//		if ( S_OK == Effect::CompileShaderFromFile(L"InstancedMesh.vs.hlsl", "vs_5_0", &InstancedMesh::pVertexShaderBlob))
//		{
//			if (InstancedMesh::pVertexShader == nullptr)
//			{
//				_pDevice->CreateVertexShader(pVertexShaderBlob->GetBufferPointer(), pVertexShaderBlob->GetBufferSize(), nullptr, &pVertexShader);
//			}
//		}
//	}
//	if (InstancedMesh::pPixelShaderBlob == nullptr)
//	{
//		if ( S_OK == Effect::CompileShaderFromFile(L"InstancedMesh.ps.hlsl", "ps_5_0", &InstancedMesh::pPixelShaderBlob))
//		{
//			if (InstancedMesh::pPixelShader == nullptr)
//			{
//				_pDevice->CreatePixelShader(pPixelShaderBlob->GetBufferPointer(), pPixelShaderBlob->GetBufferSize(), nullptr, &pPixelShader);
//			}
//		}
//	}
//	if (InstancedMesh::InstancedMeshLayout == nullptr)
//	{
//		_pDevice->CreateInputLayout(InstancedMesh::InstancedMeshDesc, ARRAYSIZE(InstancedMesh::InstancedMeshDesc), InstancedMesh::pVertexShaderBlob->GetBufferPointer(), InstancedMesh::pVertexShaderBlob->GetBufferSize(), &InstancedMesh::InstancedMeshLayout);
//	}
//
//	if (InstancedMesh::pDefaultRasterizerState == nullptr)
//	{
//		// TODO: enable back face culling
//		D3D11_RASTERIZER_DESC RasterizerDesc = {
//			D3D11_FILL_SOLID, //D3D11_FILL_MODE FillMode;
//			D3D11_CULL_NONE,//D3D11_CULL_MODE CullMode;
//			FALSE, //BOOL FrontCounterClockwise;
//			0, //INT DepthBias;
//			0.0f,//FLOAT DepthBiasClamp;
//			0.0f,//FLOAT SlopeScaledDepthBias;
//			TRUE,//BOOL DepthClipEnable;
//			FALSE,//BOOL ScissorEnable;
//			TRUE,//BOOL MultisampleEnable;
//			TRUE//BOOL AntialiasedLineEnable;        
//		};
//
//		_pDevice->CreateRasterizerState(&RasterizerDesc, &InstancedMesh::pDefaultRasterizerState);
//	}
//}
//---------------------------------------------------------------------------------------------------
//template <class T>
//void InstancedMesh<T>::Destroy()
//{
//	if (InstancedMesh::InstancedMeshLayout)
//	{
//		InstancedMeshLayout->Release();
//		InstancedMeshLayout = nullptr;
//	}
//	if (InstancedMesh::pVertexShaderBlob)
//	{
//		InstancedMesh::pVertexShaderBlob->Release();
//		InstancedMesh::pVertexShaderBlob = nullptr;
//	}
//	if (InstancedMesh::pPixelShaderBlob)
//	{
//		InstancedMesh::pPixelShaderBlob->Release();
//		InstancedMesh::pPixelShaderBlob = nullptr;
//	}
//	if (InstancedMesh::pVertexShader)
//	{
//		InstancedMesh::pVertexShader->Release();
//		InstancedMesh::pVertexShader = nullptr;
//	}
//	if (InstancedMesh::pPixelShader)
//	{
//		InstancedMesh::pPixelShader->Release();
//		InstancedMesh::pPixelShader = nullptr;
//	}
//	if (InstancedMesh::pDefaultRasterizerState)
//	{
//		InstancedMesh::pDefaultRasterizerState->Release();
//		InstancedMesh::pDefaultRasterizerState = nullptr;
//	}
//}