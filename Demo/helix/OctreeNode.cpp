//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "OctreeNode.h"
#include <DirectXColors.h>

#include <algorithm>

#include "StdAfxH.h"

using namespace helix::async;
using namespace helix::datastructures;
using namespace helix::collision;

//---------------------------------------------------------------------------------------------------
//constructor

OctreeNode::OctreeNode(OctreeNode* _pParentNode, ThreadPool* _pThreadPool, const XMFLOAT3& _vCenter, float _fRadius, float _fLooseness, int32_t _iRecursionDepth, bool _bPrealloc) :
			m_pParentNode(_pParentNode),
			m_pThreadPool(_pThreadPool),
			m_fRadius(_fRadius),
			m_fLooseness(_fLooseness),
			m_iDepth(_iRecursionDepth),
			m_bPrealloc(_bPrealloc),
			m_uCurEntityCount(0),
			m_uCurTotalEntityCount(0)
{
	// create new loose bounding box
	float fLooseRadius = _fRadius * _fLooseness;
	m_AABB = BoundingBox(_vCenter, XMFLOAT3(fLooseRadius, fLooseRadius, fLooseRadius));

	//if we prealloc the nodes and theres still some recursion depth left:
	if (_bPrealloc && --_iRecursionDepth > -1)
	{
		//split box
		_fRadius *= 0.5f; // this is NOT the loose Radius!
		
		//create new child nodes
		for (int32_t i = 0; i < 8; ++i)
		{
			m_pChildren[i] = new OctreeNode(this, _pThreadPool, GetCenterOfNode(static_cast<BoxIndices> (i)), _fRadius, _fLooseness, _iRecursionDepth, _bPrealloc);
		}
	}
	else
	{
		// null all children if we don't preallocate
		for (uint32_t i = 0; i < 8; ++i)
		{
			m_pChildren[i] = nullptr;
		}
	}
}
//---------------------------------------------------------------------------------------------------
//destructor
OctreeNode::~OctreeNode()
{
	// free all da children =)
	for (uint32_t i = 0; i < 8; ++i)
	{
		if (m_pChildren[i] != nullptr)
		{		
			delete m_pChildren[i];
			m_pChildren[i] = nullptr;
		}
	}
}
//---------------------------------------------------------------------------------------------------
//recursivly draw all aabbs
//Should be called like this: _pPrimitiveBatchPositionColor->Begin(); DebugDraw(); _pPrimitiveBatchPositionColor->End();
void OctreeNode::DebugDraw(PrimitiveBatch<VertexPositionColor>* _pPrimitiveBatchPositionColor)
{
	XMFLOAT3 vPoints[8];
	m_AABB.GetCorners(vPoints);

	// Lines in x direction
	for (int32_t i = 0; i < 8; i += 2)
	{
		_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor(XMVectorSet(vPoints[i].x, vPoints[i].y, vPoints[i].z, 1.f), Colors::Red),
			VertexPositionColor(XMVectorSet(vPoints[i + 1].x, vPoints[i + 1].y, vPoints[i + 1].z, 1.f), Colors::Red)
			);
	}

	// Lines in y direction
	_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor(XMVectorSet(vPoints[0].x, vPoints[0].y, vPoints[0].z, 1.f), Colors::Red),
		VertexPositionColor(XMVectorSet(vPoints[3].x, vPoints[3].y, vPoints[3].z, 1.f), Colors::Red)
		);
	_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor(XMVectorSet(vPoints[1].x, vPoints[1].y, vPoints[1].z, 1.f), Colors::Red),
		VertexPositionColor(XMVectorSet(vPoints[2].x, vPoints[2].y, vPoints[2].z, 1.f), Colors::Red)
		);
	_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor(XMVectorSet(vPoints[4].x, vPoints[4].y, vPoints[4].z, 1.f), Colors::Red),
		VertexPositionColor(XMVectorSet(vPoints[7].x, vPoints[7].y, vPoints[7].z, 1.f), Colors::Red)
		);
	_pPrimitiveBatchPositionColor->DrawLine(
		VertexPositionColor(XMVectorSet(vPoints[5].x, vPoints[5].y, vPoints[5].z, 1.f), Colors::Red),
		VertexPositionColor(XMVectorSet(vPoints[6].x, vPoints[6].y, vPoints[6].z, 1.f), Colors::Red)
		);

	// Lines in z direction
	for (int i = 0; i < 4; i++)
	{
		_pPrimitiveBatchPositionColor->DrawLine(
			VertexPositionColor(XMVectorSet(vPoints[i].x, vPoints[i].y, vPoints[i].z, 1.f), Colors::Red),
			VertexPositionColor(XMVectorSet(vPoints[i + 4].x, vPoints[i + 4].y, vPoints[i + 4].z, 1.f), Colors::Red)
			);
	}

	//draw all child aabbs
	for (int32_t i = 0; i < 8; ++i)
	{
		if (m_pChildren[i] != nullptr)
			m_pChildren[i]->DebugDraw(_pPrimitiveBatchPositionColor);
	}
}
//---------------------------------------------------------------------------------------------------
bool OctreeNode::Update(TplGameObject* _pEntity)
{
	OctreeNode* pNode = _pEntity->GetNode();
	if (this == pNode)
	{	
		if (_pEntity->ContainedBy(m_AABB))
		{
			return true;
		}

		//the gameobject is no longer in the old AABB, try to insert into parent node
		if (m_pParentNode != nullptr)
		{
			if (m_pParentNode->Insert(_pEntity))
			{
				return true;
			}
		}

		_pEntity->SetFlag(TplGameObjectFlag_Destroyed);
		Remove(_pEntity);
	}
	else if(pNode != nullptr)
	{
		pNode->Update(_pEntity);
	}

	return false;
}
//---------------------------------------------------------------------------------------------------
//recrusivly insert a entity into the tree
inline bool OctreeNode::Insert(TplGameObject* _pEntity, bool _bAllowBottomUp)
{

	//the current node contains the new entity
	if (_pEntity->ContainedBy(m_AABB))
	{
		//check if we can go deeper and insert into one of the children
		if (m_iDepth > 0)
		{
			//try to insert into nearest child
			if (PlaceInNearestChild(_pEntity))
			{
				return true;
			}

			//Place in any child
			if (PlaceInAnyChild(_pEntity))
			{
				return true;
			}				
		}

		HLOGD("GameObject %d inserted at depth %d", _pEntity->m_uID, m_iDepth);

		//remove the entity from its old parent node and reset ownership
		Remove(_pEntity);

		// no matching child found, insert into this node
		_pEntity->GetNode() = this;
		m_Entities.push_front(_pEntity);

		return true;
	}
	else if (m_pParentNode != nullptr && _bAllowBottomUp)
	{
		// the entity is outside of the current bounds, try to insert it into the parent node
		return m_pParentNode->Insert(_pEntity);	
	}
	else
	{
		if (_bAllowBottomUp)
		{
			//was unable to insert into current node, and we reached the root node so destroy
			_pEntity->SetFlag(TplGameObjectFlag_Destroyed);
		}		
	}

	// failed to insert into any node
	return false;
}
//---------------------------------------------------------------------------------------------------
//remove an entity from the tree, this does not clear the node pointer of the entity!
void OctreeNode::Remove(uint32_t _uID)
{
	std::lock_guard<std::mutex> lock(m_Mutex);

	TEntityList::iterator remove = std::remove_if(m_Entities.begin(), m_Entities.end(), [&](const TplGameObject* pEntitiy){ return pEntitiy->GetID() == _uID; });
	
	//remove the entity
	m_Entities.erase(remove);
}
//---------------------------------------------------------------------------------------------------
//remove an entity from the tree, this static function clears the pointer to the node
void OctreeNode::Remove(TplGameObject* _pEntity)
{
	if (_pEntity->GetNode() != nullptr)
	{
		_pEntity->GetNode()->Remove(_pEntity->m_uID);
		_pEntity->GetNode() = nullptr;

		//TODO: consider removing the node if the list is empty
	}
}
//---------------------------------------------------------------------------------------------------
bool OctreeNode::PlaceInAnyChild(TplGameObject* _pEntity)
{
	for (uint32_t i = 0; i < 8; ++i)
	{
		if (m_pChildren[i] != nullptr)
		{
			//don't allow bottom-up insertion :D
			if (m_pChildren[i]->Insert(_pEntity, false))
			{
				return true;
			}
		}
		else if (m_iDepth > 0)
		{
			//create loose child bounding box
			float fLooseRadius = m_fRadius * 0.5f * m_fLooseness;
			BoundingBox AABB = BoundingBox(GetCenterOfNode(static_cast<BoxIndices>(i)), XMFLOAT3(fLooseRadius, fLooseRadius, fLooseRadius));

			//check if the entity would fit into the new node:
			if (_pEntity->ContainedBy(AABB))
			{
				m_pChildren[i] = new OctreeNode(this, m_pThreadPool, AABB.Center, m_fRadius * 0.5f, m_fLooseness, m_iDepth - 1, m_bPrealloc);
				return m_pChildren[i]->Insert(_pEntity, false);
			}				
		}
	}

	return false;
}
//---------------------------------------------------------------------------------------------------
//selects the best matching node for given vector or creates a new needed if neccessary
bool OctreeNode::PlaceInNearestChild(TplGameObject* _pEntity)
{
	//check if this lock is necessary
	//std::lock_guard<std::recursive_mutex> lock(m_Mutex);

	XMVECTOR vEntityPos = XMLoadFloat3(&_pEntity->m_vPosition);
	XMVECTOR vChildPos;

	float fMinDist = FLT_MAX;
	int32_t iMinID = 0;
	float fDist;

	for (int32_t i = 0; i < 8; ++i)
	{
		if (m_pChildren[i] == nullptr)
		{
			//child node does not exist, calculate position
			vChildPos = XMLoadFloat3(&GetCenterOfNode(static_cast<BoxIndices>(i)));
		}
		else
		{
			//load position from child position
			vChildPos = XMLoadFloat3(&m_pChildren[i]->m_AABB.Center);
		}

		//find nearest child id
		fDist = math::XMVector3DistanceF(vEntityPos, vChildPos);
		if (fDist < fMinDist)
		{
			fMinDist = fDist;
			iMinID = i;
		}
	}

	//try to create a new child note
	if (m_iDepth > 0 && m_pChildren[iMinID] == nullptr)
	{
		XMFLOAT3 vNewChildPos;
		XMStoreFloat3(&vNewChildPos, vChildPos);

		//create loose child bounding box
		float fLooseRadius = m_fRadius * 0.5f * m_fLooseness;
		BoundingBox AABB = BoundingBox(vNewChildPos, XMFLOAT3(fLooseRadius, fLooseRadius, fLooseRadius));

		//check if the entity would fit into the new node:
		if (_pEntity->ContainedBy(AABB))
		{
			m_pChildren[iMinID] = new OctreeNode(this, m_pThreadPool, vNewChildPos, m_fRadius * 0.5f, m_fLooseness, m_iDepth - 1, m_bPrealloc);
		}
	}

	if (m_pChildren[iMinID] == nullptr)
	{
		//no suitable child node was found
		return false;
	}
	else
	{
		return m_pChildren[iMinID]->Insert(_pEntity, false);
	}
}
//---------------------------------------------------------------------------------------------------
//returns the coordinates of the child node center
XMFLOAT3 OctreeNode::GetCenterOfNode(helix::BoxIndices _ChildIndex) const
{
	float fRadius = m_fRadius * 0.5f;

	switch (_ChildIndex)
	{
	case BoxIndices_BottomFrontLeft:
		return XMFLOAT3(m_AABB.Center.x - fRadius, m_AABB.Center.y - fRadius, m_AABB.Center.z - fRadius);
	case BoxIndices_BottomFrontRight:
		return XMFLOAT3(m_AABB.Center.x + fRadius, m_AABB.Center.y - fRadius, m_AABB.Center.z - fRadius);
	case BoxIndices_TopFrontRight:
		return XMFLOAT3(m_AABB.Center.x + fRadius, m_AABB.Center.y + fRadius, m_AABB.Center.z - fRadius);
	case BoxIndices_TopFrontLeft:
		return XMFLOAT3(m_AABB.Center.x - fRadius, m_AABB.Center.y + fRadius, m_AABB.Center.z - fRadius);
	case BoxIndices_BottomBackLeft:
		return XMFLOAT3(m_AABB.Center.x - fRadius, m_AABB.Center.y - fRadius, m_AABB.Center.z + fRadius);
	case BoxIndices_BottomBackRight:
		return  XMFLOAT3(m_AABB.Center.x + fRadius, m_AABB.Center.y - fRadius, m_AABB.Center.z + fRadius);
	case BoxIndices_TopBackRight:
		return XMFLOAT3(m_AABB.Center.x + fRadius, m_AABB.Center.y + fRadius, m_AABB.Center.z + fRadius);
	case BoxIndices_TopBackLeft:
		return XMFLOAT3(m_AABB.Center.x - fRadius, m_AABB.Center.y + fRadius, m_AABB.Center.z + fRadius);
	default:
		return m_AABB.Center;
	}
}
//---------------------------------------------------------------------------------------------------
//this function should only be called on the RootNode of the Octree!
void OctreeNode::GatherCollisions(_Out_ std::vector<CollisionInfo>& _Collisions)
{
	HASSERTD(m_pParentNode == nullptr, "this function should only be called on the RootNode of the Octree!");

	// starts gather task sync or async if a ThreadPool is set
	StartGatherCollisionsTask();

	if (m_pThreadPool)
	{
		m_pThreadPool->WaitForAllTasks();
	}
	
	//Collect found collisions
	GetCollisions(_Collisions);
}
//---------------------------------------------------------------------------------------------------
void OctreeNode::StartGatherCollisionsTask()
{
	if ((m_pThreadPool && m_pThreadPool->AddTask(std::async(std::launch::async /*| std::launch::deferred*/, &OctreeNode::GatherCollisionsTask, this))) == false)
	{		
		//no ThreadPool or no free task slot available in ThreadPool, start sync
		GatherCollisionsTask();
	}
}
//---------------------------------------------------------------------------------------------------
void OctreeNode::GatherCollisionsTask()
{
	//std::lock_guard<std::mutex> lock(m_Mutex);

	//clear previous collisions
	m_Collisions.resize(0);
		
	HFOREACH_CONST(it, end, m_Entities)
		TplGameObject* pGO1 = *it;

		if (pGO1->CheckFlag(TplGameObjectFlag_NoCollision))
		{
			continue;
		}

		//check for collisions with the other nodes in the list
		for (std::list<TplGameObject*>::iterator it2 = std::next(it); it2 != end; ++it2)
		{
			TplGameObject* pGO2 = *it2;
	
			if (pGO2->CheckFlag(TplGameObjectFlag_NoCollision))
			{
				continue;
			}

			//both are static objects, no collision to evaluate
			if (pGO1->CheckFlag(TplGameObjectFlag_Static) && pGO2->CheckFlag(TplGameObjectFlag_Static))
			{
				continue;
			}

			CollisionInfo Info;
			if (pGO1->Intersects(*pGO2, Info))
			{
				Info.m_pGO1 = pGO1;
				Info.m_pGO2 = pGO2;
				m_Collisions.push_back(Info);
			}
		}

		//check for collision with child nodes
		for (uint32_t i = 0; i < 8; ++i)
		{
			//skip emtpy subnodes
			if (m_pChildren[i] != nullptr && m_pChildren[i]->GetTotalNumOfEntities() > 0)
			{
				if (pGO1->Intersects(m_pChildren[i]->m_AABB))
				{
					m_pChildren[i]->GatherCollisions(pGO1, m_Collisions);
				}
			}
		}

	HFOREACH_CONST_END

	//add subnodes to thetask list
	for (uint32_t i =  0; i < 8; ++i)
	{
		if (m_pChildren[i] != nullptr &&  m_pChildren[i]->GetTotalNumOfEntities() > 0)
		{
			m_pChildren[i]->StartGatherCollisionsTask();
		}
	}
}
//---------------------------------------------------------------------------------------------------
//this function assumes _pEntity has been locked by the caller
void OctreeNode::GatherCollisions(_In_ TplGameObject* _pEntity, _Out_ std::vector<CollisionInfo>& _Collisions) const
{
	//std::lock_guard<std::mutex> lock(m_Mutex);

	HFOREACH_CONST(it, end, m_Entities)		
		//check for collisions with the other nodes in the list

		CollisionInfo Info;
		if ((*it)->Intersects(*_pEntity, Info))
		{
			Info.m_pGO1 = _pEntity;
			Info.m_pGO2 = *it;
			_Collisions.push_back(Info);
		}

	HFOREACH_CONST_END

	//check for collision with child nodes
	for (uint32_t i = 0; i < 8; ++i)
	{
		if (m_pChildren[i] != nullptr &&  m_pChildren[i]->GetTotalNumOfEntities() > 0)
		{
			if (_pEntity->Intersects(m_pChildren[i]->m_AABB))
			{
				m_pChildren[i]->GatherCollisions(_pEntity, _Collisions);
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------
//this function copys the collision info to _Collisions
void OctreeNode::GetCollisions(_Out_ std::vector<CollisionInfo>& _Collisions)
{
	//std::lock_guard<std::mutex> lock(m_Mutex);
	_Collisions.insert(_Collisions.end(), m_Collisions.begin(), m_Collisions.end());
	
	for (uint32_t i = 0; i < 8; ++i)
	{
		if (m_pChildren[i] != nullptr)
		{
			m_pChildren[i]->GetCollisions(_Collisions);
		}
	}
}
//---------------------------------------------------------------------------------------------------
//This functions copys the objects in from previous GatherInVolumeTask to _Objects
void OctreeNode::GetObjectsInVolume(_Out_ std::vector<TplGameObject*>& _Objects)
{
	_Objects.insert(_Objects.end(), m_ObjectsInVolume.begin(), m_ObjectsInVolume.end());

	for (uint32_t i = 0; i < 8; ++i)
	{
		if (m_pChildren[i] != nullptr)
		{
			m_pChildren[i]->GetObjectsInVolume(_Objects);
		}
	}
}
//---------------------------------------------------------------------------------------------------
// This function should only be called once after all entities have been updated and before starting any Task!
uint32_t OctreeNode::GatherTotalNumOfEntities(uint32_t _uDepth)
{
	//std::unique_lock<std::mutex> lock(m_Mutex);
	uint32_t uEntities = static_cast<uint32_t>(m_Entities.size());

	m_uCurEntityCount = uEntities;

	if (_uDepth > 0)
	{
		--_uDepth;

		for (uint32_t i = 0; i < 8; ++i)
		{
			if (m_pChildren[i] != nullptr)
			{
				//lock.unlock();
				uEntities += m_pChildren[i]->GatherTotalNumOfEntities(_uDepth);
				//lock.lock();
			}
		}
	}

	m_uCurTotalEntityCount = uEntities;

	return uEntities;
}
//---------------------------------------------------------------------------------------------------
// computes the recursion depth for the minimum object radius to be contained by a cell
int32_t OctreeNode::CalculateRecursionDepth(float _fMaxRadius, float _fMinRadius)
{
	int32_t iDepth = 0;

	while (_fMaxRadius > _fMinRadius)
	{
		++iDepth;
		_fMaxRadius *= 0.5f;
	}

	return iDepth;
}
//---------------------------------------------------------------------------------------------------
// computes the number of octree nodes for the given depth
uint32_t OctreeNode::CalculateNumOfNodes(int32_t iRecursionDepth)
{
	uint32_t iNodes = 0;

	// the number of nodes in a octree with depth D is calculated by accumulating all levels:
	// D = 3 => 8^3 + 8^2 +8^1 + 8^0

	while (iRecursionDepth > 0)
	{
		// 8^x = 2 << ((x-1)*3) since 2^3 = 8
		// 8^2 = 8*8 = 64 = 2^6 = 1 << 6
		iNodes += (1 << (iRecursionDepth * 3));
		--iRecursionDepth;
	}

	// add 1 for 8^0 = 1 => root node
	return iNodes + 1;
}