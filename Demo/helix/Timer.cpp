//Copyright(c) 2014
//Authors: Steffen Wiewel, Fabian Wahlster

#include "Timer.h"
#include <time.h>
#include <Windows.h>
#include <tchar.h>
using namespace helix;

Timer::Timer(){
	m_paused = false;
	int64_t freq;
	QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
	m_secPerCount = 1.0 / (double)freq; //perform division once so we can use multiplication from now on.
	Reset();
}

inline int64_t Timer::GetCurrentCount(){
	//returns the current tick count
	int64_t cur;
	QueryPerformanceCounter((LARGE_INTEGER*)&cur);
	return cur;
}

size_t Timer::GetLocalTimeString(TCHAR* _pBuffer, size_t _BufferSize){
	time_t rawtime;
	struct tm timeinfo;
	//get current time
	time (&rawtime);
	//convert to local time and return if conversion failed
	if(localtime_s(&timeinfo,&rawtime) != 0) return -1;
	//format time string
	return _tcsftime(_pBuffer, _BufferSize, TEXT("%X"), &timeinfo);
}

//Call this function before using any other than the static functions above
void Timer::Reset(){
	//Restart the timer
	m_startTime = GetCurrentCount();
	m_prevTime = m_startTime;
	m_pausedTime = 0;
	m_totalPausedTime = 0;
	m_paused = false;
}

float Timer::ElapsedTimeF(){
	return static_cast<float>(m_elapsedTime);
}

double Timer::ElapsedTimeD(){
	return m_elapsedTime;
}

//call once per frame at start of computation
 void Timer::FrameTick(){
	if(m_paused){
		m_elapsedTime = 0.0; // no animation when game is paused
		return;
	}

	//aquire the current time
	QueryPerformanceCounter((LARGE_INTEGER*)&m_curTime);

	//compute elapsed time in seconds
	m_elapsedTime = (m_curTime - m_prevTime) * m_secPerCount;

	m_prevTime = m_curTime;

	//scheduler might produce negative timespan
	if(m_elapsedTime < 0.0) m_elapsedTime = 0.0;
}

void Timer::Pause(){
	if(m_paused) return;
	QueryPerformanceCounter((LARGE_INTEGER*)&m_pausedTime);
	m_paused = true;
}

void Timer::Unpause(){
	if(m_paused == false) return;
	QueryPerformanceCounter((LARGE_INTEGER*)&m_prevTime);
	m_totalPausedTime += (m_prevTime - m_pausedTime);//accumulate the paused interval
	m_pausedTime = 0;
	m_paused = false;
}

double Timer::TotalTimeD(){
	//(m_curTime - m_startTime) - m_totalPausedTime
	if(m_paused){
		//don't count the time the game is currently paused so use m_pausedTime as curTime
		return ((m_pausedTime-m_startTime)-m_totalPausedTime)*m_secPerCount;
	}else{
		//aquire the current time
		QueryPerformanceCounter((LARGE_INTEGER*)&m_curTime);
		return ((m_curTime-m_startTime)-m_totalPausedTime)*m_secPerCount;
	}
}