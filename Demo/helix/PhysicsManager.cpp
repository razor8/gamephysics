//Copyright(c) 2014
//Authors: Fabian Wahlster

#include "PhysicsManager.h"
#include "StdAfxH.h"
#include "Logger.h"
#include "RigidBody.h"
#include "CollisionEvaluation.h"

using namespace helix::async;
using namespace helix::datastructures;
using namespace helix::physics;

//---------------------------------------------------------------------------------------------------
PhysicsManager::PhysicsManager() :
	//no observer, looped execution, no suspend after execution
	async::IAsyncObject(nullptr, true, false),
	m_pTplGameObjects(TplGameObjectContainer::Instance())
{
}
//---------------------------------------------------------------------------------------------------
PhysicsManager::~PhysicsManager()
{
}
//---------------------------------------------------------------------------------------------------
//main work loop; has to be implemented by implemented (derived from IAsyncObject)
void PhysicsManager::Execute()
{
	m_Timer.Reset();

	//initialize data here:
	double fDeltaTime = m_fUpdateInterval;
	double fTotalDeltaTime = 0.0;

	int64_t iResumeThreadThreshold = 0;
	
	//simulation input data
	std::vector<RigidBody*> DynamicRigidBodies;

	TRigidBodies FakeEmptyInput;
	FakeEmptyInput.begin = FakeEmptyInput.end = m_pTplGameObjects->RigidBodiesEnd();

	//use fixed interval instead of approximated computation
	if (m_bFixedUpdateInterval)
		fDeltaTime = m_fUpdateInterval;

	//resize(0) does not change the capacity of the vector, saving some reallocations when inserting again
	//resulting in higher performance but also higher* mem consumption (all these vectors only store pointers)
	DynamicRigidBodies.resize(0);

	m_Collisions.resize(0);

	//get dynamic rigidbodies
	m_GameObjects.GetRigidBodiesWithFlag(DynamicRigidBodies, datastructures::GameObjectFlag_Static, false);

	//>>>>>>>>>>>>>>>>>>>ASYNCHRON<<<<<<<<<<<<<<<<<<<<<<<

	//update systems here:
	{
		SetBarrier(m_uTotalNotifyCount);

		//ATTENTION this system works directly on the m_GameObjects!
		//BVs are updated inside this system
		{	//start RigidBodySystems
			m_RigidBodySystem.SetDeltaTime(static_cast<float>(fDeltaTime));
			m_RigidBodySystem.SetData(FakeEmptyInput);
			m_RigidBodySystem.Continue();
		}

		//Thread barrier: wait for all systems to finish -> till they call SignalObserver
		WaitForNotifications();
	}

	//>>>>>>>>>>>>>>>>>>>SYNCHRONIZED<<<<<<<<<<<<<<<<<<<<<<<

	//this is the sychonized part, work that can't be parallelized should be done here
	//e.g. collecting all the simulated data to render
	{
		//std::lock_guard<std::mutex> lock(m_LockMutex);

		// >>>>>>>>OCTREE<<<<<<<<
		//if (m_pOctree)
		//{
		//	// update all dynamic octree entities

		//	//dynamic rigidbodies
		//	HFOREACH_CONST(it, end, DynamicRigidBodies)
		//		if (m_pOctree->Update(*it) == false)
		//		{
		//			HLOGD("RigidBody %d removed from Octree", (*it)->m_uID);
		//		}
		//	HFOREACH_CONST_END

		//		//this updates the number of entities in all nodes
		//		m_pOctree->GatherTotalNumOfEntities();

		//	//gather collisions
		//	m_pOctree->GatherCollisions(m_Collisions);

		//	//gather visible nodes
		//}

		////compute collisions
		//HFOREACH_PARALLEL(m_Collisions, [&], const collision::CollisionInfo&, Info)

		//	collision::CollisionEvaluation::ComputeCollision(Info.m_pGO1, Info.m_pGO2, Info);

		//HFOREACH_PARALLEL_END

		//remove destroyed gameobjects
		m_GameObjects.remove_if(GameObjectFlag_Destroyed);
	}

	//all tasks are done, measure time it took
	fDeltaTime = m_Timer.TotalTimeD();

	//calculate remaining time for UpdateInterval to be complete
	int64_t iTimeToSleepInMicroSec = static_cast<int64_t>((m_fUpdateInterval - fDeltaTime) * 1000000.0);

	//sleep for remaining time
	if (m_bFreeUpdateInterval == false && iTimeToSleepInMicroSec > iResumeThreadThreshold)
	{
		std::unique_lock<std::mutex> wait(m_WaitMutex);
		m_CV.wait_for(wait, std::chrono::microseconds(iTimeToSleepInMicroSec));

		//update the delta time
		fDeltaTime = m_Timer.TotalTimeD();

		++m_uNumOfResumes;
	}

	fTotalDeltaTime += fDeltaTime;
	m_fDeltaTime = fDeltaTime;
	++m_uNumOfSimulations;


	//double fAvgDeltaTime = m_uNumOfSimulations > 0 ? fTotalDeltaTime / static_cast<double>(m_uNumOfSimulations) : 0.f;
	//show some stats here
	//HLOG("FixedDeltaTime %f AvgDeltaTime %f\n\t NumOfSimulations %d NumOfResumes %d", m_fUpdateInterval, fAvgDeltaTime, m_uNumOfSimulations, uNumOfResumes);
}
//void PhysicsManager::SetOctree(std::shared_ptr<Octree> _pOctree)
//{
//	std::lock_guard<std::mutex> lock(m_LockMutex);
//	if (m_pOctree == nullptr)
//	{
//		m_pOctree = _pOctree;
//	}
//	else
//	{
//		HFATAL("There can only be one Octree per scene!");
//	}
//}
//---------------------------------------------------------------------------------------------------
void PhysicsManager::SetUpdateInterval(double _fUpdateInterval)
{
	std::lock_guard<std::mutex> lock(m_LockMutex);
	m_fUpdateInterval = _fUpdateInterval;
}
//---------------------------------------------------------------------------------------------------
double PhysicsManager::GetDeltaTime()
{
	if (IsPaused())
	{
		return 0.0;
	}
	else
	{
		//std::lock_guard<std::mutex> lock(m_LockMutex);
		return m_fDeltaTime;
	}
}
//---------------------------------------------------------------------------------------------------
void PhysicsManager::SetFixedUpdateInterval(bool _bFixedUpdateInterval)
{
	std::lock_guard<std::mutex> lock(m_LockMutex);
	m_bFixedUpdateInterval = _bFixedUpdateInterval;
}
//---------------------------------------------------------------------------------------------------
void PhysicsManager::SetFreeUpdateInterval(bool _bFreeUpdateInterval)
{
	std::lock_guard<std::mutex> lock(m_LockMutex);
	m_bFreeUpdateInterval = _bFreeUpdateInterval;
}
//---------------------------------------------------------------------------------------------------
void PhysicsManager::StopSubsystems()
{
	// stop all subsystems
	m_RigidBodySystem.Stop();
}
//---------------------------------------------------------------------------------------------------
void PhysicsManager::InitializeSubsystems()
{
	m_RigidBodySystem.SetObserver(this);
	//m_RigidBodySystem.SetDefaultForce(XMFLOAT3(0, -9.81f, 0));
	m_RigidBodySystem.StartInNewThread();
}
//---------------------------------------------------------------------------------------------------
void PhysicsManager::SetDefaultForce(const XMFLOAT3& _DefaultForce)
{
	m_RigidBodySystem.SetDefaultForce(_DefaultForce);
}
//---------------------------------------------------------------------------------------------------
//void PhysicsManager::AddGameObject(GameObject& _GameObject)
//{
//	std::lock_guard<std::mutex> lock(m_LockMutex);
//
//	GameObject* pGO = m_GameObjects.Insert(_GameObject);
//
//	if (pGO != nullptr)
//	{
//		// everything worked, insert into octree
//		if (m_pOctree == nullptr || m_pOctree->Insert(pGO) == false)
//		{
//			HWARNING("Unable to insert GameObject %d!", _GameObject.m_uID);
//		}
//	}
//}
//---------------------------------------------------------------------------------------------------
void PhysicsManager::GetVisibleObjects(_Out_ std::vector<GameObject>& _VisibleObjects, uint64_t& _uVersionID)
{
	std::lock_guard<std::mutex> lock(m_LockMutex);

	if (_uVersionID == m_uNumOfSimulations)
		return;

	_VisibleObjects.resize(0);
	//TODO: implement this correctly based on m_VisibleObjects indices (collect game objects from m_GameObjects);

	HFOREACH_CONST(it, end, m_GameObjects)
		//Objects need to be copied in case they get deleted during rendering
		_VisibleObjects.push_back(*it->second);
	HFOREACH_CONST_END


	_uVersionID = m_uNumOfSimulations;
}
//---------------------------------------------------------------------------------------------------
void PhysicsManager::AddRigidBodyForces(TRigidBodyForceMap& _Forces)
{
	std::lock_guard<std::mutex> lock(m_LockMutex);
	m_RigidBodySystem.AddExternalData(_Forces);
}