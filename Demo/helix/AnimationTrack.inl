#ifndef ANIMATIONTRACK_INL
#define ANIMATIONTRACK_INL
#include "AnimationTrack.h"

namespace helix
{
	namespace animation
	{
#pragma region Getters
//==================================================================================================================================
// Getters
//==================================================================================================================================
//----------------------------------------------------------------------------------------------------------------------------------
		template<class T>
		inline void AnimationTrack<T>::GetAnimationData(_Out_ T& _Output, _In_ const float _fTime)
		{
			HASSERT(m_KeyFrameList.size() > 0, "KeyFrameList is empty");

			const KeyFrame<T> FirstKeyFrame = (*m_KeyFrameList.cbegin());
			const KeyFrame<T> LastKeyFrame = m_KeyFrameList.back();

			// check if time is inside animation interval
			if (FirstKeyFrame.fTime > _fTime)
			{
				// animation hasn't startet yet
				_Output = FirstKeyFrame.Data;
				return;
			}
			if (LastKeyFrame.fTime < _fTime)
			{
				// animation is over
				_Output = LastKeyFrame.Data;
				return;
			}

			KeyFrame<T> StartKeyFrame = FirstKeyFrame;

			KeyFrame<T> EndKeyFrame;

			HFOREACH_CONST(it, end, m_KeyFrameList)

				EndKeyFrame = (*it);
			if (EndKeyFrame.fTime > _fTime)
			{
				Interpolate(_Output, _fTime, StartKeyFrame, EndKeyFrame);
				break;
			}
			else
			{
				StartKeyFrame = (*it);
			}
			HFOREACH_CONST_END
		}
//----------------------------------------------------------------------------------------------------------------------------------
		template<class T>
		inline void AnimationTrack<T>::GetAnimationOrientation(_Out_ T& _Output, _In_ const float _fTime)
		{
			HASSERT(m_KeyFrameList.size() > 0, "KeyFrameList is empty");

			const KeyFrame<T> FirstKeyFrame = (*m_KeyFrameList.cbegin());
			const KeyFrame<T> LastKeyFrame = m_KeyFrameList.back();
			const float fTime = _fTime + 0.01f;
			// check if time is inside animation interval
			if (FirstKeyFrame.fTime >= fTime)
			{
				// animation hasn't startet yet
				ExtendByTangent(_Output, FirstKeyFrame.Data, FirstKeyFrame.Tangent);
				return;
			}
			if (LastKeyFrame.fTime <= fTime)
			{
				// animation is over
				ExtendByTangent(_Output, FirstKeyFrame.Data, FirstKeyFrame.Tangent);
				return;
			}

			KeyFrame<T> StartKeyFrame = FirstKeyFrame;

			KeyFrame<T> EndKeyFrame;

			HFOREACH_CONST(it, end, m_KeyFrameList)

				EndKeyFrame = (*it);
			if (EndKeyFrame.fTime > fTime)
			{
				// TODO dynamically get future coefficient
				Interpolate(_Output, fTime, StartKeyFrame, EndKeyFrame);
				break;
			}
			else
			{
				StartKeyFrame = (*it);
			}
			HFOREACH_CONST_END
		}
#pragma endregion

#pragma region Track Keframe Managemant
//==================================================================================================================================
// Track-Keyframe
//==================================================================================================================================

		template<class T>
		inline void AnimationTrack<T>::BuildTrack(const std::vector<T>& _Data, float _fStartTime, float _fEndTime, const AnimationFlag _Flags)
		{
			float fTimeStep = (_fEndTime - _fStartTime) / _Data.size();
			float fTotalTime = 0.0f; 

			KeyFrame<T> KeyFrame(fTotalTime, _Data[0], _Data[0], _Data[0], 0.3f, 0.3f);
			
			fTotalTime += fTimeStep;
			
			PreviousKeyFrame.SetFlag(_Flags);
			
			KeyFrame.push_back(KeyFrame>);

			const int iDataSize = _Data.size() - 1; // The last keyframe must be treated seperateley
			for (int i = 1; i < iDataSize; ++i) // i = 1, because the first keyframe is already filled
			{
				KeyFrame = KeyFrame<T>(fTotalTime, _Data[i], _Data[i + 1], _Data[i - 1]);
				fTotalTime += fTimeStep;

				KeyFrame.SetFlag(_Flags);

				m_KeyFrameList.push_back(KeyFrame);
			}

			KeyFrame = KeyFrame<T>(fTotalTime, _Data[iDataSize], _Data[iDataSize], _Data[iDataSize - 1]);
			fTotalTime += fTimeStep;

			KeyFrame.SetFlag(_Flags);

			m_KeyFrameList.push_back(CurrentKeyFrame);
		}

		template<>
		inline void AnimationTrack<XMFLOAT3>::BuildTrack(const std::vector<XMFLOAT3>& _Data, float _fStartTime, float _fEndTime, const AnimationFlag _Flags)
		{
			KeyFrame<XMFLOAT3> PreviousKeyFrame(0.0f, _Data[0], _Data[0], _Data[0]);
			PreviousKeyFrame.SetFlag(AnimationFlag(_Flags));
			float fTotalLength = 0.0f;

			m_KeyFrameList.push_back(PreviousKeyFrame);

			KeyFrame<XMFLOAT3> CurrentKeyFrame;
			CurrentKeyFrame.fTime = -1.0f;

			const size_t iDataSize = _Data.size() - 1; // The last keyframe must be treated seperateley
			for (int i = 1; i < iDataSize; ++i) // i = 1, because the first keyframe is already filled
			{

				CurrentKeyFrame = KeyFrame<XMFLOAT3>(0.0f, _Data[i], _Data[i + 1], _Data[i - 1]);
				CurrentKeyFrame.SetFlag(_Flags);

				ComputeCombinedControl(CurrentKeyFrame);
				
				CurrentKeyFrame.Bezier = BezierCurve(PreviousKeyFrame.Data, PreviousKeyFrame.OutControl, CurrentKeyFrame.InControl, CurrentKeyFrame.Data);
				CurrentKeyFrame.ArcLengthFunction.Initialize(CurrentKeyFrame.Bezier.GetCurve());
				fTotalLength += CurrentKeyFrame.ArcLengthFunction.GetTotalLength();

				m_KeyFrameList.push_back(CurrentKeyFrame);

				PreviousKeyFrame = CurrentKeyFrame;
			}

			CurrentKeyFrame = KeyFrame<XMFLOAT3>(0.0f, _Data[iDataSize], _Data[iDataSize], _Data[iDataSize]);
			CurrentKeyFrame.SetFlag(_Flags);

			ComputeCombinedControl(CurrentKeyFrame);

			CurrentKeyFrame.Bezier = BezierCurve(PreviousKeyFrame.Data, PreviousKeyFrame.OutControl, CurrentKeyFrame.InControl, CurrentKeyFrame.Data);
			CurrentKeyFrame.ArcLengthFunction.Initialize(CurrentKeyFrame.Bezier.GetCurve());
			fTotalLength += CurrentKeyFrame.ArcLengthFunction.GetTotalLength();

			m_KeyFrameList.push_back(CurrentKeyFrame);
			
			float fTotalTime = _fEndTime - _fStartTime;
			float fTime = 0.0f;
			HFOREACH_CONST(it, end, m_KeyFrameList)
				float fLength = it->ArcLengthFunction.GetTotalLength();
				fTime += (fLength / fTotalLength) * fTotalTime;
				it->fTime = fTime;
			HFOREACH_CONST_END
		}

		template<>
		inline void AnimationTrack<XMFLOAT3>::ComputeCombinedControl(KeyFrame<XMFLOAT3>& _KeyFrame, float _fSmoothness)
		{
			XMVECTOR vData = XMLoadFloat3(&_KeyFrame.Data);
			XMVECTOR vOutTangent = XMVectorSubtract(XMLoadFloat3(&_KeyFrame.OutControl), vData);
			XMVECTOR vInTangent = XMVectorSubtract(vData, XMLoadFloat3(&_KeyFrame.InControl));

			float fOutLength = XMVectorGetX(XMVector3LengthEst(vOutTangent) );
			float fInLength = XMVectorGetX(XMVector3LengthEst(vInTangent));

			XMVECTOR vTangent = XMVector3Normalize(XMVectorScale(XMVectorAdd(vOutTangent, vInTangent), 0.5f));
			_fSmoothness *= 0.33333333f;
			_KeyFrame.OutControl = math::XMFloat3Get(XMVectorAdd(XMVectorScale(vTangent, fOutLength * _fSmoothness), vData));
			_KeyFrame.InControl = math::XMFloat3Get(XMVectorAdd(XMVectorScale(vTangent, -fInLength * _fSmoothness), vData));
		}

		//----------------------------------------------------------------------------------------------------------------------------------
//		///<summary>
//		///<para>The added keyframe must extend the animation time.</para>
//		///<para>
//		///An animation segment is always defined by the start and the end keyframe.
//		///</para>
//		///</summary>
//		///<param name='_fTime'>The time at which the keyframe will be inserted</param>
//		///<param name='_Data'>The value to be animated</param>
//		///<param name='_Flags'>Optional, kind of interpolation for segment before the added keyframe. Defaults to linear.</param>
//		///<param name='_fEaseIn'>Optional, factor by which time before the keyframe is extended. Used for smoothed approach.</param>
//		///<param name='_fEaseOut'>Optional, factor by which time after the keyframe is extended. Used for smoothed leave.</param>
//		///<param name='_Tangent'>Optional, slope of interpolation curve at the keyframe time.</param>
//		///<param name='_fTangentInLength'>Optional, factor of slope in the segment before the keyframe.</param>
//		///<param name='_fTangentOutLength'>>Optional, factor of slope in the segment after the keyframe.</param>
//		template<class T>
//		inline void AnimationTrack<T>::AppendKeyFrame(const float _fTime,
//			const T& _Data,
//			const AnimationFlag _Flags,
//			const float _fEaseIn,
//			const float _fEaseOut,
//			const T& _Tangent,
//			const float _fTangentInLength,
//			const float _fTangentOutLength)
//		{
//			KeyFrame<T> Frame;
//			Frame.fTime = _fTime;
//			Frame.Data = _Data;
//			Frame.SetFlag(_Flags);
//			Frame.fEaseIn = _fEaseIn;
//			Frame.fEaseOut = _fEaseOut;
//
//
//			if (Frame.CheckFlag(AnimationFlag_BezierInterpolation))
//			{
//				HWARNING("Trying to set BezierInterpolation on non Vector3 variable. This is not supported.")
//			}
//
//			m_KeyFrameList.push_back(Frame);
//		}
////----------------------------------------------------------------------------------------------------------------------------------
//		template<>
//		inline void AnimationTrack<XMFLOAT3>::AppendKeyFrame(const float _fTime,
//			const XMFLOAT3& _Data,
//			const AnimationFlag _Flags,
//			const float _fEaseIn,
//			const float _fEaseOut,
//			const XMFLOAT3& _Tangent,
//			const float _fTangentInLength,
//			const float _fTangentOutLength)
//		{
//			KeyFrame<XMFLOAT3> Frame;
//			Frame.fTime = _fTime;
//			Frame.Data = _Data;
//			Frame.SetFlag(_Flags);
//			Frame.fEaseIn = _fEaseIn;
//			Frame.fEaseOut = _fEaseOut;
//
//			XMVECTOR vP_2 = XMLoadFloat3(&_Tangent);
//			XMVECTOR vP_3 = XMLoadFloat3(&_Data);
//
//			Frame.OutControl = math::XMFloat3Get(vP_3 + XMVectorScale(vP_2, _fTangentOutLength));
//			Frame.InControl = math::XMFloat3Get(vP_3 + XMVectorScale(vP_2, _fTangentOutLength));
//			
//			// every XMFLOAT3 keyframe gets an bezier component. 
//			if (!m_KeyFrameList.empty())
//			{
//				KeyFrame<XMFLOAT3> PrevFrame = m_KeyFrameList.back();
//				XMVECTOR vP_0 = XMLoadFloat3(&PrevFrame.Data);
//				XMVECTOR vP_1 = XMLoadFloat3(&PrevFrame.OutControl);
//
//				// P_2 = P_3 - size * direction
//				vP_2 = XMLoadFloat3(&Frame.InControl);
//
//				Frame.Bezier = BezierCurve(math::XMFloat3Get(vP_0), math::XMFloat3Get(vP_1), math::XMFloat3Get(vP_2), math::XMFloat3Get(vP_3));
//				Frame.ArcLengthFunction.Initialize(Frame.Bezier.GetCurve());
//			}
//
//			// KeyFrameList is empty
//			m_KeyFrameList.push_back(Frame);
//		}
//
#pragma endregion

#pragma region Time Distance Function
//==================================================================================================================================
// Time Distance Function
//==================================================================================================================================
//----------------------------------------------------------------------------------------------------------------------------------
		template<class T>
		float AnimationTrack<T>::TimeToDistance(const float _fTime, const KeyFrame<T>& _StartFrame, const KeyFrame<T>& _EndFrame)
		{
			float fT = (_fTime - _StartFrame.fTime) / (_EndFrame.fTime - _StartFrame.fTime);
			fT = _EndFrame.ArcLengthFunction.At(fT);

			if (_EndFrame.CheckFlag(AnimationFlag_Easing))
			{
				// sinusodial segments:
				//				,--  ]- upper sinusodial segment
				//			   x  <---- k2
				//		     /   ]----- line segment
				//		    x   <------ k1 
				//		---�   ]------- lower sinusodial segment
				const float fPiOverTwo = XM_PI / 2.0f;
				const float fTwoOverPi = 2.0f / XM_PI;

				// Computer Animation: Algorithms and Techniques, S82

				const float fK1 = _StartFrame.fEaseOut;
				const float fK2 = 1.0f - _EndFrame.fEaseIn;
				const float fLine = fK1 * fTwoOverPi + fK2 - fK1 + (1.0f - fK2) * fTwoOverPi;
				float fSinusodial = 0.0f;

				if (fT < fK1)
				{
					// lower sinusodial segment
					fSinusodial = fK1 * fTwoOverPi * (XMScalarSinEst((fT / fK1) * fPiOverTwo - fPiOverTwo) + 1);
				}
				else if (fT > fK2)
				{

					// upper sinusodial segment
					fSinusodial = fK1 * fTwoOverPi + fK2 - fK1 + (1.0f - fK2) * fTwoOverPi * XMScalarSinEst(((fT - fK2) / (1.0f - fK2)) * fPiOverTwo);
				}
				else
				{
					// line segment
					fSinusodial = fK1 * fTwoOverPi + fT - fK1;
				}
				float fResult = fSinusodial / fLine;
				return fResult;
			}
			else
			{
				// linear time distance function
				return (_fTime - _StartFrame.fTime) / (_EndFrame.fTime - _StartFrame.fTime);
			}
		}
#pragma endregion

#pragma region Interpolation Selection
		
//==================================================================================================================================
// Interpolation selection
//==================================================================================================================================
//----------------------------------------------------------------------------------------------------------------------------------
		template<class T>
		inline void AnimationTrack<T>::Interpolate(	T& _Output, 
													const float _fTime, 
													const KeyFrame<T>& _StartFrame, 
													KeyFrame<T>& _EndFrame)
		{
			const float fInterpolant = TimeToDistance(_fTime, _StartFrame, _EndFrame);

			if (_EndFrame.CheckFlag(AnimationFlag_Quaternion))
			{
				LinearInterpolationQuaternion(_Output, _StartFrame.Data, _EndFrame.Data, fInterpolant);
			}
			else if (_EndFrame.CheckFlag(AnimationFlag_BezierInterpolation))
			{
				_EndFrame.Bezier.Calculate(_Output, fInterpolant);
			}
			else
			{
				LinearInterpolation(_Output, _StartFrame.Data, _EndFrame.Data, fInterpolant);
			}
		}
#pragma endregion

#pragma region Linear Interpolation
//==================================================================================================================================
// Linear
//==================================================================================================================================
//----------------------------------------------------------------------------------------------------------------------------------
		template<class T>
		inline void AnimationTrack<T>::LinearInterpolation(T& _Output, const T& _vStart, const T& _vEnd, const float _fInterpolant)
		{
			_Output = _vStart + _fInterpolant * (_vEnd - _vStart);
		}
//----------------------------------------------------------------------------------------------------------------------------------
		template<class T>
		inline void AnimationTrack<T>::LinearInterpolationQuaternion(	T& _Output, 
																		const T& _Start, 
																		const T& _End, 
																		const float _fInterpolant)
		{
			HWARNINGD("Trying to apply quaternion linear interpolation on non XMFLOAT4 value. Falling back to normal interpolation.");
			LinearInterpolation(_Output, _Start, _End, _fInterpolant);
		}
//----------------------------------------------------------------------------------------------------------------------------------
		template<>
		inline void AnimationTrack<XMFLOAT4>::LinearInterpolationQuaternion(	XMFLOAT4& _Output, 
																				const XMFLOAT4 & _vStart, 
																				const XMFLOAT4 & _vEnd, 
																				const float _fInterpolant)
		{
			XMStoreFloat4(&_Output, XMQuaternionSlerp(XMLoadFloat4(&_vStart), XMLoadFloat4(&_vEnd), _fInterpolant));
		}
//----------------------------------------------------------------------------------------------------------------------------------
		template<>
		inline void AnimationTrack<XMFLOAT2>::LinearInterpolation(	_Out_ XMFLOAT2& _Output, 
																	const _In_ XMFLOAT2& _vStart, 
																	const _In_ XMFLOAT2& _vEnd,
																	_In_ const float _fInterpolant)
		{
			XMStoreFloat2(&_Output, XMVectorLerp(XMLoadFloat2(&_vStart), XMLoadFloat2(&_vEnd), _fInterpolant));
		}
//----------------------------------------------------------------------------------------------------------------------------------
		template<>
		inline void AnimationTrack<XMFLOAT3>::LinearInterpolation(	_Out_ XMFLOAT3& _Output, 
																	const _In_ XMFLOAT3& _vStart, 
																	const _In_ XMFLOAT3& _vEnd, 
																	_In_ const float _fInterpolant)
		{
			XMStoreFloat3(&_Output, XMVectorLerp(XMLoadFloat3(&_vStart), XMLoadFloat3(&_vEnd), _fInterpolant));
		}
//----------------------------------------------------------------------------------------------------------------------------------
		template<>
		inline void AnimationTrack<XMFLOAT4>::LinearInterpolation(	_Out_ XMFLOAT4& _Output, 
																	const _In_ XMFLOAT4& _vStart, 
																	const _In_ XMFLOAT4& _vEnd, _In_ 
																	const float _fInterpolant)
		{
			XMStoreFloat4(&_Output, XMVectorLerp(XMLoadFloat4(&_vStart), XMLoadFloat4(&_vEnd), _fInterpolant));
		}
#pragma endregion
	}
}
#endif