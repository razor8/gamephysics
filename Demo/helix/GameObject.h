//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "BoundingVolume.h"
#include <mutex>

namespace helix
{
	namespace datastructures
	{
		using namespace DirectX;

		class OctreeNode;

		Flag32E(GameObjectFlags)
		{
			GameObjectFlag_GameObject =		1 << 0, // 1
			GameObjectFlag_RigidBody =		1 << 1, // 2
			GameObjectFlag_MovingObject =	1 << 2, // 4
			GameObjectFlag_Destroyed =		1 << 3, // 8
			GameObjectFlag_Static =			1 << 4, // 16
			GameObjectFlag_Invisible =		1 << 5  // 32
		};

		class GameObject : public Flag<uint32_t>
		{
			virtual void hidden(){}
		public:
			GameObject();
			GameObject(const XMFLOAT3& _vPosition, const XMFLOAT3& _vScale, const XMFLOAT3& _vEulerAngles, GameObjectFlags _kFlags = GameObjectFlag_GameObject);
			GameObject(const GameObject& _GameObject);
			GameObject(GameObject&& _GameObject);
			GameObject& operator =(GameObject& _Other);
			~GameObject();

			// returns the specialized type of the gameobject
			GameObjectFlags GetType();

			//update bounding volumes using m_vPosition and m_vOrientation
			void UpdateBoundingVolumes();

			//Lock / Unlock the objects mutex
			void Lock();
			void Unlock();
			bool TryLock();

			//GETTER / SETTER

			const uint32_t& GetID() const;
			void SetID(const uint32_t& _uID);

			const uint32_t& GetMeshID() const;
			void SetMeshID(const uint32_t& _uMeshID);

			const OctreeNode* GetOctreeNode() const;
			void SetOctreeNode(OctreeNode* _pNode);

			const XMFLOAT3& GetPosition() const;
			void SetPosition(const XMFLOAT3& _vPosition);

			const XMFLOAT3& GetScale() const;
			void SetScale(const XMFLOAT3& _vScale);

			const XMFLOAT4& GetOrientation() const;

			//Attention! This function also changes the RotationMatrix!
			void SetOrientation(const XMFLOAT4& _vOrientation);

			const XMFLOAT3X3& GetRotation() const;

			//Attention! This function also changes the OrientationQuaternion!
			void SetRotation(const XMFLOAT3X3& _mRotation);

		protected:
			mutable std::recursive_mutex m_Mutex;

		public:
			uint32_t m_uID = HUNDEFINED;
			uint32_t m_uMeshID = HUNDEFINED;

			//for unigrid only, delete after openproject
			uint32_t m_uHash = HUNDEFINED;

			OctreeNode* m_pNode = nullptr;
			collision::BoundingVolume m_BV;

			XMFLOAT3 m_vPosition;
			XMFLOAT3 m_vScale;

			XMFLOAT4 m_vOrientation;
			XMFLOAT3X3 m_mRotation;
		};

		inline void GameObject::Lock() { m_Mutex.lock(); }
		inline void GameObject::Unlock() { m_Mutex.unlock(); }
		inline bool GameObject::TryLock() { return m_Mutex.try_lock(); }

		inline const uint32_t& GameObject::GetID() const { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return m_uID; }
		inline void GameObject::SetID(const uint32_t& _uID) { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); m_uID = _uID; }

		inline const uint32_t& GameObject::GetMeshID() const { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return m_uMeshID; }
		inline void GameObject::SetMeshID(const uint32_t& _uMeshID) { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); m_uMeshID = _uMeshID; }

		inline const OctreeNode* GameObject::GetOctreeNode() const { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return m_pNode; }
		inline void GameObject::SetOctreeNode(OctreeNode* _pNode) { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); m_pNode = _pNode; }

		inline const XMFLOAT3& GameObject::GetPosition() const { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return m_vPosition; }
		inline void GameObject::SetPosition(const XMFLOAT3& _vPosition) { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); m_vPosition = _vPosition; UpdateBoundingVolumes(); }

		inline const XMFLOAT3& GameObject::GetScale() const { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return m_vScale; }
		inline void GameObject::SetScale(const XMFLOAT3& _vScale) { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); m_vScale = _vScale; UpdateBoundingVolumes(); }

		inline const XMFLOAT4& GameObject::GetOrientation() const { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return m_vOrientation; }
		inline void GameObject::SetOrientation(const XMFLOAT4& _vOrientation)
		{
			std::lock_guard<std::recursive_mutex> Lock(m_Mutex);
			m_vOrientation = _vOrientation;

			XMVECTOR Quaternion = XMLoadFloat4(&m_vOrientation);
			m_mRotation = math::XMFloat3x3Get(XMMatrixRotationQuaternion(Quaternion));

			UpdateBoundingVolumes();
		}

		inline const XMFLOAT3X3& GameObject::GetRotation() const { std::lock_guard<std::recursive_mutex> Lock(m_Mutex); return m_mRotation; }
		inline void GameObject::SetRotation(const XMFLOAT3X3& _mRotation)
		{
			std::lock_guard<std::recursive_mutex> Lock(m_Mutex);
			m_mRotation = _mRotation;

			XMMATRIX Matrix = XMLoadFloat3x3(&m_mRotation);
			m_vOrientation = math::XMFloat4Get(XMQuaternionRotationMatrix(Matrix));

			UpdateBoundingVolumes();
		}

	}; // namespace datastructures
}; // namespace helix

#endif // GAMEOBJECT_H