//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "TplGameObjectContainer.h"
#include "StdAfxH.h"

using namespace helix::datastructures;

//---------------------------------------------------------------------------------------------------
TplGameObjectContainer::TplGameObjectContainer(uint32_t _uGameObjectCount, uint32_t _uRigidBodiesCount) :
	m_GameObjectPool(_uGameObjectCount),
	m_RigidBodyPool(_uRigidBodiesCount)
{
}
//---------------------------------------------------------------------------------------------------
TplGameObjectContainer::~TplGameObjectContainer()
{
}
//---------------------------------------------------------------------------------------------------
void TplGameObjectContainer::Resize(uint32_t _uGameObjectCount, uint32_t _uRigidBodiesCount)
{
	//only allow container resize if its currently unused
	try
	{
		std::lock_guard<std::mutex> lock(m_Mutex);

		m_GameObjectPool.Resize(_uGameObjectCount);
		m_RigidBodyPool.Resize(_uRigidBodiesCount);
	}
	catch (...)
	{
		HFATALD("Unable to resize container when the container is still locked!");
	}
}
//---------------------------------------------------------------------------------------------------

TplGameObject* TplGameObjectContainer::Create(const XMFLOAT3 & _vPosition, const XMFLOAT3 & _vScale, const XMFLOAT3 & _vEulerAngles, TplGameObjectFlag _kFlags)
{
	if (m_Mutex.try_lock())
	{
		TplGameObject* pGo = m_GameObjectPool.Alloc();

		m_Mutex.unlock();

		if (nullptr == pGo)
		{
			HWARNINGD("Could not allocate GameObject");
			return nullptr;
		}

		pGo->Initialize(_vPosition, _vScale, _vEulerAngles, _kFlags);
		return pGo;
	}
	else
	{
		return nullptr;
		HWARNINGD("Unable to allocate GameObject when the container is still locked!");
	}
}
//---------------------------------------------------------------------------------------------------
TplGameObject * TplGameObjectContainer::CreateRigidbody(const XMFLOAT3 & _vPosition, const XMFLOAT3 & _vScale, const XMFLOAT3 & _vEulerAngles, float _fMass, float _fDamping, const XMFLOAT3 & _vLinearVelocity, const XMFLOAT3 & _vAngularVelocity, const XMFLOAT3X3 & _mInvInertiaTensor, TplGameObjectFlag _kFlags)
{
	if (m_Mutex.try_lock())
	{
		TplGameObject* pGo = m_RigidBodyPool.Alloc();

		m_Mutex.unlock();

		if (nullptr == pGo)
		{
			HWARNINGD("Could not allocate Rigidbody");
			return nullptr;
		}

		pGo->Initialize(_vPosition, _vScale, _vEulerAngles, _kFlags);
		pGo->InitializeRigidBody(_fMass, _fDamping, _vLinearVelocity, _vAngularVelocity, _mInvInertiaTensor);

		return pGo;
	}
	else
	{
		HWARNINGD("Unable to allocate RigidBody when the container is still locked!");
		return nullptr;
	}
}
//---------------------------------------------------------------------------------------------------
void TplGameObjectContainer::Cleanup()
{
	for (TplMemPool<TplGameObject>::iterator it = m_GameObjectPool.begin(); it != m_GameObjectPool.end();)
	{
		if (it->CheckFlag(TplGameObjectFlag_Destroyed))
		{
			it = m_GameObjectPool.Free(it);
		}
		else
		{
			++it;
		}
	}

	for (TplMemPool<TplGameObject, RigidBodyData>::iterator it = m_RigidBodyPool.begin(); it != m_RigidBodyPool.end();)
	{
		if (it->CheckFlag(TplGameObjectFlag_Destroyed))
		{
			it = m_RigidBodyPool.Free(it);
		}
		else
		{
			++it;
		}
	}
}
//---------------------------------------------------------------------------------------------------
void TplGameObjectContainer::Lock()
{
	try
	{
		m_Mutex.lock();
	}
	catch (...)
	{
		HFATALD("Failed to lock mutex");
	}
}
//---------------------------------------------------------------------------------------------------
void TplGameObjectContainer::Unlock()
{
	Cleanup();

	try
	{
		m_Mutex.unlock();
	}
	catch (...)
	{
		HFATALD("Failed to unlock mutex");
	}
}
//---------------------------------------------------------------------------------------------------