//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#ifndef STDAFXH_H
#define STDAFXH_H

//TODO: move this to project settings
//#define HPARALLEL

// HFOREACH usage :
//std::vector<SomeClass> vec;
//HFOREACH(it,end,vec)
//{
//	it->Member++
//}

// why use this instead of this ?
//for (auto &val : container)
//{
//}

//our macro enforces the convention to get the end iterator once before iterating, not in every iteration.
//it also forces the end iterator to be const, this also means that the vector size should not be changed during iteration.

#ifndef HFOREACH
#define HFOREACH(_it,_end,_container) { auto _it = _container.begin();auto _end = _container.end(); for(;_it != _end; ++_it){
#endif

#ifndef HFOREACH_END
#define HFOREACH_END }}
#endif

#ifndef HFOREACH_TYPE
#define HFOREACH_TYPE(_it,_end,_container,_type) { _type::iterator _it = _container.begin(); _type::iterator _end = _container.end(); for(;_it != _end; ++_it){
#endif

#ifndef HFOREACH_TYPE_END
#define HFOREACH_TYPE_END }}
#endif

#ifndef HFOREACH_CONST
#define HFOREACH_CONST(_it,_end,_container) { auto _it = _container.begin();const auto _end = _container.cend(); for(;_it != _end; ++_it){
#endif

#ifndef HFOREACH_CONST_END
#define HFOREACH_CONST_END }}
#endif

#ifndef HFOREACH_CONST_TYPE
#define HFOREACH_CONST_TYPE(_it,_end,_container,_type) { _type::iterator _it = _container.begin();const _type::iterator _end = _container.cend(); for(;_it != _end; ++_it){
#endif

#ifndef HFOREACH_CONST_TYPE_END
#define HFOREACH_CONST_TYPE_END }}
#endif

#ifdef HPARALLEL

//Usage:

//HFOREACH_PARALLEL(m_Collisions, [&], const collision::CollisionInfo&, Info)
//	GameObject* pGO1 = m_GameObjects.Get(Info.m_uID1);
//	GameObject* pGO2 = m_GameObjects.Get(Info.m_uID2);
//
//	MovingObject* pMO1 = dynamic_cast<MovingObject*>(pGO1);
//	MovingObject* pMO2 = dynamic_cast<MovingObject*>(pGO2);
//	if (pMO1 != nullptr && pMO2 != nullptr)
//		MovingObject::ComputeCollision(*pMO1, *pMO2, Info);
//HFOREACH_PARALLEL_END

#ifndef HFOREACH_PARALLEL
#include <experimental\algorithm>

#define HFOREACH_PARALLEL(_container,_var_capture, _type, _var_name)\
std::experimental::parallel::for_each(\
std::experimental::parallel::par,\
_container.begin(),\
_container.end(),\
_var_capture (_type _var_name){
#endif

#ifndef HFOREACH_PARALLEL_END
#define HFOREACH_PARALLEL_END });
#endif

#ifndef HFOREACH_PARALLEL_RANGE
#define HFOREACH_PARALLEL_RANGE(_start_itr, _end_itr ,_var_capture, _type, _var_name)\
std::experimental::parallel::for_each(\
std::experimental::parallel::par,\
_start_itr, _end_itr,\
_var_capture (_type _var_name){
#endif

#ifndef HFOREACH_PARALLEL_RANGE_END
#define HFOREACH_PARALLEL_RANGE_END });
#endif

#else // IMPERATIVE

#ifndef HFOREACH_PARALLEL
#define HFOREACH_PARALLEL(_container,_var_capture, _type, _var_name)\
for(_type _var_name : _container){
#endif

#include <algorithm>

#ifndef HFOREACH_PARALLEL_RANGE
#define HFOREACH_PARALLEL_RANGE(_start_itr, _end_itr ,_var_capture, _type, _var_name)\
std::for_each(_start_itr, _end_itr, _var_capture (_type _var_name){
#endif

#ifndef HFOREACH_PARALLEL_RANGE_END
#define HFOREACH_PARALLEL_RANGE_END });
#endif

#ifndef HFOREACH_PARALLEL_END
#define HFOREACH_PARALLEL_END }
#endif

#endif // HPARALLEL

#ifndef HSAFE_DELETE
#define HSAFE_DELETE(x) if(x != nullptr) {delete x; x = nullptr;}
#endif

#ifndef HSAFE_DELETE_ARRAY
#define HSAFE_DELETE_ARRAY(x) if (x != nullptr) {delete [] x; x = nullptr; }
#endif

#ifndef HSAFE_RELEASE
#define HSAFE_RELEASE(x) if(x != nullptr){ x->Release(); x = nullptr;}
#endif

#ifndef HUNDEFINED
#define HUNDEFINED -1
#endif

namespace helix
{

	// siehe DirectXCollision.inl g_BoxOffset
	//   7---------6
	//  /|		  /| 
	// / |       / |
	//3---------2  |
	//|	 |		|  |
	//|  4------|--5
	//|	/		| /
	//|/		|/
	//0---------1
	// ja hab ich selber gemacht du penner :P

	enum BoxIndices
	{
		BoxIndices_BottomFrontLeft = 0,
		BoxIndices_BottomFrontRight = 1,
		BoxIndices_TopFrontRight = 2,
		BoxIndices_TopFrontLeft = 3,
		BoxIndices_BottomBackLeft = 4,
		BoxIndices_BottomBackRight = 5,
		BoxIndices_TopBackRight = 6,
		BoxIndices_TopBackLeft = 7,
		BoxIndices_NumOfEdges = 8
	};

}; // namespace helix

#endif //STDAFXH_H