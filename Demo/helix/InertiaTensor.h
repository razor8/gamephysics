#ifndef INERTIATENSOR_H
#define INERTIATENSOR_H

#include "XMMath.h"
#include "Logger.h"
#include <vector>

namespace helix
{
	namespace physics
	{
		using namespace DirectX;
		class InertiaTensor
		{
		public:
			static XMFLOAT3X3 ComputeMeshInertiaTensor(std::vector<XMFLOAT3>& _Vertices, float _fTotalMass);
			static XMFLOAT3X3 ComputeMeshInertiaTensor(std::vector<XMFLOAT3>& _Vertices, std::vector<float>& _Masses);
			static XMFLOAT3X3 ComputeCuboidInertiaTensor(const XMFLOAT3& _vDim, float _fTotalMass);
			static XMFLOAT3X3 ComputeSolidSphereInertiaTensor(float _fRadius, float _fTotalMass);
			static XMFLOAT3X3 ComputeHollowSphereInertiaTensor(float _fRadius, float _fTotalMass);
		};
	}
}

#endif