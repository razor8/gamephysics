//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "BoundingVolume.h"

using namespace helix::collision;
using namespace helix::math;

//---------------------------------------------------------------------------------------------------
BoundingVolume::BoundingVolume()
{
}
//---------------------------------------------------------------------------------------------------
//BoundingSphere constructor
BoundingVolume::BoundingVolume(_In_ const XMFLOAT3& _vPos, _In_ float _fRadius, _In_ const XMFLOAT3& _vOffset) :
m_vBSphereOffset(_vOffset)
{
	m_pBSphere = std::make_shared<BoundingSphere>(Add(_vPos, m_vBSphereOffset), _fRadius);
}
//---------------------------------------------------------------------------------------------------
//AABB constructor
BoundingVolume::BoundingVolume(_In_ const XMFLOAT3& _vPos, _In_ const XMFLOAT3& _vExtents, _In_ const XMFLOAT3& _vOffset) :
m_vAABBOffset(_vOffset)
{
	m_pAABB = std::make_shared<BoundingBox>(Add(_vPos, m_vAABBOffset), _vExtents);
}
//---------------------------------------------------------------------------------------------------
//OOBB constructor
BoundingVolume::BoundingVolume(_In_ const XMFLOAT3& _vPos, _In_ const XMFLOAT3& _vExtents, _In_ const XMFLOAT4& _vOrientation, _In_ const XMFLOAT3& _vOffset) :
m_vOBBOffset(_vOffset)
{
	m_pOBB = std::make_shared<BoundingOrientedBox>(Add(_vPos, m_vOBBOffset), _vExtents, _vOrientation);
}
//---------------------------------------------------------------------------------------------------
//Frustum constructor
BoundingVolume::BoundingVolume(_In_ const XMFLOAT3& _vOrigin, _In_ const XMFLOAT4& _vOrientation, _In_ float _fRightSlope, _In_ float _fLeftSlope, _In_ float _fTopSlope, _In_ float _fBottomSlope, _In_ float _fNear, _In_ float _fFar, _In_ const XMFLOAT3& _vOffset) :
m_vBFrustumOffset(_vOffset)
{
	m_pBFrustum = std::make_shared<BoundingFrustum>(Add(_vOrigin, m_vBFrustumOffset), _vOrientation, _fRightSlope, _fLeftSlope, _fTopSlope, _fBottomSlope, _fNear, _fFar);
}
//---------------------------------------------------------------------------------------------------
//Frustum constructor 2
BoundingVolume::BoundingVolume(_In_ CXMMATRIX _mProjectition)
{
	m_pBFrustum = std::make_shared<BoundingFrustum>(_mProjectition);
}
//---------------------------------------------------------------------------------------------------
//Copy constructor
BoundingVolume::BoundingVolume(const BoundingVolume& _BV) : Flag<uint32_t>(_BV.GetFlag())
{
	std::shared_ptr<BoundingSphere> pBSphere = _BV.GetBSphere();
	std::shared_ptr<BoundingBox> pAABB = _BV.GetAABB();
	std::shared_ptr<BoundingOrientedBox> pOBB = _BV.GetOBB();
	std::shared_ptr<BoundingFrustum> pFrustum = _BV.GetBFrustum();

	m_vBSphereOffset = _BV.m_vBSphereOffset;
	m_vAABBOffset = _BV.m_vAABBOffset;
	m_vOBBOffset = _BV.m_vOBBOffset;
	m_vBFrustumOffset = _BV.m_vBFrustumOffset;

	if (pBSphere != nullptr)
		m_pBSphere = std::make_shared<BoundingSphere>(pBSphere->Center, pBSphere->Radius);

	if (pAABB != nullptr)
		m_pAABB = std::make_shared<BoundingBox>(pAABB->Center, pAABB->Extents);

	if (pOBB != nullptr)
		m_pOBB = std::make_shared<BoundingOrientedBox>(pOBB->Center, pOBB->Extents, pOBB->Orientation);

	if (pFrustum != nullptr)
		m_pBFrustum = std::make_shared<BoundingFrustum>(pFrustum->Origin,
														pFrustum->Orientation,
														pFrustum->RightSlope,
														pFrustum->LeftSlope,
														pFrustum->TopSlope,
														pFrustum->BottomSlope,
														pFrustum->Near,
														pFrustum->Far);
}
//---------------------------------------------------------------------------------------------------
BoundingVolume::~BoundingVolume()
{
}
//---------------------------------------------------------------------------------------------------
void BoundingVolume::SetNewBSphere(_In_ const XMFLOAT3& _vPos, _In_ float _fRadius, _In_ const XMFLOAT3& _vOffset)
{
	m_vBSphereOffset = _vOffset;
	if (m_pBSphere == nullptr)
	{
		m_pBSphere = std::make_shared<BoundingSphere>(Add(_vPos, m_vBSphereOffset), _fRadius);
	}
	else
	{
		m_pBSphere->Center = Add(_vPos, m_vBSphereOffset);
		m_pBSphere->Radius = _fRadius;
	}
}
//---------------------------------------------------------------------------------------------------
void BoundingVolume::SetNewAABB(_In_ const XMFLOAT3& _vPos, _In_ const XMFLOAT3& _vExtents, _In_ const XMFLOAT3& _vOffset)
{
	m_vAABBOffset = _vOffset;
	if (m_pAABB == nullptr)
	{
		m_pAABB = std::make_shared<BoundingBox>(Add(_vPos, m_vAABBOffset), _vExtents);
	}
	else
	{
		m_pAABB->Center = Add(_vPos, m_vAABBOffset);
		m_pAABB->Extents = _vExtents;
	}
}
//---------------------------------------------------------------------------------------------------
void BoundingVolume::SetNewOBB(_In_ const XMFLOAT3& _vPos, _In_ const XMFLOAT3& _vExtents, _In_ const XMFLOAT4& _vOrientation, _In_ const XMFLOAT3& _vOffset)
{
	m_vOBBOffset = _vOffset;
	if (m_pOBB == nullptr)
	{
		m_pOBB = std::make_shared<BoundingOrientedBox>(Add(_vPos, m_vOBBOffset), _vExtents, _vOrientation);
	}
	else
	{
		m_pOBB->Center = Add(_vPos, m_vOBBOffset);
		m_pOBB->Extents = _vExtents;
		m_pOBB->Orientation = _vOrientation;
	}
}
//---------------------------------------------------------------------------------------------------
void BoundingVolume::SetNewBFrustum(_In_ const XMFLOAT3& _vOrigin, _In_ const XMFLOAT4& _vOrientation, _In_ float _fRightSlope, _In_ float _fLeftSlope, _In_ float _fTopSlope, _In_ float _fBottomSlope, _In_ float _fNear, _In_ float _fFar, _In_ const XMFLOAT3& _vOffset)
{
	m_vBFrustumOffset = _vOffset;
	if (m_pBFrustum == nullptr)
	{
		m_pBFrustum = std::make_shared<BoundingFrustum>(Add(_vOrigin, m_vBFrustumOffset), _vOrientation, _fRightSlope, _fLeftSlope, _fTopSlope, _fBottomSlope, _fNear, _fFar);
	}
	else
	{
		m_pBFrustum->Origin = _vOrigin;
		m_pBFrustum->Orientation = _vOrientation;
		m_pBFrustum->RightSlope = _fRightSlope;
		m_pBFrustum->LeftSlope = _fLeftSlope;
		m_pBFrustum->TopSlope = _fTopSlope;
		m_pBFrustum->BottomSlope = _fBottomSlope;
		m_pBFrustum->Near = _fNear;
		m_pBFrustum->Far = _fFar;
	}
}
//---------------------------------------------------------------------------------------------------
void BoundingVolume::SetNewBFrustum(_In_ CXMMATRIX _mProjectition)
{
	if (m_pBFrustum == nullptr)
	{
		m_pBFrustum = std::make_shared<BoundingFrustum>(_mProjectition);
	}
	else
	{
		m_pBFrustum->operator=(BoundingFrustum(_mProjectition));
	}
}
//---------------------------------------------------------------------------------------------------
void BoundingVolume::Update(const XMFLOAT3& _vPos)
{
	if (m_pBSphere != nullptr)
		m_pBSphere->Center = Add(_vPos, m_vBSphereOffset);
	if (m_pAABB != nullptr)
		m_pAABB->Center = Add(_vPos, m_vAABBOffset);
	if (m_pOBB != nullptr)
		m_pOBB->Center = Add(_vPos, m_vOBBOffset);

	//TODO: check out if this update works
	if (m_pBFrustum != nullptr)
		m_pBFrustum->Origin = Add(_vPos, m_vBFrustumOffset);
}
//---------------------------------------------------------------------------------------------------
void BoundingVolume::Update(const XMFLOAT4& _vRot)
{
	if (m_pOBB != nullptr)
		m_pOBB->Orientation = _vRot;

	//TODO: check out if this update works
	if (m_pBFrustum != nullptr)
		m_pBFrustum->Orientation = _vRot;
}
//---------------------------------------------------------------------------------------------------
void BoundingVolume::Update(const XMFLOAT3& _vPos, const XMFLOAT4& _vRot)
{
	if (m_pBSphere != nullptr)
		m_pBSphere->Center = Add(_vPos, m_vBSphereOffset);
	if (m_pAABB != nullptr)
		m_pAABB->Center = Add(_vPos, m_vAABBOffset);
	if (m_pOBB != nullptr)
	{
		m_pOBB->Center = Add(_vPos, m_vOBBOffset);
		m_pOBB->Orientation = _vRot;
	}

	//TODO: check out if this update works
	if (m_pBFrustum != nullptr)
	{
		m_pOBB->Center = Add(_vPos, m_vOBBOffset);
		m_pBFrustum->Orientation = _vRot;
	}
}
//---------------------------------------------------------------------------------------------------
//Compares bounding volumes of the same type and decends to the best fitting volume if available
bool BoundingVolume::IntersectsPairwise(_In_ const BoundingVolume& sh, _Out_ VolumeType& _IntersectingVolume) const
{
	bool bIntersects = false;

	std::shared_ptr<BoundingSphere> pBS;
	if (m_pBSphere != nullptr && (pBS = sh.GetBSphere()) != nullptr)
	{
		if ((bIntersects = m_pBSphere->Intersects(*pBS)) == false)
			return false;
		_IntersectingVolume = VolumeType_Sphere;
	}

	std::shared_ptr<BoundingBox> pAABB;
	if (m_pAABB != nullptr && (pAABB = sh.GetAABB()) != nullptr)
	{
		if ((bIntersects = m_pAABB->Intersects(*pAABB)) == false)
			return false;
		_IntersectingVolume = VolumeType_AABB;
	}

	std::shared_ptr<BoundingOrientedBox> pOBB;
	if (m_pOBB != nullptr && (pOBB = sh.GetOBB()) != nullptr)
	{
		if ((bIntersects = m_pOBB->Intersects(*pOBB)) == false)
			return false;
		_IntersectingVolume = VolumeType_OBB;
	}

	return bIntersects;
}
//---------------------------------------------------------------------------------------------------
//checks if any of the volumes is inside the AABB and decends to the best fitting volume if available
ContainmentType BoundingVolume::ContainedBy(_In_ const BoundingBox& box) const
{
	ContainmentType CT = DISJOINT;

	if (m_pBSphere != nullptr)
	{
		if ((CT = box.Contains(*m_pBSphere)) == CONTAINS) return CT;
	}

	if (m_pAABB != nullptr)
	{
		if ((CT = box.Contains(*m_pAABB)) == CONTAINS) return CT;
	}

	if (m_pOBB != nullptr)
	{
		if ((CT = box.Contains(*m_pOBB)) == CONTAINS) return CT;
	}

	return CT;
}
//returns true if the BVs intersect and writes the info to _CollisionInfo
//---------------------------------------------------------------------------------------------------
bool BoundingVolume::GetPairwiseCollisionInfo(_In_ const BoundingVolume& _BV, _Out_ CollisionInfo& _CollisionInfo)
{
	VolumeType Type;
	if (IntersectsPairwise(_BV, Type))
	{
		std::shared_ptr<BoundingSphere> pBSphere;
		std::shared_ptr<BoundingBox> pAABB;
		std::shared_ptr<BoundingOrientedBox> pOBB;

		switch (Type)
		{
		case VolumeType_Sphere:
			pBSphere = _BV.GetBSphere();
			if (pBSphere != nullptr)
				CollisionContact::SimpleContact(*m_pBSphere, *pBSphere, _CollisionInfo);
			else
				HWARNINGD("BoundingSphere invalid");
			break;
		case VolumeType_AABB:
			pAABB = _BV.GetAABB();
			if (pAABB != nullptr)
				CollisionContact::SimpleContact(*m_pAABB, *pAABB, _CollisionInfo);
			else
				HWARNINGD("AABB invalid");
			break;
		case VolumeType_OBB:
			pOBB = _BV.GetOBB();
			if (pOBB != nullptr)
				CollisionContact::SimpleContact(*m_pOBB, *pOBB, _CollisionInfo);
			else
				HWARNINGD("OBB invalid");
			break;
		case VolumeType_Tree:
		default:
			HWARNINGD("Implement this");
			//Implementation missing
			_CollisionInfo.m_bIsValid = false;
			return false;
		}

		_CollisionInfo.m_bIsValid = true;
		return true;
	}

	_CollisionInfo.m_bIsValid = false;

	return false;
}
//---------------------------------------------------------------------------------------------------
bool BoundingVolume::GetCollisionInfo(_In_ const BoundingVolume& _BV, _Out_ CollisionInfo& _CollisionInfo)
{
	VolumeType Type1, Type2;
	if (Intersects(_BV, Type1, Type2))
	{
	switch (Type1)
		{
		case VolumeType_Sphere:
			if (Type2 == VolumeType_Sphere)
			{
				CollisionContact::SimpleContact(*m_pBSphere, *_BV.GetBSphere(), _CollisionInfo);
			}
			else if (Type2 == VolumeType_AABB)
			{
				CollisionContact::SimpleContact(*m_pBSphere, *_BV.GetAABB(), _CollisionInfo);
			}
			else if (Type2 == VolumeType_OBB)
			{
				HFATALD("Not implemented");
				//CollisionContact::SimpleContact(*m_pBSphere, *_BV.GetOBB(), _CollisionInfo);
				_CollisionInfo.m_bIsValid = false;
			}
			else
			{
				HWARNINGD("Type invalid");
			}
			break;
		case VolumeType_AABB:
			if (Type2 == VolumeType_Sphere)
			{
				CollisionContact::SimpleContact(*m_pAABB, *_BV.GetBSphere(), _CollisionInfo);
			}
			else if (Type2 == VolumeType_AABB)
			{
				CollisionContact::SimpleContact(*m_pAABB, *_BV.GetAABB(), _CollisionInfo);
			}
			else if (Type2 == VolumeType_OBB)
			{
				HFATALD("Not implemented");
				//CollisionContact::SimpleContact(*m_pAABB, *_BV.GetOBB(), _CollisionInfo);
				_CollisionInfo.m_bIsValid = false;
			}
			else
			{
				HWARNINGD("Type invalid");
			}
			break;
		case VolumeType_OBB:
			if (Type2 == VolumeType_Sphere)
			{
				HFATALD("Not implemented");
				//CollisionContact::SimpleContact(*m_pOBB, *_BV.GetBSphere(), _CollisionInfo);
				_CollisionInfo.m_bIsValid = false;
			}
			else if (Type2 == VolumeType_AABB)
			{
				HFATALD("Not implemented");
				//CollisionContact::SimpleContact(*m_pOBB, *_BV.GetAABB(), _CollisionInfo);
				_CollisionInfo.m_bIsValid = false;
			}
			else if (Type2 == VolumeType_OBB)
			{
				CollisionContact::SimpleContact(*m_pOBB, *_BV.GetOBB(), _CollisionInfo);
			}
			else
			{
				HWARNINGD("Type invalid");
			}
			break;
		case VolumeType_Tree:
		default:
			HWARNINGD("Implement this");
			//Implementation missing
			_CollisionInfo.m_bIsValid = false;
			return false;
		}

		_CollisionInfo.m_bIsValid = true;
		return true;
	}

	_CollisionInfo.m_bIsValid = false;

	return false;
}
//---------------------------------------------------------------------------------------------------
// this is the ultimate intersection test

bool BoundingVolume::Intersects(_In_ const BoundingVolume& _BV, _Out_ VolumeType& _Volume1, _Out_ VolumeType& _Volume2) const
{
	bool bIntersects = false;

	std::shared_ptr<BoundingSphere> pBSphere = _BV.GetBSphere();
	std::shared_ptr<BoundingBox> pAABB = _BV.GetAABB();
	std::shared_ptr<BoundingOrientedBox> pOBB = _BV.GetOBB();

	// S S
	if (m_pBSphere != nullptr && pBSphere != nullptr)
	{
		if ((bIntersects = m_pBSphere->Intersects(*pBSphere)) == false)
			return false;

		_Volume1 = VolumeType_Sphere;
		_Volume2 = VolumeType_Sphere;
	}

	// S A
	if (m_pBSphere != nullptr && pAABB != nullptr)
	{
		if ((bIntersects = m_pBSphere->Intersects(*pAABB)) == false)
			return false;

		_Volume1 = VolumeType_Sphere;
		_Volume2 = VolumeType_AABB;
	}

	// A S
	if (m_pAABB != nullptr && pBSphere != nullptr)
	{
		if ((bIntersects = m_pAABB->Intersects(*pBSphere)) == false)
			return false;

		_Volume1 = VolumeType_AABB;
		_Volume2 = VolumeType_Sphere;
	}

	// A A
	if (m_pAABB != nullptr && pAABB != nullptr)
	{
		if ((bIntersects = m_pAABB->Intersects(*pAABB)) == false)
			return false;

		_Volume1 = VolumeType_AABB;
		_Volume2 = VolumeType_AABB;
	}

	// S O
	if (m_pBSphere != nullptr && pOBB != nullptr)
	{
		if ((bIntersects = m_pBSphere->Intersects(*pOBB)) == false)
			return false;

		_Volume1 = VolumeType_Sphere;
		_Volume2 = VolumeType_OBB;
	}

	// O S
	if (m_pOBB != nullptr && pBSphere != nullptr)
	{
		if ((bIntersects = m_pOBB->Intersects(*pBSphere)) == false)
			return false;

		_Volume1 = VolumeType_OBB;
		_Volume2 = VolumeType_Sphere;
	}

	// A O
	if (m_pAABB != nullptr && pOBB != nullptr)
	{
		if ((bIntersects = m_pAABB->Intersects(*pOBB)) == false)
			return false;

		_Volume1 = VolumeType_AABB;
		_Volume2 = VolumeType_OBB;
	}

	// O A
	if (m_pOBB != nullptr && pAABB != nullptr)
	{
		if ((bIntersects = m_pOBB->Intersects(*pAABB)) == false)
			return false;

		_Volume1 = VolumeType_OBB;
		_Volume2 = VolumeType_AABB;
	}

	// O O
	if (m_pOBB != nullptr && pOBB != nullptr)
	{
		if ((bIntersects = m_pOBB->Intersects(*pOBB)) == false)
			return false;

		_Volume1 = VolumeType_OBB;
		_Volume2 = VolumeType_OBB;
	}

	return bIntersects;
}

//---------------------------------------------------------------------------------------------------
bool BoundingVolume::Intersects(_In_ const BoundingSphere& _BSphere) const
{
	bool bIntersects = false;

	if (m_pBSphere != nullptr)
	{
		if ((bIntersects = m_pBSphere->Intersects(_BSphere)) == false)
			return false;
	}

	if (m_pAABB != nullptr)
	{
		if ((bIntersects = m_pAABB->Intersects(_BSphere)) == false)
			return false;
	}

	if (m_pOBB != nullptr)
	{
		if ((bIntersects = m_pOBB->Intersects(_BSphere)) == false)
			return false;
	}

	return bIntersects;
}
//---------------------------------------------------------------------------------------------------
bool BoundingVolume::Intersects(_In_ const BoundingBox& _AABB) const
{
	bool bIntersects = false;

	if (m_pBSphere != nullptr)
	{
		if ((bIntersects = m_pBSphere->Intersects(_AABB)) == false)
			return false;
	}

	if (m_pAABB != nullptr)
	{
		if ((bIntersects = m_pAABB->Intersects(_AABB)) == false)
			return false;
	}

	if (m_pOBB != nullptr)
	{
		if ((bIntersects = m_pOBB->Intersects(_AABB)) == false)
			return false;
	}

	return bIntersects;
}
//---------------------------------------------------------------------------------------------------
bool BoundingVolume::Intersects(_In_ const BoundingOrientedBox& _OBB) const
{
	bool bIntersects = false;

	if (m_pBSphere != nullptr)
	{
		if ((bIntersects = m_pBSphere->Intersects(_OBB)) == false)
			return false;
	}

	if (m_pAABB != nullptr)
	{
		if ((bIntersects = m_pAABB->Intersects(_OBB)) == false)
			return false;
	}

	if (m_pOBB != nullptr)
	{
		if ((bIntersects = m_pOBB->Intersects(_OBB)) == false)
			return false;
	}

	return bIntersects;
}
//---------------------------------------------------------------------------------------------------
const XMFLOAT3& BoundingVolume::GetBSpherePosition()
{
	if (m_pBSphere != nullptr)
	{
		return  m_pBSphere->Center;
	}
	else
	{
		HFATALD("No Sphere Volume has been set!");
		return m_vBSphereOffset;
	}
}
//---------------------------------------------------------------------------------------------------
const XMFLOAT3& BoundingVolume::GetAABBPosition()
{
	if (m_pAABB != nullptr)
	{
		return  m_pAABB->Center;
	}
	else
	{
		HFATALD("No AABB Volume has been set!");
		return m_vAABBOffset;
	}
}
//---------------------------------------------------------------------------------------------------
const XMFLOAT3& BoundingVolume::GetOBBPosition()
{
	if (m_pOBB != nullptr)
	{
		return  m_pOBB->Center;
	}
	else
	{
		HFATALD("No OBB Volume has been set!");
		return m_vOBBOffset;
	}
}
//---------------------------------------------------------------------------------------------------
const XMFLOAT3& BoundingVolume::GetBFrustumPosition()
{
	if (m_pBFrustum != nullptr)
	{
		return  m_pBSphere->Center;
	}
	else
	{
		HFATALD("No Sphere Volume has been set!");
		return m_vBFrustumOffset;
	}
}
//---------------------------------------------------------------------------------------------------
const XMFLOAT3& BoundingVolume::GetPosition()
{
	if (m_pOBB != nullptr)
		return m_pOBB->Center;
	if (m_pAABB != nullptr)
		return m_pAABB->Center;
	if (m_pBSphere != nullptr)
		return m_pBSphere->Center;

	HFATALD("No Bounding Volume has been set!");
	return XMFLOAT3_ZERO;
}
//---------------------------------------------------------------------------------------------------
bool BoundingVolume::GetOrientation(_Out_ XMFLOAT4& _vOrientation) const
{
	if (m_pOBB == nullptr)
		return false;

	_vOrientation = m_pOBB->Orientation;
	return true;
}
//---------------------------------------------------------------------------------------------------
bool BoundingVolume::GetRadius(_Out_ float& _fRadius) const
{
	if (m_pBSphere == nullptr)
		return false;

	_fRadius = m_pBSphere->Radius;
	return true;
}
//---------------------------------------------------------------------------------------------------