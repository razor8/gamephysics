//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "UniformGrid.h"
#include <algorithm>

using namespace helix::datastructures;
using namespace helix::collision;

UniformGrid::UniformGrid(uint32_t _uCellCount, float _fGridCellSize)
{
	m_uCellCount = _uCellCount;
	m_Cells.resize(_uCellCount);
	m_fGridCellSize = _fGridCellSize;
}

//---------------------------------------------------------------------------------------------------

uint32_t UniformGrid::ComputeHash(const XMFLOAT3& _vPosition)
{
	const int32_t iP1 = 563300407;
	const int32_t iP2 = 495250453;
	const int32_t iP3 = 236350427;

	int32_t X = static_cast<int32_t>(_vPosition.x / m_fGridCellSize);
	int32_t Y = static_cast<int32_t>(_vPosition.y / m_fGridCellSize);
	int32_t Z = static_cast<int32_t>(_vPosition.z / m_fGridCellSize);

	int32_t iHash = iP1 * X + iP2 * Y + iP3 * Z;

	iHash %= m_uCellCount;

	if (iHash < 0)
		iHash += m_uCellCount;

	return static_cast<uint32_t>(iHash);
}
//---------------------------------------------------------------------------------------------------

uint32_t UniformGrid::ComputeHash(int32_t _iX, int32_t _iY, int32_t _iZ)
{
	const int32_t iP1 = 563300407;
	const int32_t iP2 = 495250453;
	const int32_t iP3 = 236350427;

	int32_t iHash = iP1 * _iX + iP2 * _iY + iP3 * _iZ;

	iHash %= m_uCellCount;

	if (iHash < 0)
		iHash += m_uCellCount;

	return static_cast<uint32_t>(iHash);
}

//---------------------------------------------------------------------------------------------------

void UniformGrid::Update(GameObject* _pEntity)
{
	uint32_t uHash = ComputeHash(_pEntity->m_vPosition);
	
	if (_pEntity->m_uHash == uHash)
		return;

	//delete from old cell
	if (_pEntity->m_uHash < m_uCellCount)
	{
		TCell& Cell = m_Cells.at(_pEntity->m_uHash);

		TCell::iterator remove = std::remove_if(Cell.begin(), Cell.end(), [&](const GameObject* pEntitiy){ return pEntitiy->m_uID == _pEntity->m_uID; });
		//remove the entity
		Cell.erase(remove);
	}

	_pEntity->m_uHash = uHash;

	TCell& Cell = m_Cells.at(uHash);

	Cell.push_back(_pEntity);
}

//---------------------------------------------------------------------------------------------------

void UniformGrid::GatherCollisions(_Out_ std::vector<CollisionInfo>& _Collisions)
{
	
	GameObject* pGO1, *pGO2;
	XMFLOAT3 vPos;

	
	HFOREACH_CONST(it, end, m_Cells)	
		TCell& Cell = *it;

		//test cell entities
		TCell::iterator cell_end = Cell.end();
		for (TCell::iterator go = Cell.begin(); go != cell_end; ++go)
		{
			pGO1 = *go;
			for (TCell::iterator go2 = std::next(go); go2 != cell_end; ++go2)
			{
				pGO2 = *go2;
				if (pGO1->CheckFlag(datastructures::GameObjectFlag_Static) && pGO2->CheckFlag(datastructures::GameObjectFlag_Static))
					continue;

				CollisionInfo Info;
				if (pGO1->m_BV.GetCollisionInfo(pGO2->m_BV, Info))
				{
					Info.m_pGO1 = pGO1;
					Info.m_pGO2 = pGO2;
					_Collisions.push_back(Info);
				}
			}


			float fDelta;

			pGO1->m_BV.GetRadius(fDelta);

			fDelta *= 2.f;

			//fDelta += m_fGridCellSize;

			vPos = pGO1->m_vPosition;

			uint32_t uHash;

			int32_t X1 = static_cast<int32_t>(floorf(vPos.x - fDelta) / m_fGridCellSize);
			int32_t X2 = static_cast<int32_t>(ceilf(vPos.x + fDelta) / m_fGridCellSize);

			int32_t Y1 = static_cast<int32_t>(floorf(vPos.y - fDelta) / m_fGridCellSize);
			int32_t Y2 = static_cast<int32_t>(ceilf(vPos.y + fDelta) / m_fGridCellSize);

			int32_t Z1 = static_cast<int32_t>(floorf(vPos.z - fDelta) / m_fGridCellSize);
			int32_t Z2 = static_cast<int32_t>(ceilf(vPos.z + fDelta) / m_fGridCellSize);

			for (int32_t i32XIndex = X1; i32XIndex <= X2; i32XIndex++)
			{
				for (int32_t i32YIndex = Y1; i32YIndex <= Y2; i32YIndex++)
				{
					for (int32_t i32ZIndex = Z1; i32ZIndex <= Z2; i32ZIndex++)
					{
						uHash = ComputeHash(i32XIndex, i32YIndex, i32ZIndex);

						TCell& OtherCell = m_Cells.at(uHash);

						TCell::iterator other_cell_end = OtherCell.end();
						for (TCell::iterator go3 = OtherCell.begin(); go3 != other_cell_end; ++go3)
						{
							GameObject *pGO3 = *go3;
							for (TCell::iterator go4 = std::next(go3); go4 != other_cell_end; ++go4)
							{
								GameObject *pGO4 = *go4;
								if (pGO3->CheckFlag(datastructures::GameObjectFlag_Static) && pGO4->CheckFlag(datastructures::GameObjectFlag_Static))
									continue;

								CollisionInfo Info;
								if (pGO3->m_BV.GetCollisionInfo(pGO4->m_BV, Info))
								{
									Info.m_pGO1 = pGO3;
									Info.m_pGO2 = pGO4;
									_Collisions.push_back(Info);
								}
							}
						}
					}
				}
			}
		}
	HFOREACH_CONST_END

	
}