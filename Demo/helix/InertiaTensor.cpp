#include "InertiaTensor.h"
#include "StdAfxH.h"
using namespace helix::physics;
using namespace helix::math;

//---------------------------------------------------------------------------------------------------
// Mesh uniform mass
XMFLOAT3X3 InertiaTensor::ComputeMeshInertiaTensor(std::vector<XMFLOAT3>& _Vertices, float _fTotalMass)
{
	XMMATRIX CovarianceMatrix = XMMATRIX_ZERO;

	// compute covariance matrix
	// C = m * SUM_k( X_k * X_k^T)
	HFOREACH_CONST(it, end, _Vertices)
		CovarianceMatrix += XMMatrixTensor(*it);
	HFOREACH_CONST_END
	CovarianceMatrix *= _fTotalMass;
	
	// Trace(A) = a_1,1 + a_2,2 + a_3,3
	float fTrace = CovarianceMatrix.r[0].m128_f32[0] + CovarianceMatrix.r[1].m128_f32[1] + CovarianceMatrix.r[2].m128_f32[2];
	
	// I = Id * Trace(C) - C
	XMMATRIX InertiaTensor = XMMatrixSet(	fTrace, 0.f, 0.f, 0.f,
											0.f, fTrace, 0.f, 0.f,
											0.f, 0.f, fTrace, 0.f,
											0.f, 0.f, 0.f, 0.f);
	InertiaTensor -= CovarianceMatrix;
	
	return XMFloat3x3Get(InertiaTensor);
}

//---------------------------------------------------------------------------------------------------
// Mesh distributed mass
XMFLOAT3X3 InertiaTensor::ComputeMeshInertiaTensor(std::vector<XMFLOAT3>& _Vertices, std::vector<float>& _Masses)
{
	XMMATRIX CovarianceMatrix = XMMATRIX_ZERO;
	
	size_t VerticesSize = _Vertices.size();
	
	HASSERTD(VerticesSize == _Masses.size(),"Vertex and mass vectors are not of equal size");
		
	// compute covariance matrix
	// C = SUM_k( m_k * X_k * X_k^T)
	for (size_t i = 0; i < VerticesSize; ++i)
	{
		CovarianceMatrix += _Masses[i] * XMMatrixTensor(_Vertices[i]);
	}
		
	// Trace(A) = a_1,1 + a_2,2 + a_3,3
	float fTrace = CovarianceMatrix.r[0].m128_f32[0] + CovarianceMatrix.r[1].m128_f32[1] + CovarianceMatrix.r[2].m128_f32[2];
	
	// I = Id * Trace(C) - C
	XMMATRIX InertiaTensor = XMMatrixSet(fTrace, 0.f, 0.f, 0.f,
		0.f, fTrace, 0.f, 0.f,
		0.f, 0.f, fTrace, 0.f,
		0.f, 0.f, 0.f, 0.f);
	InertiaTensor -= CovarianceMatrix;

	return XMFloat3x3Get(InertiaTensor);
}
//---------------------------------------------------------------------------------------------------
// Cuboid 
XMFLOAT3X3 InertiaTensor::ComputeCuboidInertiaTensor(const XMFLOAT3& _vDim, float _fTotalMass)
{
	_fTotalMass /= 12.f;
	return XMFLOAT3X3(_fTotalMass * (_vDim.y + _vDim.z), 0.f, 0.f,
		0.f, _fTotalMass * (_vDim.x + _vDim.z), 0,
		0.f, 0.f, _fTotalMass * (_vDim.x + _vDim.y));
}
//---------------------------------------------------------------------------------------------------
// Solid Sphere
XMFLOAT3X3 InertiaTensor::ComputeSolidSphereInertiaTensor(float _fRadius, float _fTotalMass)
{
	float fElem = 0.4f * _fTotalMass * _fRadius * _fRadius;
	return XMFLOAT3X3(	fElem, 0.f, 0.f,
						0.f, fElem, 0.f,
						0.f, 0.f, fElem);
}
//---------------------------------------------------------------------------------------------------
// Hollow Sphere
XMFLOAT3X3 InertiaTensor::ComputeHollowSphereInertiaTensor(float _fRadius, float _fTotalMass)
{
	float fElem = (2.f/3.f) * _fTotalMass * _fRadius * _fRadius;
	return XMFLOAT3X3(	fElem, 0.f, 0.f,
						0.f, fElem, 0.f,
						0.f, 0.f, fElem);
}
