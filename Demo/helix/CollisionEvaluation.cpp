//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "CollisionEvaluation.h"
#include "Logger.h"

using namespace helix::collision;
using namespace helix::datastructures;

//---------------------------------------------------------------------------------------------------
void CollisionEvaluation::ComputeCollision(GameObject* _pGO1, GameObject* _pGO2, const CollisionInfo& _CollisionInfo)
{
	RigidBody* pRB1 = nullptr;
	RigidBody* pRB2 = nullptr;

	MovingObject* pMO1 = nullptr;
	MovingObject* pMO2 = nullptr;

	switch (_pGO1->GetType())
	{
	case GameObjectFlag_GameObject:
		switch (_pGO2->GetType())
		{
		case GameObjectFlag_GameObject:
			break;
		case GameObjectFlag_MovingObject:
			break;
		case GameObjectFlag_RigidBody:
			break;
		default:
			break;
		}
		HLOGD2("Implementation missing!");
		break;
	case GameObjectFlag_MovingObject:
		switch (_pGO2->GetType())
		{
		case GameObjectFlag_GameObject:
			HLOGD2("Implementation missing!");
			break;
		case GameObjectFlag_MovingObject:
			pMO1 = dynamic_cast<MovingObject*>(_pGO1);
			pMO2 = dynamic_cast<MovingObject*>(_pGO2);
			if (pMO1 != nullptr && pMO2 != nullptr)
				ComputeCollision(*pMO1, *pMO2, _CollisionInfo);
			else
				HLOGD2("Type conversion failed!");
			break;
		case GameObjectFlag_RigidBody:
			HLOGD2("Implementation missing!");
			break;
		default:
			break;
		}
		break;
	case GameObjectFlag_RigidBody:
		switch (_pGO2->GetType())
		{
		case GameObjectFlag_GameObject:
			HLOGD2("Implementation missing!");
			break;
		case GameObjectFlag_MovingObject:
			HLOGD2("Implementation missing!");
			break;
		case GameObjectFlag_RigidBody:
			pRB1 = dynamic_cast<RigidBody*>(_pGO1);
			pRB2 = dynamic_cast<RigidBody*>(_pGO2);
			if (pRB1 != nullptr && pRB2 != nullptr)
				ComputeCollision(*pRB1, *pRB2, _CollisionInfo);
			else
				HLOGD2("Type conversion failed!");
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}
}
//---------------------------------------------------------------------------------------------------
//MovingObject - MovingObject
void CollisionEvaluation::ComputeCollision(MovingObject& _MO1, MovingObject& _MO2, const CollisionInfo& _CollisionInfo)
{
	if (_CollisionInfo.m_bIsValid == false)
	{
		HLOGD2("Invaid CollisionInfo for MovingObjects %d %d", _MO1.m_uID, _MO2.m_uID);
		return;
	}

	//if (_MO1.m_uID != _CollisionInfo.m_pGO1->m_uID || _MO2.m_uID != _CollisionInfo.m_pGO2->m_uID)
	//{
	//	HLOGD2("Wrong GameObjectIDs in CollisionInfo %d %d", _CollisionInfo.m_pGO1->m_uID, _CollisionInfo.m_pGO2->m_uID);
	//	return;
	//}

	if (_CollisionInfo.m_Contacts1.size() != 1 && _CollisionInfo.m_Contacts2.size() != 1)
	{
		//HLOGD2("Invaid CollisionInfo for MovingObjects %d %d, to few contacts", _CollisionInfo.m_pGO1->m_uID, _CollisionInfo.m_pGO2->m_uID);
		return;
	}

	bool bStatic1 = _MO1.CheckFlag(datastructures::GameObjectFlag_Static);
	bool bStatic2 = _MO2.CheckFlag(datastructures::GameObjectFlag_Static);

	_MO1.Lock();
	_MO2.Lock();

	XMVECTOR n, f, /*pos,*/ v;

	const float fDist = math::XMVector3DistanceF(_MO1.m_vPosition, _MO2.m_vPosition);

	float fRadius1 = 0.f; _MO1.m_BV.GetRadius(fRadius1);
	float fRadius2 = 0.f; _MO2.m_BV.GetRadius(fRadius2);

	const float fKernel = (1.f - (fDist / (fRadius1 + fRadius2))) * 10.f;
	const float fBounceFactor = 10.f;

	HLOGD2("Collision %d %d %f", _MO1.m_uID, _MO2.m_uID, fKernel);

	//force on object 1
	if (bStatic1 == false)
	{
		n = XMLoadFloat3(&_CollisionInfo.m_Contacts1.front().m_vNormal);
		f = XMLoadFloat3(&_MO1.m_vCurForce);

		//pos = XMLoadFloat3(&_MO1.m_vPosition);
		//pos = XMVectorAdd(pos, n);
		//XMStoreFloat3(&_MO1.m_vPosition, pos);

		n = XMVectorScale(n, fKernel);

		if (bStatic2 == false)
		{
			f = XMVectorAdd(f, n);
		}
		else
		{
			n = XMVectorScale(n, fBounceFactor);
			f = n;
			//_MO1.m_vLinearVelocity = XMFLOAT3_ZERO;

			v = XMLoadFloat3(&_MO1.m_vLinearVelocity);
			v = XMVectorNegate(v);
			XMStoreFloat3(&_MO1.m_vLinearVelocity, v);
		}

		XMStoreFloat3(&_MO1.m_vCurForce, f);
	}

	//force on object 2
	if (bStatic2 == false)
	{
		n = XMLoadFloat3(&_CollisionInfo.m_Contacts2.front().m_vNormal);
		f = XMLoadFloat3(&_MO2.m_vCurForce);

		//pos = XMLoadFloat3(&_MO2.m_vPosition);
		//pos = XMVectorAdd(pos, n);
		//XMStoreFloat3(&_MO2.m_vPosition, pos);

		n = XMVectorScale(n, fKernel);

		if (bStatic1 == false)
		{
			f = XMVectorAdd(f, n);
		}
		else
		{
			n = XMVectorScale(n, fBounceFactor);
			f = n;
			//_MO2.m_vLinearVelocity = XMFLOAT3_ZERO;

			v = XMLoadFloat3(&_MO2.m_vLinearVelocity);
			v = XMVectorNegate(v);
			XMStoreFloat3(&_MO2.m_vLinearVelocity, v);
		}

		XMStoreFloat3(&_MO2.m_vCurForce, f);
	}

	_MO1.Unlock();
	_MO2.Unlock();
}

//---------------------------------------------------------------------------------------------------
//RigidBody - RigidBody
void CollisionEvaluation::ComputeCollision(RigidBody& _RB1, RigidBody& _RB2, const CollisionInfo& _CollisionInfo)
{
	if (_CollisionInfo.m_bIsValid == false)
	{
		HLOGD2("Invaid CollisionInfo for MovingObjects %d %d", _RB1.m_uID, _RB2.m_uID);
		return;
	}

	//if (_RB1.m_uID != _CollisionInfo.m_pGO1->m_uID || _RB2.m_uID != _CollisionInfo.m_pGO2->m_uID)
	//{
	//	HLOGD2("Wrong GameObjectIDs in CollisionInfo %d %d", _CollisionInfo.m_pGO1->m_uID, _CollisionInfo.m_pGO2->m_uID);
	//	return;
	//}

	if (_CollisionInfo.m_Contacts1.size() != 1 && _CollisionInfo.m_Contacts2.size() != 1)
	{
		//HLOGD2("Invaid CollisionInfo for MovingObjects %d %d, to few contacts", _CollisionInfo.m_pGO1->m_uID, _CollisionInfo.m_pGO2->m_uID);
		return;
	}

	bool bStatic1 = _RB1.CheckFlag(datastructures::GameObjectFlag_Static);
	bool bStatic2 = _RB2.CheckFlag(datastructures::GameObjectFlag_Static);
	
	_RB1.Lock();
	_RB2.Lock();

	//TODO: this whole functions needs to be recoded

	HFOREACH_CONST(it, end, _CollisionInfo.m_Contacts1)
		float J = RigidBody::ComputeImpulse(_RB1, _RB2, *it);

		//Workaround
		if (fabsf(J) > FLT_EPSILON)
		{
			//TODO: check if any of the collision partners is static, apply double impulse to other
			if (bStatic1 == false)
			{
				HLOGD2("Collision %d %d %f", _RB1.m_uID, _RB2.m_uID, J);
				if (bStatic2 == true)
				{
					//TODO: FIX THIS
					_RB1.ApplyImpulse(2.f * J, *it);
				}
				else
				{
					_RB1.ApplyImpulse(J, *it);
				}
			}
		}
	HFOREACH_CONST_END


	HFOREACH_CONST(it, end, _CollisionInfo.m_Contacts2)
		float J = RigidBody::ComputeImpulse(_RB1, _RB2, *it);

		//Workaround
		if (fabsf(J) > FLT_EPSILON)
		{
			//TODO: check if any of the collision partners is static, apply double impulse to other
			if (bStatic2 == false)
			{
				HLOGD2("Collision %d %d %f", _RB1.m_uID, _RB2.m_uID, J);
				if (bStatic1 == true)
				{
					//TODO: FIX THIS
					_RB2.ApplyImpulse(2.f * J, *it);
				}
				else
				{
					_RB2.ApplyImpulse(J, *it);
				}
			}
		}
	HFOREACH_CONST_END

	_RB1.Unlock();
	_RB2.Unlock();
}