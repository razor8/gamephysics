//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel

#include "K3TreeNode.h"
#include "Logger.h"

using namespace helix::datastructures;
using namespace helix::collision;

K3TreeNode::K3TreeNode(TEntityVector::iterator _Begin, TEntityVector::iterator _End, uint32_t _uDepth)
{
	m_Axis = static_cast<SeparatingAxis>(_uDepth % 3);

	if (_Begin == _End)
		return;

	//there is only one object left
	if (_End - _Begin == 1)
	{
		m_pEntity = *_Begin;
		return;
	}

	switch (m_Axis)
	{
	case SeparatingAxis_X:
		std::sort(_Begin, _End, [](const GameObject* pGO1, const GameObject* pGO2){ return pGO1->m_vPosition.x < pGO2->m_vPosition.x; });
		break;
	case SeparatingAxis_Y:
		std::sort(_Begin, _End, [](const GameObject* pGO1, const GameObject* pGO2){ return pGO1->m_vPosition.y < pGO2->m_vPosition.y; });
		break;
	case SeparatingAxis_Z:
		std::sort(_Begin, _End, [](const GameObject* pGO1, const GameObject* pGO2){ return pGO1->m_vPosition.z < pGO2->m_vPosition.z; });
		break;
	default:
		break;
	}

	TEntityVector::size_type Pivot = (_End - _Begin) / 2;
	m_pEntity = *(_Begin + Pivot);

	//there is atleast one element on the "lesser" side of pivot
	if (_Begin != (_Begin + Pivot))
		m_pLeft = new K3TreeNode(_Begin, _Begin + Pivot, _uDepth + 1);

	if ((_Begin + Pivot + 1) != _End)
		m_pRight = new K3TreeNode(_Begin + Pivot + 1, _End, _uDepth + 1);
}

//---------------------------------------------------------------------------------------------------
K3TreeNode::~K3TreeNode()
{
	HSAFE_DELETE(m_pLeft);
	HSAFE_DELETE(m_pRight);
}

//---------------------------------------------------------------------------------------------------

void K3TreeNode::GatherCollisions(_In_ GameObject* _pEntity, _Out_ std::vector<CollisionInfo>& _Collisions)
{
	float fDist;
	switch (m_Axis)
	{
	case SeparatingAxis_X:
		fDist = m_pEntity->m_vPosition.x - _pEntity->m_vPosition.x;
		break;
	case SeparatingAxis_Y:
		fDist = m_pEntity->m_vPosition.y - _pEntity->m_vPosition.y;
		break;
	case SeparatingAxis_Z:
		fDist = m_pEntity->m_vPosition.z - _pEntity->m_vPosition.z;
		break;
	default:
		break;
	}

	if (_pEntity != m_pEntity)
	{
		if (_pEntity->CheckFlag(GameObjectFlag_Static) == false || m_pEntity->CheckFlag(GameObjectFlag_Static) == false)
		{
			//if (_pEntity->CheckFlag(GameObjectFlag_Invisible) == false && m_pEntity->CheckFlag(GameObjectFlag_Invisible) == false)
			//{
			//	HLOGD(\1);
			//}

			CollisionInfo Info;
			if (m_pEntity->m_BV.GetCollisionInfo(_pEntity->m_BV, Info))
			{
				Info.m_pGO1 = m_pEntity;
				Info.m_pGO2 = _pEntity;
				_Collisions.push_back(Info);
			}
		}		
	}

	float fRadius =_pEntity->m_BV.GetBSphere()->Radius;
	if (fabsf(fDist) > fRadius)
	{
		if (fDist <= 0.f)
		{
			//entity is left of the separating axis
			if (m_pLeft != nullptr)
				m_pLeft->GatherCollisions(_pEntity, _Collisions);
		}
		else
		{
			//entity is right of the separating axis
			if (m_pRight != nullptr)
				m_pRight->GatherCollisions(_pEntity, _Collisions);
		}
	}
	else
	{
		//entity overlaps the separating axis
		if (m_pLeft != nullptr)
			m_pLeft->GatherCollisions(_pEntity, _Collisions);
		if (m_pRight != nullptr)
			m_pRight->GatherCollisions(_pEntity, _Collisions);
	}

}