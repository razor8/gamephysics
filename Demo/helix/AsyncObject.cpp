//Copyright(c) 2014
//Authors: Fabian Wahlster, Steffen Wiewel, Moritz Becher

#include "AsyncObject.h"

using namespace helix::async;
//---------------------------------------------------------------------------------------------------
IAsyncObject::IAsyncObject(IAsyncObserver * _pObserver, bool _bLoopedExecution, bool _bPauseAfterExecution) :
	m_bPauseAfterExecution(_bPauseAfterExecution),
	m_Observer(_pObserver)
{
	m_StdThread = nullptr;

	m_bRun = true;
	//Start in a suspended state
	m_bPause = true;

	if (_bLoopedExecution)
	{
		m_Function = std::bind(&IAsyncObject::ExecuteLooped, this);
	}
	else
	{
		m_Function = std::bind(&IAsyncObject::Execute, this);
	}
}
//---------------------------------------------------------------------------------------------------
IAsyncObject::~IAsyncObject()
{
	Stop();
	StopThread();
}
//---------------------------------------------------------------------------------------------------
void IAsyncObject::StopThread()
{
	std::lock_guard<std::mutex> lock(m_ThreadLockMutex);
	if (m_StdThread != nullptr)
	{
		try
		{
			if (m_StdThread->joinable())
				m_StdThread->join();

			m_StdThread.reset(nullptr);
		}
		catch (std::system_error& ex)
		{
			HWARNINGD("Failed to join thread!");
		}
	}
}
//---------------------------------------------------------------------------------------------------
bool IAsyncObject::MoveToThread(MutableThread* thread)
{
	InitializeSubsystems();
	return thread->Start(m_Function);
}
//---------------------------------------------------------------------------------------------------
bool IAsyncObject::StartInNewThread()
{
	if (m_StdThread != nullptr)
		return false;

	InitializeSubsystems();

	std::lock_guard<std::mutex> lock(m_ThreadLockMutex);
	
	m_StdThread = std::make_unique<std::thread>(m_Function);
	
	return true;
}
//---------------------------------------------------------------------------------------------------
//main work loop;
void IAsyncObject::ExecuteLooped()
{
	while (m_bRun)
	{
		//suspend simulation
		{
			std::unique_lock<std::mutex> wait(m_WaitMutex);
			while (m_bPause)
			{
				m_CV.wait(wait);
			}
		}

		if (m_bRun == false)
		{
			//skip the last execution if Stop has been called during last wait
			SignalObserverBarrier();
			return;
		}

		//next execution
		Execute();

		if (m_bPauseAfterExecution)
		{
			std::lock_guard<std::mutex> wait(m_WaitMutex);
			m_bPause = true;
		}

		//notify the physics manager that work is done here
		SignalObserverBarrier();
	}
}
//---------------------------------------------------------------------------------------------------
//call this function afte each simulation step
void IAsyncObject::SignalObserverBarrier()
{
	std::lock_guard<std::mutex> lock(m_ObserverLockMutex);
	if (m_Observer != nullptr)
	{
		m_Observer->NotifyBarrier();
	}
	//else
	//{
	//	//this can be desired behaviour - the warning is just here to inform the developer
	//	HWARNINGD("No observer has been assigned");
	//}
}
//---------------------------------------------------------------------------------------------------
void IAsyncObject::Stop()
{
	if (m_bRun)
	{		
		{
			std::lock_guard<std::mutex> lock(m_WaitMutex);
			m_bRun = false;
			m_bPause = false;
		}

		m_CV.notify_one();

		StopSubsystems();

		StopThread();
	}
}
//---------------------------------------------------------------------------------------------------
void IAsyncObject::Pause()
{
	{
		std::lock_guard<std::mutex> lock(m_WaitMutex);
		m_bPause = true;
	}
	m_CV.notify_one();
}
//---------------------------------------------------------------------------------------------------
void IAsyncObject::Continue()
{
	{
		std::lock_guard<std::mutex> lock(m_WaitMutex);
		m_bPause = false;
	}
	m_CV.notify_one();
}
//---------------------------------------------------------------------------------------------------